<?php
// profiler_start("Open Session");
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/include/constants.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));
// profiler_stop("Open Session");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");

// profiler_start("Load Dico");
$lang = $auth->auth['lang'];
Translation::init('gettext', HOMEPATH . '/i18n/messages', $lang, 'UTF-8', "main", true, HOMEPATH . '/i18n/messages/cache', $filename = "");
// profiler_stop("Load Dico");

$db = DB::getDB();
// profiler_start("Load Player");
$nacridan = new NacridanModule($sess, $auth, $db);

// Tribune
require_once (HOMEPATH . "/diplomacy/dtribune.inc.php");
include_once HOMEPATH . '/class/tribune/tribune.include.php';
$type = filter_input(INPUT_GET, 'type', FILTER_VALIDATE_INT);
$tribune = new DTribune($nacridan, $db, $type);

// Mise en évidence des messages de tribune non-lus
$infos = $tribune->getUnreadMessagesInfos();

$responseJSON = array();
if ($infos['success'] && $infos['id_Group'] != 0) {
    $responseJSON['unreads'] = $infos['unreads'];
} else {
    $responseJSON['unreads'] = 0;
}

// On ajoute les informations de DLA dépassée
require_once (HOMEPATH . "/conquest/cqright.inc.php");
$menu = new CQRight($nacridan, $db);
$ATBInfos = $menu->getATBInfos();
$responseJSON['isATBOutdated'] = $ATBInfos['isATBOutdated'];
$responseJSON['currentDate'] = date("H:i:s");
if ($ATBInfos['isATBOutdated']) {
    $responseJSON['ATBInfos'] = $menu->getATBInfosString();
}


echo json_encode($responseJSON);
?>