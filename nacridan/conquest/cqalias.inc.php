<?php
DEFINE("MAXCONTACT", 40);

class CQAlias extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAlias($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $id = $curplayer->get("id");
        $err = "";
        
        $aliasContent = "";
        $aliasName = "";
        
        if (isset($_POST["Del"])) {
            $err = $this->deleteAlias($db);
        }
        
        if (isset($_POST["Edit"])) {
            if (isset($_POST["check"][0])) {
                $dbs = new DBCollection("SELECT name,alias FROM MailAlias WHERE id_Player=" . $id . " AND id=\"" . $_POST["check"][0] . "\"", $db);
                if (! $dbs->eof()) {
                    $aliasName = $dbs->get("name");
                    $arr = array_keys(unserialize($dbs->get("alias")));
                    $aliasContent = "";
                    foreach ($arr as $value)
                        $aliasContent .= $value . ",";
                }
            }
        }
        
        if (isset($_POST["Add"])) {
            $err = $this->addAlias($id, $_POST["AliasName"], $_POST["AliasContent"], $db);
        }
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=search' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "</form>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=alias' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel' width='150px'><b>" . localize('Nom de L\'Alias :') . "</b></td><td>\n";
        $str .= "<input type='text' id='Name' name='AliasName' value='" . htmlspecialchars($aliasName, ENT_QUOTES, 'UTF-8') . "' style='width:450px;'></td></tr>";
        $str .= "<tr><td class='mainbglabel' width='150px' valign='top'><b>" . localize('Destinataire :') . "</b></td><td class='mainbgtitle'>";
        
        $dbp = new DBCollection("SELECT id_Player\$friend,name FROM MailContact LEFT JOIN Player ON id_Player\$friend=Player.id WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0);
        
        $item[] = array(
            localize("-- Le(s) Contact(s) Enregistré(s)--") => - 1
        );
        while (! $dbp->eof()) {
            $item[] = array(
                $dbp->get("name") => $dbp->get("id_Player\$friend")
            );
            $dbp->next();
        }
        
        $str .= "<select id='DestDrop' class='cqselector' style='width:250px;' name='DestDrop' onChange='javascript:addDest(\"AliasContent\",this,\"" . localize("Pour limiter le SPAM, le nombre de destinaire est limité à 10") . "\")'>";
        
        $str .= "<option value='0' selected='selected'>" . localize('-- Choisissez un destinataire --') . "</option>";
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $val) {
                if ($val == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $val . "'>" . $key . "</option>";
            }
        }
        
        $str .= "</select><br/>";
        $str .= "(" . localize('Le nom de chaque destinataire doit être séparé par une virgule') . ")<br/>";
        $str .= "<tr><td class='mainbglabel' width='150px'><b>" . localize('Contenu de l\'Alias :') . "</b></td><td class='mainbgtitle'><textarea id='AliasContent' style='width:450px;' name='AliasContent' rows='5' cols='40'>" . htmlspecialchars($aliasContent, ENT_QUOTES, 'UTF-8') . "</textarea></td></tr>";
        $str .= "</table>";
        
        $str .= "<input id='Add' type='submit' name='Add' value='" . localize('Créer / Mise à Jours') . "' />";
        $str .= "<br/><br/><br/><br/>";
        
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td width='30px'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
        $str .= "<td class='mainbglabel'>" . localize("Nom de L'Alias") . "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable centerareawidth'>\n";
        
        $dbc = new DBCollection("SELECT  MailAlias.id,id_Player,alias,name FROM MailAlias WHERE id_Player =" . $id, $db);
        
        $data = array();
        while (! $dbc->eof()) {
            $alias = $dbc->get("name");
            $arr = array_keys(unserialize($dbc->get("alias")));
            $body = "";
            foreach ($arr as $value)
                $body .= $value . " / ";
            
            $data[] = array(
                "id" => $dbc->get("id"),
                "name" => $alias . " : " . $body
            );
            $dbc->next();
        }
        
        $cpt = 0;
        
        foreach ($data as $arr) {
            $str .= sprintf("<tr><td class='mainbgtitle' width='30px'><input name='check[]' type='checkbox' value='%s'/>", $arr["id"]);
            $str .= "</td>";
            $str .= sprintf("<td class='mainbgtitle'  align='left'>%s</td></tr>\n", $arr["name"]);
            $cpt ++;
        }
        $str .= "</table>";
        if ($cpt) {
            $str .= "<input id='Edit' type='submit' name='Edit' value='" . localize("Editer") . "' />";
            $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Effacer") . "' />";
        }
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</form>\n";
        
        return $str;
    }

    public function addAlias($id, $name, $alias, $db)
    {
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = explode(",", $alias);
        $cond = MailFactory::getNamesCond($dest, MAXCONTACT);
        
        $dbp = new DBCollection("SELECT id,name FROM Player WHERE " . $cond, $db);
        
        $dest = array();
        while (! $dbp->eof()) {
            $dest[$dbp->get("name")] = $dbp->get("id");
            $dbp->next();
        }
        
        $alias = serialize($dest);
        
        $dbs = new DBCollection("SELECT id FROM MailAlias WHERE id_Player=" . $id . " AND name=\"" . addslashes($name) . "\"", $db);
        
        if ($dbs->eof())
            $dbi = new DBCollection("INSERT INTO MailAlias (id_Player,name,alias) VALUES (\"" . $id . "\",\"" . addslashes($name) . "\",'" . addslashes($alias) . "');", $db, 0, 0, false);
        else
            $dbi = new DBCollection("UPDATE MailAlias SET alias='" . addslashes($alias) . "' WHERE id=" . $dbs->get("id"), $db, 0, 0, false);
        
        if ($dbi->errorNoDB() == 0) {
            return localize("Alias ajouté");
        } else {
            return localize("Erreur");
        }
    }

    public function deleteAlias($db)
    {
        $msgToDelete = "";
        if (isset($_POST["check"]))
            $msgToDelete = getSQLCondFromArray(quote_smart($_POST["check"]), "id", "OR");
        
        if ($msgToDelete != "") {
            $dbm = new DBCollection("DELETE FROM MailAlias WHERE " . $msgToDelete, $db, 0, 0, false);
            return localize("Alias effacé(s)");
        } else {
            return localize("Aucun Alias effacé");
        }
    }
}
?>
