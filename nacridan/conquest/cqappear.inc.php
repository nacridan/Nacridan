<?php

class CQAppear extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAppear($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        $xp = $curplayer->get("x");
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $str .= "<table class='maintable bottomareawidth'><tr><td  class='mainbgtitle' width='500px'>";
        $str .= "Vous êtes sur le point de redevenir visible pour tous, continuer ?" . "</td>";
        $str .= "<td class='mainbgtitle' align='center'><input id='submitbt' type='submit' name='submitbt' value='Action' />";
        $str .= "<input name='action' type='hidden' value='" . APPEAR . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</td></tr></table>";
        $str .= "</form>";
        
        return $str;
    }
}

?>
