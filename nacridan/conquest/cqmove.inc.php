<?php
include (HOMEPATH . "/factory/PlayerFactory.inc.php");

class CQMove extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQMove($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $state = $curplayer->get("state");
        // calcul du nombre de PA d'un déplacement
        $PA = $curplayer->getTimeToMove($this->db);
        
        if ($curplayer->get("ap") < $PA) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser un déplacement.");
            $str .= "</td></tr></table>";
        } elseif ($curplayer->get("levelUp")) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous devez d'abord passer votre niveau.");
            $str .= "</td></tr></table>";
        } elseif ($curplayer->get("recall")) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous devez d'abord accepter ou refuser l'offre de rappel.");
            $str .= "</td></tr></table>";
        } else {
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($this->db);
            $xp = $curplayer->get("x");
            $yp = $curplayer->get("y");
            $validzone = $mapinfo->getValidMap($xp, $yp, 2, $curplayer->get("map"));
            require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
            $str .= "<table class='maintable bottomareawidth'>";
            
            for ($j = 0; $j < 3; $j ++) {
                
                $str .= "<tr class='tr_move'>";
                
                for ($i = 0; $i < 3; $i ++) {
                    
                    if ($j == 0 && $i == 0) {
                        $str .= "<td width='175px' >&nbsp;";
                        $str .= "</td>";
                    }
                    if ($j == 1 && $i == 0) {
                        $str .= "<td  align='center'>";
                        $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                        $str .= "<input name='action' type='hidden' value='" . MOVE . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</td>";
                    }
                    if ($j == 2 && $i == 0) {
                        $str .= "<td width='175px' >&nbsp;";
                        $str .= "</td>";
                    }
                    if (BasicActionFactory::freePlace($xp - 1 + $i, $yp - 1 + $j, $curplayer->get("map"), $this->db))
                        $CoordEnable = "";
                    else
                        $CoordEnable = "disabled";
                    
                    if (! ($i == 0 && $j == 0) && ! ($i == 2 && $j == 2)) {
                        if ($validzone[$i][$j])
                            $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>(" . ($xp - 1 + $i) . ", " . ($yp - 1 + $j) . ")</div></td>";
                        else
                            $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>( - , - )</div></td>";
                    }
                }
                
                $str .= "</tr>";
            }
            
            $str .= "</table>";
            $str .= "</form>";
        }
        return $str;
    }
}
  
