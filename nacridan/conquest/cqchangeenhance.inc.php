<?php
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");
require_once (HOMEPATH . "/class/DBStaticModifier.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");

class CQChangeEnhance extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQChangeEnhance($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize("A M É L I O R A T I O N") . "</b>\n";
        $str .= "</tr><tr><td colspan='6'><b>" . localize("Ici, vous pouvez rachetez les points d'expérience dépensés dans une amélioration (1 PO pour 1 PX)") . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel width='30' align='center'></td>\n";
        $str .= "<td class='mainbglabel width='180' align='left'>" . localize("Compétences") . "</td>\n";
        $str .= "<td class='mainbglabel width='120' align='center'>" . localize("Vendre") . "</td>\n";
        $str .= "<td class='mainbglabel width='150' align='right'>" . localize("Nombre d'améliorations") . "</td>\n";
        $str .= "<td class='mainbglabel width='150' align='right'>" . localize("PX récupérés") . "</td>\n";
        $str .= "</tr>\n";
        
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $curplayer->externDBObj("Upgrade,BasicRace::StaticModifierProfil");
        $curplayer->loadExternDBObj($db);
        
        $modifpr = &$curplayer->getObj("BasicRace");
        $modifpr = &$modifpr->getObj("StaticModifierProfil");
        
        $attr = array(
            "attack",
            "dodge",
            "damage",
            "regen",
            "magicSkill",
            "hp"
        ); // ,"time"
        
        $enoughxp = 0;
        
        foreach ($attr as $key) {
            $arraycharac = &$modifpr->getCharac($key);
            
            // TODO: the code below is just a trick
            $dice = 0;
            
            for ($i = 0; $i < DICE_CHARAC; $i ++) {
                if ($arraycharac[$i] != "" && $arraycharac[$i] != - 1) {
                    $dice = $i;
                }
            }
            
            $upgradeOld = $curplayer->getSub("Upgrade", $key);
            if ($upgradeOld == "")
                $upgradeOld = 0;
                
                // echo $dice;
            
            $price = $modifpr->getModif($key . "Cost", $dice) * $upgradeOld;
            $value = $modifpr->getModif($key . "Value", $dice);
            
            $dicevalue = $modifpr->getDiceValueFromIndex($dice);
            
            if ($dice < DICE_NB)
                if ($value > 0)
                    $value = "(-" . $value . "D" . $dicevalue . ")";
                else
                    $value = "(" . $value . "D" . $dicevalue . ")";
            
            if ($key == "time") {
                $value = "(" . $value . " min)";
            }
            
            if ($key == "hp") {
                $value = "(-" . $value . " " . localize("PV") . ")";
            }
            
            $enable = "";
            
            if ($upgradeOld == 1)
                $enable = "disabled='disabled'";
            
            $data[] = array(
                "enable" => $enable,
                "name" => localize($key),
                "field" => $key,
                "value" => localize($value),
                "nbenhance" => $upgradeOld,
                "cost" => $price,
                "dice" => $dice
            );
        }
        
        foreach ($data as $arr) {
            $str .= "<tr><td class='mainbgbody' align='center'><input id='Item0' type='radio' name='FIELD' value='" . $arr["field"] . ":" . $arr["dice"] . "' " . $arr["enable"] . " ></td>\n";
            $str .= "<td class='mainbgbody' align='left'> " . $arr["name"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='left'> " . $arr["value"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='right'> " . $arr["nbenhance"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='right'> " . $arr["cost"] . " " . localize("PX") . "</td></tr>\n";
        }
        
        $str .= "</table><div class='maintable mainbgtitle centerareawidth'>";
        
        if ($curplayer->get("id_Mission") != 0) {
            $str .= "<br/>&nbsp;" . localize("Ce personnage n'est pas améliorable.") . "<br/>&nbsp;";
        } else {
            
            if ($curplayer->get("ap") < ENHANCE_AP && $curplayer->get("incity") == 0) {
                $str .= "<br/>&nbsp;" . localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser un changement de profil.") . "<br/>&nbsp;";
            } else {
                if (($curplayer->get("ap") >= ENHANCE_AP || $curplayer->get("incity") == 1)) {
                    $str .= "<input id='submitbt' type='submit' name='submitbt' value='" . localize("Action") . "' />";
                    $str .= "<input name='action' type='hidden' value='46' />";
                }
            }
        }
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</div></form>";
        
        return $str;
    }
}
?>
