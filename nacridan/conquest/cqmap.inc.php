<?php

class CQMap extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQMap($nacridan, $head, $body, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $dbc = new DBCollection("SELECT * FROM City WHERE name!=type", $db);
        
        while (! $dbc->eof()) {
            $name = $dbc->get("name");
            $this->city["$name"] = array(
                $dbc->get("x"),
                $dbc->get("y"),
                "$name"
            );
            $dbc->next();
        }
        
        $head->add("<style type=\"text/css\">\n" . $this->getCityCss() . "</style>");
        
        $str = "<script language='javascript'>\n";
        $str .= "<!-- CData\n";
        $str .= "function mousecoord(obj,e) {\n";
        $str .= "var parent=obj.offsetParent;\n";
        $str .= "var offX=0;\n";
        $str .= "var offY=0;\n";
        $str .= "while (parent) {\n";
        $str .= "offX+=parent.offsetLeft;\n";
        $str .= "offY+=parent.offsetTop;\n";
        $str .= "parent=parent.offsetParent;\n";
        $str .= "};\n";
        $str .= "  if (window.event) {var mouseX=window.event.x-2; var mouseY=window.event.y-2}\n";
        $str .= "  else {var mouseX=e.pageX; var mouseY=e.pageY}\n";
        $str .= "setInnerHTML('coordX',mouseX-offX);\n";
        $str .= "setInnerHTML('coordY',mouseY-offY);\n";
        $str .= "}\n";
        $str .= "//  End -->";
        $str .= "</script>\n";
        $head->add($str);
        
        // $body->add("<img src='../pics/misc/redcross.gif' class='redcross'/>\n");
        $body->add(
            "<div style='position:absolute;left:20px;top:300px;width:150px;'><div> Passez la souris sur la carte pour afficher ci-dessous la position correspondante et pour connaître les noms des villages qui en possèdent un. <br><br>X : <span id='coordX'>-</span></div><div>Y : <span id='coordY'>-</span></div></div>");
        
        $body->add($this->getCityHtml($db));
    }

    public function toString()
    {
        // Les cartes principales (le choix de la carte se fait dans cqmaplegend.inc.php)
        $str = "<div class='centerareaMap' onMousemove='mousecoord(this,event)'><img id='map01' src='../pics/conquest/map01.jpg' style='visibility:visible; border: 0'/>";
        $str .= "<img id='map02' src='../pics/conquest/map02.jpg' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;'/>";
        
        // La croix rouge : position du joueur
        
        if ($this->curplayer->get("map") == 1) {
            
            $defaultmap1 = 'visible';
            $defaultmap2 = 'hidden';
        } else {
            
            $defaultmap1 = 'hidden';
            $defaultmap2 = 'visible';
        }
        
        // Les cartes principales (le choix de la carte se fait dans cqmaplegend.inc.php)
        $str = "<div class='centerareaMap' onMousemove='mousecoord(this,event)'><img id='map01' src='../pics/conquest/map01.jpg' style='visibility:" . $defaultmap1 .
             "; border: 0'/>";
        $str .= "<img id='map02' src='../pics/conquest/map02.jpg' style='visibility:" . $defaultmap2 . "; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;'/>";
        
        // Les cartes secondaires, attention maincity et maincity2 envoient sur des cartes légèrement différentes !
        $str .= "<img id='levelmonstermap01' src='../pics/conquest/map01b.jpg' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;'/>";
        $str .= "<img id='levelmonstermap02' src='../pics/conquest/map02b.jpg' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;'/>";
        $str .= "<img id='kingdommap01' style='visibility:" . $defaultmap2 .
             "; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='../pics/conquest/mapKingdom01.gif' usemap='#map'/>";
        $str .= "<img id='roadmap01' style='visibility:" . $defaultmap2 .
             "; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='../pics/conquest/mapRoad01.png' usemap='#map'/>";
        $str .= "<img id='maincity' style='visibility:" . $defaultmap1 .
             "; border: 0; position: absolute; z-index: 10; top: 0px; left: 0px;' src='../pics/conquest/map02.png' usemap='#map'/>";
        $str .= "<img id='maincity2' style='visibility:hidden; border: 0; position: absolute; z-index: 11; top: 0px; left: 0px;' src='../pics/conquest/map02b.png' usemap='#map'/>";
        $str .= "<img id='player01' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='.." . MAPS_DYNAMIC_FOLDER .
             "/mapghostplayer01.png?expire=" . gmdate("mdY") . "'/>";
        $str .= "<img id='player02' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='.." . MAPS_DYNAMIC_FOLDER .
             "/mapghostplayer02.png?expire=" . gmdate("mdY") . "'/>";
        $str .= "<img id='smallcity01' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='.." . MAPS_STATIC_FOLDER .
             "/mapghostcity01.png'/>";
        $str .= "<img id='smallcitycaptured01' style='visibility:" . $defaultmap2 . "; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='.." .
             MAPS_DYNAMIC_FOLDER . "/mapghostcitycaptured01.png'/>";
        $str .= "<img id='maptopo' style='visibility:hidden; border: 0; position: absolute; z-index: 2; top: 0px; left: 0px;' src='../map/scripts-IA/getMapTopographie.php?expire=" .
             gmdate("mdY") . "'/>";
        if ($this->curplayer->get("id_Team") != 0) {
            $str .= "<img id='teammember01' style='visibility:hidden; border: 0; position: absolute; z-index: 12; top: 0px; left: 0px;' src='../map/scripts-IA/getMapTeam.php?expire=" .
                 gmdate("mdY") . "&map=1'/>";
            $str .= "<img id='teammember02' style='visibility:hidden; border: 0; position: absolute; z-index: 12; top: 0px; left: 0px;' src='../map/scripts-IA/getMapTeam.php?expire=" .
                 gmdate("mdY") . "&map=2'/>";
        }
        $str .= "<img id='redcross' src='../pics/misc/redcross.gif' style='visibility:visible; z-index: 20; top: " . ($this->curplayer->get("y") - 4) . "px; left: " .
             ($this->curplayer->get("x") - 4) . "px; position: absolute;'/>\n";
        
        $isDest = false;
        $isMultipleDest = false;
        $isCaravan = false;
        if (isset($_POST["IdCity"])) { // teleportation
            $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE id=" . quote_smart($_POST["IdCity"]), $this->db, 0, 0);
            if ($dbp->count() > 0) {
                $str .= "<img src='../pics/misc/blackcross.gif' style= 'z-index: 13; top: " . ($dbp->get("y") - 4) . "px; left: " . ($dbp->get("x") - 4) .
                     "px; position: absolute;' class=' blinking'/>";
                $isDest = true;
            }
        } else {
            if (isset($_POST["CARAVAN_TARGET"])) { // caravane
                $dbp = new DBCollection("SELECT Building.x,Building.y FROM Building WHERE id=" . quote_smart($_POST["CARAVAN_TARGET"]), $this->db);
                if ($dbp->count() > 0) {
                    $str .= "<img src='../pics/misc/blackcross.gif' style= 'z-index: 13; top: " . ($dbp->get("y") - 4) . "px; left: " . ($dbp->get("x") - 4) .
                         "px; position: absolute;' class=' blinking'/>";
                    $isDest = true;
                }
            }
        }
        
        if (isset($_POST["IdCityList"])) { // teleportation
            $str .= "<form id='formMap' method='POST' target='_self'>\n";
            $str .= "<input id='IdCity' name='IdCity' type='hidden' value='' />\n";
            $str .= "<input name='IdCityList' type='hidden' value='" . $_POST["IdCityList"] . "' />\n";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
            $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE id in (" . quote_smart($_POST["IdCityList"]) . ")", $this->db);
            
            while (! $dbp->eof()) {
                if ($_POST["IdCity"] != $dbp->get("id")) {
                    $str .= "<img src='../pics/misc/bluecross.gif' onclick='document.getElementById(\"IdCity\").value=" . $dbp->get("id") .
                         ";document.getElementById(\"formMap\").submit();' style='z-index: 13;top: " . ($dbp->get("y") - 4) . "px;left: " . ($dbp->get("x") - 4) .
                         "px;position: absolute;cursor: pointer;'/>\n";
                }
                $dbp->next();
            }
            $isMultipleDest = true;
        } else {
            if (isset($_POST["IdCoComList"])) { // caravane
                $load = array();
                $load["type1"] = $_POST["CARAVAN_LOAD_TYPE_1"];
                $load["level1"] = $_POST["CARAVAN_LOAD_LEVEL_1"];
                $load["quantity1"] = $_POST["CARAVAN_LOAD_QUANTITY_1"];
                
                if ($_POST["CARAVAN_LOAD_TYPE_2"] != 0) {
                    $load["type2"] = $_POST["CARAVAN_LOAD_TYPE_2"];
                    $load["level2"] = $_POST["CARAVAN_LOAD_LEVEL_2"];
                    $load["quantity2"] = $_POST["CARAVAN_LOAD_QUANTITY_2"];
                }
                
                if ($_POST["CARAVAN_LOAD_TYPE_3"] != 0) {
                    $load["type3"] = $_POST["CARAVAN_LOAD_TYPE_3"];
                    $load["level3"] = $_POST["CARAVAN_LOAD_LEVEL_3"];
                    $load["quantity3"] = $_POST["CARAVAN_LOAD_QUANTITY_3"];
                }
                $loadFinal = base64_encode(serialize($load));
                $str .= "<form id='formMap' method='POST' target='_self'>\n";
                $str .= "<input id='CARAVAN_TARGET' name='CARAVAN_TARGET' type='hidden' value='' />\n";
                $str .= "<input name='IdCoComList' type='hidden' value='" . $_POST["IdCoComList"] . "' />\n";
                $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . (isset($_POST["Nc"]) ? $_POST["Nc"] : $_POST["CARAVAN_LEVEL"]) . "' />\n";
                $str .= "<input name='Nc' type='hidden' value='" . (isset($_POST["Nc"]) ? $_POST["Nc"] : $_POST["CARAVAN_LEVEL"]) . "' />\n";
                $str .= "<input name='LOAD' type='hidden' value='" . $loadFinal . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_TYPE_1' type='hidden' value='" . $_POST["CARAVAN_LOAD_TYPE_1"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_LEVEL_1' type='hidden' value='" . $_POST["CARAVAN_LOAD_LEVEL_1"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_QUANTITY_1' type='hidden' value='" . $_POST["CARAVAN_LOAD_QUANTITY_1"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_TYPE_2' type='hidden' value='" . $_POST["CARAVAN_LOAD_TYPE_2"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_LEVEL_2' type='hidden' value='" . $_POST["CARAVAN_LOAD_LEVEL_2"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_QUANTITY_2' type='hidden' value='" . $_POST["CARAVAN_LOAD_QUANTITY_2"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_TYPE_3' type='hidden' value='" . $_POST["CARAVAN_LOAD_TYPE_3"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_LEVEL_3' type='hidden' value='" . $_POST["CARAVAN_LOAD_LEVEL_3"] . "' />\n";
                $str .= "<input name='CARAVAN_LOAD_QUANTITY_3' type='hidden' value='" . $_POST["CARAVAN_LOAD_QUANTITY_3"] . "' />\n";
                $str .= "<input name='action' type='hidden' value='" . CARAVAN_CREATE . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>\n";
                $dbp = new DBCollection("SELECT Building.id,Building.name,Building.x,Building.y FROM Building WHERE id in (" . quote_smart($_POST["IdCoComList"]) . ")", $this->db);
                
                while (! $dbp->eof()) {
                    if ($_POST["CARAVAN_TARGET"] != $dbp->get("id")) {
                        $str .= "<img src='../pics/misc/bluecross.gif' onclick='document.getElementById(\"CARAVAN_TARGET\").value=" . $dbp->get("id") .
                             ";document.getElementById(\"formMap\").submit();' style='z-index: 13;top: " . ($dbp->get("y") - 4) . "px;left: " . ($dbp->get("x") - 4) .
                             "px;position: absolute;cursor: pointer;'/>\n";
                    }
                    $dbp->next();
                }
                $isMultipleDest = true;
            }
        }
        
        $dbcca = new DBCollection(
            "SELECT Building.x,Building.y FROM Caravan inner join Building on id_endbuilding = Building.id where Caravan.id_Player=" . $this->curplayer->get("id"), $this->db);
        if (! $dbcca->eof()) {
            while (! $dbcca->eof()) {
                $str .= "<img src='../pics/misc/blackcross_squared.gif' style='z-index: 13;top: " . ($dbcca->get("y") - 4) . "px;left: " . ($dbcca->get("x") - 4) .
                     "px;position: absolute;'/>\n";
                $dbcca->next();
            }
            $isCaravan = true;
        }
        $str .= "<img id='smallcity02' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='.." . MAPS_STATIC_FOLDER .
             "/mapghostcity02.png'/>";
        // $str.="<img id='smallcitycaptured02' style='visibility:hidden; border: 0; position: absolute; z-index: 3; top: 0px; left: 0px;' src='..".MAPS_DYNAMIC_FOLDER."/mapghostcitycaptured02.png'/>";
        
        $str .= "</div>";
        $str .= "<div class='centerareaMapLegend'>";
        $currmap = $this->curplayer->get("map");
        // Choix de la carte - une seule carte disponible au début.
        /*
         * $str="<table class='maintable bottomleftareawidth'>";
         *
         * $str.="<tr><td class='mainbgtitle'><input type=radio name='map' onClick='loadMap(\"map01\",".$currmap.")'>".localize("Contrées Centrales")."</td></tr>";
         * $str.="<tr><td class='mainbgtitle'><input type=radio name='map' onClick='loadMap(\"map02\",".$currmap.")'>".localize("Contrées Nordiques")."</td></tr>";
         * $str.="</table>";
         */
        
        // Options de la carte
        $str .= "<table class='legend'>";
        $str .= "<tr><td class='mainbglabel aligncenter'>Options</td></tr>";
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleCity(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Village") .
             "</td></tr>";
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleCityCaptured(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" .
             localize("Villages contrôlés") . "</td></tr>";
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleRoad(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Les routes") .
             "</td></tr>";
        // Affichage des royaumes
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleKingdom(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Royaumes") .
             "</td></tr>";
        
        if ($this->curplayer->get("id_Team") != 0)
            $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleTeam( \"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" .
                 localize("Membres de l'Ordre") . "</td></tr>";
            
            // $str.="<tr><td class='mainbgtitle'><input type=checkbox onClick='setStylePlayers(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>".localize("Démographie")."</td>";
            
        // if($this->curplayer->get("id_Team")!=0)
            // $str.="<td class='mainbgtitle'> </td>";
            // $str .= "</tr>";
        $str .= "<tr><td class='mainbglabel aligncenter'>Autres cartes</td></tr>";
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleLevelMonster(\"visibility\", this.checked==true ? \"visible\" : \"hidden\" )'>" .
             localize("Niveau monstres") . "</td></tr>";
        $str .= "<tr><td class='mainbgbody'><input type=checkbox onClick='setStyleMapTopo(\"visibility\", this.checked==true ? \"visible\" : \"hidden\" )'>" .
             localize("Carte topographique") . "</td></tr>";
        $str .= "<tr><td class=' aligncenter'>&nbsp;</td></tr>";
        $str .= "<tr><td class=' aligncenter'>&nbsp;</td></tr>";
        $str .= "<tr><td class='mainbglabel aligncenter'>Légende</td></tr>";
        $str .= "<tr><td class='mainbgbody'><img src='../pics/misc/redcross.gif' style='visibility:visible;'/>" . localize(": Position actuelle") . "</td></tr>";
        if ($isDest)
            $str .= "<tr><td class='mainbgbody'><img src='../pics/misc/blackcross.gif' class='blinking' style='visibility:visible; '/>" . localize(": Prochaine position") .
                 "</td></tr>";
        if ($isMultipleDest)
            $str .= "<tr><td class='mainbgbody'><img src='../pics/misc/bluecross.gif' style='visibility:visible;'/>" . localize(": Position possible") . "</td></tr>";
        if ($isCaravan)
            $str .= "<tr><td class='mainbgbody'><img src='../pics/misc/blackcross_squared.gif' style='visibility:visible; '/>" . localize(": Dest. caravane") . "</td></tr>";
        
        $str .= "</table>";
        $str .= "</div>";
        return $str;
    }

    function getCityHtml(&$db)
    {
        $str = "<map name='map'>\n";
        
        foreach ($this->city as $key => $val) {
            $str .= "<area shape=circle coords='" . $val[0] . "," . $val[1] . ",20' onMouseOut=\"hideTip()\" onMouseOver=\"showTip('" . $key . "div')\" href='#'>\n"; // HERE ADRESSE
        }
        
        $str .= "</map>\n";
        $dbcity = new DBCollection("SELECT * FROM City", $db);
        foreach ($this->city as $key => $val) {
            switch ($key) {
                case 'Nephy':
                case 'Izandar':
                case 'Octobian':
                case 'Landar':
                case 'Krima':
                case 'Djin':
                case 'Earok':
                case 'Dust':
                case 'Artasse':
                case 'Tonak':
                    $str .= "<div style='left:" . ($val[0] + 280) . "px; top:" . ($val[1] + 50) . "px;' class='city' id='" . $key . "div'>" . localize("Cité de {city}", 
                        array(
                            "city" => $val[2]
                        )) . "<br/>(X:" . $val[0] . "&nbsp; Y:" . $val[1] . ")</div>\n";
                    break;
                default:
                    $str .= "<div style='left:" . ($val[0] + 280) . "px; top:" . ($val[1] + 50) . "px; z-index: 13;' class='city' id='" . $key . "div'>" . localize("{city}", 
                        array(
                            "city" => $val[2]
                        )) . "<br/>(X:" . $val[0] . "&nbsp; Y:" . $val[1] . ")</div>\n";
                    break;
            }
        }
        return $str;
    }

    public function getCityCss()
    {
        $str = ".city {visibility: hidden;
                z-index: 11;
               text-indent: 0;
               vertical-align: top;
               color: #000;
               background-color: #F4F4F4;
               font-weight: bold;
               position: absolute;
               border: 3px ridge #AC9C9C;
               width: 130px;
               height: 40px;}\n";
        
        return $str;
    }
}
?>
