<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/CityFactory.inc.php");
require_once (HOMEPATH . "/factory/MailFactory.inc.php");

class CQHostel extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQHostel($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('G R A N D - S A L L E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        if (isset($_POST['inhostel'])) {
            $inhostel = $_POST['inhostel'];
            
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth'>\n";
            
            switch ($inhostel) {
                case "drink":
                    if ($curplayer->get("ap") < HOSTEL_DRINK_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_DRINK, $db)) {
                        $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<td><input name='action' type='hidden' value='" . HOSTEL_DRINK . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td>Cela vous coutera " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_DRINK, $db) . " PO (et " . HOSTEL_DRINK_AP . " PA), vous confirmez la commande ?</td></tr>";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                    }
                    break;
                case "rest":
                    if ($curplayer->get("ap") < SLEEPING_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), BEDROOM_SLEEPING, $db)) {
                        $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<td><input name='action' type='hidden' value='" . BEDROOM_SLEEPING . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td>Cela vous coutera " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), BEDROOM_SLEEPING, $db) . " PO (et " . SLEEPING_AP . " PA), vous confirmez la commande ?</td></tr>";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                    }
                    break;
                case "chat":
                    if ($curplayer->get("ap") < HOSTEL_CHAT_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("status") == "NPC") {
                        $str .= "<tr><td>Ce personnage ne peut pas obtenir de mission d'escorte.</td></tr>";
                    } else {
                        
                        $str .= "<td><input name='action' type='hidden' value='" . HOSTEL_CHAT . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td>Entamez-vous la conversation ? (" . HOSTEL_CHAT_AP . " PA)</td></tr>";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                    }
                    
                    break;
                case "pay":
                    if ($curplayer->get("ap") < HOSTEL_ROUND_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_ROUND, $db)) {
                        $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                    } else {
                        
                        $str .= "<td><input name='action' type='hidden' value='" . HOSTEL_ROUND . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td>Payez vous la tournée ? (" . HOSTEL_ROUND_AP . " PA et " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_ROUND, $db) . " PO)</td></tr>";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                    }
                    break;
                /* -------------------------------------------- Création du message commercial ------------------------------------------- */
                case "trading":
                    if ($curplayer->get("ap") < HOSTEL_TRADE_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("money") < HOSTEL_TRADE_PRICE) {
                        $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<tr><td  class='mainbglabel'>Ecrivez votre message commercial ici.</td></tr>\n";
                        $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='30' name='Title'  id='Title' /></td></tr>";
                        $str .= "<tr>";
                        $str .= "<td class='mainbgbody' align='left'><label for='Body'> Messsages: </label> </td>";
                        $str .= "</tr>";
                        $str .= "<tr>";
                        $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='Body' id='Body' /> </textarea></td>";
                        $str .= "</tr>";
                        $str .= "<tr>\n";
                        $str .= "<td><input name='action' type='hidden' value='" . HOSTEL_TRADE . "' /></td>";
                        $str .= "<tr>\n";
                        $str .= "<tr><td class='mainbgbody'>Envoyer le message vous coutera " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_TRADE, $db) . " PO (et " . HOSTEL_TRADE_AP . " PA) </td></tr>";
                        $str .= "<tr>\n";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        
                        $str .= "<tr><td><input type='submit' name='submitbt' value='Envoyer'/> </td></tr>";
                        // $str.="</table>";
                    }
                    break;
                default:
                    break;
            }
            $str .= "</table></form>";
        } else {
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td class='mainbglabel'>" . 'Vous désirez ... ?' . "</td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='inhostel' value='drink' id='drink' />" . "<label for='drink'>" . "Boire un coup pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_DRINK, $db) . "PO (et " . HOSTEL_DRINK_AP . "PA)" . "</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='inhostel' value='rest' id='rest' />" . "<label for='rest'>" . "Dormir en chambre pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), BEDROOM_SLEEPING, $db) . " PO (et " . SLEEPING_AP . " PA)" . "</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='inhostel' value='chat' id='chat' />" . "<label for='chat'>" . "Discuter avec les clients. (" . HOSTEL_CHAT_AP . " PA)" . "</label></td></tr>";
            // $str.="<tr><td class='mainbgbody'>"."<input type='radio' name='inhostel' value='pay' id='pay' />"."<label for='pay'>"."Payer une tournée pour ".CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"),HOSTEL_ROUND,$db)." PO (et ".HOSTEL_ROUND_AP." PA)"."</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='inhostel' value='trading' id='trading' />" . "<label for='trading'>" . "Envoyer un message commercial pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_TRADE, $db) . " PO (et " . HOSTEL_TRADE_AP . " PA)" . "</label></td></tr>";
            $str .= "<tr><td><input type='submit' name='ok' value='Ok'/> </td></tr>";
            $str .= "</table>";
            $str .= "</form>";
        }
        
        return $str;
    }
}
?>
"
</form>
"; } return $str; } } ?>
