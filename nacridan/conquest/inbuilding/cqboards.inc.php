<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQBoards extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQBoards($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E &nbsp;&nbsp;&nbsp; D E S &nbsp;&nbsp;&nbsp; A N N O N C E S') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td>\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&bottom=viewpanel&board=1' class='tabmenu'>" . localize("Listes de Ventes") . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&bottom=viewpanel&board=2' class='tabmenu'>" . localize("Avis de recherches") . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&bottom=viewpanel&board=3' class='tabmenu'>" . localize("Autres annonces") . "</a> \n";
        $str .= "</tr></td>\n";
        $str .= "</table>\n";
        
        if (isset($_GET["board"]))
            $board = quote_smart($_GET["board"]);
        else
            $board = 3;
        
        switch ($board) {
            case 1: // ------------------------------- LISTES DE VENTES
                if (isset($_POST['eqtype'])) // --------------------- Affichage des listes de ventes détaillées
{
                    
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    if (! is_numeric($_POST["lvlmin"]))
                        $str .= "<tr><td>Le niveau min doit être un nombre.</td></tr>";
                    elseif (! is_numeric($_POST["lvlmax"]))
                        $str .= "<tr><td>Le niveau max doit être un nombre.</td></tr>";
                    elseif (! is_numeric($_POST["pricemin"]))
                        $str .= "<tr><td>Le prix min doit être un nombre.</td></tr>";
                    elseif (! is_numeric($_POST["pricemax"]))
                        $str .= "<tr><td>Le prix max doit être un nombre.</td></tr>";
                    else {
                        
                        $str .= $this->displayOnSale($_POST["lvlmin"], $_POST["lvlmax"], $_POST["pricemin"], $_POST["pricemax"], $_POST["template"], $_POST["eqtype"], $curplayer, $db);
                    }
                    
                    $str .= "</table>";
                } else // ------------------------------- Affichage des deux premiers objets en vente et formulaire de recherche
{
                    $str .= $this->displayOnSale(1, 100, 1, 1000000, "both", "all", $curplayer, $db);
                    $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel&board=1' target='_self'>\n";
                    
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td class='mainbglabel' colspan='5'> Rechercher un objet en vente : </td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'><label for='pricemin'>Niveau minimum : </label><input type='text' name='lvlmin'  id='lvlmin' /></td></tr>";
                    $str .= "<tr><td class='mainbgbody'colspan='5'><label for='pricemax'>Niveau maximum : </label><input type='text' name='lvlmax'  id='lvlmax' /></td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'><label for='pricemin'>Prix minimum : </label><input type='text' name='pricemin'  id='pricemin' /></td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'><label for='pricemax'>Prix maximum : </label><input type='text' name='pricemax'  id='pricemax' /></td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'>" . "<input type='radio' name='template' value='yes' id='yes' />" . "<label for='yes'>Objet enchanté uniquement</label></td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'>" . "<input type='radio' name='template' value='no' id='no' />" . "<label for='no'>Objet sans enchantement</label></td></tr>";
                    $str .= "<tr><td class='mainbgbody' colspan='5'>" . "<input type='radio' name='template' value='both' checked id='both' />" . "<label for='both'>Objet avec ou sans enchantement</label></td></tr>";
                    
                    $dbe = new DBCollection("SELECT name,id FROM BasicEquipment", $db, 0, 0);
                    $str .= "<tr><td class='mainbgbody'><label for='eqtype' >Type d'objet</label></td></tr>\n";
                    $str .= "<tr><td class='mainbglabel'  align='left'><select name='eqtype'><option value='all'>Tout type</option>";
                    while (! $dbe->eof()) {
                        $str .= "<option value='" . $dbe->get("id") . "'>" . $dbe->get("name") . "</option>";
                        $dbe->next();
                    }
                    $str .= "</select></td></tr>";
                    
                    $str .= "<tr><td><input type='submit' name='ok' value='Ok'/> </td></tr>";
                }
                break;
            
            case 3: // ----------------------------------------- AUTRES ANNONCES ---------------------
                if (isset($_POST['check'])) {
                    if ($curplayer->get("ap") < HOSTEL_NEWS_AP) {
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_NEWS, $db)) {
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                    } else // ---------- Affichage de l'annonce qui va être créer, pour permettre une vérification avant envoie.
{
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td  class='mainbglabel' colspan='3'>Cette annonce convient-elle ? </td></tr>\n";
                        $str .= "<tr class='mainbgbody' >";
                        
                        $str .= "<td width='100px'>" . gmdate("d-m-Y") . "</td>";
                        $str .= "<td width='250px' >*** <b>" . stripslashes($_POST["title"]) . "</b> ***</td>";
                        
                        $authorName = "<a href='../conquest/profile.php?id=" . $curplayer->get("id") . "' class='stylepc popupify'>" . $curplayer->get("name") . "</a>";
                        
                        $str .= "<td align='right'>" . $authorName . "</td>";
                        
                        $content = stripslashes($_POST["content"]);
                        $content = nl2br($content);
                        $str .= "</tr>";
                        $str .= "<tr class='mainbgbody' >";
                        $str .= "<td colspan=3>" . $content . "</td>";
                        $str .= "</tr>";
                        $str .= "<input type='hidden' name='TITLE' value='" . base64_encode($_POST['title']) . "'/>";
                        $str .= "<input type='hidden' name='CONTENT' value='" . base64_encode($_POST['content']) . "'/>";
                        $str .= "<td><input name='action' type='hidden' value='" . HOSTEL_NEWS . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td class='mainbgbody' colspan='3'><input type='radio' name='CREATE' value='yes' id='yes' /><label for='yes'>Oui, je vais poster cette annonce pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_NEWS, $db) . " pièces d'or (" . HOSTEL_NEWS_AP . "PA)</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody' colspan='3'><input type='radio' name='CREATE' value='no' id='no' checked /><label for='no'>Non, cette annonce ne me convient pas.</label></td></tr>";
                        $str .= "<tr><td><input type='submit' name='createnews' value='Ok'/> </td></tr>";
                    }
                } else {
                    $str .= $this->displayNews($curplayer, $db);
                    $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel&board=3' target='_self'>\n";
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td  class='mainbglabel'><a name='create'> </a>Créer une annonce pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), HOSTEL_NEWS, $db) . " PO (et 3PA) </td></tr>\n";
                    $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='30' name='title'  id='title' /></td></tr>";
                    $str .= "<tr>";
                    $str .= "<td class='mainbgbody' align='left'><label for='content'> Texte de l'annonce : </label> </td>";
                    $str .= "</tr>";
                    $str .= "<tr>";
                    $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='content' id='content' /> </textarea></td>";
                    $str .= "</tr>";
                    if ($curplayer->get("authlevel") > 10) {
                        $str .= "<tr><td class='mainbgbody'>Annonce de l'équipe de dev, elle sera affichée dans les auberges avant toutes les autres.</td></tr>";
                    }
                    $str .= "<tr><td><input type='submit' name='check' value='Ok'/> </td></tr>";
                }
                break;
            
            case 2:
                $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                $str .= "<tr><td>Liste des avis de recherche.</td></tr>";
                break;
        }
        
        $str .= "</table></form>";
        
        return $str;
    }
    
    // ------------------------------------------ AFFICHAGE LISTES DE VENTES ------------------------------
    public function displayOnSale($lvlmin, $lvlmax, $pricemin, $pricemax, $withtemplate, $type, $curplayer, $db)
    {
        $eq = new Equipment();
        $owner = new Player();
        $id = $curplayer->get("id");
        $str = "";
        
        if (isset($_POST["__stepEvt"])) {
            $step = quote_smart($_POST["__stepEvt"]);
            if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"]))
                $step += 1;
            if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"]))
                $step -= 1;
            if (isset($_POST["NextEvt10"]) || isset($_POST["NextEvt10_x"]))
                $step += 10;
            if (isset($_POST["PrevEvt10"]) || isset($_POST["PrevEvt10_x"]))
                $step -= 10;
        }
        
        if (! isset($step) || $step < 0)
            $step = 0;
        
        if ($withtemplate == 'yes')
            $template = " AND extraname!='' ";
        elseif ($withtemplate == 'no')
            $template = " AND extraname='' ";
        else
            $template = "";
        
        if ($type == "all")
            $basicequip = "";
        else
            $basicequip = "AND id_BasicEquipment=" . $type;
        
        $dbe = new DBCollection("SELECT " . $eq->getASRenamer("Equipment", "EQ") . ", " . $owner->getASRenamer("Player", "P") . "  FROM Equipment LEFT JOIN Player ON Equipment.id_Player=Player.id WHERE Equipment.level>=" . $lvlmin . " AND Equipment.level<=" . $lvlmax . " AND Equipment.sell>=" . $pricemin . " AND Equipment.sell<=" . $pricemax . " " . $template . " " . $basicequip . " AND Equipment.sell!=0 AND id_Player!=" . $id, $db, $step * STEP_SALE_LISTS, STEP_SALE_LISTS);
        
        $currsize = $dbe->count();
        
        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel&board=1' target='_self'>\n";
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='5' class='mainbglabel' colspan='5'> <b>Quelques objets en vente :</b> </td>\n";
        
        if ($currsize == STEP_SALE_LISTS) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../pics/misc/left.gif' value='" . localize("previous") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/left.gif' /></td>\n";
        }
        
        if ($step > 0) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../pics/misc/right.gif' value='" . localize("next") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/right.gif' /></td>\n";
        }
        
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        // $str.="<td class='mainbglabel' width='30px' align='center'>".localize('Id')."</td>\n";
        $str .= "<td class='mainbglabel' width='30px' align='center'>" . localize('Niv.') . "</td>\n";
        $str .= "<td class='mainbglabel' width='210px' align='center'>" . localize('Nom') . "</td>\n";
        $str .= "<td class='mainbglabel' width='60px'>" . localize('Prix') . "</td>\n";
        $str .= "<td class='mainbglabel' >" . localize('Propriétaire') . "</td>\n";
        
        $str .= "</tr></table>";
        $str .= "<table class='maintable insidebuildingleftwidth'>";
        
        while (! $dbe->eof()) {
            
            $eq->DBLoad($dbe, "EQ");
            $owner->DBLoad($dbe, "P");
            
            $str .= "<tr class='mainbgbody' >";
            
            // $str.="<td>".$eq->get("id")."</td>";
            $str .= "<td width='30px'>" . $eq->get("level") . "</td>";
            $str .= "<td width='210px'>" . "<span class='". ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")). "</span>";
            if ($eq->get("extraname") != "") {
                $str .= " <span class='template'>" . $eq->get("extraname") . "</span>";
            }
            $str .= "</td>";
            $str .= "<td width='60px'>" . $eq->get("sell") . " </td>";
            
            $ownerName = "<a href='../conquest/profile.php?id=" . $owner->get("id") . "' class='stylepc popupify'>" . $owner->get("name") . "</a>";
            
            $str .= "<td>" . $ownerName . "</td>";
            
            $str .= "</tr>";
            
            $dbe->next();
        }
        
        $str .= "</table>";
        $str .= "<input name='__stepEvt' type='hidden' Value='" . $step . "' />\n";
        $str .= "<input name='lvlmin' type='hidden' Value='" . $lvlmin . "' />\n";
        $str .= "<input name='lvlmax' type='hidden' Value='" . $lvlmax . "' />\n";
        $str .= "<input name='pricemin' type='hidden' Value='" . $pricemin . "' />\n";
        $str .= "<input name='pricemax' type='hidden' Value='" . $pricemax . "' />\n";
        $str .= "<input name='eqtype' type='hidden' Value='" . $type . "' />\n";
        $str .= "<input name='template' type='hidden' Value='" . $withtemplate . "' />\n";
        $str .= "</form>";
        
        return $str;
    }
    
    // ------------------------------------------------------ AFFICHAGE ANNONCES -----------------------
    public function displayNews($curplayer, $db)
    {
        $news = new News();
        $newsd = new News();
        $author = new Player();
        $authord = new Player();
        $id = $curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        $str = "";
        $msg = "";
        
        if (isset($_POST["__stepEvt"])) {
            $step = quote_smart($_POST["__stepEvt"]);
            if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"]))
                $step += 1;
            if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"]))
                $step -= 1;
            if (isset($_POST["NextEvt10"]) || isset($_POST["NextEvt10_x"]))
                $step += 10;
            if (isset($_POST["PrevEvt10"]) || isset($_POST["PrevEvt10_x"]))
                $step -= 10;
        }
        
        if (! isset($step) || $step < 0)
            $step = 0;
        if (! isset($_POST["currsize"]))
            $currsize = 0;
        else
            $currsize = $_POST["currsize"];
            
            // Dernière Annonce dev
        
        $dbnd = new DBCollection("SELECT " . $newsd->getASRenamer("News", "N") . ", " . $authord->getASRenamer("Player", "P") . "  FROM News LEFT JOIN Player ON News.id_Player=Player.id WHERE News.type=1", $db);
        
        if ($dbnd->count() > 0) {
            $newsd->DBLoad($dbnd, "N");
            $authord->DBLoad($dbnd, "P");
            
            $str .= "<table class='maintable insidebuildingleftwidth'>";
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td colspan='3' >*** ANNONCE OFFICIELLE ***</td>";
            $str .= "</tr>";
            
            $str .= "<tr class='mainbgbody' >";
            
            $date = strtotime($newsd->get("date"));
            $str .= "<td width='100px'>" . gmdate("d-m-Y", $date) . "</td>";
            $str .= "<td width='250px' >*** <b>" . stripslashes($newsd->get("title")) . "</b> ***</td>";
            
            $authorName = "<a href='../conquest/profile.php?id=" . $authord->get("id") . "' class='stylepc popupify'>" . $authord->get("name") . "</a>";
            
            $str .= "<td align='right'>" . $authorName . "</td>";
            
            $str .= "</tr>";
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td colspan=3>" . nl2br(stripslashes($newsd->get("content"))) . "</td>";
            $str .= "</tr>";
            $str .= "</table>";
        }
        
        $str .= "<table class='maintable insidebuildingleftwidth'>";
        $str .= "<tr class='mainbgbody' >";
        $str .= "<td colspan='3' >*** ANNONCES STANDARDS ***</td>";
        $str .= "</tr>";
        $str .= "</table>";
        
        // Toutes les Annonces joueurs
        
        // $dbn=new DBCollection("SELECT ".$news->getASRenamer("News","N").", ".$author->getASRenamer("Player","P")." FROM News LEFT JOIN Player ON News.id_Player=Player.id WHERE News.type=0 ORDER BY News.id ASC",$db,$step*STEP_NEWS,STEP_NEWS);
        
        $dbn = new DBCollection("SELECT " . $news->getASRenamer("News", "N") . ", " . $author->getASRenamer("Player", "P") . "  FROM News LEFT JOIN Player ON News.id_Player=Player.id WHERE News.type=0 AND Player.map=News.map AND (abs(News.x-" . $xp . ") + abs(News.y-" . $yp . ") + abs(News.x+News.y-" . $xp . "-" . $yp . "))/2<(50*News.level) ORDER BY News.id ASC", $db, $step * STEP_NEWS, STEP_NEWS);
        
        while (! $dbn->eof()) {
            
            $news->DBLoad($dbn, "N");
            $author->DBLoad($dbn, "P");
            
            // if(($dbb->get("map") == $curplayer->get("map")) && (distHexa($xp,$yp,$xb,$yb)<(50*$dbb->get("level"))) && ($currsize>=$step*STEP_NEWS) && ($currsize<($step+1)*STEP_NEWS))
            // {
            $msg .= "<table class='maintable insidebuildingleftwidth'>";
            $msg .= "<tr class='mainbgbody' >";
            
            $date = strtotime($news->get("date"));
            $msg .= "<td width='100px'>" . gmdate("d-m-Y", $date) . "</td>";
            $msg .= "<td width='250px' >*** <b>" . stripslashes($news->get("title")) . "</b> ***</td>";
            
            $authorName = "<a href='../conquest/profile.php?id=" . $author->get("id") . "' class='stylepc popupify'>" . $author->get("name") . "</a>";
            
            $msg .= "<td align='right'>" . $authorName . "</td>";
            
            $msg .= "</tr>";
            $msg .= "<tr class='mainbgbody' >";
            $msg .= "<td colspan=3>" . nl2br(stripslashes($news->get("content"))) . "</td>";
            $msg .= "</tr>";
            $msg .= "</table>";
            $currsize ++;
            
            // }
            
            $dbn->next();
        }
        
        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel&board=3' target='_self'>\n";
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='3' class='mainbglabel'><a href='#create' class='styleall'>Poster une annonce.</a> </td>\n";
        
        if ($currsize == STEP_NEWS) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../pics/misc/left.gif' value='" . localize("previous") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/left.gif' /></td>\n";
        }
        
        if ($step > 0) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../pics/misc/right.gif' value='" . localize("next") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/right.gif' /></td>\n";
        }
        
        $str .= "<tr>\n";
        $str .= "<td colspan='3' class='mainbglabel'>  <b>Quelques annonces : </b>  </td>\n";
        $str .= "</tr></table>";
        
        $str .= "<input name='__stepEvt' type='hidden' Value='" . $step . "' />\n";
        $str .= "<input name='currsize' type='hidden' Value='" . $currsize . "' />\n";
        
        $str .= "</form>";
        
        $str .= $msg;
        
        return $str;
    }
}
?>
