<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQTower extends HTMLObject
{

    public $db;

    public $curplayer;

    public $curbuilding;

    public $nacridan;

    public $err;

    public $body;

    public function CQTower($building, &$body, $nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        $this->curbuilding = $building;
        $this->body = $body;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $curbuilding = $this->curbuilding;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $blevel = $curbuilding->get("level");
        $bsp = $curbuilding->get("sp");
        $idbb = $curbuilding->get("id_BasicBuilding");
        $bvalue = $curbuilding->get("value");
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'> <b>" . localize('T O U R &nbsp;&nbsp;&nbsp;&nbsp; D E &nbsp;&nbsp;&nbsp;&nbsp; G A R D E ') . "</b> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel'> Depuis la tour de garde, vous pouvez tirer sur tous les ennemis en vue\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        require_once (HOMEPATH . '/conquest/cqview2D.inc.php');
        $tempview = new CQView2D($this->nacridan, $this->body, $this->db);
        $minimap = $tempview->minimap;
        $str .= $tempview->str;
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
