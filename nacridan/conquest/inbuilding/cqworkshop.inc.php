<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQWorkshop extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQWorkshop($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $money = $curplayer->get("money");
        $err = $this->err;
        $str = "";
        
        $dbB = new DBCollection("SELECT level,id_City FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
        $dbC = new DBCollection("SELECT id,id_Player FROM City WHERE id=" . $dbB->get("id_City"), $db, 0, 0);
        
        $Blevel = $dbB->get("level");
        $percentBank = 100 - max(2, (6 - $Blevel));
        $percentTaxe = min(9, 2 * $Blevel - 1);
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        else
            $type = 1;
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('A T E L I E R') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td>";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Commandes') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Libre-Service') . "</a> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbgtitle'> ATTENTION !!! N'utilisez pas vos savoir-faire d'artisanat dans cette pièce sans avoir lu le panneau \"Libre-Service\" .</td></tr>";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>";
        
        if (isset($_POST["order"])) // PASSER UNE COMMANDE
{
            $dbbe = new DBCollection("SELECT id,name,frequency  FROM BasicEquipment WHERE id=" . quote_smart($_POST["ID_BASIC_EQUIP"]), $db);
            $LvL = $_POST["EQUIP_LEVEL"];
            $price = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $LvL, $db);
            $price += floor(((CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), GUILD_ORDER_EQUIP, $db) - 10) / 100) * $price);
            
            if ($curplayer->get("money") < $price) {
                $str .= "<tr><td class='mainbgbody'> Cet objet coûte " . $price . ", vous n'avez pas assez d'or pour passer la commande.</td></tr>";
            } else {
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous allez commander un(e) " . $dbbe->get("name") . " pour " . $price . " PO. La fabrication prendra " . $LvL .
                     " jour(s).</td></tr>";
                $str .= "<input name='action' type='hidden' value='" . GUILD_ORDER_EQUIP . "' />";
                $str .= "<input name='ID_BASIC' type='hidden' value='" . $dbbe->get("id") . "' />";
                $str .= "<input name='EQ_LEVEL' type='hidden' value='" . $LvL . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } elseif (isset($_POST["take"])) // RETRAIT
{
            
            $dbpe = new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND weared='No' AND id_Player=" . $id, $db);
            if ($dbpe->count() >= NB_MAX_ELEMENT)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez de place dans votre sac principal.</td></tr>";
            else {
                $dbe = new DBCollection("SELECT id,name FROM Equipment WHERE id=" . quote_smart($_POST["take"]), $db, 0, 0);
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous vous apprêtez à retirer votre commande : <b> " . $dbe->get("name") . " </b> </td></tr>";
                $str .= "<tr><td class='mainbgbody' >";
                $str .= "<input name='action' type='hidden' value='" . GUILD_TAKE_EQUIP . "' />";
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $dbe->get("id") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } else {
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
            switch ($type) {
                case 1:
                    // Commandes
                    $str .= "<tr><td class='mainbgtitle'> <b>Passer une commande</b> </td></tr>";
                    $str .= "<tr><td class='mainbgbody'>";
                    $str .= "Vous pouvez commander la fabrication d'un objet. Le prix est majoré de " .
                         (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), GUILD_ORDER_EQUIP, $db) - 10) . " %. Cette guilde ne peut pas assurer plus de " .
                         (2 * $Blevel) . " commandes simultanément.";
                    $str .= "</td></tr>";
                    
                    $dbe = new DBCollection(
                        "SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE extraname='' AND id_Shop=" . $curplayer->get("inbuilding") . " AND date>NOW()", $db, 0, 0);
                    if ($dbe->count() < (2 * $Blevel)) {
                        
                        $str .= "<tr><td class='mainbgbody'>  ";
                        $str .= " Choisissez l'objet à fabriquer : ";
                        $str .= "<select id='target' class='selector cqattackselectorsize' name='ID_BASIC_EQUIP'>";
                        $dbbe = new DBCollection(
                            "SELECT id,name FROM BasicEquipment WHERE id_EquipmentType<28 OR id_EquipmentType=44 OR id_EquipmentType=40 OR id_EquipmentType=41 OR id_EquipmentType=42 OR id_EquipmentType=43 OR id_EquipmentType=46 order by name", 
                            $db, 0, 0);
                        
                        while (! $dbbe->eof()) {
                            $item[] = array(
                                $dbbe->get("name") => $dbbe->get("id")
                            );
                            $dbbe->next();
                        }
                        
                        foreach ($item as $arr) {
                            foreach ($arr as $key => $value) {
                                if ($value == - 1)
                                    $str .= "<optgroup class='group' label='" . $key . "' />";
                                else
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                        }
                        $str .= "</select></td></tr>";
                        
                        $str .= "<tr><td class='mainbgbody'>  ";
                        $str .= " Choisissez le niveau de l'objet à fabriquer : ";
                        $str .= "<select id='target' class='selector' name='EQUIP_LEVEL'>";
                        
                        for ($i = 1; $i <= $Blevel; $i ++) {
                            $str .= "<option value='" . $i . "'>" . $i . "</option>";
                        }
                        
                        $str .= "</select></td></tr>";
                        $str .= "<input name='order' type='hidden' value='create' />\n";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Commander' /></tr></td>";
                    } else
                        $str .= "<tr><td class='mainbglabel' > Le nombre maximum de commandes simultanées est atteint </td></tr>";
                    
                    $str .= "</form>";
                    $str .= "</table>\n";
                    
                    $dbe = new DBCollection("SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE extraname='' AND id_Shop=" . $curplayer->get("inbuilding"), $db, 0, 0);
                    // Commandes en cours
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td class='mainbgtitle' colspan='3'> <b>Commandes en cours</b> </td></tr>";
                    $str .= "<tr class='mainbglabel'>  <td> Commande </td> <td> Client </td> <td> Date de livraison </td> </tr>";
                    while (! $dbe->eof()) {
                        if (gmstrtotime(date("Y-m-d H:i:s", gmstrtotime($dbe->get("date")))) > gmstrtotime(date("Y-m-d H:i:s"))) {
                            $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("id_Warehouse"), $db, 0, 0);
                            $str .= "<tr class='mainbgbody'> <td> " . $dbe->get("name") . " </td> <td> " . $dbp->get("name") . " </td> <td> " .
                                 date("Y-m-d H:i:s", gmstrtotime($dbe->get("date"))) . " </td> </tr>";
                        }
                        $dbe->next();
                    }
                    $ok = 0;
                    // Commandes prêtes
                    
                    // echo $dbe->count();
                    $dbe = new DBCollection("SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE extraname='' AND id_Shop=" . $curplayer->get("inbuilding"), $db, 0, 0);
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel'>";
                    $str .= "<form name='form'  method='POST' target='_self'>";
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td class='mainbgtitle' colspan='4'> <b>Commandes terminées</b> </td></tr>";
                    $str .= "<tr class='mainbglabel'> <td>&nbsp;</td> <td> Commande </td> <td> Client </td> <td> Date de livraison </td> </tr>";
                    while (! $dbe->eof()) {
                        
                        if (gmstrtotime(date("Y-m-d H:i:s", gmstrtotime($dbe->get("date")))) <= gmstrtotime(date("Y-m-d H:i:s"))) {
                            $ok = 1;
                            $dbp = new DBCollection("SELECT id,name FROM Player WHERE id=" . $dbe->get("id_Warehouse"), $db, 0, 0);
                            if ($dbp->count() > 0 && $dbp->get("id") == $id)
                                $enable = "";
                            else
                                $enable = "disabled";
                            $str .= "<tr class='mainbgbody'><td><input name='take' type='radio'" . $enable . " value='" . $dbe->get('id') . "' /></td> <td> " . $dbe->get("name") .
                                 " </td> <td> " . ($dbp->count() > 0 ? $dbp->get("name") : "Inconnu") . " </td> <td> " . date("Y-m-d H:i:s", gmstrtotime($dbe->get("date"))) .
                                 " </td> </tr>";
                        }
                        $dbe->next();
                    }
                    if ($ok) {
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Prendre cet objet' /></tr></td>";
                    }
                    
                    break;
                
                case 2:
                    
                    // Dépot
                    
                    $str .= "<tr><td class='mainbgtitle' > <b>Libre-Service</b> </td></tr>";
                    $str .= "<tr><td class='mainbgbody' >";
                    $str .= " Vous pouvez utilisez ici vos compétences d'artisanat de la même façon que d'habitude. La guilde vous autorise à vous servir des outils, de plus les maîtres artisans vous conseilleront pour réussir des pièces de qualité. Notez que les outils de raffinage de la guilde ne sont pas cumulables avec les vôtres. <b>Ce service vous coûtera " .
                         GUILD_BONUS_PRICE . " PO. </b>";
                    
                    $str .= "</td></tr>";
                    
                    break;
            }
        }
        $str .= "</table>\n";
        $str .= "</form>";
        
        return $str;
    }
}
?>
