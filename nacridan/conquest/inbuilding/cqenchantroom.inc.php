<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQEnchantroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQEnchantroom($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $money = $curplayer->get("money");
        $err = $this->err;
        $str = "";
        
        $dbB = new DBCollection("SELECT level,id_City FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
        $dbC = new DBCollection("SELECT id,id_Player FROM City WHERE id=" . $dbB->get("id_City"), $db, 0, 0);
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        else
            $type = 1;
        
        $Blevel = $dbB->get("level");
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E &nbsp; &nbsp; &nbsp; D E S &nbsp;&nbsp;&nbsp; E N C H A N T E M E N T S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td>";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Enchantements') . "</a>";
        $str .= " |\n <a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Rune d\'équilibre') . "</a> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>";
        
        if (isset($_POST["order"])) // PASSER UNE COMMANDE
{
            
            $LvL = $_POST["ENCHANT_LEVEL"];
            $price = EquipFactory::getPriceEnchant($LvL, $db);
            $price += floor(((CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), GUILD_ORDER_ENCHANT, $db) - 10) / 100) * $price);
            
            $dbe = new DBCollection("SELECT id,name,level FROM Equipment WHERE id=" . quote_smart($_POST["ID_EQUIP"]), $db, 0, 0);
            if ($curplayer->get("money") < $price) {
                $str .= "<tr><td class='mainbgbody'> Cet objet coûte " . $price . ", vous n'avez pas assez d'or pour passer la commande.</td></tr>";
            } elseif ($dbe->get("level") < $LvL)
                $str .= "<tr><td class='mainbgbody'> Le niveau de l'objet doit être supérieur ou égal au niveau de l'enchantement.</td></tr>";
            else {
                
                $dbt = new DBCollection("SELECT id,name FROM BasicTemplate WHERE id=" . quote_smart($_POST["ID_BASIC_ENCHANT"]), $db, 0, 0);
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous allez commander un enchantement <span class='template'>" . $dbt->get("name") . "</span> sur votre " . $dbe->get("name") . " pour " . $price . " PO. La fabrication prendra " . (2 * $LvL) . " jour(s).</td></tr>";
                $str .= "<input name='action' type='hidden' value='" . GUILD_ORDER_ENCHANT . "' />";
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $dbe->get("id") . "' />";
                $str .= "<input name='ID_BASIC_ENCHANT' type='hidden' value='" . $_POST["ID_BASIC_ENCHANT"] . "' />";
                $str .= "<input name='EN_LEVEL' type='hidden' value='" . $LvL . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } elseif (isset($_POST["take"])) // RETRAIT
{
            
            $dbpe = new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND weared='No' AND id_Player=" . $id, $db);
            if ($dbpe->count() >= NB_MAX_ELEMENT)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez de place dans votre sac principal.</td></tr>";
            else {
                $dbe = new DBCollection("SELECT id,name,extraname FROM Equipment WHERE id=" . quote_smart($_POST["take"]), $db, 0, 0);
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous vous apprêtez à retirer votre commande :  " . $dbe->get("name") . " <span class='template'>" . $dbe->get("extraname") . "</span> </td></tr>";
                $str .= "<tr><td class='mainbgbody' >";
                $str .= "<input name='action' type='hidden' value='" . GUILD_TAKE_ENCHANT . "' />";
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $dbe->get("id") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } elseif (isset($_POST["resetgauge"])) // REMETTRE A ZERO UNE JAUGE
{
            if ($curplayer->get("ap") < RESET_GAUGE_AP) {
                $str = "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgbody'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour remettre à zéro l'une de vos jauges.");
                $str .= "</td></tr></table>";
            } elseif ($curplayer->get("money") < RESET_GAUGE_PRICE) {
                $str = "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgbody'>";
                $str .= localize("Vous n'avez pas assez d'or pour remettre à zéro l'une de vos jauges.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth'><tr><td  class='mainbgbody' >";
                $str .= localize("Choisissez la jauge que vous souhaitez remettre à zéro : <br />") . "<select id='Jauge' class='selector cqattackselectorsize' name='CARACT'>";
                /* $dbe=new DBCollection("SELECT U.hp, U.strength, U.dexterity, U.speed, U.magicSkill FROM Upgrade U INNER JOIN Player P ON U.id=P.id_Upgrade WHERE P.id=".$curplayer->get("id"),$db); */
                
                $caract = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une caractéristique --") . "</option>";
                $graffs[] = array(
                    localize("Vie") => 1
                );
                $graffs[] = array(
                    localize("Force") => 2
                );
                $graffs[] = array(
                    localize("Dextérité") => 3
                );
                $graffs[] = array(
                    localize("Vitesse") => 4
                );
                $graffs[] = array(
                    localize("Maitrise de la magie") => 5
                );
                foreach ($graffs as $arr) {
                    foreach ($arr as $key => $value) {
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</td><td class='mainbgbody' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . GUILD_RESET_GAUGE . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        } else {
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
            switch ($type) {
                case 1:
                    // Commandes
                    $str .= "<tr><td class='mainbgtitle'> <b>Passer une commande</b> </td></tr>";
                    
                    if ($Blevel < 2) {
                        $str .= "<tr><td class='mainbgbody'>";
                        $str .= "Le niveau de cette guilde n'est pas suffisant pour réaliser un enchantement.";
                        $str .= "</td></tr>";
                    } else {
                        $str .= "<tr><td class='mainbgbody'>";
                        $str .= "Vous pouvez commander un enchantement pour l'un de vos objets. Le prix est majoré de " . (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), GUILD_ORDER_ENCHANT, $db) - 10) . " %.  Cette guilde ne peut pas assurer plus de " . $Blevel . " commandes simultanément.";
                        $str .= "</td></tr>";
                        
                        $dbe = new DBCollection("SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE extraname!='' AND id_Shop=" . $curplayer->get("inbuilding") . " AND date>NOW()", $db, 0, 0);
                        $dbbe = new DBCollection("SELECT id,name, level FROM Equipment WHERE progress=100 AND state='Carried' AND weared='NO' AND extraname='' AND id_Player=" . $id . " AND id_EquipmentType<23", $db, 0, 0);
                        if ($dbbe->count() == 0)
                            $str .= "<tr><td class='mainbglabel' > Vous n'avez pas d'objet à enchanter. </td></tr>";
                        elseif ($dbe->count() < $Blevel) {
                            
                            $str .= "<tr><td class='mainbgbody'>  ";
                            $str .= " Choisissez l'objet à enchanter : ";
                            
                            $str .= "<select id='target' class='selector cqattackselectorsize' name='ID_EQUIP'>";
                            
                            while (! $dbbe->eof()) {
                                $item[] = array(
                                    $dbbe->get("name") . " niveau " . $dbbe->get("level") . " (" . $dbbe->get("id") . ")" => $dbbe->get("id")
                                );
                                $dbbe->next();
                            }
                            
                            foreach ($item as $arr) {
                                foreach ($arr as $key => $value) {
                                    if ($value == - 1)
                                        $str .= "<optgroup class='group' label='" . $key . "' />";
                                    else
                                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                                }
                            }
                            $str .= "</select></td></tr>";
                            
                            $str .= "<tr><td class='mainbgbody'>  ";
                            $str .= " Choisissez le niveau de l'enchantement : ";
                            $str .= "<select id='target' class='selector' name='ENCHANT_LEVEL'>";
                            
                            for ($i = 1; $i <= floor($Blevel / 2); $i ++) {
                                $str .= "<option value='" . $i . "'>" . $i . "</option>";
                            }
                            
                            $str .= "</select></td></tr>";
                            
                            $dbbt = new DBCollection("SELECT id,name FROM BasicTemplate", $db, 0, 0);
                            $item = array();
                            
                            while (! $dbbt->eof()) {
                                $item[] = array(
                                    $dbbt->get("name") => $dbbt->get("id")
                                );
                                $dbbt->next();
                            }
                            $str .= "<tr><td class='mainbgbody'>  ";
                            $str .= " Choisissez le type d'enchantement : ";
                            $str .= "<select id='target' class='selector'  name='ID_BASIC_ENCHANT'>";
                            
                            foreach ($item as $arr) {
                                foreach ($arr as $key => $value)
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                            $str .= "</select></td></tr>";
                            
                            $str .= "<input name='order' type='hidden' value='create' />\n";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Commander' /></tr></td>";
                        } else
                            $str .= "<tr><td class='mainbglabel' > Le nombre maximum de commandes simultanées est atteint </td></tr>";
                        
                        $str .= "</form>";
                        $str .= "</table>\n";
                        
                        // Commandes en cours
                        
                        $dbe = new DBCollection("SELECT id,name,date,id_Shop,id_Warehouse,extraname FROM Equipment WHERE extraname!='' AND id_Shop=" . $curplayer->get("inbuilding") . " AND date>NOW()", $db, 0, 0);
                        
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td class='mainbgtitle' colspan='3'> <b>Commandes en cours</b> </td></tr>";
                        $str .= "<tr class='mainbglabel'>  <td> Commande </td> <td> Client </td> <td> Date de livraison </td> </tr>";
                        while (! $dbe->eof()) {
                            $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("id_Warehouse"), $db, 0, 0);
                            $str .= "<tr class='mainbgbody'> <td> " . $dbe->get("name") . " <span class='template'>" . $dbe->get("extraname") . "</span></td> <td> " . $dbp->get("name") . " </td> <td> " . $dbe->get("date") . " </td> </tr>";
                            $dbe->next();
                        }
                        
                        // Commandes prêtes
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel'>";
                        $dbe = new DBCollection("SELECT id,name,extraname,date,id_Shop,id_Warehouse FROM Equipment WHERE extraname!='' AND id_Shop=" . $curplayer->get("inbuilding") . " AND date<NOW()", $db, 0, 0);
                        $str .= "<form name='form'  method='POST' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td class='mainbgtitle' colspan='4'> <b>Commandes terminées</b> </td></tr>";
                        $str .= "<tr class='mainbglabel'> <td>&nbsp;</td> <td> Commande </td> <td> Client </td> <td> Date de livraison </td> </tr>";
                        while (! $dbe->eof()) {
                            $dbp = new DBCollection("SELECT id,name FROM Player WHERE id=" . $dbe->get("id_Warehouse"), $db, 0, 0);
                            if ($dbp->get("id") == $id)
                                $enable = "";
                            else
                                $enable = "disabled";
                            $str .= "<tr class='mainbgbody'><td><input name='take' type='radio'" . $enable . " value='" . $dbe->get('id') . "' /></td> <td> " . $dbe->get("name") . " <span class='template'>" . $dbe->get("extraname") . "</span> </td> <td> " . $dbp->get("name") . " </td> <td> " . $dbe->get("date") . " </td> </tr>";
                            $dbe->next();
                        }
                        
                        if ($dbe->count() > 0) {
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Prendre cet objet' /></tr></td>";
                        }
                    }
                    break;
                
                case 2:
                    if ($curplayer->get("level") > 5) {
                        $str = "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgbody'>";
                        $str .= localize("Vous êtes de niveau trop élevé pour remettre à zéro l'une de vos jauges. Il va falloir se rééquilibrer à la dure.<br/><br/> Ceci n'est disponible que pour les personnages de niveau 5 ou moins et nécessite 30 pièces d'or.");
                        $str .= "</td></tr></table>";
                    } else {
                        $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgbody'>" . localize('L\'enchanteur vous propose, contre la modique somme de 30 pièces d\'or et 10 Points d\'action (PA), de remettre à zéro l\'une de vos jauges.') . "<br /><br /><input name='resetgauge' type='submit' value='Rejauger'></td></tr>";
                    }
                    break;
            }
        }
        $str .= "</table>\n";
        $str .= "</form>";
        
        return $str;
    }
}

?>