<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
class CQBedroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQBedroom($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        $idBuilding = $curplayer->get("inbuilding");
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbb->get("id_City"), $db);
        $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Prêtre' AND id_City=" . $dbc->get("id"), $db);
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('C H A M B R E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if ($dbb->get("id_Player") == $this->curplayer->get("id") || ($dbc->get("captured") > 3 && $dbp->eof())) {
            $str .= "<tr><td> Caisse de la chambre : " . $dbb->get("money") . " pièce(s) d'or</td></tr>";
        }
        $str .= "</table>\n";
        if ($dbb->get("id_Player") == $this->curplayer->get("id") || ($dbc->get("captured") > 3 and $dbp->eof())) {
            if (isset($_POST['bedroomchoice'])) {
                switch ($_POST['bedroomchoice']) {
                    case "withdraw":
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr><td class='mainbgbody'> Retirer : <input type='textbox' name='withdraw' />" . " " . localize("PO");
                        $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Valider' /></td></tr>";
                        $str .= "<input name='action' type='hidden' value='" . BEDROOM_WITHDRAW . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</table></form>";
                        break;
                    
                    case "deposit":
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr><td class='mainbgbody'> Déposer : <input type='textbox' name='deposit' />" . " " . localize("PO");
                        $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Valider' /></td></tr>";
                        $str .= "<input name='action' type='hidden' value='" . BEDROOM_DEPOSIT . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</table></form>";
                        break;
                    
                    case "sleep":
                        if ($curplayer->get("ap") < SLEEPING_AP) {
                            $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour vous cette Action.");
                            $str .= "</td></tr></table>";
                        } else {
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td class='mainbglabel'>Dormir pour " . SLEEPING_AP . " PA ?";
                            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                            $str .= "<input name='action' type='hidden' value='" . BEDROOM_SLEEPING . "' />";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "</table></form>";
                        }
                        
                        break;
                }
            } else {
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='mainbglabel'>Dans cette salle, vous pouvez dormir, et utiliser votre caisse. Que souhaitez-vous faire ? </td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='bedroomchoice' value='deposit' id='deposit' />" .
                     "<label for='deposit'> Déposer de l'argent dans la caisse.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='bedroomchoice' value='withdraw' id='withdraw' />" .
                     "<label for='withdraw'> Retirer de l'argent de la caisse.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='bedroomchoice' value='sleep' id='sleep' />" .
                     "<label for='sleep'>Vous pouvez dormir dans cette chambre, cela vous permettra de vous régénérer. (" . SLEEPING_AP . " PA)</label></td></tr>";
                $str .= "<tr><td><input type='submit' name='ok' value='Ok'/> </td></tr>";
                $str .= "</table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
