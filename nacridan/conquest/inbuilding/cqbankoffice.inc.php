<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQBankoffice extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQBankoffice($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $money = $curplayer->get("money");
        $err = $this->err;
        $str = "";
        
        $dbB = new DBCollection("SELECT level,id_City FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
        $dbC = new DBCollection("SELECT id,id_Player FROM City WHERE id=" . $dbB->get("id_City"), $db, 0, 0);
        
        $Blevel = $dbB->get("level");
        
        $depositPrice = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), BANK_DEPOSIT, $db);
        $depositProfit = CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), BANK_DEPOSIT, $db);
        // $withdrawPrice = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"),BANK_WITHDRAW,$db);
        $transfertPrice = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), BANK_TRANSFERT, $db);
        $transfertPrice += CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), BANK_TRANSFERT, $db);
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        else
            $type = 2;
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('B A N Q U E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td>";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Transfert') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Dépot') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=view2d&type=3' class='tabmenu'>" . localize('Retrait') . "</a> |\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbgtitle'>\n";
        $Reserve = $this->curplayer->get("moneyBank");
        $str .= localize("Votre solde est de <b> {argent} </b>PO.", array(
            "argent" => $Reserve
        ));
        $str .= "</td></tr></table>\n";
        $str .= "<table class='maintable insidebuildingleftwidth'>";
        
        if (isset($_POST["deposit"])) // DEPOT
{
            if ($_POST["deposit"] > $money) {
                $str .= "<tr><td class='mainbgbody'> Vous ne pouvez déposer en banque plus d'or que vous n'en possédez.</td></tr>";
            } else {
                $finalDeposit = floor($_POST["deposit"] * (100 - $depositPrice) / 100);
                // $finalTaxe = floor($_POST["deposit"]*$depositPrice/100);
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous vous apprêtez à déposer " . $_POST["deposit"] . " sur votre compte bancaire (pour 0 PA).</td></tr>";
                $str .= "<tr><td class='mainbgbody' >";
                $str .= "Seuls " . $finalDeposit . " PO arriveront effectivement sur votre compte, le reste est prélevé par la banque.";
                $str .= "</tr></td>";
                $str .= "<tr><td class='mainbgbody' >";
                // if($dbC->get("id_Player")!=0)
                // $str.="De plus ".$finalTaxe." PO arriveront sur le compte du Palais du Gouverneur. Cette somme est une taxe payée par la banque, elle n'est pas prélevée sur votre compte.";
                $str .= "</tr></td>";
                $str .= "<input name='action' type='hidden' value='" . BANK_DEPOSIT . "' />";
                $str .= "<input name='MONEYDEP' type='hidden' value='" . $_POST["deposit"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } elseif (isset($_POST["withdraw"])) // RETRAIT
{
            if ($_POST["withdraw"] > $Reserve) {
                $str .= "<tr><td class='mainbgbody'> Vous ne pouvez retirer plus d'or que vous n'en possédez en banque.</td></tr>";
            } else {
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous vous apprêtez à retirer " . $_POST["withdraw"] . " de votre compte bancaire (pour 0 PA).</td></tr>";
                $str .= "<tr><td class='mainbgbody' >";
                $str .= "<input name='action' type='hidden' value='" . BANK_WITHDRAW . "' />";
                $str .= "<input name='MONEY' type='hidden' value='" . $_POST["withdraw"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } elseif (isset($_POST["transfert"])) {
            
            $arr = unserialize(stripslashes($_POST["TARGET_ID"]));
            if ($_POST["transfert"] > $Reserve) {
                $str .= "<tr><td class='mainbgbody'> Vous ne pouvez transférer plus d'or que vous n'en possédez sur votre compte bancaire.</td></tr>";
            }  /*
               * elseif($arr["type"] == 2 and $arr["id"] == $dbC->get("id"))
               * {
               * $str.="<tr><td class='mainbgbody'> Vous ne pouvez pas faire un transfert vers la ville où vous vous trouvez. Vous êtes sûr place, allez directement au palais du gouverneur pour faire un versement.</td></tr>";
               * }
               */
else {
                $name = "";
                
                if ($arr["type"] == 1 and is_numeric($arr["id"])) {
                    $dbplayer = new DBCollection("SELECT id,name FROM Player WHERE id=" . $arr["id"], $db, 0, 0);
                    $name .= " à <a href='../conquest/profile.php?id=" . $dbplayer->get("id") . "' class='stylepc popupify'>" . $dbplayer->get("name") . "</a>";
                }
                if ($arr["type"] == 2 and is_numeric($arr["id"])) {
                    $dbcity = new DBCollection("SELECT id,name,x,y,map FROM City WHERE id=" . $arr["id"], $db, 0, 0);
                    $name .= "au village <a href='../conquest/profilecity.php?id=" . $dbcity->get("id") . "' class='stylepc popupify'>" . $dbcity->get("name") . "</a>";
                }
                
                $finalCost = floor($_POST["transfert"] + $_POST["transfert"] * $transfertPrice / 100);
                // $finalTaxe = floor($_POST["transfert"]*$percentTaxe/100);
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbgbody'> Vous vous apprêtez à transférer " . $_POST["transfert"] . " PO de votre compte bancaire " . $name . " (pour 0 PA).</td></tr>";
                $str .= "<tr><td class='mainbgbody' >";
                $str .= "Cela vous coûtera au total " . $finalCost . " PO.";
                $str .= "</tr></td>";
                $str .= "<tr><td class='mainbgbody' >";
                // if($dbC->get("id_Player")!=0)
                // $str.="De plus ".$finalTaxe." PO arriveront sur le compte du Palais du Gouverneur. Cette somme est une taxe payée par la banque, elle n'est pas prélevée sur votre compte.";
                $str .= "</tr></td>";
                $str .= "<input name='action' type='hidden' value='" . BANK_TRANSFERT . "' />";
                $str .= "<input name='MONEY' type='hidden' value='" . $_POST["transfert"] . "' />";
                $str .= "<input name='TARGET_ID' type='hidden' value='" . $arr["id"] . "' />";
                $str .= "<input name='TYPE' type='hidden' value='" . $arr["type"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
            }
        } else {
            $str .= "<form name='form'  method='POST' target='_self'>";
            switch ($type) {
                case 1:
                    // transfert
                    
                    $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                    $str .= localize("Transfert de") . " <input type='textbox' name='transfert' /> PO";
                    $str .= "</td></tr>";
                    $str .= "<tr><td class='mainbgtitle'> à ";
                    $str .= "<select id='target' class='selector cqattackselectorsize' name='TARGET_ID'>";
                    $dbp = new DBCollection("SELECT id_Player\$friend,name FROM MailContact LEFT JOIN Player ON id_Player\$friend=Player.id WHERE status='PC' AND id_Player=" . $curplayer->get("id"), $db, 0, 0);
                    $dbCT = new DBCollection("SELECT id,name,x,y,map,id_Player FROM City WHERE id_Player!=0", $db, 0, 0);
                    
                    $item[] = array(
                        localize("-- Les Contacts Enregistrés --") => - 1
                    );
                    $item2[] = array(
                        localize("-- Les Villages Protégés --") => - 1
                    );
                    while (! $dbp->eof()) {
                        $item[] = array(
                            $dbp->get("name") => $dbp->get("id_Player\$friend")
                        );
                        $dbp->next();
                    }
                    while (! $dbCT->eof()) {
                        $map = "";
                        $item2[] = array(
                            $dbCT->get("name") . ", (X=" . $dbCT->get("x") . "/Y=" . $dbCT->get("y") . ")" => $dbCT->get("id")
                        );
                        $dbCT->next();
                    }
                    
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value) {
                            if ($value == - 1)
                                $str .= "<optgroup class='group' label='" . $key . "' />";
                            else
                                $str .= "<option value='" . serialize(array(
                                    "type" => 1,
                                    "id" => $value
                                )) . "'>" . $key . "</option>";
                        }
                    }
                    
                    foreach ($item2 as $arr) {
                        foreach ($arr as $key => $value) {
                            if ($value == - 1)
                                $str .= "<optgroup class='group' label='" . $key . "' />";
                            else
                                $str .= "<option value='" . serialize(array(
                                    "type" => 2,
                                    "id" => $value
                                )) . "'>" . $key . "</option>";
                        }
                    }
                    $str .= "</select></td></tr>";
                    
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td class='mainbgbody' >";
                    $str .= "Cela vous coûtera " . $transfertPrice . " % de la somme en plus, prélevé par la banque.";
                    $str .= "</tr></td>";
                    $str .= "<tr><td class='mainbgbody' >";
                    // if($dbC->get("id_Player")!=0)
                    // $str.="De plus ".$percentTaxe." % de la somme arrivera sur le compte du Palais du Gouverneur. Cette somme est une taxe payée par la banque, elle n'est pas prélevée sur votre compte.";
                    $str .= "</tr></td>";
                    $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Action' /></tr></td>";
                    $str .= "</table>\n";
                    
                    break;
                
                case 2:
                    
                    // Dépot
                    
                    $str .= "<tr><td class='mainbgtitle' >";
                    $str .= localize("Déposer :") . " <input type='textbox' name='deposit' />" . " " . localize("PO");
                    
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</td></tr>";
                    $str .= "<tr><td class='mainbgbody' >";
                    $str .= "Seul " . (100 - $depositPrice) . " % de la somme arrivera effectivement sur votre compte, le reste est prélevé par la banque.";
                    $str .= "</tr></td>";
                    
                    // if($dbC->get("id_Player")!=0)
                    // $str.="<tr><td class='mainbgbody' > De plus ".$percentTaxe." % de la somme arrivera sur le compte du Palais du Gouverneur. Cette somme est une taxe payée par la banque, elle n'est pas prélevée sur votre compte.";
                    
                    $str .= "<tr><td class='mainbgbody' >";
                    $str .= "L'argent sur votre compte vous rapporte 1% d'intérêt par mois";
                    $str .= "</tr></td>";
                    $str .= "<tr><td> <input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                    
                    break;
                
                case 3:
                    // Retrait
                    
                    $str .= "<tr class='mainbgtitle' ><td >";
                    $str .= localize("Retirer :") . " <input type='textbox' name='withdraw' />" . " " . localize("PO");
                    $str .= "</td>";
                    
                    $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "</td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    
                    break;
            }
        }
        $str .= "</table>\n";
        $str .= "</form>";
        
        return $str;
    }
}
?>
