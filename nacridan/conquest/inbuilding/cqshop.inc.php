<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQShop extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQShop($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('B O U T I Q U E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        elseif (isset($_POST["type"]))
            $type = $_POST["type"];
        else
            $type = 1;
        
        if (isset($_POST["Buy"]) && isset($_POST["idequip"])) {
            require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
            $dbe = new DBCollection("SELECT id,id_EquipmentType, name,extraname  FROM Equipment WHERE id=" . quote_smart($_POST["idequip"]), $db);
            
            // $dbpe=new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND state='Carried' AND weared='No' AND id_Player=".$id,$db);
            
            if ($curplayer->get("money") < EquipFactory::getPriceEquipment($dbe->get("id"), $db))
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez d'or pour acheter cet objet.</td></tr>";
            elseif (PlayerFactory::checkingInvFullForObject($curplayer, $dbe->get("id_EquipmentType"), $param, $db) == - 1)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez de place dans vos sacs.</td></tr>";
            
            else {
                $price = EquipFactory::getPriceEquipment($dbe->get("id"), $db);
                $price += floor(
                    EquipFactory::getPriceEquipment($dbe->get("id"), $db) * (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), MAIN_SHOP_BUY, $db) - 10) / 100);
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='mainbglabel'>Vous allez acheter un(e) <b> " . $dbe->get("name") . " " . $dbe->get("extraname") . " </b>pour " . $price . " PO.";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                $str .= "<input name='action' type='hidden' value='" . MAIN_SHOP_BUY . "' />";
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $dbe->get("id") . "' />";
                $str .= "<input name='type' type='hidden' value='" . $type . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
            }
        } else {
            // Affichage des objets en vente
            switch ($type) {
                case 1:
                    $cond = "(EquipmentType.id<8 OR EquipmentType.id in (28,21,22))";
                    break;
                case 2:
                    $cond = "(EquipmentType.id>7 AND EquipmentType.id<28 AND EquipmentType.id not in (21,22))";
                    break;
                case 3:
                    $cond = "(EquipmentType.id>39)";
                    break;
                case 4:
                    $cond = "(EquipmentType.id=29)";
                    break;
                case 5:
                    $cond = "(EquipmentType.id=30 OR EquipmentType.id=31 OR EquipmentType.id=32)";
                    break;
            }
            
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $template = new Template();
            $tmodifier = new Modifier();
            $mission = new Mission();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                     $mission->getASRenamer("Mission", "MISS") .
                     ",mask,wearable,frequency,BasicEquipment.durability AS dur   FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE " .
                     $cond . " AND id_Shop=" . $curplayer->get("inbuilding") . " order by EQname", $db);
            
            $array = array();
            
            while (! $dbe->eof()) {
                
                $eq->DBLoad($dbe, "EQ");
                if ($dbe->get("EQMid") != 0) {
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    
                    $checkbox = "<input type='radio' name='idequip' value='" . $eq->get("id") . "'>";
                    $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                    
                    if ($eq->get("extraname") != "") {
                        $i = 0;
                        $tmodifier = new Modifier();
                        $template = new Template();
                        $dbt = new DBCollection(
                            "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                                 $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                 " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                 $eq->get("id") . " order by Template.pos asc", $db);
                        
                        while (! $dbt->eof()) {
                            $i ++;
                            $template->DBLoad($dbt, "TP");
                            $tmodifier->DBLoad($dbt, "MD");
                            $tmodifier->updateFromTemplateLevel($template->get("level"));
                            $eqmodifier->addModifObj($tmodifier);
                            
                            if ($i == 1)
                                $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                            if ($i == 2)
                                $extraname .= $dbt->get("name2") . "(" . $template->get("level") . ")";
                            
                            $dbt->next();
                        }
                        $name .= " " . "<span class='template'>" . $extraname . "</span>";
                    }
                    $eqmodifier->initCharacStr();
                }
                
                $name .= "<br/>";
                $name .= localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")  <br/>";
                
                // Affichages des bonus de caractéristique pour les équipements
                if ($eq->get("id_EquipmentType") < 30) {
                    $modstr = "";
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0")
                            $modstr .= translateAttShort($key) . " : " . $tmp . " | ";
                    }
                    $name .= "(" . substr($modstr, 0, - 3) . ")";
                }
                
                $price = EquipFactory::getPriceEquipment($eq->get("id"), $db);
                $price += floor(
                    EquipFactory::getPriceEquipment($eq->get("id"), $db) * (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), MAIN_SHOP_BUY, $db) - 10) / 100);
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            
            // ------------- Affichage -------------------
            
            $target = "/conquest/conquest.php?center=view2d&type=" . $type;
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "Nous rappelons à notre aimable clientèle que les listes de ventes privées de tous les aventuriers de l'île sont affichées dans toutes les auberges.";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Armes') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Armures') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=3' class='tabmenu'>" . localize('Outils') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=4' class='tabmenu'>" . localize('Potions') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=5' class='tabmenu'>" . localize('Matières Premières') . "</a> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            switch ($type) {
                case 1:
                    $str .= localize("Les Armes ");
                    break;
                case 2:
                    $str .= localize("Les Armures ");
                    break;
                case 3:
                    $str .= localize("Les Outils ");
                    break;
                case 4:
                    $str .= localize("Objets Divers ");
                    break;
                case 5:
                    $str .= localize("Matières Premières");
                    break;
            }
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= createTable(3, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='60%' align='left'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix"),
                        "class='mainbglabel' width='15%' align='right'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
            
            $str .= "<tr><td class='mainbgtitle'><input id='Buy' type='submit' name='Buy' value='" . localize("Acheter") . "' /></td></tr>";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
