<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/BasicEquipment.inc.php");

class CQEnchant extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQEnchant($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        
        if (isset($_POST["Enchant"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdEnchant"]) && isset($_POST["IdEquip"]))
                    EquipFactory::enchantEquipment($this->curplayer, quote_smart($_POST["IdEquip"]), quote_smart($_POST["IdEnchant"]), min(min(quote_smart($_POST["Level"]), $this->nacridan->getCity()->get("level") + 1), 10), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un enchantement ET un équipement valide.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        if (isset($_POST["Disenchant"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdEquipDis"]))
                    EquipFactory::disenchantEquipment($this->curplayer, quote_smart($_POST["IdEquipDis"]), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un équipement à désenchanter.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        $eqmodifier = new Modifier();
        
        if ($err == "" && $str == "") {
            
            if (isset($_GET["level"])) {
                $selectedlevel = min(quote_smart($_GET["level"]), $this->nacridan->getCity()->get("level") + 1);
            } else {
                $selectedlevel = 1;
            }
            $selectedlevel = max(1, $selectedlevel);
            
            $dbe = new DBCollection("SELECT BasicTemplate.*," . $eqmodifier->getASRenamer("Modifier_BasicTemplate", "MD") . " FROM BasicTemplate,Modifier_BasicTemplate WHERE BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id ORDER BY rank ASC", $db);
            
            $target = "/conquest/conquest.php?center=enchant";
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'><b>" . localize('E N C H A N T E U R') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= localize("Les Enchantements ");
            
            if ($this->nacridan->getCity()->get("level") != 0) {
                $str .= "<select name=url onchange=window.location=this.options[selectedIndex].value>";
                for ($i = 1; $i < min($this->nacridan->getCity()->get("level") + 1, 11); $i ++) {
                    if ($i == $selectedlevel)
                        $extra = "selected";
                    else
                        $extra = "";
                    
                    $str .= "<option value='../conquest/conquest.php?center=enchant&level=" . $i . "' " . $extra . ">" . localize('Niveau') . " " . $i . "</option>\n";
                }
                $str .= "</select>";
            } else {
                $str .= " : (" . localize("non disponible dans cette ville") . ")";
                $str .= "</table>";
                $str .= "</form>\n";
                return $str;
            }
            $str .= "</td></tr>\n";
            $str .= "</table>\n";
            $array = array();
            while (! $dbe->eof()) {
                $checkbox = "<input type='radio' name='IdEnchant' value='" . $dbe->get("id") . "'>";
                $name = "<b>" . localize($dbe->get("name")) . "</b> - ";
                
                $eqmodifier->DBLoad($dbe, "MD");
                $eqmodifier->updateFromTemplateLevel($selectedlevel);
                $eqmodifier->initCharacStr();
                $modstr = "";
                foreach ($eqmodifier->m_characLabel as $key) {
                    $tmp = $eqmodifier->getModifStr($key);
                    if ($tmp != "0") {
                        $modstr .= "" . localize($key) . ": " . $tmp .= " | ";
                    }
                }
                
                $name .= "(" . substr($modstr, 0, - 3) . ")";
                
                $price = floor((1000 * ($selectedlevel) * ($selectedlevel)) / $dbe->get("frequency")) . " " . localize("PO");
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            $str .= "</table>\n";
            
            $str .= createTable(3, $array, array(), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("Nom de l'enchantement"),
                    "class='mainbglabel' width='70%' align='left'"
                ),
                array(
                    localize("Prix"),
                    "class='mainbglabel' width='25%' align='right'"
                )
            ), "class='maintable centerareawidth'");
            
            $str .= "<table class='maintable' ><tr><td class='mainbgtitle' width='510px'>";
            
            $str .= localize("Equipement à enchanter :") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id='Object' class='selector cqattackselectorsize' name='IdEquip'>";
            
            $gap = 0;
            $dbp = new DBCollection("SELECT Equipment.id,Equipment.name,Equipment.level FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE wearable=\"Yes\" AND extraname='' AND id_EquipmentType != '7' AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisir un équipement --") . "</option>";
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "") {
                    $item[] = array(
                        localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td><td>";
            $str .= "<input id='Enchant' type='submit' name='Enchant' value='" . localize("Enchanter") . "' />";
            $str .= "</td></tr>";
            $str .= "<tr><td>&nbsp;</td></tr>";
            $str .= "<tr><td class='mainbgtitle'>";
            
            $str .= localize("Equipement à désenchanter :") . " <select id='Object' class='selector cqattackselectorsize' name='IdEquipDis'>";
            
            $gap = 0;
            $dbp = new DBCollection("SELECT Equipment.id,Equipment.name,Equipment.level FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE wearable=\"Yes\" AND extraname!='' AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisir un équipement --") . "</option>";
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "") {
                    $item[] = array(
                        localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select> (coût : 500 PO)</td><td>";
            $str .= "<input id='Disenchant' type='submit' name='Disenchant' value='" . localize("Désenchanter") . "' />";
            $str .= "<input type='hidden' name='Level' value='" . $selectedlevel . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr>";
            $str .= "</table>";
            $str .= "</form>\n";
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=enchant' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
