<?php

/**
 *Définit la classe CQMenuCity
 *
 * La fonction <b>toString</b> affiche le menu à l'intérieur des villes.
 *
 *
 *@author Nacridan
 *@version 1.0
 *@package NacridanV1
 *@subpackage Conquest
 */
class CQMenuCity extends HTMLObject
{

    public $nacridan;

    public function CQMenuCity($nacridan, $db)
    {
        $this->nacridan = $nacridan;
        $this->db = $db;
    }

    public function toString()
    {
        $sess = $this->nacridan->sess;
        $id = $sess->get("cq_playerid");
        
        $str = "<div class='menu'>";
        $str .= "<dl class='menuright'>";
        $curplayer = $this->nacridan->loadCurSessPlayer($this->db);
        $limited = $curplayer->get("status") == 'NPC' ? 1 : 0;
        
        if ($sess->has("DLA" . $id)) {
            if (! $limited) {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=enhance'>" . localize("S'améliorer ({ap} PA)", array(
                    "ap" => 0
                )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=changeenhance'>" . localize("Changer ses améliorations ({ap} PA)", array(
                    "ap" => 0
                )) . "</a></li>\n";
                
                // CODE DE LA VERSION TEST
                /*
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=addAP&".getGoogleConquestKeywords()."'>".localize("Ajouter des PA.")."</a></li>\n";
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=addXP&".getGoogleConquestKeywords()."'>".localize("Ajouter des PX.")."</a></li>\n";
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=addMoney&".getGoogleConquestKeywords()."'>".localize("Ajouter des PO.")."</a></li>\n";
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=instantTP&".getGoogleConquestKeywords()."'>".localize("Téléportation.")."</a></li>\n";
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=addATTbonus&".getGoogleConquestKeywords()."'>".localize("Bonus en Attaque")."</a></li>\n";
                 * $str.="<li><a href='../conquest/conquest.php?center=view&bottom=addDEGbonus&".getGoogleConquestKeywords()."'>".localize("Bonus en Dégât")."</a></li>\n";
                 */
                // FIN DU CODE DE LA VERSION TEST
                
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            }
            
            $str .= "<dt onClick=\"showSubMenu('smenu2');\">" . localize('Objets') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu2'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=wear'>" . localize("S'équiper ({ap} PA)", array(
                "ap" => 0
            )) . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=unwear'>" . localize("Se déséquiper ({ap} PA)", array(
                "ap" => 0
            )) . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=store'>" . localize("Ranger un objet ({ap} PA)", array(
                "ap" => STORE_OBJECT_AP
            )) . "</a></li>\n";
            $str .= "</ul>\n";
            $str .= "</dd>\n";
        }
        /*
         * $str.="<dt onClick=\"showSubMenu('smenu4');\">".localize("Dons/DLA")."</dt>\n";
         * $str.="<dd style='display: none;' id='smenu4'>\n";
         * $str.="<ul>\n";
         * $str.="<li><a href='../conquest/conquest.php?bottom=atbshift'>".localize("Décaler sa DLA ({ap} PA)",array ("ap" => SHIFT_ATB_AP))."</a></li>\n";
         * $str.="</ul>\n";
         * $str.="</dd>\n";
         */
        $str .= "<dt onClick=\"showSubMenu('smenu5');\">" . localize('Boutique(s)') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu5'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=arms'>" . localize("Échoppe") . "</a></li>\n";
        // $str.="<li><a href='../conquest/conquest.php?center=enchant'>".localize("Enchanteur")."</a></li>\n";
        /*
         * $dbb=new DBCollection("SELECT id,id_Player FROM Building WHERE map=".$curplayer->get("map")." AND x=".$curplayer->get("x")." AND y=".$curplayer->get("y"),$this->db);
         * if($dbb->count()>0) // est-ce qu'on est sur un village fortifié
         * {
         * $dbg=new DBCollection("SELECT id_Team FROM Player WHERE id=".$dbb->get("id_Player"),$this->db);
         * $dbh=new DBCollection("SELECT id_TeamRankInfo FROM TeamRank WHERE id_Player=".$curplayer->get("id"),$this->db);
         *
         * if($dbb->get("id_Player")==$curplayer->get("id") OR ($dbg->get("id_Team")==$curplayer->get("id_Team") AND $dbg->get("id_Team")!= 0 AND ($dbh->get("id_TeamRankInfo")-1)%20 < 16))
         * {
         * $str.="<li><a href='../conquest/conquest.php?center=factory'>".localize("Guilde des artisans")."</a></li>\n";
         * $str.="<li><a href='../conquest/conquest.php?center=warehouse'>".localize("Entrepôt")."</a></li>\n";
         * $str.="<li><a href='../conquest/conquest.php?center=hireguard'>".localize("Engager un garde")."</a></li>\n";
         * $str.="<li><a href='../conquest/conquest.php?center=plant'>".localize("Planter un buisson")."</a></li>\n";
         * $str.="<li><a href='../conquest/conquest.php?center=prospect'>".localize("Prospecter un gisement")."</a></li>\n";
         * }
         *
         * }
         */
        $str .= "<li><a href='../conquest/conquest.php?center=hostel'>" . localize("Auberge") . "</a></li>\n";
        // $str.="<li><a href='../conquest/conquest.php?center=alchemist'>".localize("Alchimiste")."</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=magicschool'>" . localize("École de magie") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=abilityschool'>" . localize("École des compétences") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=bank'>" . localize("Banque") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=sale'>" . localize("Vendre ses objets") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=repair'>" . localize("Faire réparer ses objets") . "</a></li>\n";
        
        // $str.="<li><a href='../conquest/conquest.php?center=transport'>".localize("Transport")."</a></li>\n";
        // $str.="<li><a href='../conquest/conquest.php?center=respawn'>".localize("Temple de résurrection")."</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=map&city=1&bottom=teleport'>" . localize("Temple de la téléportation ({ap} PA)", array(
            "ap" => 8
        )) . "</a></li>\n";
        
        $str .= "<li><a href='../conquest/conquest.php?center=map&city=1&bottom=arena'>" . localize("Arène ({ap} PA)", array(
            "ap" => 4
        )) . "</a></li>\n";
        
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        /*
         * if($sess->has("DLA".$id))
         * {
         * $str.="<dt style='background: #BB8F81;' onClick=\"showSubMenu('smenu6');\">".localize("Lieu")."</dt>\n";
         * $str.="<dd style='display: none;' id='smenu6'>\n";
         * $str.="<ul>\n";
         * $str.="<li><a href='../conquest/conquest.php?bottom=leavecity'>".localize("Sortir du lieu ({ap} PA)",array ("ap" => 1))."</a></li>\n";
         * $str.="</ul>\n";
         * $str.="</dd>\n";
         * }
         */
        $str .= "</dl>\n";
        $str .= "</div>\n";
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }
}
?>
