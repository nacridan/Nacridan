<?php

class CQAbility extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $id;

    public function CQAbility($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_GET["id"])) {
            $this->id = $id = quote_smart($_GET["id"]);
            $this->str = $this->getForm();
        } else {
            $this->str = "";
        }
    }

    protected function getArmSharpen()
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $item = array();
        $str = "<select class='selector cqattackselectorsize' name='OBJECT_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une arme --") . "</option>";
        $dbe = new DBCollection("SELECT * FROM Equipment WHERE id_EquipmentType < 5 and id_BasicEquipment <> 205 AND durability!=0 AND  id_Player=" . $id, $this->db, 0, 0);
        while (! $dbe->eof()) {
            $item[] = array(
                localize($dbe->get("name")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
            );
            $dbe->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value)
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
        }
        $str .= "</select>";
        return $str;
    }

    protected function getBuilding($gap = 1)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        $inbuilding = $this->curplayer->get("inbuilding");
        
        $item = array();
        $str = "<select class='selector cqattackselectorsize' name='BUILDING_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un batiment --") . "</option>";
        if ($inbuilding && $this->curplayer->get("room") != TOWER_ROOM)
            $dbe = new DBCollection("SELECT * FROM Building WHERE id=" . $inbuilding, $this->db, 0, 0);
        else
            $dbe = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding!=14 AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap, 
                $this->db, 0, 0);
        while (! $dbe->eof()) {
            $item[] = array(
                localize($dbe->get("name")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
            );
            $dbe->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value)
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
        }
        $str .= "</select>";
        return $str;
    }

    protected function getArrow($name = "ARROW_ID")
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $item = array();
        $str = "<select class='selector cqattackselectorsize' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une flèche --") . "</option>";
        require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
        $id_Bag = playerFactory::getIdBagByNum($id, 5, $this->db);
        $dbe = new DBCollection("SELECT * FROM Equipment WHERE  id_EquipmentType=28 AND id_Equipment\$bag=0 AND id_Player=" . $id, $this->db, 0, 0);
        
        $item[] = array(
            localize("-- Inventaire -- ") => - 1
        );
        while (! $dbe->eof()) {
            $item[] = array(
                localize($dbe->get("name") . " niveau " . $dbe->get("level")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
            );
            $dbe->next();
        }
        
        if ($id_Bag != 0) {
            $dbe = new DBCollection("SELECT * FROM Equipment WHERE  id_EquipmentType=28 AND id_Equipment\$bag=" . $id_Bag . " AND id_Player=" . $id, $this->db, 0, 0);
            $item[] = array(
                localize("-- Carquois -- ") => - 1
            );
            while (! $dbe->eof()) {
                $item[] = array(
                    localize($dbe->get("name") . " niveau " . $dbe->get("level")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                );
                $dbe->next();
            }
        }
        
        if (constant($this->id) == ABILITY_FLYARROW && $name == "ARROW3_ID") {
            $item[] = array(
                localize("-- Ne pas tirer cette flèche") => - 1
            );
            $item[] = array(
                localize("Ignorer ce champs") => 0
            );
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getSelector($myself = 0, $pj = 1, $pnj = 1, $gap = 1, $name = "TARGET_ID")
    {
        $id = $this->curplayer->get("id");
        $curplayer = $this->curplayer;
        $inbuilding = $this->curplayer->get("inbuilding");
        $room = $this->curplayer->get("room");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        $db = $this->db;
        $action = constant($this->id);
        
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $inbuilding, $this->db);
        
        $item = array();
        
        $str = "<select class='selector cqattackselectorsize' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
        
        $dpnpc = array();
        $dbp = array();
        
        for ($i = 0; $i < $gap + 1; $i ++) {
            // PNJ
            $dbnpc[] = new DBCollection(
                "SELECT * FROM Player WHERE status='NPC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                     " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i, $db, 0, 0);
            // PJ
            $dbp[] = new DBCollection(
                "SELECT * FROM Player WHERE status='PC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                     " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i, $db, 0, 0);
        }
        
        if ($room == TOWER_ROOM) {
            if ($gap == 1)
                $gap = 0;
            for ($i = 0; $i < $gap + 1; $i ++) {
                $cond = "";
                
                if ($i > 1 && ($action == ABILITY_FLYARROW || $action == ABILITY_DISABLING || $action == ABILITY_NEGATION))
                    $cond = (10 * ($i - 1)) . " % malus attaque";
                
                if ($i > 2 && $action == ABILITY_FAR)
                    $cond = (10 * ($i - 2)) . " % malus attaque";
                
                if ($pnj) {
                    if ($i == 0 && ! $dbnpc[0]->eof())
                        $item[] = array(
                            localize("-- Créature à portée -- ") => - 1
                        );
                    elseif (! $dbnpc[$i]->eof())
                        $item[] = array(
                            localize("-- Créature à " . $i . " case(s) -- " . $cond) => - 1
                        );
                    
                    while (! $dbnpc[$i]->eof()) {
                        if (($dbnpc[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                            );
                        $dbnpc[$i]->next();
                    }
                }
                if ($pj) {
                    if ($i == 0 && ! $dbp[0]->eof())
                        $item[] = array(
                            localize("-- Personnage à portée -- ") => - 1
                        );
                    elseif (! $dbp[$i]->eof()) {
                        
                        $item[] = array(
                            localize("-- Personnage à " . $i . " case(s) -- " . $cond) => - 1
                        );
                    }
                    while (! $dbp[$i]->eof()) {
                        if (($dbp[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                            );
                        $dbp[$i]->next();
                    }
                }
            }
        } elseif ($inbuilding) {
            if ($pnj) {
                $item[] = array(
                    localize("-- Créature à portée -- ") => - 1
                );
                while (! $dbnpc[0]->eof()) {
                    if (($dbnpc[0]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[0]->get("id"), $db))
                        $item[] = array(
                            localize($dbnpc[0]->get("racename")) . " (" . $dbnpc[0]->get("id") . ")" => $dbnpc[0]->get("id")
                        );
                    $dbnpc[0]->next();
                }
            }
            if ($pj) {
                $item[] = array(
                    localize("-- Personnage à portée -- ") => - 1
                );
                while (! $dbp[0]->eof()) {
                    if (($dbp[0]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp[0]->get("id"), $db))
                        $item[] = array(
                            localize($dbp[0]->get("name")) . " (" . $dbp[0]->get("id") . ")" => $dbp[0]->get("id")
                        );
                    $dbp[0]->next();
                }
            }
        } else {
            for ($i = 1; $i < $gap + 1; $i ++) {
                $cond = "";
                if ($i > 1 && ($action == ABILITY_FLYARROW || $action == ABILITY_DISABLING || $action == ABILITY_NEGATION))
                    $cond = (10 * ($i - 1)) . " % malus attaque";
                if ($i > 2 && $action == ABILITY_FAR)
                    $cond = (10 * ($i - 2)) . " % malus attaque";
                
                if ($pnj) {
                    if ($i == 1 && ! $dbnpc[1]->eof())
                        $item[] = array(
                            localize("-- Créature à portée -- ") => - 1
                        );
                    elseif (! $dbnpc[$i]->eof())
                        $item[] = array(
                            localize("-- Créature à " . $i . " case(s) -- " . $cond) => - 1
                        );
                    while (! $dbnpc[$i]->eof()) {
                        if (($dbnpc[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                            );
                        $dbnpc[$i]->next();
                    }
                }
                if ($pj) {
                    if (($i == 1 && ! $dbnpc[1]->eof()) || $myself) {
                        $item[] = array(
                            localize("-- Personnage à portée -- ") => - 1
                        );
                        if ($myself)
                            $item[] = array(
                                localize($curplayer->get("name")) . " (" . $curplayer->get("id") . ")" => $curplayer->get("id")
                            );
                    } elseif (! $dbp[$i]->eof())
                        $item[] = array(
                            localize("-- Personnage à " . $i . " case(s) -- " . $cond) => - 1
                        );
                    while (! $dbp[$i]->eof()) {
                        if (($dbp[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                            );
                        $dbp[$i]->next();
                    }
                }
            }
        }
        
        if ($action == ABILITY_FLYARROW && $name == "TARGET3_ID") {
            $item[] = array(
                localize("-- Ne pas choisir de cible") => - 1
            );
            $item[] = array(
                localize("Ignorer ce champs") => 0
            );
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    public function toString()
    {
        return $this->str;
    }

    public function getForm()
    {
        $dbc = new DBCollection(
            "SELECT * FROM Ability LEFT JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE ident='" . $this->id . "' AND id_Player=" . $this->curplayer->get("id"), 
            $this->db);
        if (! $dbc->eof()) {
            $time = $dbc->get("modifierPA");
            if ($dbc->get("timeAttack") == 'YES')
                $time += $this->curplayer->getScore("timeAttack");
            
            if ($this->curplayer->get("ap") < $time) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser la compétence : {ability}.", 
                    array(
                        "ability" => localize($dbc->get("name"))
                    ));
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='550px'>";
                $str .= localize($dbc->get("name")) . " ";
                switch (constant($this->id)) {
                    case ABILITY_POWERFUL:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_FIRSTAID:
                        $str .= $this->getSelector(1, 1, 0);
                        break;
                    case ABILITY_GUARD:
                        break;
                    case ABILITY_DAMAGE:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_THRUST:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_TWIRL:
                        break;
                    case ABILITY_SHARPEN:
                        $str .= $this->getArmSharpen();
                        break;
                    case ABILITY_TREACHEROUS:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_STUNNED:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_KNOCKOUT:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_DISARM:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_LIGHT:
                        $str .= $this->getSelector();
                        break;
                    case ABILITY_AUTOREGEN:
                        break;
                    case ABILITY_EXORCISM:
                        $str .= "Libérer l'énergie noire accumulée ?";
                        break;
                    case ABILITY_BRAVERY:
                        $str .= " / Activer ? ";
                        break;
                    case ABILITY_RESISTANCE:
                        $str .= " / Activer ? ";
                        break;
                    case ABILITY_BOLAS:
                        $str .= $this->getSelector(0, 1, 1, 2);
                        break;
                    case ABILITY_STEAL:
                        $str .= $this->getSelector(0, 1, 0);
                        break;
                    case ABILITY_LARCENY:
                        $str .= $this->getSelector(0, 1, 0, 2);
                        break;
                    case ABILITY_PROTECTION:
                        $str .= $this->getSelector(0, 1, 1);
                        break;
                    case ABILITY_PROJECTION:
                        $str .= $this->getSelector(0, 1, 1);
                        break;
                    case ABILITY_FAR:
                        $str .= $this->getSelector(0, 1, 1, 4);
                        $str .= " avec " . $this->getArrow();
                        break;
                    case ABILITY_DISABLING:
                        $str .= $this->getSelector(0, 1, 1, 3);
                        $str .= " avec " . $this->getArrow();
                        break;
                    case ABILITY_NEGATION:
                        $str .= $this->getSelector(0, 1, 1, 3);
                        $str .= " avec " . $this->getArrow();
                        break;
                    case ABILITY_FLYARROW:
                        $str .= "<br>" . $this->getSelector(0, 1, 1, 3, "TARGET1_ID");
                        $str .= " avec " . $this->getArrow("ARROW1_ID");
                        $str .= $this->getSelector(0, 1, 1, 3, "TARGET2_ID");
                        $str .= " avec " . $this->getArrow("ARROW2_ID");
                        $str .= $this->getSelector(0, 1, 1, 3, "TARGET3_ID");
                        $str .= " avec " . $this->getArrow("ARROW3_ID");
                        break;
                    case ABILITY_FLAMING:
                        $str .= $this->getBuilding(3);
                        $str .= " avec " . $this->getArrow();
                        break;
                    case ABILITY_RUN:
                        break;
                    case ABILITY_AMBUSH:
                        // $str.=" Vous allez devenir invisible pour les gens qui ne sont pas dans votre vue";
                        break;
                }
                $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . constant($this->id) . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        } else {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous ne possédez pas cette compétence.");
            $str .= "</td></tr></table>";
        }
        return $str;
    }
}
?>
