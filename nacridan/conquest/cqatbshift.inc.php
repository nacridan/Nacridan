<?php

class CQATBShift extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQATBShift($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        if ($curplayer->get("ap") < ATBSHIFT_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) décaler votre DLA.");
            $str .= "</td></tr></table>";
        } else {
            if (! isset($_POST["action"])) {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Déclaler votre DLA de") . " <select id='hour' class='selector' name='HOUR'>";
                
                for ($i = 0; $i < 12; $i ++) {
                    $hour[] = array(
                        "+" . $i . localize(" H") => $i
                    );
                }
                
                foreach ($hour as $arr) {
                    foreach ($arr as $key => $value) {
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select> ";
                
                $str .= "<select id='min' class='selector' name='MIN'>";
                for ($i = 0; $i <= 59; $i ++) {
                    $min[] = array(
                        "+" . $i . localize(" min") => $i
                    );
                }
                
                foreach ($min as $arr) {
                    foreach ($arr as $key => $value) {
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                
                $str .= "</select></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . ATBSHIFT . "' />";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                
                $time = gmstrtotime($curplayer->get("nextatb")) - date("I") * 3600;
                $time += $_POST["MIN"] * 60 + $_POST["HOUR"] * 3600;
                $date = date("Y-m-d H:i:s", $time);
                
                $str .= localize("Décaler votre DLA pour cette nouvelle heure ? : ") . $date;
                $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='HOUR' type='hidden' value='" . quote_smart($_POST["HOUR"]) . "' />";
                $str .= "<input name='MIN' type='hidden' value='" . quote_smart($_POST["MIN"]) . "' />";
                $str .= "<input name='action' type='hidden' value='" . ATBSHIFT . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}

?>
