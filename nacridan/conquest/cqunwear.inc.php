<?php

class CQUnWear extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQUnWear($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        if ($curplayer->get("ap") < UNWEAR_EQUIPMENT_AP) {
            $str = "<table  class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour vous enlever un équipement.");
            $str .= "</td></tr></table>";
        } else {
            if (! (PlayerFactory::checkPlaceBag($curplayer->get("id"), 0, $db))) {
                $str = "<table  class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Votre sac d'équipement est plein. Vous devez d'abord faire une place.");
                $str .= "</td></tr></table>";
            } else {
                if (! isset($_POST["action"])) {
                    if ($curplayer->get("incity") == 1)
                        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act&center=equip" . "' target='_self'>";
                    else
                        $str = "<form name='form'  method='POST' target='_self'>";
                    
                    $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
                    $str .= localize("Enlever") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
                    
                    $gap = 0;
                    $dbp = new DBCollection("SELECT * FROM Equipment WHERE weared=\"Yes\" AND id_Player=" . $id, $db, 0, 0);
                    
                    $item = array();
                    $str .= "<option value='0' selected='selected'>" . localize("-- Équipement à enlever ?--") . "</option>";
                    while (! $dbp->eof()) {
                        if ($dbp->get("name") != "") {
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " " . $dbp->get("extraname") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        }
                        $dbp->next();
                    }
                    
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value) {
                            if ($value == - 1)
                                $str .= "<optgroup class='group' label='" . $key . "' />";
                            else
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                        }
                    }
                    $str .= "</select></td><td align='center' >";
                    $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "<input name='action' type='hidden' value='" . UNWEAR_EQUIPMENT . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</td></tr></table>";
                    $str .= "</form>";
                } else {
                    if ($_POST["OBJECT_ID"] == 0) {
                        $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                        $str .= localize("L'équipement que vous avez choisi n'est pas valide !") . "</td>";
                        $str .= "</tr></table>";
                    } else {
                        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act&center=equip" . "' target='_self'>";
                        $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px' >";
                        $str .= localize("Cette action vous coûtera {nbpa} PA. Continuer ?", array(
                            "nbpa" => UNWEAR_EQUIPMENT_AP
                        )) . " :</td><td>";
                        $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                        $str .= "<input name='OBJECT_ID' type='hidden' value='" . quote_smart($_POST["OBJECT_ID"]) . "' />";
                        $str .= "<input name='action' type='hidden' value='" . UNWEAR_EQUIPMENT . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</td></tr></table>";
                        $str .= "</form>";
                    }
                }
            }
        }
        return $str;
    }
}

?>
