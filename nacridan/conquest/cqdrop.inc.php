<?php

class CQDrop extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQDrop($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE state='Ground' AND inbuilding=" . $curplayer->get("inbuilding") . " AND room=" . $curplayer->get("room"), $db, 0, 0);
        
        if ($curplayer->get("ap") < DROP_OBJECT_AP) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour déposer un objet.");
            $str .= "</td></tr></table>";
        } elseif (($curplayer->get("inbuilding") != 0) && ($dbe->count() > NB_MAX_ROOM_ITEMS)) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Il n'y a plus assez de place dans la pièce pour déposer l'objet..");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr><td  class='mainbgtitle' >";
            $str .= localize("Poser") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE weared='No' AND state='Carried' AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            $item[] = array(
                localize("-- Mon inventaire --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "" && $dbp->get("id_Equipment\$bag") == 0) {
                    if ($dbp->get("id_BasicEquipment") > 499)
                        $item[] = array(
                            localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    else
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                }
                $dbp->next();
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 1, $db)) {
                $item[] = array(
                    localize("-- Sac de gemmes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 2, $db)) {
                $item[] = array(
                    localize("-- Sac de matières premières --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 3, $db)) {
                $item[] = array(
                    localize("-- Sac d'herbes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                $item[] = array(
                    localize("-- Ceinture --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                $item[] = array(
                    localize("-- Carquois --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                $item[] = array(
                    localize("-- Trousse à outils --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td><td class='mainbgtitle' align='center'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . DROP_OBJECT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}

?>
