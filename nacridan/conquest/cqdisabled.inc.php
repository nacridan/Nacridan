<?php
DEFINE("STEP", 15);

class CQDisabled extends HTMLObject
{

    public $nacridan;

    public $players;

    public $db;

    public $style;

    public function CQDisabled($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $id = 0;
        
        if (is_object($this->nacridan))
            $curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (! isset($_GET["id"])) {
            $id = $curplayer->get("id");
        } else {
            $id = quote_smart($_GET["id"]);
            // $npc=0;
            // $curplayer= new Player();
            // $curplayer->load($id,$db);
        }
        if (isset($_POST["__stepEvt"])) {
            $step = quote_smart($_POST["__stepEvt"]);
            if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"])) {
                $step += 1;
            }
            if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"])) {
                $step -= 1;
            }
            if (isset($_POST["NextEvt10"]) || isset($_POST["NextEvt10_x"])) {
                $step += 10;
            }
            if (isset($_POST["PrevEvt10"]) || isset($_POST["PrevEvt10_x"])) {
                $step -= 10;
            }
        }
        
        if (! isset($step) || $step < 0) {
            $step = 0;
        }
        
        $dbe = new DBCollection("SELECT Player.* FROM Player WHERE disabled=99 ORDER BY nextatb DESC", $db, $step * STEP, STEP);
        
        $data = array();
        $cptEvents = $dbe->count();
        
        while (! $dbe->eof()) {
            $owner = 0;
            
            $art = "";
            $namePlayer = $dbe->get("name");
            if ($namePlayer == "") {
                $namePlayer = $dbe->get("racename");
                $gendersrc = ($dbe->get("gender") == "M") ? "" : localize("e");
                $dbe->get("gender");
                $namePlayer = localize($namePlayer);
                
                if ($dbe->get("gender") == "M")
                    $art = localize("un") . " ";
                else
                    $art = localize("une") . " ";
                
                $style = "stylenpc";
            } else {
                $style = ($dbe->get("status") == "PC") ? "stylepc" : "stylenpc";
            }
            
            $gendersrc = ($dbe->get("gender") == "M") ? "" : localize("e");
            $dbe->get("gender");
            
            if ($this->nacridan != null)
                $namePlayer = "<a href=\"" . CONFIG_HOST . "/conquest/profile.php?id=" . $dbe->get("id") . "\" class='" . $style . " popupify'>" . $art . $namePlayer . "</a>";
            else
                $namePlayer = "<a href=\"" . CONFIG_HOST . "/main/profile.php?id=" . $dbe->get("id") . "\" class='" . $style . " popupify'>" . $art . $namePlayer . "</a>";
            
            $data[] = array(
                "name" => $namePlayer,
                "nextatb" => $dbe->get("nextatb")
            );
            
            $dbe->next();
        }
        
        $extra = "";
        if (isset($_GET["body"])) {
            $extra = "&body=1";
        }
        if (isset($_GET["center"])) {
            $extra .= "&center=" . $_GET["center"];
        }
        
        $str = "<form name='form' action='?id=" . $id . $extra . "' method=post target='_self'>\n";
        $str .= "<table class='maintable " . $this->style . "'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D É S A C T I V A T I O N S') . "</b></td>\n";
        
        if ($cptEvents == STEP) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../pics/misc/left.gif' value='" . localize("previous") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/left.gif' /></td>\n";
        }
        
        if ($step > 0) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../pics/misc/right.gif' value='" . localize("next") . "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/right.gif' /></td>\n";
        }
        
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel' width='230px' align='center'>" . localize('Date') . "</td>\n";
        $str .= "<td class='mainbglabel' align='center'>" . localize('Nom') . "</td>\n";
        $str .= "</tr></table>";
        $str .= "<table class='maintable " . $this->style . "'>";
        
        foreach ($data as $arr) {
            $str .= "<tr><td class='mainbgbody' width='230px' align='center'> " . $arr["nextatb"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='left'> " . $arr["name"] . " </td>";
            $str .= "</tr>\n";
        }
        
        $str .= "</table>";
        $str .= "<input name='__stepEvt' type='hidden' Value='" . $step . "' />\n";
        $str .= "</form>";
        return $str;
    }
}
?>

 
      