<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
require_once (HOMEPATH . "/class/HActionMsg.inc.php");
require_once (HOMEPATH . "/class/HActionMsgHisto.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = "";
switch (getBrowserDefaultLang()) {
    case "fr-fr":
    case "fr":
        $lang = "fr";
        break;
    default:
        $lang = "en";
        break;
}

Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<table><tr><td style='vertical-align: top'><div class='maintable mainbgtitle'>\n";
$str .= "<b><h1>" . localize("H I S T O R I Q U E") . "</h1></b>\n";
$str .= "</div>\n";
$str .= "<br/>\n";
$str .= "<div class='histomsg'>\n";

$MAIN_BODY->add($str);

$players = $nacridan->loadSessPlayers();

if (isset($_GET["id"]) && isset($_GET["type"])) {
    $id = quote_smart($_GET["id"]);
    $class = quote_smart($_GET["type"]);
    $event = new $class();
    $event->load($id, $db);
    
    $owner1 = $event->get("id_Player\$src");
    $owner2 = $event->get("id_Player\$dest");
    $mode = 0;
    
    if (isset($players[$owner1]))
        $mode = 1;
    else {
        if (isset($players[$owner2])) {
            $mode = 0;
        } else {
            $mode = 2;
        }
    }
    
    if ($mode != 2 || $curplayer->get("authlevel") > 5) {
        $class = $class . "Histo";
        $obj = new $class();
        $param = unserialize($event->get("body"));
        // $param=unserialize('a:22:{s:13:"PLAYER_ATTACK";d:6;s:12:"TARGET_DODGE";d:6;s:11:"LEVEL_SPELL";d:5;s:11:"PLAYER_NAME";s:10:"Rat géant";s:11:"PLAYER_RACE";s:3:"144";s:9:"PLAYER_ID";s:3:"179";s:13:"PLAYER_GENDER";s:1:"M";s:9:"MEMBER_ID";s:1:"0";s:13:"PLAYER_IS_NPC";i:1;s:6:"ATTACK";b:1;s:13:"TARGET_IS_NPC";i:0;s:13:"TARGET_KILLED";i:0;s:9:"PLAYER_XP";i:1;s:13:"TARGET_GENDER";s:1:"M";s:11:"TARGET_NAME";s:10:"Aerosquall";s:11:"TARGET_RACE";s:1:"1";s:9:"TARGET_ID";s:3:"223";s:13:"TARGET_DAMAGE";i:16;s:9:"TARGET_HP";i:16;s:11:"TYPE_ATTACK";s:4:"5400";s:2:"AP";i:5;s:5:"ERROR";i:0;}');
        $error = 0;
        if (isset($param["ERROR"])) {
            $error = $param["ERROR"];
        }
        if ($owner1 == $nacridan->sess->get("cq_playerid"))
            $msg = $obj->msgBuilder($error, $param, 1);
        else
            $msg = $obj->msgBuilder($error, $param, 0);
        
        $MAIN_BODY->add($msg);
    }
    $MAIN_BODY->add("</div>");
    $MAIN_BODY->add("</td><td style='vertical-align: top'>");
    
    $MAIN_BODY->add("</td></tr></table>");
    $google = "<script src='http://www.google-analytics.com/urchin.js' type='text/javascript'>
</script>
<script type='text/javascript'>
_uacct = 'UA-1166023-1';
urchinTracker();
</script>";
    
    $MAIN_BODY->add($google);
    
    $MAIN_BODY->add("<script src='" . CONFIG_HOST . "/javascript/astrack.js' type='text/javascript'></script>\n");
    $MAIN_PAGE->render();
}

?>
