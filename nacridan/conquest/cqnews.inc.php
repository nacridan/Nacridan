<?php
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

class CQNews extends HTMLObject
{

    public $nacridan;

    public $players;

    public $curplayer;

    public $db;

    public $style;

    public function CQNews($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $curplayer = $this->curplayer;
        $id = 0;
        
        /*
         * if(is_object($this->nacridan))
         * $curplayer=$this->nacridan->loadCurSessPlayer($db);
         *
         * if(!isset($_GET["id"]))
         * {
         * $id=$curplayer->get("id");
         * }
         * else
         * {
         * $id=quote_smart($_GET["id"]);
         * //$npc=0;
         * //$curplayer= new Player();
         * //$curplayer->load($id,$db);
         * }
         * $extra="";
         * if(isset($_GET["body"]))
         * {
         * $extra="&body=1";
         * }
         * if(isset($_GET["center"]))
         * {
         * $extra.="&center=".$_GET["center"];
         * }
         */
        
        if (isset($_POST['createconfirm'])) // ------------------- CREATION -----------------
{
            if ($_POST['createconfirm'] == 'yes') {
                require_once (HOMEPATH . "/class/News.inc.php");
                $news = new News();
                
                $news->set("id_Player", $curplayer->get("id"));
                $news->set("id_Building", $curplayer->get("inbuilding"));
                $news->set("title", base64_decode($_POST["TITLE"]));
                $news->set("content", base64_decode($_POST["CONTENT"]));
                $news->set("date", gmdate("Y-m-d h:m:s"));
                $news->set("type", 2);
                
                $news->updateDB($db);
                
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>L'annonce a été créée, elle devrait être visible sur la page d'accueil.</td></tr>";
                // $str.="<tr><td class='mainbgbody'>".base64_decode($_POST["content"])."</td></tr>";
                $str .= "</table>";
            } else {
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>L'annonce n'a pas été créée.</td></tr>";
                $str .= "</table>";
            }
            return $str;
        } elseif (isset($_POST['check'])) // --------------VERIFICATION DE L'ANNONCE
{
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE ANNONCE') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody' colspan='3'>Cette annonce convient-elle ?</td></tr>";
            $str .= "<tr class='mainbgbody' >";
            
            $str .= "<td width='100px'>" . gmdate("d-m-Y") . "</td>";
            $str .= "<td width='250px' >*** <b>" . stripslashes($_POST["title"]) . "</b> ***</td>";
            
            $authorName = "<a href='../conquest/profile.php?id=" . $curplayer->get("id") . "' class='stylepc popupify'>" . $curplayer->get("name") . "</a>";
            
            $str .= "<td align='right'>" . $authorName . "</td>";
            
            $content = stripslashes($_POST["content"]);
            $content = nl2br(bbcode($content));
            $str .= "</tr>";
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td colspan=3>" . $content . "</td>";
            $str .= "</tr>";
            $str .= "<input type='hidden' name='TITLE' value='" . base64_encode($_POST['title']) . "'/>";
            $str .= "<input type='hidden' name='CONTENT' value='" . base64_encode($_POST['content']) . "'/>";
            $str .= "<input type='hidden' name='createconfirm' value='yes'/>";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</table></form>";
            
            return $str;
        } elseif (isset($_POST['create'])) // ------- FORMULAIRE DE CREATION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE ANNONCE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='30' name='title'  id='title' /></td></tr>";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' align='left'><label for='content'> Texte de l'annonce : </label> </td>";
            $str .= "</tr>";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='content' id='content' /> </textarea></td>";
            $str .= "</tr>";
            
            $str .= "<td><input type='submit' name='check' value='Créer'/></td></tr>";
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['deleteconfirm'])) // --------------------- SUPPRESSION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            if ($_POST['deleteconfirm'] == 'yes') {
                $newslist = unserialize(stripslashes($_POST['newslist']));
                foreach ($newslist as $id) {
                    $dbd = new DBCollection("DELETE FROM News WHERE id=" . $id, $db, 0, 0, false);
                }
                $str .= "<tr><td>Les annonces sélectionnées ont normalement été supprimées.</td></td>";
            } else
                $str .= "<td><td>Rien n'a été changé.</td></td>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['delete'])) // --------------- CONFIRMATION DE LA SUPPRESSION
{
            
            $newslist = unserialize(stripslashes($_POST['newslist']));
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td class='mainbgbody'>Êtes-vous sûr de vouloir supprimer ces news ?</td>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui, suppression.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='no' id='no' />" . "<label for='no'>Non, on garde.</label></td></tr>";
            $str .= "</table>";
            
            $newsidlist = array();
            
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='100px' align='center'>" . localize('type') . "</td>\n";
            $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Auteur') . "</td>\n";
            $str .= "<td class='mainbglabel' align='center'>" . localize('Date') . "</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            foreach ($newslist as $arr) {
                if (array_key_exists("news" . $arr["id"], $_POST)) {
                    $str .= "<tr><td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["type"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='200px' align='left'>" . $arr["author"] . " </td>";
                    $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                    $str .= "</tr>\n";
                    
                    $newsidlist[] = $arr["id"];
                }
            }
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='newslist' value=" . serialize($newsidlist) . "/>";
            $str .= "<input type='submit' name='ok' value='Ok'/>";
            $str .= "</form>";
            return $str;
        } else // ---------------- LISTE des NEWS -------------
{
            
            $dbq = new DBCollection("SELECT * FROM News", $db);
            
            $data = array();
            
            while (! $dbq->eof()) {
                $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbq->get("id_Player"), $db);
                
                if ($dbq->get("type") == 0)
                    $type = "standard";
                elseif ($dbq->get("type") == 1)
                    $type = "dev auberge";
                elseif ($dbq->get("type") == 2)
                    $type = "dev accueil";
                else
                    $type = "inconnu";
                
                $data[] = array(
                    'id' => $dbq->get('id'),
                    "type" => $type,
                    "author" => $dbp->get("name"),
                    "date" => $dbq->get("date")
                );
                $dbq->next();
            }
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='4' class='mainbgtitle'><b>" . localize('LISTES DES ANCIENNES ANNONCES') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('Type') . "</td>\n";
            $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Auteur') . "</td>\n";
            $str .= "<td class='mainbglabel' align='center'>" . localize('Date') . "</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>";
            
            foreach ($data as $arr) {
                $str .= "<tr><td class='mainbgbody' width='27px' align='center'> <input type='checkbox' name='news" . $arr["id"] . "' id='news" . $arr["id"] . "' /> </td>";
                $str .= "<td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["type"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='200px' align='left'><label for='news" . $arr["author"] . "'> " . $arr["author"] . "</label> </td>";
                $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                $str .= "</tr>\n";
            }
            $str .= "</table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td><input type='submit' name='delete' value='Supprimer'/></td>";
            
            $str .= "</table>";
            $str .= "<input type='hidden' name='newslist' value='" . serialize($data) . "'/>";
            
            $str .= "</form>";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE ANNONCE POUR LA PAGE D\'ACCUEIL') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<td><input type='submit' name='create' value='Créer'/></td></tr>";
            $str .= "</table>";
            $str .= "</form>";
            return $str;
        }
    }
}
?>

 
      
