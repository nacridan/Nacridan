<?php

class CQEvent extends HTMLObject
{

    public $nacridan;

    public $players;

    public $db;

    public $style;

    public function CQEvent($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->style = $style;
    }

    public function toString()
    {
        /* ************************************************** LES EVENTS DES ACTIONS ****************************************************** */
        $db = $this->db;
        
        if (is_object($this->nacridan))
            $curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (! isset($_GET["id"])) {
            $id = $curplayer->get("id");
        } else {
            $id = quote_smart($_GET["id"]);
        }
        
        if (isset($_POST["__stepEvt"])) {
            if (is_numeric($_POST["__stepEvt"])) {
                $step = quote_smart($_POST["__stepEvt"]);
                if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"]))
                    $step += 1;
                if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"]))
                    $step -= 1;
                if (isset($_POST["NextEvt10"]) || isset($_POST["NextEvt10_x"]))
                    $step += 10;
                if (isset($_POST["PrevEvt10"]) || isset($_POST["PrevEvt10_x"]))
                    $step -= 10;
            } else {
                $step = 0;
            }
        }
        if (! isset($step) || $step < 0)
            $step = 0;



        $date_regex = '#^[0-9]{4}-[0-9]{2}-[0-9]{2}\s00:00:00$#';
        $filter_date_from = filter_input(INPUT_GET, 'date_from', FILTER_VALIDATE_REGEXP, array('options'=>array('regexp'=>$date_regex)));
        $filter_date_to = filter_input(INPUT_GET, 'date_to', FILTER_VALIDATE_REGEXP, array('options'=>array('regexp'=>$date_regex)));

        $playersList = array();
        if (!empty($this->nacridan)) {
            $playersList = $this->nacridan->loadAllPlayers();
        }
        $filter_target = filter_input(INPUT_GET, 'target', FILTER_DEFAULT);
        if (!is_numeric($filter_target)) {
            $filter_target = array_search($filter_target, $playersList);
        }
        $filter_types = filter_input_array(INPUT_GET, array(
            'types' => array(
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_REQUIRE_ARRAY)
        ));
        if (isset($_POST["idBasicEventType"]) && $_POST["idBasicEventType"] != "-1") {
            $filter_types = false;
        } else {
            $filter_types = $filter_types['types'];
        }

        $hasFiltersActive = false;
        if (!empty($filter_target) || !empty($filter_date_from) || !empty($filter_date_to) || !empty($filter_types)) {
            $hasFiltersActive = true;
        }

        /*
         * auth = 0 -> visible de tous
         * auth = 1 -> n'apparait pas sur les évènement du src --> feinte pour les sorts de zone
         * auth = 2 --> n'est pas visible pas le dest ni sur ses évènements ni sur les évènement du src
         */
            
        // Disjonction de cas suivant si on mate des évènements
        if (isset($curplayer) && $curplayer->get("authlevel") > 10)
            $cond = "((id_Player\$src=" . $id . " AND auth != 1) OR (id_Player\$dest=" . $id . "))";
        elseif (isset($curplayer) && $curplayer->get("id") == $id)
            $cond = "((id_Player\$src=" . $id . " AND auth != 1) OR (id_Player\$dest=" . $id . " AND auth<2))";
        else
            $cond = "((id_Player\$src=" . $id . " AND auth=0) OR (id_Player\$dest=" . $id . " AND auth<2))";
        
        if (isset($_POST["idBasicEventType"]) && $_POST["idBasicEventType"] != "-1") {
            $cond .= " and (BasicEventType.id = " . $_POST["idBasicEventType"] . " or RolePlayAction.idBasicEventType = " . $_POST["idBasicEventType"] . ")";
        } else if (!empty($filter_types)) {
            $cond .= ' AND (';
            foreach($filter_types as $k => $ft) {
                if ($k > 0) {
                    $cond .= ' OR ';
                }
                $cond .= '(BasicEventType.id = ' . $ft . ' or RolePlayAction.idBasicEventType = ' . $ft . ')';
            }
            $cond .= ')';
        }
        if (!empty($filter_target)) {
            $cond .= ' AND (id_Player$dest='.$filter_target.' OR id_Player$src='.$filter_target.')';
        }
        if (!empty($filter_date_from)) {
            $cond .= ' AND Event.date>="'.$filter_date_from.'"';
        }
        if (!empty($filter_date_to)) {
            $cond .= ' AND Event.date<="'.$filter_date_to.'"';
        }


        
        $dbe = new DBCollection(
            "SELECT
                Event.*,
                ifnull(BasicEventType.code,RolePlayAction.name) as codeEvent,
                ifnull(BasicEvent.description,RolePlayAction.description) as descriptionEvent,
                P1.id AS idsrc,
                P1.name AS namesrc,
                P1.gender AS gendersrc,
                P1.status as statussrc,
                P2.id AS iddest,
                P2.name AS namedest,
                P2.gender AS genderdest,
                P2.status as statusdest,
                BR1.name AS nameracesrc,
                BR1.gender AS racegendersrc,
                BR2.name AS nameracedest,
                BR2.gender AS racegenderdest
            FROM
                Event
            LEFT JOIN
                BasicEvent
                ON
                    BasicEvent.id = Event.typeEvent
            LEFT JOIN
                BasicEventType
                ON
                    BasicEvent.idBasicEventType = BasicEventType.id
            LEFT JOIN
                RolePlayAction
                ON
                    (2000+RolePlayAction.id) = Event.typeEvent
            LEFT JOIN
                Player as P1
                ON
                    id_Player\$src=P1.id
            LEFT JOIN Player as P2
                ON
                    id_Player\$dest=P2.id
            LEFT JOIN
                BasicRace as BR1
                ON
                    id_BasicRace\$src=BR1.id
            LEFT JOIN
                BasicRace as BR2
                ON
                    id_BasicRace\$dest=BR2.id
            WHERE
                " . $cond . "
            ORDER BY
                date DESC"
            , $db, $step * STEP_EVENT, STEP_EVENT);
        $data = array();
        $cptEvents = $dbe->count();
        
        while (! $dbe->eof()) {
            $owner = 0;
            $style = "";
            if (isset($this->players[$dbe->get("id_Player\$src")]) || (isset($curplayer) && $curplayer->get("authlevel") > 5) || isset($this->players[$dbe->get("id_Player\$dest")])) {
                $owner = 1;
                switch ($dbe->get("codeEvent")) {
                    case "DÉPLACEMENT":
                    case "OBJET":
                    case "PARCHEMIN":
                    case "POTION":
                    case "BOUSCULADE":
                    case "GAIN":
                    case "RESSOURCE":
                        $style = "classicevent";
                        break;
                    
                    case "COMPÉTENCE":
                    case "VOL":
                    case "LARCIN":
                    case "ATTAQUE":
                    case "BOLAS":
                    case "DESARMER":
                        $style = "attackevent";
                        break;
                    
                    case "MORT":
                        $style = "deathevent";
                        break;
                    
                    case "SORTILÈGE":
                    case "TÉLÉPORTATION":
                        $style = "magicevent";
                        break;
                    
                    case "DON":
                    case "NIVEAU":
                    case "EXPÉRIENCE":
                        $style = "givemoneyevent";
                        break;
                    
                    case "SAVOIR-FAIRE":
                    case "SERVICE AUBERGE":
                    case "SERVICE BANCAIRE":
                    case "SERVICE ECHOPPE":
                    case "SERVICE GUILDE":
                    case "SERVICE TEMPLE":
                    case "REPOS":
                    case "RECEPTION DE COLIS":
                    case "PUTSCH":
                    case "PRISON":
                    case "ENVOI DE COLIS":
                    case "GESTION VILLAGE":
                    case "CARAVANE":
                    case "BÂTIMENT":
                    case "ARRESTATION":
                    case "APPRENTISSAGE":
                        $style = "talentevent";
                        break;
                }
            }
            
            $time = gmstrtotime($dbe->get("date"));
            $date = '<span title="Le '.date("d-m-Y à H:i:s", $time).'">'.date("d/m H:i:s", $time).'<span>';
            
            if (isset($curplayer) && ($owner || $curplayer->get("authlevel") > 5) && $dbe->get("idtype") != 0)
                $type = "<a href=\"" . CONFIG_HOST . "/conquest/history.php?type=" . $dbe->get("type") . "&id=" . $dbe->get("idtype")
                    . "\"  class='" . $style . " popupify'>" . $dbe->get("codeEvent") . "</a>";
            else {
                $type = localize($dbe->get("codeEvent"));
            }

            if ($this->nacridan != null)
                $link = '/conquest/profile.php';
            else
                $link = '/main/profile.php';
            
            $art = "";
            $envelopeSrc = '';
            $namePlayer = $dbe->get("namesrc");
            if ($namePlayer == "") {
                $namePlayer = $dbe->get("nameracesrc");
                $namePlayer = localize($namePlayer);
                
                if ($dbe->get("racegendersrc") == "M")
                    $art = localize("un") . " ";
                else
                    $art = localize("une") . " ";
                
                $style = "stylenpc";
            } else {
                $style = ($dbe->get("statussrc") == "PC") ? "stylepc" : "stylenpc";
                if (isset($curplayer) && !$this->isCurrentPlayerOrWatchedPlayer($dbe->get('idsrc'), $id, $curplayer)) {
                    $envelopeSrc = Envelope::render($namePlayer);
                }
            }
            $gendersrc = ($dbe->get("gendersrc") == "M") ? "" : localize("e");
            $namePlayer = $envelopeSrc . "<a href=\"" . CONFIG_HOST . $link. '?id=' . $dbe->get("id_Player\$src") . "\" "
                . "class='" . $style . " popupify'>" . $art . $namePlayer . "</a> (" . $dbe->get("id_Player\$src") . ")";

            
            $art = "";
            $envelopeTarget = '';
            $nameTarget = $dbe->get("namedest");
            if ($nameTarget == "") {
                $nameTarget = $dbe->get("nameracedest");
                $nameTarget = localize($nameTarget);
                
                if ($dbe->get("racegenderdest") == "M")
                    $art = localize("un") . " ";
                else
                    $art = localize("une") . " ";
                $style = "stylenpc";
            } else {
                $style = ($dbe->get("statusdest") == "PC") ? "stylepc" : "stylenpc";
                if (isset($curplayer) && !$this->isCurrentPlayerOrWatchedPlayer($dbe->get('iddest'), $id, $curplayer)) {
                    $envelopeTarget = Envelope::render($nameTarget);
                }
            }
            $genderdest = ($dbe->get("genderdest") == "M") ? "" : localize("e");
            $nameTarget = $envelopeTarget . "<a href=\"" . CONFIG_HOST . $link . "?id=" . $dbe->get("id_Player\$dest") . "\" "
                . "class='" . $style . " popupify'>" . $art . $nameTarget . "</a>" . " (" .
                     $dbe->get("id_Player\$dest") . ")";
            
            $body = localize($dbe->get("descriptionEvent"), 
                array(
                    "target" => $nameTarget,
                    "player" => $namePlayer,
                    "gendersrc" => $gendersrc,
                    "genderdest" => $genderdest
                ));
            
            $data[] = array(
                "time" => $time,
                "date" => $date,
                "typeAction" => $type,
                "action" => $dbe->get("typeAction"),
                "idtype" => $dbe->get("idtype"),
                "type" => $dbe->get("type"),
                "typeEvent" => $dbe->get("typeEvent"),
                "eventBody" => $body,
                "ip" => $dbe->get("ip"),
                "isOwnEvent" => isset($curplayer) && ($dbe->get('idsrc') == $curplayer->get('id'))
            );
            
            $dbe->next();
        }
        $extra = "";
        if (isset($_GET["body"])) {
            $extra = "&body=1";
        }
        if (isset($_GET["center"])) {
            $extra .= "&center=" . $_GET["center"];
        }

        $type_event_list = array();
        $dbe = new DBCollection("SELECT * FROM BasicEventType", $this->db);
        while (! $dbe->eof()) {
            $type_event_list[$dbe->get('id')] = $dbe->get("code");
            $dbe->next();
        }


        // On crée le bout d'URL contenant les filtres pour les conserver pour la pagination (formulaire de type POST)
        $filtersAsUrl = "";
        $filtersAsUrl .= !empty($filter_target) ? '&target='.$filter_target : '';
        $filtersAsUrl .= !empty($filter_date_from) ? '&date_from='.urlencode($filter_date_from) : '';
        $filtersAsUrl .= !empty($filter_date_to) ? '&date_to='.urlencode($filter_date_to) : '';
        if (!empty($filter_types)) {
            foreach ($filter_types as $ft) {
                $filtersAsUrl .= '&types[]='.$ft;
            }
        }


        $str  = '<form id="search" class="centerareawidth" style="height:0px; padding-top:0px; padding-bottom: 0px " name="search" action="?id=' . $id . $extra . '" method="GET" target="_self">';
        $str .= '<input type="hidden" name="id" value="'.$id.'"/>';
        if (isset($_GET["body"])) {
            $str .= '<input type="hidden" name="body" value="1"/>';
        }
        if (isset($_GET["center"])) {
            $str .= '<input type="hidden" name="center" value="'.$_GET["center"].'"/>';
        }
        $str .= '<div class="left">';
        $str .= '<div class="inputGroup"><label for="target">';
        $str .= 'Interagit avec';
        if (empty($this->nacridan)) {
            $str .= ' (id uniquement)';
        }

        $str .= '</label>';
        $str .= '<input type="text" name="target" id="target" value="';
        if (!empty($filter_target)) {
            if (!empty($playersList) && isset($playersList[$filter_target])) {
                $str .= $playersList[$filter_target];
            } else {
                $str .= $filter_target;
            }
        }

        $str .= '"';
        if (!empty($this->nacridan)) {
            $str .= ' class="players-autocomplete"';
        }
        $str .= '/></div>';

        $str .= '<div class="input-group">';
        $str .= '<label for="date_from">A partir du</label>';
        $str .= '<input type="text" id="date_from" autocomplete="off"/>';
        $str .= '<input type="hidden" name="date_from" id="filter_date_from" value="'.$filter_date_from.'"/>';
        $str .= '<a href="#" id="clear_date_from"><img src="../pics/misc/clear_white.png" alt="Nettoyer"/></a>';
        $str .= '</div>';
        $str .= '<div class="input-group">';
        $str .= '<label for="date_to">Jusqu\'au</label>';
        $str .= '<input type="text" id="date_to" autocomplete="off"/>';
        $str .= '<input type="hidden" name="date_to" id="filter_date_to" value="'.$filter_date_to.'"/>';
        $str .= '<a href="#" id="clear_date_to"><img src="../pics/misc/clear_white.png" alt="Nettoyer"/></a>';
        $str .= '</div>';
        $str .= '<a href="?id=' . $id . $extra . '" id="cancelSearch">Annuler la recherche</a>';
        $str .= '<input type="submit" value="Rechercher !"/>';
        $str .= '</div><div class="right">';

        $str .= '<div class="input-group select-types">';
        $str .= '<label for="filter_types">Types d\'évenement<br/><span class="small">(Vous pouvez en selectionner plusieurs.)</span></label>';
        $str .= '<select id="filter_types" name="types[]" multiple="multiple">';
        foreach($type_event_list as $type_id => $type_code) {
            if (!empty($filter_types) && in_array($type_id, $filter_types)) {
                $strSelected = "selected='selected'";
            } else {
                $strSelected = "";
            }
            $str .= "<option value='" . $type_id . "' " . $strSelected . ">" . $type_code . "</option>";
        }
        $str .= '</select>';
        $str .= '</div>';
        $str .= '</div>';

        $str .= '</form>';


        $str .= "<form name='form' action='?id=" . $id . $extra . $filtersAsUrl ."' method=post target='_self'>\n";
        $str .= "<table class='maintable " . $this->style . " header'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('É V È N E M E N T S') . "</b></td>\n";
        $str .= "<td class='mainbgtitle' align='right' valign='middle'>";
        $str .= "<a href='#' id='searchIcon' ". ($hasFiltersActive?'class=" alwaysactive"':'') ." title='Recherche avancée'>";
        $str .= "<img src='../pics/misc/find.png' alt='Chercher'/>Recherche</a>";
        $str .= "Filtrer: ";
        $str .= "<select id='classic_filter_select' class='selector cqattackselectorsize' ". ($hasFiltersActive?'disabled="disabled"':'') ." name='idBasicEventType' onchange='document.getElementById(\"__stepEvt\").value=0;submit();' align='right'>";
        $str .= "<option value='-1' selected='selected' >" . localize("-- Choisissez un type --") . "</option>";
        
        foreach($type_event_list as $type_id => $type_code) {
            if (isset($_POST["idBasicEventType"]) && $_POST["idBasicEventType"] == $type_id) {
                $strSelected = "selected='selected'";
            } else {
                $strSelected = "";
            }
            $str .= "<option value='" . $type_id . "' " . $strSelected . ">" . $type_code . "</option>";
        }
        $str .= "</select>";
        $str .= "</td>\n";
        
        if ($cptEvents == STEP_EVENT) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../pics/misc/left.gif' value='" . localize("previous") .
                 "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/left.gif' /></td>\n";
        }
        
        if ($step > 0) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../pics/misc/right.gif' value='" . localize("next") .
                 "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/right.gif' /></td>\n";
        }
        
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel' width='85px' align='center'>" . localize('Date') . "</td>\n";
        $str .= "<td class='mainbglabel' width='132px' align='center'>" . localize('Type') . "</td>\n";
        $str .= "<td class='mainbglabel' >" . localize('Détails') . "</td>\n";
        
        $str .= "</tr></table>";




        $str .= "<table class='maintable " . $this->style . " cqevent'>";


        $previousTime = null;
        $currentDay = 'dayOne';
        $i = 0;
        $timeLastReadEvent = time();
        if ($this->isSessionPlayers($id)) {
            $timeLastReadEvent = gmstrtotime($curplayer->get('time_Event_read'));
        }

        foreach ($data as $arr) {
            if ($previousTime != null && date('Ymd', $previousTime) !=  date('Ymd', $arr['time'])) {
                $currentDay = ($currentDay=='dayOne')?'dayTwo':'dayOne';
            }
            $unread = false;
            if ($this->isSessionPlayers($id) && $timeLastReadEvent < $arr['time'] && (!$arr['isOwnEvent'] || ($arr['isOwnEvent'] && Event::isUnintentionalOwnEvent($arr['typeEvent'])))) {
                $unread = true;
            }

            $str .= '<tr class="' . $currentDay .' '. ((++$i%2==0)?'even':'odd') . ($unread?' unread':'') . '"';
            $str .= "><td class='mainbgbody date' width='85px' align='center'> " . $arr["date"] . " </td>\n";
            $str .= "<td class='mainbgbody' width='132px' align='center'> " . $arr["typeAction"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='left'> " . $arr["eventBody"] . " </td>";
            if (isset($curplayer) && $curplayer->get("authlevel") > 5) {
                $str .= "<td class='mainbgbody' align='left'> " . $arr["ip"] . " </td>";
                if ($arr["action"] == GIVE_MONEY) {
                    $id = quote_smart($arr["idtype"]);
                    $class = quote_smart($arr["type"]);
                    $event = new $class();
                    $event->load($id, $db);
                    $param = unserialize($event->get("body"));
                    $str .= "<td class='mainbgbody' align='left'> " . $param["VALUE"] . " </td>";
                } else {
                    $str .= "<td class='mainbgbody' align='left'></td>";
                }
            }
            $str .= "</tr>\n";
            $previousTime = $arr['time'];
        }

        if ($this->isSessionPlayers($id)) {
            new DBCollection(
            'UPDATE Player SET time_Event_read="'.gmdate('Y-m-d H:i:s', time()).'" WHERE id=' . $curplayer->get('id') . ' AND id_Member=' . $curplayer->get("id_Member") . ' ', $db,
            0, 0, false);
        }



        
        $str .= "</table>";
        $str .= "<input name='__stepEvt' id='__stepEvt' type='hidden' Value='" . $step . "' />\n";
        
        $str .= "</form>";
        return $str;
    }

    private function isSessionPlayers($watchedId) {
        return array_key_exists($watchedId, $this->players);
    }



    /**
     * Tells if the $idToCompare id is the one of the $curplayer or is $watchedId.
     *
     * @param $idToCompare
     * @param $watchedId
     * @param $curplayer
     * @return bool
     */
    private function isCurrentPlayerOrWatchedPlayer($idToCompare, $watchedId, $curplayer)
    {
        return $watchedId == $idToCompare || $curplayer->get('id') == $idToCompare;
    }
}
?>

 
      
