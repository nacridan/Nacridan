<?php

class CQParchment extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQParchment($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        if ($curplayer->get("ap") < USE_PARCHMENT_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour utiliser ce parchemin.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            $str .= localize("Utiliser") . " <select id='object' class='selector cqattackselectorsize' name='PARCHMENT_ID'>";
            
            $dbo = new DBCollection("SELECT * FROM Equipment WHERE id_BasicEquipment=600 AND id_Caravan=0 AND id_Player=" . $id, $db);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre objet --") . "</option>";
            while (! $dbo->eof()) {
                $item[] = array(
                    localize($dbo->get("name")) . " (" . $dbo->get("id") . ")" => $dbo->get("id")
                );
                $dbo->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . USE_PARCHMENT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>