<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");

class CQSale extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQSale($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        
        if (isset($_POST["Sell"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["check"]))
                    EquipFactory::equipmentToMoney($this->curplayer, quote_smart($_POST["check"]), $this->nacridan->getCity()->get("id"), $this->nacridan->getCity()->get("level"), 
                        $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner des équipements à vendre.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if ($err == "" && $str == "") {
            $target = "/conquest/conquest.php?center=sale";
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'><b>" . localize('V E N T E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $array = array();
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $modifier = new Modifier();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                     $modifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                     ",mask,wearable,BasicEquipment.frequency,BasicTemplate.frequency AS freq  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN BasicTemplate ON Equipment.id_BasicTemplate=BasicTemplate.id LEFT JOIN Modifier_BasicTemplate ON id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE Equipment.weared='No' AND id_Player=" .
                     $id, $db);
            
            $dbh = new DBCollection("SELECT id FROM Equipment WHERE bagType=1 AND id_Player=" . $curplayer->get("id"), $db);
            $herbe = $dbh->count();
            
            $dbg = new DBCollection("SELECT id FROM Equipment WHERE bagType=2 AND id_Player=" . $curplayer->get("id"), $db);
            $gemme = $dbg->count();
            
            $dbm = new DBCollection("SELECT id FROM Equipment WHERE bagType=3 AND id_Player=" . $curplayer->get("id"), $db);
            $matiere = $dbm->count();
            
            while (! $dbe->eof()) {
                $eqmodifier = new Modifier();
                $eq->DBLoad($dbe, "EQ");
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                $eqmodifier->initCharacStr();
                if (! ($eq->get("id_BasicEquipment") == 84 && $herbe != 0) and ! ($eq->get("id_BasicEquipment") == 85 && $gemme != 0) and
                     ! ($eq->get("id_BasicEquipment") == 86 && $matiere != 0)) {
                    
                    $checkbox = "<input type='checkbox' name='check[]' value='" . $eq->get("id") . "'>";
                    
                    $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                    
                    $price = 0;
                    if (($eq->get("extraname") != "") && ($dbe->get("freq") != 0)) {
                        $name .= " <span class='template'>" . localize($eq->get("extraname")) . "</span>";
                        $price = floor((500 * ($dbe->get("EQtemplateLevel")) * ($dbe->get("EQtemplateLevel"))) / $dbe->get("freq"));
                    }
                    $name .= "<br/>" . localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")<br/>";
                    
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0") {
                            $name .= " " . localize($key) . ": " . $tmp . " |";
                        }
                    }
                    if ($eq->get("name") == 'Parchemin')
                        $price = 2;
                    else
                        $price += floor(50 * ($eq->get("level") * $eq->get("level") + 1) / $dbe->get("frequency"));
                    $price .= " " . localize("PO");
                    
                    $array[] = array(
                        "0" => array(
                            $checkbox,
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $name,
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            $price,
                            "class='mainbgbody' align='right'"
                        )
                    );
                }
                $dbe->next();
            }
            
            $dbp = new DBCollection("SELECT * FROM Player WHERE status='NPC' AND rider='1' AND id_Member=" . $curplayer->get("id_Member"), $db, 0, 0);
            $founded = $dbp->count();
            if ($founded && $dbp->get("map") == $curplayer->get("map") && $dbp->get("x") == $curplayer->get("x") and $dbp->get("y") == $curplayer->get("y")) {
                $dbe = new DBCollection(
                    "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                         $modifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                         ",mask,wearable,BasicEquipment.frequency,BasicTemplate.frequency AS freq  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN BasicTemplate ON Equipment.id_BasicTemplate=BasicTemplate.id LEFT JOIN Modifier_BasicTemplate ON id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE Equipment.weared='No' AND id_Player=" .
                         $dbp->get("id"), $db);
                
                $dbh = new DBCollection("SELECT id FROM Equipment WHERE bagType=1 AND id_Player=" . $dbp->get("id"), $db);
                $herbe = $dbh->count();
                
                $dbg = new DBCollection("SELECT id FROM Equipment WHERE bagType=2 AND id_Player=" . $dbp->get("id"), $db);
                $gemme = $dbg->count();
                
                $dbm = new DBCollection("SELECT id FROM Equipment WHERE bagType=3 AND id_Player=" . $dbp->get("id"), $db);
                $matiere = $dbm->count();
                while (! $dbe->eof()) {
                    $eqmodifier = new Modifier();
                    $eq->DBLoad($dbe, "EQ");
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    $eqmodifier->initCharacStr();
                    
                    if (! ($eq->get("id_BasicEquipment") == 84 && $herbe != 0) and ! ($eq->get("id_BasicEquipment") == 85 && $gemme != 0) and
                         ! ($eq->get("id_BasicEquipment") == 86 && $matiere != 0)) {
                        
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $eq->get("id") . "'>";
                        
                        $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                        
                        $price = 0;
                        if (($eq->get("extraname") != "") && ($dbe->get("freq") != 0)) {
                            $name .= " <span class='template'>" . localize($eq->get("extraname")) . "</span>";
                            $price = floor((500 * ($dbe->get("EQtemplateLevel")) * ($dbe->get("EQtemplateLevel"))) / $dbe->get("freq"));
                        }
                        $name .= "<br/>" . localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")<br/>";
                        
                        foreach ($eqmodifier->m_characLabel as $key) {
                            $tmp = $eqmodifier->getModifStr($key);
                            if ($tmp != "0") {
                                $name .= " " . localize($key) . ": " . $tmp . " |";
                            }
                        }
                        if ($eq->get("name") == 'Parchemin')
                            $price = 2;
                        else
                            $price += floor(50 * ($eq->get("level") * $eq->get("level") + 1) / $dbe->get("frequency"));
                        $price .= " " . localize("PO");
                        
                        $array[] = array(
                            "0" => array(
                                $checkbox,
                                "class='mainbgbody' align='center'"
                            ),
                            "1" => array(
                                $name,
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $price,
                                "class='mainbgbody' align='right'"
                            )
                        );
                    }
                    $dbe->next();
                }
            }
            
            $str .= "</table>\n";
            
            $str .= createTable(3, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='70%' align='left'"
                    ),
                    array(
                        localize("Prix à la vente"),
                        "class='mainbglabel' width='25%' align='right'"
                    )
                ), "class='maintable centerareawidth'");
            
            $str .= "<input id='Sell' type='submit' name='Sell' value='" . localize("Vendre") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=sale' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
