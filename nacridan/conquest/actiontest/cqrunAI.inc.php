<?php
DEFINE("LOGFILE", HOMEPATH . "/AI/logfolder/nacridanDeamon.log");

class CQRunAI extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQRunAI($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["NBRUN"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize(
                    "En faisant tourner l'IA une fois elle fera une fois toute les actions de base qui lui sont propres. Un tour peut prendre une bonne minute. Combien de fois voulez-vous la faire tourner ?</td></tr>");
                $str .= "<tr><td><input type='textbox' name='NBRUN' />";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                require_once (HOMEPATH . "/AI/AIObject.class.php");
                ob_start(); // début de la temporisation de sortie
                for ($i = 0; $i < $_POST["NBRUN"]; $i ++) {
                    $AI = new AIObject();
                    $AI->init();
                    
                    // cartes
                    $AI->MakeMapTeam();
                    
                    // gestion de l'île
                    $AI->deleteEquipment();
                    $AI->autoRepair();
                    $AI->deleteCorpse();
                    $AI->construction();
                    $AI->destroyBuilding();
                    $AI->UpdateEquipment();
                    $AI->deleteNewsPJ();
                    $AI->storeInactivePlayer();
                    $AI->payBankInterest();
                    $AI->IncreaseLoyalty();
                    
                    // gestion des ressources
                    $AI->updateCrop();
                    $AI->updateEmerald();
                    $AI->updateRuby();
                    $AI->updateBush();
                    $AI->updateKradjeck();
                    
                    // IA monstres
                    $AI->updateNumberGate();
                    $AI->createNPC();
                    $AI->playNPC();
                    
                    $fp = fopen(LOGFILE, 'a');
                    fwrite($fp, ob_get_contents());
                    fclose($fp);
                    ob_flush(); // fin de la temporisation de sortie
                }
                ob_end_clean(); // vide le tampon
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("L'IA a tourné " . $_POST["NBRUN"] . " fois.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>