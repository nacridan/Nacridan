<?php

class CQAddTalentToPNJ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAddTalentToPNJ($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idBasicTalent"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                
                $dbr = new DBCollection("SELECT name,id FROM BasicTalent", $db, 0, 0);
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='idBasicTalent' >Savoir-faire à donner au perso</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='idBasicTalent'>";
                while (! $dbr->eof()) {
                    $str .= "<option value='" . $dbr->get("id") . "'>" . $dbr->get("name") . "</option>";
                    $dbr->next();
                }
                $str .= "</select></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='lvl' >Pourcentage de maîtrise</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='lvl'>";
                for ($j = 1; $j <= 100; $j ++) {
                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                }
                $str .= "</select></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='owner_id' >Identifiant du perso</label></td><td><input type='textbox' name='owner_id' value='0' /></td></tr>\n";
                
                $str .= "<tr><td class='mainbgtitle'> Ce savoir-faire va être offert au perso sélectionné </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $Talent = new Talent();
                $Talent->set("id_BasicTalent", quote_smart($_POST["idBasicTalent"]));
                $Talent->set("skill", quote_smart($_POST["lvl"]));
                $Talent->set("id_Player", quote_smart($_POST["owner_id"]));
                
                $Talent->updateDBr($db);
                $this->errorDB($Talent->errorNoDB());
                $dbBT = new DBCollection("SELECT * FROM BasicTalent WHERE id=" . quote_smart($_POST["idBasicTalent"]), $db, 0, 0);
                
                // Cas de l'artisanat apprentissage d'un nouveau patron
                if ($dbBT->get("type") == 3) {
                    switch ($dbBT->get("id")) {
                        case 7:
                            $cond = " id_BasicMaterial=1";
                            break;
                        case 8:
                            $cond = " id_BasicMaterial=2";
                            break;
                        case 9:
                            $cond = " id_BasicMaterial=4";
                            break;
                        case 10:
                            $cond = " id_BasicMaterial=3";
                            break;
                        case 11:
                            $cond = " id_BasicMaterial=5";
                            break;
                        case 12:
                            $cond = " (id_BasicMaterial=6 or id_BasicMaterial=7 or id_BasicMaterial=8) ";
                            break;
                        case 13:
                            $cond = " (id_BasicMaterial=9 or id_BasicMaterial=10)";
                            break;
                    }
                    
                    $dbbe = new DBCollection("SELECT * FROM BasicEquipment WHERE " . $cond . " AND (id_Modifier_BasicEquipment != 999 OR id=300 OR id=301 OR id=302) AND id NOT IN (SELECT id_BasicEquipment FROM Knowledge WHERE id_Player=" . $curplayer->get("id") . " AND id_BasicTalent=" . $dbBT->get("id") . ")", $db);
                    
                    while (! $dbbe->eof()) {
                        $knowledge = new Knowledge();
                        $knowledge->set("id_Player", quote_smart($_POST["owner_id"]));
                        $knowledge->set("id_BasicTalent", quote_smart($_POST["idBasicTalent"]));
                        $knowledge->set("id_BasicEquipment", $dbbe->get("id"));
                        $knowledge->updateDBr($db);
                        $this->errorDB($knowledge->errorNoDB());
                        
                        $dbbe->next();
                    }
                    
                    if ($dbBT->get("id") == 13) {
                        $dbtemp = new DBCollection("SELECT * FROM BasicTemplate", $db);
                        while (! $dbtemp->eof()) {
                            
                            $knowledge = new Knowledge();
                            $knowledge->set("id_Player", quote_smart($_POST["owner_id"]));
                            $knowledge->set("id_BasicTalent", quote_smart($_POST["idBasicTalent"]));
                            $knowledge->set("id_BasicEquipment", $dbtemp->get("id"));
                            $knowledge->updateDBr($db);
                            $this->errorDB($knowledge->errorNoDB());
                            $dbtemp->next();
                        }
                    }
                }
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize("Okey vous avez normalement donné un savoir-faire à un perso, et tous les patrons correspondant si c'est un artisanat.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion bâtiment (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau perso/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
