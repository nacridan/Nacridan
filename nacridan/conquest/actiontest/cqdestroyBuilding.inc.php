<?php
require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");

class CQDestroyBuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQDestroyBuilding($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["BUILDING_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle' width='650px'><label for='BUILDING_ID' >Identifiant du bâtiment à détruire: &nbsp;</label>";
                $dbu = new DBCollection(
                    "select * from Building where (id_basicBuilding <> 14 or id_City =0) and (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp .
                         "))/2 <=5 and map=1 order by x,y,map", $this->db);
                if ($dbu->count() > 0) {
                    $str .= "<select class='selector cqattackselectorsize' name='BUILDING_ID'>";
                    while (! $dbu->eof()) {
                        $str .= "<option value='" . $dbu->get("id") . "'>" . $dbu->get("name") . " (" . $dbu->get("x") . "/" . $dbu->get("y") . ")" . "</option>";
                        $dbu->next();
                    }
                    $str .= "</select>";
                }
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' ><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $error_message = "Le bâtiment avec id : " . $_POST["BUILDING_ID"] . " a été détruit. Pensez à recharger la vue pour voir l'effet. ";
                $building = new Building();
                if ($building->load($_POST["BUILDING_ID"], $db) == - 1) {
                    $error_message = "Le bâtiment n'existe pas.";
                } else {
                    BasicActionFactory::collapseBuilding($building, $curplayer, $db);
                    $building->deleteDB($db);
                }
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= $error_message . "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>