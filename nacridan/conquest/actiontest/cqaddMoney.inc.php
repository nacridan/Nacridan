<?php

class CQAddMoney extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAddMoney($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["ADDMONEY"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Combien de PO voulez vous ?");
                $str .= "<input type='textbox' name='ADDMONEY' />";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $curplayer->set("money", $_POST["ADDMONEY"]);
                $curplayer->updateDB($db);
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Okey vous avez normalement " . $_POST["ADDMONEY"] . " P0.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>