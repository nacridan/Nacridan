<?php

class CQPlayIAMonster extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $errorValue;

    public function CQPlayIAMonster($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->errorValue = "";
    }

    public function toString()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        
        $db = $this->db;
        $curplayer = $this->curplayer;
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["controlMonstre"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "Entrez l'identifiant du monstre à faire bouger.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='controlMonstre' value='0' /></td></tr>";
                $str .= "</td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'></td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Action!' />";
                
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                try {
                    require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                    require_once (HOMEPATH . "/lib/Behavior.inc.php");
                    $player = new Player();
                    $player->externDBObj("Modifier");
                    $player->load($_POST["controlMonstre"], $db);
                    PlayerFactory::newATB($player, $param, $db);
                    
                    $m_behavior1 = new Behavior($db);
                    $m_behavior1->playNPC($_POST["controlMonstre"]);
                } catch (exception $e) {
                    $str = $e->getMessage();
                }
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize("Okey le monstre " . $_POST["controlMonstre"] . " a agi.");
                $str .= "</td></tr>	</table>";
                $str .= $this->errorValue;
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion PNJ/Equipement (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
