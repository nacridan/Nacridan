<?php

class CQTerraformation extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQTerraformation($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["GROUND_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle' width='650px'><label for='GROUND_ID' >Case à terraformer: &nbsp;</label>";
                
                $dbu = new DBCollection(
                    "SELECT MapGround.id,MapGround.x,MapGround.y,MapGround.map from MapGround 
			   where (abs(MapGround.x-" . $xp . ") + abs(MapGround.y-" . $yp . ") + abs(MapGround.x+MapGround.y-" . $xp . "-" . $yp . "))/2 <=5 and MapGround.map = " . $map .
                         " order by MapGround.x,MapGround.y", $db);
                if ($dbu->count() > 0) {
                    $str .= "<select class='selector cqattackselectorsize' name='GROUND_ID'>";
                    while (! $dbu->eof()) {
                        $str .= "<option value='" . $dbu->get("id") . "'>" . $dbu->get("x") . "/" . $dbu->get("y") . "</option>";
                        $dbu->next();
                    }
                    $str .= "</select>";
                }
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' width='650px'><label for='GROUND' >Nouveau terrain: &nbsp;</label>";
                $dbjGround = new DBCollection("SELECT * from Ground order by label", $db);
                $str .= "<select id='ground' name='GROUND'><option>Choisissez un type de terrain</option>";
                while (! $dbjGround->eof()) {
                    $str .= "<option value='" . $dbjGround->get("ground") . "'>" . $dbjGround->get("label") . " (coef " . $dbjGround->get("cost_PA") . ")</option>";
                    $dbjGround->next();
                }
                $str .= "</select>";
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' ><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $dbjChoice = new DBCollection(
                    "SELECT MapGround.id,MapGround.x,MapGround.y from MapGround where (abs(MapGround.x-" . $xp . ") + abs(MapGround.y-" . $yp . ") + abs(MapGround.x+MapGround.y-" .
                         $xp . "-" . $yp . "))/2 <=5 and MapGround.map = " . $map . " and MapGround.id = " . $_POST["GROUND_ID"] . " order by MapGround.x,MapGround.y", $db);
                if ($dbjChoice->count() != 1) {
                    $error_message = "La case n'existe pas dans votre vue.";
                } else {
                    $case = $dbjChoice->get("x") . "/" . $dbjChoice->get("y");
                    $error_message = "La case " . $case . " a été mise à jour. Pensez à recharger la vue pour voir l'effet. ";
                    $dbGround = new DBCollection("select * from Ground where ground = '" . $_POST["GROUND"] . "'", $db);
                    if ($dbGround->count() != 1) {
                        $error_message = "Le terrain n'existe pas.";
                    } else {
                        $dbjChoice = new DBCollection("UPDATE MapGround set MapGround.id_Ground = '" . $dbGround->get("id") . "' where  MapGround.id = " . $_POST["GROUND_ID"], $db, 
                            0, 0, false);
                    }
                }
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= $error_message . "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>