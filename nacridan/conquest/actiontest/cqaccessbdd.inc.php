<?php

class CQAccessBDD extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAccessBDD($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["OBJECT_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle' width='650px'>";
                $str .= "ID_Objet : ";
                $str .= "<input type='textbox' name='OBJECT_ID' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='650px'>";
                $str .= "Nom de la table : ";
                $str .= "<input type='textbox' name='Table' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='650px'>";
                $str .= "Nom de l'attribut : ";
                $str .= "<input type='textbox' name='Attribut' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='650px'>";
                $str .= "Nouvelle valeur : ";
                $str .= "<input type='textbox' name='Valeur' /></td></tr>";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $dbc = new DBCollection("UPDATE " . $_POST["Table"] . " SET " . $_POST["Attribut"] . "=" . $_POST["Valeur"] . " WHERE id=" . $_POST["OBJECT_ID"], $db, 0, 0, false);
                $founded = $dbc->count();
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                if ($founded == 1)
                    $str .= localize("La valeur a été changé");
                else
                    $str .= localize("Impossible d'effecteur la requête. Vous avez dû faire une erreur.");
                
                $str .= "</td></tr></table>";
            }
            
            return $str;
        }
    }
}
?>