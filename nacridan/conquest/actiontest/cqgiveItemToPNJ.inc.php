<?php

class CQGiveItemToPNJ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGiveItemToPNJ($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["OBJECT_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='650px'>";
                $item = array();
                $str .= "Créer ";
                $str .= "<select class='selector cqattackselectorsize' name='OBJECT_ID'>";
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
                $dbe = new DBCollection("SELECT * FROM BasicEquipment order by name", $this->db, 0, 0);
                while (! $dbe->eof()) {
                    $item[] = array(
                        localize($dbe->get("name")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                    );
                    $dbe->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value)
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
                $str .= "</select>";
                
                $str .= " de niveau ";
                $str .= "<select class='selector' name='NIVEAU'>";
                
                $str .= "<option value='0' selected='selected'>" . localize("-- Niveau --") . "</option>";
                for ($i = 1; $i < 16; $i ++)
                    $str .= "<option value='" . $i . "'>" . $i . "</option>";
                
                $str .= "</select><label for='maudit' >&nbsp;&nbsp;&nbsp;Objet maudit?</label><input type='checkbox' name='maudit' value='1' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle'><label for='owner_id' >Identifiant du futur propriétaire</label><input type='textbox' name='owner_id' value='" .
                     $curplayer->get("id") . "' />";
                $str .= "&nbsp;&nbsp;&nbsp;<label for='new_name' >Nom de l'objet:</label><input type='textbox' name='new_name' value='' /></td></tr>\n";
                
                $str .= "<tr><td class='mainbgtitle'>Enchantement mineur: ";
                $str .= "<select id='ID_BASIC_ENCHANT_MINEUR' class='selector'  name='ID_BASIC_ENCHANT_MINEUR'>";
                $dbbt = new DBCollection("SELECT id,name,name2 FROM BasicTemplate", $db, 0, 0);
                $str .= "<option value='-1'>Pas d'enchantement</option>";
                while (! $dbbt->eof()) {
                    $str .= "<option value='" . $dbbt->get("id") . "'>" . $dbbt->get("name") . "</option>";
                    $dbbt->next();
                }
                $str .= "</select>&nbsp;&nbsp;Niveau:";
                $str .= "<select id='ID_BASIC_ENCHANT_MINEUR_LEVEL' class='selector' name='ID_BASIC_ENCHANT_MINEUR_LEVEL'>";
                for ($i = 1; $i <= 15; $i ++) {
                    $str .= "<option value='" . $i . "'>" . $i . "</option>";
                }
                $str .= "</select>";
                $str .= "<BR/>Enchantement majeur: ";
                $str .= "<select id='ID_BASIC_ENCHANT_MAJEUR' class='selector'  name='ID_BASIC_ENCHANT_MAJEUR'>";
                $str .= "<option value='-1'>Pas d'enchantement</option>";
                $dbbt->first();
                while (! $dbbt->eof()) {
                    $str .= "<option value='" . $dbbt->get("id") . "'>" . $dbbt->get("name2") . "</option>";
                    $dbbt->next();
                }
                $str .= "</select>&nbsp;&nbsp;Niveau:";
                $str .= "<select id='ID_BASIC_ENCHANT_MAJEUR_LEVEL' class='selector' name='ID_BASIC_ENCHANT_MAJEUR_LEVEL'>";
                for ($i = 1; $i <= 15; $i ++) {
                    $str .= "<option value='" . $i . "'>" . $i . "</option>";
                }
                $str .= "</select>";
                $str .= "</td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'></td></tr>\n";
                $str .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $equip = new Equipment();
                $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $_POST["OBJECT_ID"], $this->db);
                $equip->set("level", $_POST["NIVEAU"]);
                $equip->set("id_Shop", 0);
                $equip->set("id_Player", $_POST["owner_id"]);
                $equip->set("id_BasicEquipment", $dbc->get("id"));
                $equip->set("date", gmdate("Y-m-d H:i:s"));
                $equip->set("collected", gmdate("Y-m-d H:i:s"));
                $equip->set("id_EquipmentType", $dbc->get("id_EquipmentType"));
                $equip->set("name", isset($_POST["new_name"]) && $_POST["new_name"] != "" ? quote_smart($_POST["new_name"]) : $dbc->get("name"));
                $coef = 1;
                if ($equip->get("id_BasicEquipment") >= 200 && $equip->get("id_BasicEquipment") <= 206 && $equip->get("id_BasicEquipment") != 205)
                    $coef = $equip->get("level");
                $equip->set("durability", $dbc->get("durability") * $coef);
                $equip->set("maudit", isset($_POST["maudit"]) ? 1 : 0);
                
                $equip->addDB($db);
                $equip->reload($db);
                
                $dbt = new DBCollection("SELECT id,name FROM BasicTemplate WHERE id=" . quote_smart($_POST["ID_BASIC_ENCHANT_MINEUR"]), $db, 0, 0);
                if ($dbt->count() > 0) {
                    $enchant = new Template();
                    $enchant->set("id_Equipment", $equip->get("id"));
                    $enchant->set("id_BasicTemplate", $dbt->get("id"));
                    $enchant->set("level", $_POST["ID_BASIC_ENCHANT_MINEUR_LEVEL"]);
                    $enchant->set("pos", 1);
                    $enchant->addDB($db);
                    
                    $equip->set("extraname", $dbt->get("name") . " (" . $_POST["ID_BASIC_ENCHANT_MINEUR_LEVEL"] . ")");
                    
                    $dbt = new DBCollection("SELECT id,name2 FROM BasicTemplate WHERE id=" . quote_smart($_POST["ID_BASIC_ENCHANT_MAJEUR"]), $db, 0, 0);
                    if ($dbt->count() > 0) {
                        $enchant = new Template();
                        $enchant->set("id_Equipment", $equip->get("id"));
                        $enchant->set("id_BasicTemplate", $dbt->get("id"));
                        $enchant->set("level", $_POST["ID_BASIC_ENCHANT_MAJEUR_LEVEL"]);
                        $enchant->set("pos", 2);
                        $enchant->addDB($db);
                        
                        $equip->set("extraname", $equip->get("extraname") . " " . $dbt->get("name2") . " (" . $_POST["ID_BASIC_ENCHANT_MAJEUR_LEVEL"] . ")");
                    }
                    $equip->updateDB($db);
                }
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("L'objet est dans l'inventaire du perso choisi");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>