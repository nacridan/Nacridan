<?php
require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");

class CQDestroyObject extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQDestroyObject($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["OBJECT_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle' width='650px'><label for='OBJECT_ID' >Identifiant de l'objet à détruire: &nbsp;</label>";
                $dbu = new DBCollection(
                    "select * from Equipment where state='Ground' and (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=5 and map=1 order by x,y,map", 
                    $this->db);
                if ($dbu->count() > 0) {
                    $str .= "<select class='selector' name='OBJECT_ID'>";
                    while (! $dbu->eof()) {
                        $str .= "<option value='" . $dbu->get("id") . "'>" . ($dbu->get("name") == "" ? $dbu->get("po") . " " . localize("Pièce(s) d'Or") : $dbu->get("name")) . " (" .
                             $dbu->get("id") . ") -> (" . $dbu->get("x") . "/" . $dbu->get("y") . ")" . "</option>";
                        $dbu->next();
                    }
                    $str .= "</select>";
                }
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' ><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $error_message = "L'objet avec id: " . $_POST["OBJECT_ID"] . " a été détruit. Pensez à recharger la vue pour voir l'effet. ";
                $equip = new Equipment();
                if ($equip->load($_POST["OBJECT_ID"], $db) == - 1) {
                    $error_message = "L'objet n'existe pas.";
                } else {
                    if ($equip->get("state") == "Ground")
                        $equip->deleteDB($db);
                }
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= $error_message . "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>