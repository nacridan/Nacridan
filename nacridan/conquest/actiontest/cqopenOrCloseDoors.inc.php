<?php

class CQOpenOrCloseDoors extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQOpenOrCloseDoors($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idBasicBuilding"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                
                $str .= "<tr><td class='mainbgtitle' width='550px'> Les portes des cités des royaumes sont actuellement</td><td>";
                
                $dbb = new DBCollection("SELECT id,name,x,y,id_BasicBuilding FROM Building WHERE (id_BasicBuilding=26 OR id_BasicBuilding=21) AND (id_City=151 OR id_City=135 OR id_City=159) ", $db, 0, 0);
                $str .= $dbb->get("name") . "</td></tr>";
                
                if ($dbb->get("id_BasicBuilding") == 21) {
                    $action = "Ouvrir";
                    $actionvalue = 26;
                } elseif ($dbb->get("id_BasicBuilding") == 26) {
                    $action = "Fermer";
                    $actionvalue = 21;
                }
                $str .= "<tr><td class='mainbgtitle'>" . $action . " les lourdes portes.</td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Pousser!' />";
                $str .= "<input name='idBasicBuilding' type='hidden' value='" . $actionvalue . "' />\n";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } elseif (isset($_POST["idBasicBuilding"])) {
                $dbbasic = new DBCollection("SELECT id,name FROM BasicBuilding WHERE id=" . quote_smart($_POST["idBasicBuilding"]), $db, 0, 0);
                $dbb2 = new DBCollection("SELECT id,name,x,y,id_BasicBuilding FROM Building WHERE (id_BasicBuilding=26 OR id_BasicBuilding=21) AND (id_City=151 OR id_City=135 OR id_City=159) ", $db, 0, 0);
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                while (! $dbb2->eof()) {
                    $dbb = new DBCollection("UPDATE Building SET name='" . $dbbasic->get('name') . "',id_BasicBuilding=" . $dbbasic->get('id') . " WHERE id=" . $dbb2->get("id"), $db, 0, 0, false);
                    $dbb2->next();
                }
                
                $str .= localize("Bien joué, vous avez poussé très fort sur les portes ! :P <br>Rechargez la vue pour voir le résultat, si vous êtes prêt d'une porte.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }
}
?>
