<?php

class CQCloneMonster extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCloneMonster($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idPNJ"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "Entrez l'identifiant du perso qui contrôlera de ce monstre. Choisissez 0 si vous le donnez à l'IA.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='controlPNJ' value='0' /></td></tr>";
                $str .= "</td></tr>";
                
                $str .= "<tr class='mainbgtitle'><td>Facultatif - Entrez le nom de ce PNJ. Seuls les PNJ avec un nom peuvent recevoir des messages.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='PNJname' value='' /></td></tr>";
                $str .= "</td></tr>";
                
                $dbr = new DBCollection("SELECT name,id FROM BasicProfil order by name asc", $db, 0, 0);
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='idPNJ' >Id du PNJ à cloner</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='idPNJ' value='' /></td></tr>";
                
                $xfinal = $xp + 1;
                $yfinal = $yp;
                
                $str .= "<tr><td class='mainbgtitle'>ATTENTION le PNJ va apparaitre en X=" . $xfinal . "/Y=" . $yfinal .
                     " </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                
                $npcInit = new Player();
                $npcInit->load($_POST["idPNJ"], $db);
                
                $npc = PNJFactory::getPNJFromLevel($npcInit->get("id_BasicRace"), $npcInit->get("level"), $db);
                
                $xfinal = $xp + 1;
                $yfinal = $yp;
                $npc->set("x", $xfinal);
                $npc->set("y", $yfinal);
                $npc->set("map", $map);
                $npc->set("hidden", $curplayer->get("hidden"));
                
                if ($_POST["controlPNJ"] != 0) {
                    $dbmaster = new DBCollection("SELECT name,id,id_Member,status FROM Player WHERE id=" . quote_smart($_POST["controlPNJ"]), $db, 0, 0);
                    $npc->set("id_Member", $dbmaster->get("id_Member"));
                    $npc->set("status", $dbmaster->get("status"));
                    $npc->set("resurrectid", 0);
                    $npc->set("authlevel", - 1);
                    $npc->set("name", $npc->get("racename"));
                }
                
                // $npc->set("currhp",1);
                $npc->set("playATB", gmdate(time() - 20 * date("I")));
                // $npc->set("id_Member", $curplayer->get("id_Member"));
                if ($npc->get("status") == "NPC") {
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                } else {
                    $upgrade = new Upgrade();
                    $npc->setObj("Upgrade", $upgrade, true);
                }
                $npc->updateDBr($db);
                $this->errorDB($npc->errorNoDB());
                // $npc->set("id", $id);
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                PlayerFactory::NewATB($npc, $param, $db);
                
                $dbu = new DBCollection("UPDATE Player SET name='" . quote_smart($_POST["PNJname"]) . "' WHERE id=" . $npc->get("id"), $db, 0, 0, false);
                
                $dbe = new DBCollection(
                    "SELECT E1.*, E2.id_EquipmentType as bagEqutyp   FROM Equipment E1 left outer join Equipment E2 on E1.id_Equipment\$bag = E2.id WHERE  E1.id_Player=" .
                         $npcInit->get("id") . " order by id_Equipment\$bag asc", $db);
                
                while (! $dbe->eof()) {
                    $equipmentClone = new Equipment();
                    $equipmentClone->set("id_Player", $npc->get("id"));
                    $equipmentClone->set("id_Shop", $dbe->get("id_Shop"));
                    $equipmentClone->set("id_BasicEquipment", $dbe->get("id_BasicEquipment"));
                    $equipmentClone->set("id_EquipmentType", $dbe->get("id_EquipmentType"));
                    $equipmentClone->set("id_Mission", $dbe->get("id_Mission"));
                    $equipmentClone->set("id_Caravan", $dbe->get("id_Caravan"));
                    if ($dbe->get("id_Equipment\$bag") > 0) {
                        $dbbag = new DBCollection("SELECT id FROM Equipment WHERE  Equipment.id_Player=" . $npc->get("id") . " and id_EquipmentType=" . $dbe->get("bagEqutyp"), $db);
                        $equipmentClone->set("id_Equipment\$bag", $dbbag->get("id"));
                    } else {
                        $equipmentClone->set("id_Equipment\$bag", 0);
                    }
                    $equipmentClone->set("id_Warehouse", $dbe->get("id_Warehouse"));
                    $equipmentClone->set("name", $dbe->get("name"));
                    $equipmentClone->set("extraname", $dbe->get("extraname"));
                    $equipmentClone->set("level", $dbe->get("level"));
                    $equipmentClone->set("po", $dbe->get("po"));
                    $equipmentClone->set("durability", $dbe->get("durability"));
                    $equipmentClone->set("sharpen", $dbe->get("sharpen"));
                    $equipmentClone->set("state", $dbe->get("state"));
                    $equipmentClone->set("inbuilding", $dbe->get("inbuilding"));
                    $equipmentClone->set("room", $dbe->get("room"));
                    $equipmentClone->set("hidden", $dbe->get("hidden"));
                    $equipmentClone->set("weared", $dbe->get("weared"));
                    $equipmentClone->set("sell", $dbe->get("sell"));
                    $equipmentClone->set("x", $dbe->get("x"));
                    $equipmentClone->set("y", $dbe->get("y"));
                    $equipmentClone->set("progress", $dbe->get("progress"));
                    $equipmentClone->set("templateProgress", $dbe->get("templateProgress"));
                    $equipmentClone->set("map", $dbe->get("map"));
                    $equipmentClone->set("maudit", $dbe->get("maudit"));
                    $equipmentClone->addDB($db);
                    
                    $dbt = new DBCollection("SELECT * FROM Template WHERE id_Equipment=" . $dbe->get("id"), $db);
                    while (! $dbt->eof()) {
                        $template = new Template();
                        $template->set("id_Equipment", $equipmentClone->get("id"));
                        $template->set("id_BasicTemplate", $dbt->get("id_BasicTemplate"));
                        $template->set("level", $dbt->get("level"));
                        $template->set("pos", $dbt->get("pos"));
                        $template->addDB($db);
                        $dbt->next();
                    }
                    $dbe->next();
                }
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize(
                    " Okey vous avez normalement un clone " . ($npc->get("status") == "NPC" ? "PNJ" : "PJ") . " de " . $npcInit->get("id") . " de niveau " . $npcInit->get("level") .
                         " en X=" . $xfinal . "/Y=" . $yfinal . ", rechargez la vue pour le voir.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion PNJ/Equipement (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
