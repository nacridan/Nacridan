<?php

class CQCreateBuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCreateBuilding($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idBasicBuilding"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                
                $dbr = new DBCollection("SELECT name,id FROM BasicBuilding", $db, 0, 0);
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='idBasicBuilding' >Type de bâtiment à créer</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='idBasicBuilding' id='idBasicBuilding' onChange='document.getElementById(\"nomBatiment\").value;document.getElementById(\"nomBatiment\").value= document.getElementById(\"idBasicBuilding\").options[document.getElementById(\"idBasicBuilding\").selectedIndex].text;'>";
                while (! $dbr->eof()) {
                    $str .= "<option value='" . $dbr->get("id") . "'>" . $dbr->get("name") . "</option>";
                    $dbr->next();
                }
                $str .= "</select></td></tr>";
                $str .= "</select></td></tr>";
                $str .= "<tr><td class='mainbgtitle'>Nom du bâtiment</td><td class='mainbglabel'  align='left'> <input id='nomBatiment' type='input' name='nomBatiment' value='Auberge' />";
                $str .= "</td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='lvl' >Niveau du Bâtiment</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='lvl'>";
                for ($j = 1; $j <= 10; $j ++) {
                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                }
                $str .= "</select></td></tr>";
                $str .= "<tr><td class='mainbgtitle'>Point de structure du bâtiments </td><td class='mainbglabel'  align='left'> <input id='psBatiment' type='input' name='psBatiment' value='1000' />";
                $str .= "</td></tr>";
                $xfinal = $xp + 1;
                $yfinal = $yp;
                
                $str .= "<tr><td class='mainbgtitle'>ATTENTION le bâtiment va apparaitre à droite de vous en X=" . $xfinal . "/Y=" . $yfinal .
                     " </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                if (isset($_POST["psBatiment"]) && $_POST["psBatiment"] > 0) {
                    $psBatiment = $_POST["psBatiment"];
                } else {
                    $psBatiment = SOLID;
                }
                
                $building = new Building();
                $xfinal = $xp + 1;
                $yfinal = $yp;
                $building->set("x", $xfinal);
                $building->set("y", $yfinal);
                $building->set("map", $map);
                $building->set("name", $_POST["nomBatiment"]);
                $building->set("id_BasicBuilding", $_POST["idBasicBuilding"]);
                $building->set("level", $_POST["lvl"]);
                $building->set("sp", $psBatiment);
                $building->set("currsp", $psBatiment);
                $building->set("progress", $psBatiment);
                $building->set("id_City", 0);
                $building->set("id_Player", 1);
                
                $building->updateDBr($db);
                $this->errorDB($building->errorNoDB());
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize("Okey vous avez normalement un bâtiment de niveau " . $_POST["lvl"] . " en X=" . $xfinal . "/Y=" . $yfinal . ", rechargez la vue pour le voir.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion bâtiment (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
