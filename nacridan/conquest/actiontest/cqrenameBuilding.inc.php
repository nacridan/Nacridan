<?php

class CQRenameBuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQRenameBuilding($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["BUILDING_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle' width='650px'><label for='BUILDING_ID' >Identifiant du bâtiment à renommer</label><input type='textbox' name='BUILDING_ID' value='0' /></td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' ><label for='NEW_NAME' >Nouveau nom du bâtiment</label><input type='textbox' name='NEW_NAME' value='0' />\n";
                $str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $dbu = new DBCollection("UPDATE Building SET name='" . $_POST["NEW_NAME"] . "' WHERE id=" . $_POST["BUILDING_ID"], $this->db, 0, 0, false);
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "Le bâtiment avec id : " . $_POST["BUILDING_ID"] . " possède désormais le nom \"" . $_POST["NEW_NAME"] . "\". Pensez à recharger la vue pour voir l'effet. ";
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>