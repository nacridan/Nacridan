<?php

class CQNewDLA extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQNewDLA($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $date = gmdate('Y-m-d H:i:s');
            $curplayer->set("nextatb", $date);
            $curplayer->updateDB($db);
            $str .= "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Simuler une nouvelle DLA") . "</td><td class='mainbgtitle' align='center'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='" . localize("Valider") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "<input name='action' type='hidden' value='" . NEWATB . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
            
            /*
             * else
             * {
             * require_once(HOMEPATH."/factory/PlayerFactory.inc.php");
             * PlayerFactory::newATB($curplayer, $param, $db);
             * $curplayer->updateDB($db);
             * $str="<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>".localize("Vous avez un level up ")."</td></tr></table>";
             * }
             */
            return $str;
        }
    }
}
?>
