<?php

class CQAddMagicSpellToPNJ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAddMagicSpellToPNJ($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idBasicMagicSpell"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                
                $dbr = new DBCollection("SELECT name,id FROM BasicMagicSpell order by name", $db, 0, 0);
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='idBasicMagicSpell' >Sorte à donner au perso</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='idBasicMagicSpell'>";
                while (! $dbr->eof()) {
                    $str .= "<option value='" . $dbr->get("id") . "'>" . $dbr->get("name") . "</option>";
                    $dbr->next();
                }
                $str .= "</select></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='lvl' >Pourcentage de maîtrise</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='lvl'>";
                for ($j = 1; $j <= 100; $j ++) {
                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                }
                $str .= "</select></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='owner_id' >Identifiant du perso</label></td><td><input type='textbox' name='owner_id' value='0' /></td></tr>\n";
                
                $str .= "<tr><td class='mainbgtitle'> Cette compétence va être offerte au perso sélectionné </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $magicSpell = new MagicSpell();
                $magicSpell->set("id_BasicMagicSpell", quote_smart($_POST["idBasicMagicSpell"]));
                $magicSpell->set("skill", quote_smart($_POST["lvl"]));
                $magicSpell->set("id_Player", quote_smart($_POST["owner_id"]));
                
                $magicSpell->updateDBr($db);
                $this->errorDB($magicSpell->errorNoDB());
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize("Okey vous avez normalement donné une comp à un perso.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion bâtiment (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau perso/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
