<?php

class CQRespawn extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQRespawn($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        if ($curplayer->get("ap") < RESPAWN_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour vous cette Action.");
            $str .= "</td></tr></table>";
        } else {
            if ($curplayer->get("money") < RESPAWN_PRICE) {
                $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Pièces d'Or (PO)") . "</td>";
                $str .= "</tr></table>";
            } else {
                if (! isset($_POST["action"])) {
                    $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act'>";
                    $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                    $str .= localize("Changer votre lieu de résurrection actuel : X={resx}, Y={resy}", array(
                        "resx" => $curplayer->get("resurrectx"),
                        "resy" => $curplayer->get("resurrecty")
                    )) . "<br/>";
                    $str .= localize("pour ce nouveau lieu : X={x}, Y={y} ?", array(
                        "x" => $curplayer->get("x"),
                        "y" => $curplayer->get("y")
                    )) . "<br/>";
                    $str .= localize("Cette action vous coûtera {price} PO", array(
                        "price" => RESPAWN_PRICE
                    ));
                    $str .= "</td></tr></table>";
                    $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' />";
                    $str .= "<input name='action' type='hidden' value='" . RESPAWN . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</form>";
                }
            }
        }
        return $str;
    }
}

?>