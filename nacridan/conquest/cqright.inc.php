<?php

class CQRight extends HTMLObject
{

    public $nacridan;

    public $db;

    public function CQRight($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($this->db);
    }

    public function getATBDay()
    {
        $time = gmstrtotime($this->curplayer->get("nextatb")) - date("I") * 3600;
        $day = localizedday(date("j M Y", $time));
        return $day;
    }

    public function getATBHour($time = null)
    {
        if (is_null($time)) {
            $time = gmstrtotime($this->curplayer->get("nextatb")) - date("I") * 3600;
        }
        $str = date("Y-m-d H:i:s", $time);
        
        // list($day,$hour)=preg_split(" ",$str);
        list ($day, $hour) = explode(" ", $str);
        return $hour;
    }

    public function toString()
    {
        $previousPage1 = $_SERVER["REQUEST_URI"];
        $prevPos1 = strrpos($previousPage1, "/");
        $prevPos2 = strrpos(substr($previousPage1, 0, $prevPos1 - 1), "/");
        $prevPos3 = strrpos($previousPage1, "?");
        $folder = substr($previousPage1, $prevPos2 + 1, $prevPos1 - 1 - $prevPos2);
        $file = "";
        if ($prevPos3 == false) {
            $file = substr($previousPage1, $prevPos1 + 1);
        } else {
            $file = substr($previousPage1, $prevPos1 + 1, $prevPos3 - 1 - $prevPos1);
        }
        $parameters = "?";
        $isValue = 0;
        foreach ($_GET as $key => $value) {
            if ($key != "action" && $key != "bottom" && !is_array($value)) {
                if ($isValue == 1)
                    $parameters .= "&";
                $parameters .= $key . "=" . $value;
                $isValue = 1;
            } else {
                $parameters = "?center=view2d";
            }
        }
        
        $players = $this->nacridan->loadSessPlayers();
        $playerid = $this->curplayer->get("id");
        $dbp = new DBCollection("SELECT authlevel FROM Player WHERE id_Member=" . $this->curplayer->get("id_Member"), $this->db);
        $anim = 0;
        while (! $dbp->eof()) {
            if ($dbp->get("authlevel") > 2)
                $anim = 1;
            $dbp->next();
        }
        
        $level = $this->curplayer->get("level");
        $modif = $this->curplayer->getObj("Modifier");
        

        
        $str = "<div class='cqright'>";
        // $str.="<div class='cqrighttop'>";
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/" . $folder . "/" . $file . $parameters . "' target='_self'>\n";
        $str .= "<select class='selector cqrightselectorsize' name='__idCurPlayer' style='margin-bottom: 4px' size='1' onchange='submit();'>";
        foreach ($players as $key => $val) {
            if ($key != $playerid) {
                if ($anim)
                    $animstr = " - " . $key;
                else
                    $animstr = "";
                
                $str .= "<option value='$key'>" . $val . $animstr . "</option>";
            } else {
                $str .= "<option value='$key' selected>" . $val . "</option>";
            }
        }
        $str .= "</select>";
        $str .= "</form>";

        $bm = array();
        
        foreach ($modif->m_characLabel as $label) {
            $bm[$label] = $modif->getModifStr($label . '_bm');
            if ($bm[$label] == "0") {
                $bm[$label] = "";
            }
        }
        
        if (! ($this->curplayer->get("id_BasicRace") == 112)) {
            $str .= "<div class='level'>&nbsp;" . localize('Niveau') . ": " . $this->curplayer->get('level') . "</div>\n";
            $str .= "<b>&nbsp;" . localize('PV') . ": " . $this->curplayer->get('currhp') . "/" . $this->curplayer->getModif('hp', DICE_ADD) . "</b>\n";
            
            $str .= "<table class='hp'>\n";
            $str .= "<tr>";
            $str .= "<td class='hpcell'><img class='hpimg'  src='" . CONFIG_HOST . "/pics/misc/pxred.jpg' width='" . $this->curplayer->getHPPercent() . "%'></td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            $str .= "<br/>\n";
            $str .= "<div class='charac1'>\n";
            $str .= "<table>";
            // $str.="<tr><b>".localize('--- Base ---')."</b></tr><tr></tr>";
            
            $str .= "<tr><td width=40px></td><td width=40px></td> <td  align='left'><tr><td></tr>";
            $str .= "<tr><td ><b>Att : </b></td><td >" . $modif->getModifMean('attack') . "</td></tr>";
            $str .= "<tr><td><b>Def : </b></td><td>" . $modif->getModifMean('defense') . "</td></tr>";
            $str .= "<tr><td><b>Deg : </b></td><td>" . $modif->getModifMean('damage') . "</td></tr>";
            $str .= "<tr><td><b>MM  : </b></td><td>" . $modif->getModifMean('magicSkill') . "</td></tr>";
            $str .= "<tr><td><b>Arm  : </b></td><td>" . $modif->getModifMean('armor') . "</td></tr>";
            $str .= "</table> </div>\n";
            
            /*
             * $str.="<tr><td ></td><td></td> <td align='left'><span class='bonus'>B</span>/<span class='malus'>M</span><tr><td></tr>";
             * $str.="<tr><td ><b>For : </b></td><td >".$modif->getModifStr('strength')."</td><td width=60px>".$bm['strength']."</td></tr>";
             * $str.="<tr><td><b>Dex : </b></td><td>".$modif->getModifStr('dexterity')."</td><td>".$bm['dexterity']."</td></tr>";
             * $str.="<tr><td><b>Vit : </b></td><td>".$modif->getModifStr('speed')."</td><td>".$bm['speed']."</td></tr>";
             * $str.="<tr><td><b>MM : </b></td><td>".$modif->getModifStr('magicSkill')."</td><td>".$bm['magicSkill']."</td></tr>";
             * $str.="</table> </div>\n";
             *
             * $str.="<div class='charac1'>\n";
             * $str.="<table><tr><b>".localize('--- Combat ---')."</b></tr>\n";
             * $str.="<tr><td>\n";
             * $str.="<b>".localize('Att')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('attack')."</td><td></td></tr><tr><td>\n";
             * $str.="<b>".localize('Def')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('defense')."</td><td></td></tr><tr><td>\n";
             * $str.="<b>".localize('Deg')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('damage')."</td><td></td></tr><tr><td>\n";
             * $str.="<b>".localize('MM')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('magicSkill')."</td><td></td></tr><tr><td>\n";
             * $str.="<b>".localize('Arm')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('armor')."</td><td></td></tr><tr><td>\n";
             * //$str.="<b>".localize('Tps')."</b></td><td><b>:&nbsp</b>".$modif->getModifMean('timeAttack')."</td><td></td></tr><tr><td>\n";
             * $str.="</tr></table></tr></table></div>";
             */
            
            $str .= "<div ><b>" . localize('Coordonnées') . "</b>:<br/>&nbsp;<b>X</b>=" . $this->curplayer->get('x') . " &nbsp;/&nbsp;  <b>Y</b>=" . $this->curplayer->get('y') .
                 "</div>\n";
            $str .= "<div>\n";
            $str .= "<br/>";
            
            if ($this->curplayer->get("id_BasicRace") != 263 && $this->curplayer->get("id_BasicRace") != 112) {
                $str .= "<table id='infos' class='infos'>";
                $str .= "<tr><td><b>\n";
                $xpLevelUp = getXPLevelUP($level);
                $str .= localize('PA') . "</b></td><td>:</td><td>" . $this->curplayer->get('ap') . "<br/></td></tr><tr><td><b>\n";
                $str .= localize('PX') . "</b></td><td>:</td><td>" . $this->curplayer->get('xp') . "/" . $xpLevelUp . "<br/></td></tr><tr><td><b>\n";
                $str .= localize('PO') . "</b></td><td>:</td><td>" . $this->curplayer->get('money') . "</td></tr>\n";
                $str .= "</table>\n";
                $str .= "</div>\n";


                $str .= $this->getATBInfosString();
            }
            $str .= "<a class='logout' href='../main/logout.php'>" . localize('Déconnexion') . "</a>\n";
        }
        $str .= "</div></form>\n";
        
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }

    public function getATBInfos() {
        $infos = array();

        $newATBTime = gmstrtotime($this->curplayer->get("nextatb")) - date("I") * 3600;
        $currentTime = time();
        $isATBOutdated = false; // Si la DLA est dépassée
        $isATBExpired = false; // Si la DLA est dépassée de plus de ATB_MAX_TIMEOUT secondes
        if ($newATBTime < $currentTime) {
            $isATBOutdated = true;
            if ($newATBTime + ATB_MAX_TIMEOUT < $currentTime) {
                $isATBExpired = true;
            }
        }

        $infos['newATBTime'] = $newATBTime;
        $infos['isATBOutdated'] = $isATBOutdated;
        $infos['isATBExpired'] = $isATBExpired;

        return $infos;
    }

    /**
     * Get the ATB infos block of the right menu
     * @return string
     */
    public function getATBInfosString()
    {
        $infos = $this->getATBInfos();
        $newATBTime = $infos['newATBTime'];
        $isATBOutdated = $infos['isATBOutdated'];
        $isATBExpired = $infos['isATBExpired'];

        $str = '<div class="nextatb' . ($isATBOutdated ? ' atboutdated' : '') . '">';
        $str .= localize('Prochaine <b>DLA</b>') . "<br/>\n";
        $str .= $this->getATBDay() . "</b><br/>\n";
        $str .= "<b class='atbhour'>" . $this->getATBHour() . "</b>\n";
        if (!$this->nacridan->sess->has("DLA" . $this->curplayer->get("id"))) {
            $str = $this->getActivateDLAButton($str);
            $str .= '<span class="atbdelay restricted">Session restreinte !</span>';
        } else if ($isATBOutdated && !$isATBExpired) {
            $str = $this->getActivateDLAButton($str);
            $str .= '<span class="atbdelay">Restriction à <b>' . $this->getATBHour($newATBTime + ATB_MAX_TIMEOUT) . '</b></span>';
            $str .= '<a href="../i18n/rules/fr/rules.php?page=step2#DLA" id="helpLinkDLA" target="_blank">?</a>';
        }
        $str .= "<br/>(" . localize('Heure : ') . "<span class='hour'>" . date("H:i:s") . "</span>)\n";
        $str .= "</div>\n";
        return $str;
    }

    /**
     * @param $str
     * @return string
     */
    public function getActivateDLAButton($str)
    {
        $str .= '<form method="POST" action="../conquest/conquest.php?action=act" id="activate_dla">';
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "<input name='action' type='hidden' value='2' />\n";
        $str .= "<input name='check0' type='hidden' value='".$this->curplayer->get('id')."' />\n";
        $str .= '<input type="image" value="Activer !"';
        $str .= ' title="Activer la DLA de ce personnage"';
        $str .= ' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABFklEQVQ4ja2SPUoEQRCFa0UWIxFkA7MJWpl2q/9eVScGgggGYuIZDEwMXDOvIJiLiV5BMDHxFgYaeAQRzDRog2XEH3Rn1C9t3kfVqyb6L5ZTWh/GGH8t8MB+0FwccGaMGXQWuCQHQXMJmosTfXTAiIj6nSfgGFdY5CpoLj7JrYtxs5Ogqqo5IupxwBYDd0FzYZHLOoSlLoKGvgNG45XkxYkcf3qfKCAiImPMgKEnbx0Bu0Q03VpgrV1wIueNgJPuENHUZEFVzbDIoRd9cqLPDD0yxsy2WaHHwLZPcj++hl7YGBe/qe+jwKa0yiLXQXNh4IaDbPwYbHj/kbzIgwP2vhTVZgIHnNZ1Pd862DAMsma9587Bv/IK4OpfX2sp3LwAAAAASUVORK5CYII="/>';
        $str .= '</form>';
        return $str;
    }
}
?>
