<?php

class CQPick extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQPick($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $building = $curplayer->get("inbuilding");
        $room = $curplayer->get("room");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("ap") < PICK_UP_OBJECT_AP) {
            $str = "<div><table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour ramasser un objet.");
            $str .= "</td></tr></table></div>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' >";
            $str .= localize("Ramasser") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE state=\"Ground\" AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 1 AND inbuilding=" . $building . " AND room=" . $room, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            $item[] = array(
                localize("-- Equipement(s) sur votre zone --") => - 1
            );
            while (! $dbp->eof()) {
                $dbc = new DBCollection("SELECT * FROM Player WHERE x=" . $dbp->get("x") . " AND y=" . $dbp->get("y") . " AND id!=" . $curplayer->get("id"), $db);
                if ($dbc->eof() || $dbc->get("room") != 0) {
                    if ($dbp->get("id_BasicEquipment") > 499) {
                        $item[] = array(
                            localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    } elseif ($dbp->get("name") != "") {
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    } else {
                        $item[] = array(
                            $dbp->get("po") . " " . localize("Pièce(s) d'Or") => $dbp->get("id")
                        );
                    }
                }
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select>";
            
            if (playerFactory::getIdBagByNum($id, 1, $db) || playerFactory::getIdBagByNum($id, 2, $db) || playerFactory::getIdBagByNum($id, 3, $db) || playerFactory::getIdBagByNum($id, 4, $db) || playerFactory::getIdBagByNum($id, 5, $db) || playerFactory::getIdBagByNum($id, 6, $db)) {
                $str .= " mettre dans ";
                $str .= localize("") . " <select class='selector' name='INV_ID'>";
                $str .= "<option value='1'>Sac d'equipement</option>";
                if (playerFactory::getIdBagByNum($id, 1, $db))
                    $str .= "<option value='2'>Sac de gemmes</option>";
                if (playerFactory::getIdBagByNum($id, 2, $db))
                    $str .= "<option value='3'>Sac de matières premières</option>";
                if (playerFactory::getIdBagByNum($id, 3, $db))
                    $str .= "<option value='4'>Sac de d'herbes</option>";
                if (playerFactory::getIdBagByNum($id, 4, $db))
                    $str .= "<option value='5'>Ceinture</option>";
                if (playerFactory::getIdBagByNum($id, 5, $db))
                    $str .= "<option value='6'>Carquois</option>";
                if (playerFactory::getIdBagByNum($id, 6, $db))
                    $str .= "<option value='7'>Carquois</option>";
                $str .= "</select>";
            }
            $str .= "</td><td class='mainbgtitle' align='center'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . PICK_UP_OBJECT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
