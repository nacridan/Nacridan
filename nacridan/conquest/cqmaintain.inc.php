<?php

class CQMaintain extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQMaintain($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        
        if ($curplayer->get("ap") < MAINTAIN_EQUIPMENT_AP) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour entretenir votre équipement.");
            $str .= "</td></tr></table>";        
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'>";
            $str .= "<tr class='mainbgtitle'><td>Entretenir cet équipement </td> <td colspan=2><select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE weared='No' AND state='Carried' AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            $item[] = array(
                localize("-- Mon inventaire --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "" && $dbp->get("id_Equipment\$bag") == 0 && $dbp->get("id_EquipmentType") < 23) {                    
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                }
                $dbp->next();
            }
            
            
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td></tr>";
            
            $str .= "<tr class='mainbgtitle'><td   >En utilisant </td> <td><select id='Consumable' class='selector cqattackselectorsize' name='CONSUMABLE_ID'>";
            
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE weared='No' AND state='Carried' AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            $item[] = array(
                localize("-- Mon inventaire --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("id_EquipmentType") == 47 && $dbp->get("name") != "" && $dbp->get("id_Equipment\$bag") == 0) {                    
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                }
                $dbp->next();
            }
            
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                $item[] = array(
                    localize("-- Ceinture --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag && $dbp->get("id_EquipmentType") == 47)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td>";
            
            $str .= "<td class='mainbgtitle' align='center'>";            
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . MAINTAIN_EQUIPMENT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}

?>
