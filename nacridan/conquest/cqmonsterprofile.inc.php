<?php
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

class CQMonsterProfile extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQMonsterProfile($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $str = "";
        $err = "";
        
        $panel = 1;
        
        if (isset($_POST["minipic"]))
            $panel = 2;
        if (isset($_POST["bigpic"]))
            $panel = 3;
        if (isset($_POST["description"]))
            $panel = 4;
        if (isset($_POST["updateMinipic"]))
            $panel = 5;
        if (isset($_POST["updateBigpic"]))
            $panel = 6;
        if (isset($_POST["updateDesc"]))
            $panel = 7;
        
        switch ($panel) {
            case 1: // ------------- LISTE DES RACES -----------
                
                $str = "<form name='form'  method=post target='_self'>\n";
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('LISTES DES RACES DE NACRIDAN') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel' width='70px' align='center'>" . localize('id') . "</td>\n";
                $str .= "<td class='mainbglabel' width='150px' align='center'>" . localize('Nom') . "</td>\n";
                $str .= "<td class='mainbglabel' align='center'>" . localize('Description') . "</td>\n";
                $str .= "<td class='mainbglabel' align='center'><a href='#racemenu' class='maintable '><b>Aller au menu de gestion.</b></a></td>\n";
                $str .= "</tr></table>";
                $str .= "<table class='maintable '>";
                
                $dbm = new DBCollection("SELECT id,name,pic,description FROM BasicRace", $db);
                
                while (! $dbm->eof()) {
                    
                    $idm = $dbm->get("id");
                    
                    $str .= "<tr><td class='mainbgbody' width='20px' align='center'> <input type='radio' name='RaceProfile' id='" . $idm . "' value='" . $idm . "' /> </td>";
                    $str .= "<td class='mainbgbody' width='50px' align='left'> " . $idm . " </td>\n";
                    $str .= "<td class='mainbgbody' width='150px' align='left'><label for='" . $idm . "'> " . $dbm->get("name") . "</label> </td>";
                    
                    $str .= "</tr>\n";
                    
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbgbody' width='50px' align='left'>  </td>\n";
                    if (file_exists("../pics/map/players/" . $idm . ".png"))
                        $picname = $idm;
                    else
                        $picname = "unknown";
                    $str .= "<td class='mainbgbody' ><img src='../pics/map/players/" . $picname . ".png' alt='" . $dbm->get("name") . "'/> </td>";
                    
                    $str .= "<td class='mainbgbody' ><img src='../pics/character/" . $dbm->get("pic") . "' alt='...'/> </td>";
                    $str .= "<td class='mainbgbody'  align='left'> " . bbcode($dbm->get("description")) . " </td>";
                    
                    $str .= "</tr>\n";
                    
                    $dbm->next();
                }
                
                $str .= "</table>";
                
                $str .= "</table>";
                $str .= "<a name='racemenu'></a>";
                $str .= "<table class='maintable '>\n";
                $str .= "<tr><td><input type='submit' name='minipic' value='Changer la miniature'/></td>";
                $str .= "<td><input type='submit' name='bigpic' value='Changer le portrait'/></td>";
                $str .= "<td><input type='submit' name='description' value='Changer la description'/></td></tr>";
                $str .= "</table>";
                
                $str .= "</form>";
                break;
            
            case 2: // ------------- MAJ DE LA MINIATURE, etape 1
                $str = "<form name='form'  method=post target='_self' enctype='multipart/form-data'>\n";
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('MAJ Portrait de la race ') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "<tr>\n";
                
                $dbm = new DBCollection("SELECT name FROM BasicRace WHERE id=" . $_POST["RaceProfile"], $db);
                
                if (isset($_POST["RaceProfile"])) {
                    $str .= "<td class='mainbglabel'  align='center'> Vous être en train de modifier la miniature de la race " . $dbm->get("name") . "</td>\n";
                    $str .= "<td class='mainbgtitle'>" . localize("Format PNG obligatoire") . "<br/><br/><input type='file' name='minipicfile' size='30'>";
                    $str .= "<input type='hidden' name='max_file_size' value='50000'>\n";
                    $str .= "<input type='hidden' name='raceid' value='" . $_POST["RaceProfile"] . "'>\n";
                    $str .= "<input type='hidden' name='racename' value='" . $dbm->get("name") . "'>\n";
                    $str .= "<input type='submit' name='updateMinipic' value='" . localize("Mettre à jour la miniature") . "'></td>";
                } else
                    $str .= "<td class='mainbglabel'  align='center'> Vous devez sélectionner un profile</td>\n";
                
                $str .= "</tr></table>";
                $str .= "</form>";
                break;
            
            case 3: // ------------- MAJ DU PORTRAIT, etape 1
                $str = "<form name='form'  method=post target='_self' enctype='multipart/form-data'>\n";
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('MAJ Portrait de la race ') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "<tr>\n";
                
                $dbm = new DBCollection("SELECT name FROM BasicRace WHERE id=" . $_POST["RaceProfile"], $db);
                
                if (isset($_POST["RaceProfile"])) {
                    $str .= "<td class='mainbglabel'  align='center'> Vous être en train de modifier le portrait de la race " . $dbm->get("name") . "</td></tr>\n";
                    $str .= "<tr><td class='mainbgtitle'>" . localize("Fichier à uploader : ") . "<br/><br/><input type='file' name='bigpicfile' size='30'></td></tr>";
                    $str .= "<tr><td class='mainbglabel'>nom du fichier, avec l'extension et SANS caractères spéciaux (pas d'espace accents etc... les tirets sont OK')<br/><br/><input type='text' name='bigpicname' ></td></tr>";
                    $str .= "<input type='hidden' name='max_file_size' value='50000'>\n";
                    $str .= "<input type='hidden' name='raceid' value='" . $_POST["RaceProfile"] . "'>\n";
                    $str .= "<input type='hidden' name='racename' value='" . $dbm->get("name") . "'>\n";
                    $str .= "<tr><td><input type='submit' name='updateBigpic' value='" . localize("Mettre à jour le portrait") . "'></td>";
                } else
                    $str .= "<td class='mainbglabel'  align='center'> Vous devez sélectionner un profile</td>\n";
                
                $str .= "</tr></table>";
                $str .= "</form>";
                break;
            
            case 4: // ----------------------------- MAJ DE LA DESCRIPTION, etape 1 ----------------
                
                $dbm = new DBCollection("SELECT name FROM BasicRace WHERE id=" . $_POST["RaceProfile"], $db);
                
                $str = "<form name='form'  method=post target='_self'>\n";
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>Vous être en train de remplacer la description de la race " . $dbm->get("name") . "</td></tr>\n";
                $str .= "<tr><td><textarea name='desc' cols=80 rows=15></textarea></td></tr>";
                $str .= "</table>\n";
                $str .= "<input type='hidden' name='raceid' value='" . $_POST["RaceProfile"] . "'>\n";
                $str .= "<input type='submit' name='updateDesc' value='" . localize("Mettre à jour votre description") . "' /><br/><br/><br/>";
                $str .= "</form>";
                break;
            
            case 5: // ---------------- MAJ DE LA MINIATURE, etape 2
                
                move_uploaded_file($_FILES['minipicfile']['tmp_name'], HOMEPATH . "/pics/map/players/" . $_FILES['minipicfile']['name']);
                exec("rm " . HOMEPATH . "/pics/map/players/" . $_POST["raceid"] . ".png");
                exec("mv " . HOMEPATH . "/pics/map/players/" . $_FILES['minipicfile']['name'] . " " . HOMEPATH . "/pics/map/players/" . $_POST["raceid"] . ".png");
                
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbgtitle'> La nouvelle miniature devrait être là. Mais il vous faudra probablement vider le cache de votre navigateur pour la voir.</td>\n";
                $str .= "</tr></table>";
                
                break;
            
            case 6: // ---------------- MAJ DU PORTRAIT, etape 2
                
                move_uploaded_file($_FILES['bigpicfile']['tmp_name'], HOMEPATH . "/pics/character/" . $_FILES['bigpicfile']['name']);
                exec("rm " . HOMEPATH . "/pics/character/" . $_POST["bigpicname"]);
                exec("mv " . HOMEPATH . "/pics/character/" . $_FILES['bigpicfile']['name'] . " " . HOMEPATH . "/pics/character/" . $_POST["bigpicname"]);
                
                $basicrace = new BasicRace();
                $basicrace->load($_POST["raceid"], $db);
                $basicrace->set("pic", $_POST["bigpicname"]);
                $basicrace->updateDBr($db);
                
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbgtitle'> fichier uploadé : <b>" . $_FILES['bigpicfile']['name'] . "</b>. Le nouveau portrait devrait être là sous le nom <b>" . $_POST["bigpicname"] . "</b></td>\n";
                $str .= "</tr></table>";
                
                break;
            
            case 7: // ----------------------------- MAJ DE LA DESCRIPTION, etape 2 ----------------
                
                $newdescription = stripslashes(unHTMLTags($_POST["desc"]));
                $basicrace = new BasicRace();
                $basicrace->load($_POST["raceid"], $db);
                $basicrace->set("description", $newdescription);
                $basicrace->updateDB($db);
                
                $str .= "<table class='maintable '>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbgtitle'> La nouvelle description devrait être là </td>\n";
                $str .= "</tr></table>";
                break;
        }
        
        /*
         * $str.="<table class='maintable centerareawidth'>\n";
         * $str.="<tr><td class='mainbgtitle'>\n";
         * $str.="<b><h3>".$curplayer->get('name')."</h3></b></td>\n";
         * $str.="</tr>\n";
         * $str.="</table>\n";
         *
         * if($curplayer->get('status')=='PC')
         * {
         * $str.="<table class='maintable centerareawidth'><tr><td class='mainbglabel'>".localize("Avatar")."</td></tr>\n";
         * $str.="<tr><td class='mainbgtitle'><table><tr><td>";
         * if($curplayer->get("bigpic")!="")
         * {
         * $str.="<img src='".CONFIG_HOST."/pics/PJ/".$curplayer->get("bigpic")."' width='80px' height='96px' />\n";
         * }
         * else
         * {
         * $str.="<img src='".CONFIG_HOST."/pics/character/undef.jpg' />\n";
         * }
         *
         * $str.="</td><td class='mainbgtitle'>".localize("Format recommandé : (252 x 302)")."<br/><br/><input type='file' name='avatar' size='30'>";
         * $str.="<input type='hidden' name='max_file_size' value='50000'>\n";
         * $str.="<tr></table>\n";
         * $str.="<font color='#ff0000'><b>attention</b></font> <font color='#ffffff'>".localize("L'utilisation d'images pouvant heurter la morale pourra donner suite à l'annulation de votre compte sans préavis").".<br/>\n";
         * $str.=localize("L'image ne doit pas être protégée en copyright ou protégée par une marque déposée.").".<br/>\n";
         * $str.=localize("les images dont la taille dépasse 50 kb seront refusées.")."</font><br/><br/>";
         * $str.="<input type='submit' name='updateAvatar' value='".localize("Mettre à jour votre Avatar")."'>\n";
         *
         * $str.="</td></tr>";
         * $str.="</table><br/><br/>\n";
         *
         * $str.="<table class='maintable centerareawidth'><tr><td class='mainbglabel'>".localize("Description")."</td></tr>\n";
         * $body= new Detail();
         * if($curplayer->get("id_Detail")!=0)
         * $body->load($curplayer->get("id_Detail"),$db);
         * $str.="<tr><td><textarea name='desc' cols=80 rows=15>".$body->get("body")."</textarea></td></tr>";
         * $str.="</table>\n";
         * $str.="<input type='submit' name='updateDesc' value='".localize("Mettre à jour votre description")."' /><br/><br/><br/>";
         *
         *
         *
         *
         * $str.="<br/><br/>\n";
         * }
         * else
         * {
         * $str.="<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
         * $str.=localize("Ce joueur ne peut pas être personnalisé");
         * $str.="</td></tr></table>\n";
         * }
         */
        
        return $str;
    }
}
?>
