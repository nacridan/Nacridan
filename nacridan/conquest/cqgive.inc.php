<?php

class CQGive extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGive($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $building = $curplayer->get("inbuilding");
        $room = $curplayer->get("room");
        
        if ($curplayer->get("ap") < GIVE_OBJECT_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour donner un objet.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='80px'>";
            
            /* ************************************************* OBJET A DONNER ************************************************ */
            $str .= localize("Donner") . " </td><td><select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE state='carried' AND weared='NO' AND id_Player=" . $curplayer->get("id"), $db, 0, 0);
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            
            $item[] = array(
                localize("-- Sac équipements --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("id_Equipment\$bag") == 0) {
                    if ($dbp->get("id_BasicEquipment") > 499)
                        $item[] = array(
                            localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    else
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                }
                $dbp->next();
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 1, $db)) {
                $item[] = array(
                    localize("-- Sac de gemmes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 2, $db)) {
                $item[] = array(
                    localize("-- Sac de matières premières --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 3, $db)) {
                $item[] = array(
                    localize("-- Sac d herbes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                $item[] = array(
                    localize("-- Ceinture --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                $item[] = array(
                    localize("-- Carquois --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                $item[] = array(
                    localize("-- Trousse à outils --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td>";
            
            /**
             * ************************************************* ACTION ********************************
             */
            
            $str .= "<td align='center' rowspan=2> <input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . GIVE_OBJECT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr>";
            
            /* *************************************************** PERSONNE A QUI ON DONNE L OBJET ***************************************** */
            $str .= "<tr class='mainbgtitle'> <td  width='80px'>à </td><td> <select id='Player' class='selector cqattackselectorsize' name='PLAYER_ID'>";
            $gap = 1;
            $dbp = new DBCollection("SELECT * FROM Player WHERE name!='' AND id!=" . $id . " AND inbuilding=" . $building . " AND room=" . $room . " AND disabled=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un personnage --") . "</option>";
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "" && $curplayer->canSeePlayerById($dbp->get("id"), $this->db))
                    $item[] = array(
                        localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                    );
                
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select>";
            
            $str .= "<tr class='mainbgtitle'><td style='font-size:10' colspan=3>" . localize("Attention, si l'objet est un sac tous les objets qu'il contient seront donnés aussi.") . "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}

?>
