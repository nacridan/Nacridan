<?php
require_once (HOMEPATH . "/lib/MapInfo.inc.php");

class CQView extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    private $data = array();
    private $dist = array();
    private $list = array();

    public function CQView($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $str = "";
        $str .= "<table class='maintable centerareawidth'>\n";
        
        $str .= "<tr><td colspan='6' class='mainbgtitle'><b>" . localize('P E R S O N N A G E S') . "</b></td></tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel' width='30' align='center'>" . localize('Dist') . "</td>\n";
        $str .= "<td class='mainbglabel' width='30' align='center'>" . localize('Niveau') . "</td>\n";
        $str .= "<td class='mainbglabel' width='175'>" . localize('Nom') . "</td>\n";
        $str .= "<td class='mainbglabel' width='175' >" . localize('Allégeance') . "</td>\n";
        $str .= "<td class='mainbglabel' width='50' align='right'>" . localize('X') . "</td>\n";
        $str .= "<td class='mainbglabel' width='50' align='right'>" . localize('Y') . "</td>\n";
        $str .= "</tr>\n";
        
        $this->data = array();
        $this->dist = array();
        $this->list = array();
        $this->build_pj();
        array_multisort($this->dist, SORT_ASC, $this->data);
        $str .= join($this->data, "");
        $str .= "</table>\n";


        // Intéressant s'l y a au moins 2 autres joueur.
        if (count($this->list) > 2) {
            $players = $this->list;
            unset($players[$this->curplayer->get('id')]);
            $str .= '<tr><td colspan="6">';
            $str .= Envelope::render($players, 'Écrire à toute ma vue');
            $str .= '</td></tr>';
        }
        $str .= "<br/><br/><table class='maintable centerareawidth'>\n<tr>\n" . "<td colspan='5' class='mainbgtitle'><b>" . localize('P N J') . "</b>\n" . "</td></tr><tr>\n" .
             "<td class='mainbglabel' width='30' align='center'>" . localize('Dist') . "</td>\n" . "<td class='mainbglabel' width='30' align='center'>" . localize('Niveau') .
             "</td>\n" . "<td class='mainbglabel' width='350'>" . localize('Nom') . "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('X') . "</td>\n" .
             "<td class='mainbglabel' width='50' align='right'>" . localize('Y') . "</td>\n" . "</tr>\n";
        
        $this->dist = array();
        $this->data = array();
        $this->build_pnj();
        array_multisort($this->dist, SORT_ASC, $this->data);
        $str .= join($this->data, "");
        $str .= "</table>\n";
        
        $str .= "<br/><br/>" . "<table class='maintable centerareawidth'>\n" . "<tr><td colspan='4' class='mainbgtitle'><b>" . localize('O B J E T S') . '&nbsp;&nbsp;/&nbsp;&nbsp;' .
             localize('T R É S O R S') . "</b>\n" . "</td></tr>\n" . "<tr><td class='mainbglabel' width='50' align='center'>" . localize('Dist') . "</td>\n" .
             "<td class='mainbglabel' width='400'>" . localize('Nom') . "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('X') . "</td>\n" .
             "<td class='mainbglabel' width='50' align='right'>" . localize('Y') . "</td></tr>\n";
        
        $this->dist = array();
        $this->data = array();
        $this->build_items();
        
        array_multisort($this->dist, SORT_ASC, $this->data);
        $str .= join($this->data, "");
        $str .= "</table>\n";
        
        $str .= "<br/><br/>\n" . "<table class='maintable centerareawidth'>\n" . "<tr><td colspan='4' class='mainbgtitle'><b>" . localize('B Â T I M E N T S') . "</b></td>\n" .
             "</tr><tr>\n" . "<td class='mainbglabel' width='50' align='center'>" . localize('Dist') . "</td>\n" . "<td class='mainbglabel' width='400'>" . localize('Nom') .
             "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('X') . "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('Y') .
             "</td></tr>\n";
        
        $this->dist = array();
        $this->data = array();
        $this->build_buildings();
        array_multisort($this->dist, SORT_ASC, $this->data);
        $str .= join($this->data, "");
        $str .= "</table>\n";
        
        $str .= "<br/><br/>\n" . "<table class='maintable centerareawidth'>\n" . "<tr><td colspan='4' class='mainbgtitle'><b>" . localize('L I E U X') . "</b></td>\n" .
             "</tr><tr>\n" . "<td class='mainbglabel' width='50' align='center'>" . localize('Dist') . "</td>\n" . "<td class='mainbglabel' width='400'>" . localize('Nom') .
             "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('X') . "</td>\n" . "<td class='mainbglabel' width='50' align='right'>" . localize('Y') .
             "</td></tr>\n";
        
        $this->dist = array();
        $this->data = array();
        $this->build_places();
        array_multisort($this->dist, SORT_ASC, $this->data);
        $str .= join($this->data, "");
        $str .= "</table>\n";
        
        return $str;
    }

    public function build_places()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $gap = 2;
        if ($curplayer->get("inbuilding") == 0 || $curplayer->get("room") == TOWER_ROOM)
            $gap = 5;
        $dbplace = new DBCollection(
            "SELECT id,name,x,y FROM Place WHERE map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= " . $gap, $db, 0, 0);
        
        $data = array();
        $dist = array();
        
        while (! $dbplace->eof()) {
            $xx = $dbplace->get("x");
            $yy = $dbplace->get("y");
            $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
            $name = "<span class=\"placename\">" . localize($dbplace->get("name")) . "</span>";
            $lieutmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $name
            );
            $this->add_place($xx, $yy, $lieutmp);
            $dbplace->next();
        }
        
        $dbgate = new DBCollection("SELECT id,x,y FROM Gate WHERE map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= " . $gap, 
            $db, 0, 0);
        
        while (! $dbgate->eof()) {
            $xx = $dbgate->get("x");
            $yy = $dbgate->get("y");
            $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
            $name = "<span class=\"gatename\">" . localize("Portail") . "</span>";
            $lieutmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $name
            );
            
            $this->add_portail($xx, $yy, $lieutmp);
            $dbgate->next();
        }
        
        $dbex = new DBCollection(
            "SELECT id,x,y,name,type FROM Exploitation WHERE map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= " . $gap, $db, 0, 
            0);
        
        while (! $dbex->eof()) {
            $xx = $dbex->get("x");
            $yy = $dbex->get("y");
            $exName = $dbex->get("name");
            $exType = $dbex->get("type");
            
            $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
            $name = "<span class=\"ressourcename\">" . localize($exName) . "</span>";
            $exploitationtmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $name,
                "type" => $exType
            );
            
            $this->add_place($xx, $yy, $exploitationtmp);
            
            $dbex->next();
        }
    }

    public function build_buildings()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $gap = 2;
        if ($curplayer->get("inbuilding") == 0 || $curplayer->get("room") == TOWER_ROOM)
            $gap = 5;
        
        $dbbuilding = new DBCollection(
            "SELECT id,id_City,x,y,name,id_BasicBuilding,sp,currsp FROM Building WHERE map=" . $map . " AND id_City>0 AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp .
                 "-" . $yp . "))/2 <= " . $gap, $db, 0, 0);
        if ($dbbuilding->count() > 0) {
            $dbcity = new DBCollection("SELECT id,name,id_Player FROM City WHERE id=" . $dbbuilding->get("id_City"), $db, 0, 0);
            // $cityname = "<span class=\"cityname\">".localize($dbcity->get("name"))."</span>";
            $cityname = "<a href='../conquest/profile.php?id=" . $dbcity->get("id") . "' class=\"cityname popupify\">" . $dbcity->get("name") . "</a>";
            
            $citytmp = array(
                "xx" => '',
                "yy" => '',
                "dist" => '',
                "name" => $cityname
            );
            $this->add_place(0, 0, $citytmp);
            
            while (! $dbbuilding->eof()) {
                $xx = $dbbuilding->get("x");
                $yy = $dbbuilding->get("y");
                $sp = $dbbuilding->get("sp");
                $buildingName = $dbbuilding->get("name");
                $buildingType = $dbbuilding->get("id_BasicBuilding");
                $currsp = $dbbuilding->get("currsp");
                $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
                $name = "<span class=\"buildingname\">" . localize($buildingName) . "</span>";
                $buildingtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $buildingName,
                    "type" => $buildingType,
                    "sp" => $sp,
                    "currsp" => $currsp
                );
                
                $this->add_place($xx, $yy, $buildingtmp);
                
                $dbbuilding->next();
            }
        }
    }

    public function build_items()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $gap = 2;
        if ($curplayer->get("inbuilding") == 0 || $curplayer->get("room") == TOWER_ROOM)
            $gap = 5;
            
            // Equipment
        $dbequipment = new DBCollection(
            "SELECT id,x,y,name,po,extraname,level,maudit FROM Equipment WHERE inbuilding=" . $curplayer->get("inbuilding") . " AND room=" . $curplayer->get("room") . " AND map=" .
                 $map . " AND  state=\"Ground\" AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= " . $gap, $db, 0, 0);
        
        while (! $dbequipment->eof()) {
            $xx = $dbequipment->get("x");
            $yy = $dbequipment->get("y");
            $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
            
            if ($dbequipment->get("name") != "") {
                $name = "<span class='" . ($dbequipment->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($dbequipment->get("name"));
                if ($distVal == 0) {
                    /*
                     * if($dbequipment->get("extraname")!="")
                     * {
                     * $tlevel=$dbequipment->get("templateLevel");
                     * $name.=" <span class='template'>".localize($dbequipment->get("extraname"))."(".$tlevel.")</span>";
                     * }
                     */
                    $name .= " (" . localize("Niveau") . " " . $dbequipment->get("level") . ")";
                }
                $name .= "</span>";
                
                $itemtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $name
                );
                $this->add_item($xx, $yy, $itemtmp);
            } else {
                $name = $dbequipment->get("po") . " " . localize("Pièce(s) d'Or");
                $itemtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $name
                );
                $this->add_gold($xx, $yy, $itemtmp);
            }
            
            $dbequipment->next();
        }
    }

    public function build_pj()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        $gap = 2;
        if ($curplayer->get("inbuilding") == 0 || $curplayer->get("room") == TOWER_ROOM)
            $gap = 5;
        
        $idteam = $curplayer->get("id_Team");
        $query = "SELECT
				p.id,p.name,p.id_Team,p.x,p.y,p.hp,p.currhp,
				p.level,p.racename,t.name as alliegance,
				ppj.type as dppj,
				po.type as dpo,
				opj.type as dopj,
				oo.type as doo
			FROM 
				Player as p
				LEFT JOIN Team as t ON t.id=p.id_Team
				LEFT JOIN PJAlliancePJ as ppj ON p.id=ppj.id_Player\$dest and ppj.id_Player\$src='$id'
				LEFT JOIN PJAlliance as po ON p.id_Team=po.id_Team and po.id_Player\$src='$id'
				LEFT JOIN TeamAlliancePJ as opj ON p.id=opj.id_Player and opj.id_Team\$src='$idteam'
				LEFT JOIN TeamAlliance as oo ON p.id_Team=oo.id_Team\$dest and oo.id_Team\$src='$idteam'
			WHERE
				map=" .
             $map . " AND (inbuilding=0 OR (inbuilding=" . $curplayer->get("inbuilding") . " AND room=" . $curplayer->get("room") . ")) AND state!='prison'  AND disabled=0 AND status='PC' 				
				AND(abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" .
             $gap . "
		
			GROUP BY p.id
			ORDER BY alliegance, level DESC";
        
        $dbplayer = new DBCollection($query, $db, 0, 0);
        
        $distVal = 0;
        while (! $dbplayer->eof()) {
            $xx = $dbplayer->get("x");
            $yy = $dbplayer->get("y");
            $opponent = new Player();
            $opponent->load($dbplayer->get("id"), $db);
            $hidden = $curplayer->canSeePlayer($opponent, $db);
            if (($hidden == 1) || ($hidden == 2)) {
                
                $level = $dbplayer->get("level");
                $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
                $id_Team = $dbplayer->get("id_Team");
                
                if (($value = $dbplayer->get("dppj")) != null) {
                    $style = "self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("dopj")) != null) {
                    $style = "guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $style = "stylepc";
                
                if (($value = $dbplayer->get("dpo")) != null) {
                    $gc = " self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("doo")) != null) {
                    $gc = " guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $gc = "styleall";
                
                $gc = ($id_Team == $idteam) ? "myalliegence" : $gc;

                $name  = '';
                if ($dbplayer->get("id") != $id) {
                    $name .= Envelope::render($dbplayer->get('name'));
                } else {
                    $name .= Envelope::renderEmptySize();
                }
                $name .= "<a href='../conquest/profile.php?id=" . $dbplayer->get("id") . "' class='$style popupify'>" .
                    $dbplayer->get("name") . "</a>";
                
                $alliegance = "<a href='../conquest/alliegance.php?id=$id_Team' class='$gc popupify'>" .
                     $dbplayer->get("alliegance") . "</a>";
                
                $pjtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "level" => $level,
                    "guildcolor" => $gc,
                    "dist" => $distVal,
                    "name" => $name,
                    "cleanName" => $dbplayer->get("name"),
                    "race" => $dbplayer->get("racename"),
                    "id" => $dbplayer->get("id"),
                    "alliegance" => $alliegance,
                    "id_Team" => $id_Team
                );
                $this->add_pj($xx, $yy, $pjtmp);
            }
            $dbplayer->next();
        }
    }

    function build_pnj()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $idb = $curplayer->get("inbuilding");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $this->pnj = array();
        $gap = 2;
        if ($curplayer->get("inbuilding") == 0 || $curplayer->get("room") == TOWER_ROOM)
            $gap = 5;
        
        $dbnpc = new DBCollection(
            "SELECT * FROM Player WHERE ((inbuilding=" . $idb . " AND room=" . $curplayer->get("room") . ") OR inbuilding=0) AND disabled=0 AND status='NPC' AND map=" . $map .
                 " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap . " order by level desc, racename", $db, 0, 0);
        $state = array(
            localize("Agonisant"),
            localize("Gravement blessé(e)"),
            localize("Blessé(e)"),
            localize("Légèrement blessé(e)"),
            ""
        );
        
        while (! $dbnpc->eof()) {
            if ($curplayer->canSeePlayerById($dbnpc->get("id"), $db) && ($curplayer->get("room") != 2 || $dbnpc->get("id_BasicRace") != 263)) {
                
                $opponent = new Player();
                $opponent->load($dbnpc->get("id"), $db);
                $hidden = $curplayer->canSeePlayer($opponent, $db);
                
                if ($dbnpc->get("id_Owner") != 0) {
                    $dbowner = new DBCollection("SELECT name FROM Player WHERE id=" . $dbnpc->get("id_Owner"), $db, 0, 0);
                    if (! $dbowner->eof()) {
                        $owner = " appartenant à <a href='../conquest/profile.php?id=" . $dbnpc->get("id_Owner") .
                             "' class='stylepc popupify'>" . $dbowner->get("name") . "</a>";
                    } else {
                        $owner = "";
                    }
                } else
                    $owner = "";
                
                if ($hidden) {
                    $hp = $dbnpc->get("hp");
                    $curstate = min(floor($dbnpc->get("currhp") * 5 / $hp), 4);
                    
                    $xx = $dbnpc->get("x");
                    $yy = $dbnpc->get("y");
                    $level = $dbnpc->get("level");
                    $distVal = (abs($xx - $xp) + abs($yy - $yp) + abs($xx + $yy - $xp - $yp)) / 2;
                    $comp = "";
                    
                    if ($dbnpc->get("name") != '') {
                        $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") . "\" class='stylenpc popupify'>" .
                            localize($dbnpc->get("name")) . "</a> (" . $dbnpc->get("id") . ")" . $comp;
                    } else {
                        $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") . "\" class='stylenpc popupify'>" .
                            localize($dbnpc->get("racename")) . "</a> (" . $dbnpc->get("id") . ")" . $comp;
                    }
                    
                    $pnjtmp = array(
                        "xx" => $xx,
                        "yy" => $yy,
                        "level" => $level,
                        "dist" => $distVal,
                        "name" => $name,
                        "id" => $dbnpc->get("id"),
                        "state" => $state[$curstate],
                        "id_BasicRace" => $dbnpc->get("id_BasicRace")
                    );
                    
                    $this->add_pnj($xx, $yy, $pnjtmp);
                }
            }
            $dbnpc->next();
        }
    }

    function add_portail($xx, $yy, $arr)
    {
        $this->add_village($xx, $yy, $arr);
    }

    function add_city($xx, $yy, $arr)
    {
        $this->add_village($xx, $yy, $arr);
    }

    function add_wall($xx, $yy, $arr)
    {
        $this->add_village($xx, $yy, $arr);
    }

    function add_deposit($xx, $yy, $arr)
    {
        $this->add_village($xx, $yy, $arr);
    }

    function add_bush($xx, $yy, $arr)
    {
        $this->add_village($xx, $yy, $arr);
    }

    function add_village($xx, $yy, $arr)
    {
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody'>" . $arr["name"] . "</td>\n" .
             "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }

    function add_place($xx, $yy, $arr)
    {
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody'>" . $arr["name"] . "</td>\n" .
             "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }

    function add_pnj($xx, $yy, $arr)
    {
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody' align='center'> " . $arr["level"] . " </td>\n" .
             "<td class='mainbgbody'>" . $arr["name"] . " " . $arr["state"] . "</td>\n" . "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" .
             "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }

    function add_item($xx, $yy, $arr)
    {
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody'>" . $arr["name"] . "</td>\n" .
             "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }

    function add_gold($xx, $yy, $arr)
    {
        $this->add_item($xx, $yy, $arr);
    }

    function add_pj($xx, $yy, $arr)
    {
        $this->list[$arr['id']] = $arr['cleanName'];
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody' align='center'> " . $arr["level"] . " </td>\n" .
             "<td class='mainbgbody'>" . $arr["name"] . " (" . $arr["race"] . ")</td>\n" . "<td class='" . $arr["guildcolor"] . " mainbgbody' align='left'> " . $arr["alliegance"] .
             " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }
}


