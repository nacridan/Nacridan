<?php

/**
 *Affiche le contenu du parchemin
 *
 * Définit une fonction par type de mission
 *
 *@author Nacridan
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open ( array (
		"sess" => "Session",
		"auth" => "Auth" 
) );

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");

$db = DB::getDB ();
$nacridan = new NacridanModule ( $sess, $auth, $db, 0 );

$lang = $auth->auth ['lang'];
Translation::init ( 'gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "" );

$curplayer = $nacridan->loadCurSessPlayer ( $db );
$prefix = "";

$MAIN_PAGE = new HTMLObject ( "html" );
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject ( "head" );

$MAIN_HEAD->add ( "<title>Nacridan</title>\n" );
$MAIN_HEAD->add ( "<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n" );
$MAIN_HEAD->add ( "<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n" );
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add ( "<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n" );

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject ( "body" );

$str = "<table><tr><td valign='top'>\n";
$str .= "<table class='maintable' width='620px'>\n";
$str .= "<tr>\n";
$str .= "<td colspan='2' class='mainbgtitle'><b>" . localize ( 'D E S C R I P T I O N' ) . "</b>\n";
$str .= "</td>\n";
$str .= "</tr>\n";

if (isset ( $_GET ["id"] )) {
	$id = quote_smart ( $_GET ["id"] );
	$dbBT = new DBCollection ( "SELECT * FROM BasicTalent WHERE id=" . $id, $db );
	$str = "<table class='maintable mainbgtitle' width='640px'>\n";
	$str .= "<tr><td>\n";
	$str .= "<b><h1>" . localize ( $dbBT->get ( 'name' ) ) . "</h1></b></td>\n";
	$str .= "</tr>\n";
	$str .= "</table>\n";
	$str .= "<table class='maintable mainbgtitle' width='640px'><tr>";
	$str .= "<td class='mainbglabel' width='640px'>";
	
	switch ($id) {
		
		case 1 :
			$str .= <<<EOF
Le savoir-faire MINER permet d'extraire des gemmes des différents gisements d'émeraude et de rubis affleurant à la surface de Nacridan.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 8 Points d'Action, en échange desquels, vous obtiendrez une gemme du niveau du gisement exploité.<br/><br/>

Les émeraudes de niveau 1 se revendent 10 pièces d'or dans les échoppes des villes et les rubis 20 PO mais vous pourrez peut-être en tirer davantage en les vendant à un autre personnage.<br/><br/>

Les émeraudes sont utilisées dans la fabrication des sceptres et des bâtons magiques et sont nécessaires pour tous les enchantements mineurs. Les rubis, eux, permettent de réaliser des enchantements majeurs.<br/><br/>

Les gemmes sont extraites des différents gisements affleurant à la surface de Nacridan.<br/>
Plusieurs sites d'émeraude ont été repérés sur l'île de Nacridan. Les plus connus sont ceux à l'Est d'Artasse au Sud-Est d'Earok et au Nord-Ouest de Tonak.<br/>
Les gisements de rubis sont plus rares et personne ne connait leur localisation exacte.<br/><br/>

Une <b>pioche de mineur</b> équipée avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir une gemme d'un niveau supérieur au gisement exploité. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
Résumé :
	Talent : <b>Miner (8 PA)</b><br/>
	Outil : <b>Pioche de mineur</b><br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 2 :
			$str .= <<<EOF
Le savoir-faire DEPECER permet d'extraire des peaux des dépouilles de certains monstres de Nacridan.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 3 Points d'Action, en échange desquels, vous obtiendrez une pièce de cuir ou une pièce d'écaille du niveau de la dépouille dépecée.<br/><br/>

Les pièces de cuir et d'écaille de niveau 1 se revendent 4 pièces d'or dans les échoppes des villes mais vous pourrez peut être en tirer davantage en les vendant à un autre personnage. <br/><br/>

Le cuir et les écailles servent dans la fabrication de toutes les pièces d'armures de cuir ou d'écaille comme la cuirasse, les bottes ou encore les casques.<br/><br/>

Une <b>pince à découper</b> équipée avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir une pièce d'un niveau supérieur à la dépouille exploitée. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>

Attention, aucun monstre de niveau faible ne laisse de dépouille permettant d'obtenir du cuir ou de l'écaille. Ce savoir-faire n'est donc peut-être pas un talent à apprendre dès le début de votre aventure pour bien la débuter.<br/><br/>

Résumé : <br/>

	Talent : <b>Dépecer (3 PA)</b><br/>
	Outil : <b>Pince à dépecer</b><br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 3 :
			$str .= <<<EOF
Le savoir-faire TEILLER permet de recolter les champs de lin. <br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 8 Points d'Action, en échange desquels, vous obtiendrez un morceau de lin du niveau du champ récolté. Le Lin se récolte dans des champs de lin.<br/><br/>

Une récolte de lin de niveau 1 se revend 4 pièces d'or dans les échoppes des villes mais vous pourrez peut être en tirer davantage en les vendant à un autre personnage. <br/><br/>

Le lin est nécessaire dans la création de toutes les pièces d'armures magiques, du diadème aux bottes de lins en passant par la toge magique.<br/><br/>

Au moins trois zones sont réputées pour la culture du lin sur l'île : à l'Est d'Artasse, à l'Ouest d'Earok et le long de la cote, au Nord-Ouest de Tonak.<br/><br/>

Une <b>teilleuse</b> équipée avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir une pièce d'un niveau supérieur au champ exploité. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
Résumé : 
	Talent : <b>Teiller (8 PA)</b><br/>
	Outil : <b>Teilleuse</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			
			break;
		
		case 4 :
			$str .= <<<EOF
Le savoir-faire RECOLTER permet de recueillir des Feuilles, Graines ou Racines des différents buissons sauvages de Nacridan.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 5 Points d'Action, en échange desquels, vous obtiendrez une botte d'herbe du niveau du buisson correspondant.<br/><br/>

Le fruit de la récolte se revend 2 pièces d'or dans les échoppes des villes mais vous pourrez peut être en tirer davantage en les vendant à un autre personnage.<br/><br/>

On trouve trois types de buissons sauvages:<br><ul>
<li>Le Rhazot dont on extrait les racines</li>
<li>La Garnach dont on recueille les graines </li>
<li>La Folliane dont on utilise les feuilles</li>
</ul><br/>		
Ces buissons sont répartis à peu près partout sur l'île de Nacridan mais leur présence est toutefois plus riche en forêt que dans le desert. Des zones particulièrement luxuriantes ont d'ailleurs été repérées au Sud-Est d'Earok, au Nord-Est d'Artasse et juste à la sortie de Tonak, plein Nord.<br/><br/>

Les herbes sont utilisées dans la confection de potions. Les racines de Razhot permettent de fabriquer des potions de vie, les graines de Garnach permettent de fabriquer des potions de force et de dextérité tandis que les feuilles de Folianne permettent de confectionner des potions de magie et de vitesse.<br/><br/>

Un <b>pressoir</b> équipé avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir une botte d'herbe d'un niveau supérieur au buisson exploité. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
Résumé : 
	Talent : <b>Récolter (5 PA)</b><br/>
	Outil : <b>Pressoir</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 5 :
			$str .= <<<EOF
Le savoir-faire DIALECTE KRADJECKIEN permet de parler et négocier avec des Kradjeck Ferreux qui de ce qu'on sait, empruntent les portails des monstres pour venir visiter notre île. <br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 3 Points d'Action, en échange desquels, vous obtiendrez un morceau de Fer d'un niveau en rapport avec le niveau du Kradjeck Ferreux. Une fois sur deux, le Kradjeck Ferreux repart dans son monde après avoir délivré un fer.<br/><br/>

Les morceaux de fer de niveau 1 se revendent 4 pièces d'or dans les échoppes des villes mais vous pourrez peut être en tirer davantage en les vendant à un autre personnage.<br/><br/>

Le fer est nécessaire pour la fabrication de toutes les pièces d'armure de plate ainsi que de toutes les armes.<br/><br/>

On peut trouver des Kradjeck Ferreux partout à la surface de Nacridan.<br/><br/>

Une <b>gemme de conviction</b> équipée avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir un morceau de fer d'un niveau supérieur. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
Résumé : 
	Talent : <b>Dialecte Kradjeckien (6 PA)</b><br/>
	Outil : <b>Gemme de Conviction</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 6 :
			$str .= <<<EOF
Le savoir-faire COUPER DU BOIS permet de couper du bois.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 70%. Chaque utilisation réussie vous coûtera 5 Points d'Action, en échange desquels, vous obtiendrez un morceau de bois d'un niveau dépendant de l'environnement dans lequel il est récolté. Contrairement aux autres ressources, il ne faut pas se trouver à coté d'une source particulière pour le récolter : le Bois peut être coupé presque partout.<br/><br/>

Les morceaux de bois de niveau 1 se revendent 2 pièces d'or dans les échoppes des villes mais vous pourrez peut être en tirer davantage en les vendant à un autre personnage.<br/><br/>

Le Bois est utilisé dans la confection des arcs et des flèches mais aussi pour fabriquer des boucliers de taille moyenne.<br/><br/>

Le Bois peut être coupé presque partout mais sa qualité varie en fonction du lieu, sachant que les déserts, les routes, les marécages et les montagnes y sont peu propices.<br/><br/>

Un <b>cauchoir de bucheron</b> équipé avant l'utilisation du savoir-faire donnera un bonus dans les chances d'obtenir un morceau de bois d'un niveau supérieur. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.
<br/><br/>
Résumé : 
	Talent : <b>Couper du Bois (5 PA)</b><br/>
	Outil : <b>Cauchoir de bucheron</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 7 :
			$str .= <<<EOF
L'Artisanat du Fer permet de créer les pièces d'armure de plates ainsi que toutes les armes en fer à partir d'un morceau de fer.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Force</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant des <b>tenailles d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat du Fer (8 PA, For)</b><br/>
	Outil : <b>Tenaille d'artisan</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 8 :
			$str .= <<<EOF
L'Artisanat du Bois permet de créer toutes les pièces d'équipement en bois: arcs, flèches et petit bouclier.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Dextérité</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant une <b>rape d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat du Bois (8 PA, Dex)</b>
	Outil : <b>Râpe d'artisan</b></br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 9 :
			$str .= <<<EOF
L'Artisanat du Cuir permet de créer toutes les pièces d'équipement en cuir, y compris les sacs.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Dextérité</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant une <b>alène d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat du Cuir (8 PA, Dex)</b></br/>
	Outil : <b>Alène d'artisan</b><br/></br/>
Régles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 10 :
			$str .= <<<EOF
L'Artisanat des Ecailles permet de créer toutes les pièces d'armure en écailles.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Vitesse</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant un <b>plioir d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat des Ecailles (8 PA, Dex)</b></br/>
	Outil : <b>Plioir d'artisan</b><br/></br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 11 :
			$str .= <<<EOF
L'Artisanat du Lin permet de créer tous les objets magiques en lin.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Maitrise de la Magie</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant des <b>ciseaux d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat du Lin (8 PA, MM)</b></br/>
	Outil : <b>Ciseaux d'artisan</b></br/></br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 12 :
			$str .= <<<EOF
L'Artisanat des Plantes permet de créer des potions à partir de plantes.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur votre <b>Vitesse</b> et votre <b>Maîtrise Magique</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant un <b>alambic d'artisan</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat des Plantes (8 PA, MM+Vit)</b></br/>
	Outil : <b>Alambic d'artisan</b></br/></br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 13 :
			$str .= <<<EOF
L'Artisanat des Gemmes permet de créer tous les enchantements ainsi que des sceptres et des batons magiques. Les Emeraudes permettent de réaliser des enchantements mineurs tandis que les Rubis servent pour les enchantements majeurs.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 Points d'Action.<br/><br/>

Le niveau de l'équipement créé est limité par le niveau de la matière première utilisée et par votre jet de maitrise d'artisanat basé sur la <b>Maitrise de la Magie</b>.<br/><br/>

Il peut être nécessaire d'utiliser plusieurs fois le savoir-faire pour achever un équipement. Plus le niveau de l'équipement que vous voulez créer est élevé et plus l'avancement dans sa confection sera lente et plus vous devrez réitérer l'utilisation du savoir-faire pour achever l'objet.<br/><br/>

En équipant un <b>cristal d'enchantement</b> avant l'utilisation du savoir-faire, vous obtiendrez un bonus dans la vitesse de fabrication de l'équipement que vous souhaitez créer. Ce bonus dépend du niveau de l'outil utilisé et du niveau de la pièce d'équipement confectionné.<br/><br/>

Enfin, en vous rendant dans une guilde des artisans et en payant 5 pièces d'or, vous pourrez utilisez les outils à disposition et obtenir un bonus sur votre jet de maitrise du savoir-faire.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Artisanat des Gemmes (8 PA, MM)</b></br/>
	Outil : <b>Cristal d'enchantement</b></br/></br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 14 :
			$str .= <<<EOF
FERRONNERIE est le savoir-faire de raffinage du FER.<br/>
A partir de deux morceaux de fer, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'un <b>marteau de ferronnier</b> octroie un bonus lors d'une tentative de Ferronnerie. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Ferronnerie (5 PA)</b><br/>
	Outil : <b>Marteau de ferronnier</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 15 :
			$str .= <<<EOF
TANNER est le savoir-faire de raffinage du CUIR.<br/>
A partir de deux pièces de cuir, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'une <b>griffe à lacer de tanneur</b> octroie un bonus lors d'une tentative de Tanner. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Tanner (5 PA)</b><br/>
	Outil : <b>Griffe à lacer de tanneur </b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 16 :
			$str .= <<<EOF
ECAILLER est le savoir-faire de raffinage des ECAILLES.<br/>
A partir de deux écailles, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'un <b>abat-carre d'écailleur</b> octroie un bonus lors d'une tentative d'Ecailler. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Ecailler (5 PA)</b><br/>
	Outil : <b>Abat-carre d'écailleur</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 17 :
			$str .= <<<EOF
EFFILER est le savoir-faire de raffinage du LIN.<br/>
A partir de deux pièces de lin, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'une <b>roue à molette d'effileur</b> octroie un bonus lors d'une tentative d'Effiler. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Effiler (5 PA)</b><br/>
	Outil : <b>Roue à molette d'effileur</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 18 :
			$str .= <<<EOF
TAILLER est le savoir-faire de raffinage des EMERAUDES et RUBIS.<br/>
A partir de deux émeraudes ou deux rubis, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'un <b>burin de tailleur</b> octroie un bonus lors d'une tentative de Tailler. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Tailler (5 PA)</b><br/>
	Outil : <b>Burin de tailleur</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 19 :
			$str .= <<<EOF
FILTRER est le savoir-faire de raffinage des HERBES.<br/>
A partir de deux plantes de même type, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'un <b>filtre d'alchimiste</b> octroie un bonus lors d'une tentative de Filtrer. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Filtrer (5 PA)</b><br/>
	Outil : <b>Filtre d'alchimiste</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 20 :
			$str .= <<<EOF
CHARPENTERIE est le savoir-faire de raffinage du BOIS.<br/>
A partir de deux morceaux de bois, il permet de créer une pièce de deux niveaux supérieurs au niveau le plus élevé des deux pièces de départ.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 Points d'Action.<br/><br/>
	 
Réussir le jet de maitrise du savoir-faire n'est pas suffisant pour confectionner une pièce de niveau supérieur, il faut ensuite réussir un nouveau jet dont la difficulté dépend du niveau de la pièce la plus élevée et de l'écart entre les niveaux des deux pièces utilisées.<br/><br/>

L'utilisation d'une <b>chignole de charpentier</b> octroie un bonus lors d'une tentative de Charpenterie. Plus le niveau de l'outil utilisé est élevé, plus le bonus est important.<br/><br/>
<br/>
Résumé : 
	Talent : <b>Charpenterie (5 PA)</b><br/>
	Outil : <b>Chignole de charpentier</b><br/><br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Les Talents d_Artisanat'>Les savoir-faire.</a></br/>
EOF;
			break;
		
		case 21 :
			$str .= <<<EOF
APPEL DU KRADJECK est un talent de la Nature qui permet d'appeler un Kradjeck par delà un portail en vue. Si le talent est réussi, le Kradjek apparaitra par le portail.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 2 Points d'Action.<br/><br/>
	 
<br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Talents de la Nature'>Les Talents de la Nature.</a></br/>
EOF;
			break;
		
		case 22 :
			$str .= <<<EOF
DETECTION DES RESSOURCES est un talent de la Nature qui permet de repérer la direction et la distance approximative d'une ressource d'un type et d'un niveau donné. Ce talent ne fonctionne qu'en decà d'une certaine distance et la direction n'est donnée que si la ressource est suffisamment proche.  <br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 2 Points d'Action.<br/><br/>
	 
<br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Talents de la Nature'>Les Talents de la Nature.</a></br/>
EOF;
			break;
		
		case 23 :
			$str .= <<<EOF
CONNAISSANCE DES MONSTRES est un talent de la Nature qui permet d'obtenir une estimation de certaines caractéristiques d'un monstre en vue. En plus du nombre de points de vie courants du monstre, ce talent permet d'obtenir deux caractéristiques de combat, au choix, parmi l'attaque, la défense, les dégâts, l'armure et la maitrise de la magie.<br/><br/>

Après apprentissage de ce savoir-faire, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 2 Points d'Action.<br/><br/>
	 
<br/>
Règles : <a href='../i18n/rules/fr/rules.php?page=step11#Talents de la Nature'>Les Talents de la Nature.</a></br/>
EOF;
			break;
		
		default :
			$str . " En cours";
			break;
	}
	$str .= "</td></tr>\n";
	$MAIN_BODY->add ( $str );
	$MAIN_PAGE->render ();
}
 
