<?php

/**
 *Affiche le contenu du parchemin
 *
 * Définit une fonction par type de mission
 *
 *@author Nacridan
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");

$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<table><tr><td valign='top'>\n";
$str .= "<table class='maintable' width='620px'>\n";
$str .= "<tr>\n";
$str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
$str .= "</td>\n";
$str .= "</tr>\n";

if (isset($_GET["id"])) {
    $id = quote_smart($_GET["id"]);
    $dbBT = new DBCollection("SELECT * FROM BasicAbility WHERE id=" . $id, $db);
    $str = "<table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    $str .= "<b><h1>" . localize($dbBT->get('name')) . "</h1></b></td>\n";
    $str .= "</tr>\n";
    $str .= "</table>\n";
    $str .= "<table class='maintable mainbgtitle' width='640px'><tr>";
    $str .= "<td class='mainbglabel' width='640px'>";
    switch ($id) {
        
        case 1:
            $str .= <<<EOF
ATTAQUE PUISSANTE est une compétence de niveau aspirant de l'école de la Garde d'Octobian. C'est une attaque au contact qui permet d'augmenter le score d'Attaque du personnage en contre partie d'un malus à ses dégâts.<br/><br/>

Cette compétence est basée sur votre Force. Plus elle sera élevée et plus le bonus en Attaque sera important.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Garde d_Octobian'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            break;
        
        case 2:
            $str .= <<<EOF
GARDE est une compétence de base. Elle permet au combattant d'augmenter temporairement son score de Défense, jusqu'à l'activation de sa prochaine DLA.<br/><br/>

Cette compétence est basée sur votre Vitesse. Plus elle sera élevée et plus le bonus en Défense sera important.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Compétences de Base'>Les compétences de base.</a></br/>
EOF;
            break;
        
        case 3:
            $str .= <<<EOF
PREMIERS SOINS est une compétence de base. Elle permet au combattant de guérir la créature ciblée dans la limite de 75% des points de vie maximum de la cible.<br/><br/>

Cette compétence est basée sur votre Dextérité. Plus elle sera élevée et plus les soins seront importants.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Compétences de Base'>Les compétences de base.</a></br/>
EOF;
            break;
        
        case 4:
            $str .= <<<EOF
DEGATS ACCRUS est une compétence de base. C'est une attaque au contact qui permet d'augmenter les dégâts infligés à votre cible en contre partie d'un malus sur votre jet d'Attaque.<br/>
A réserver sur les cibles que vous toucher facilement, dégâts accrus est la compétence idéale pour viser les records de dégât.<br/>
Son utlisation coûte le même nombre de PA qu'une attaque classique soit 7,8 ou 9pa selon le type d'arme que vous portez.<br/>
L'utilisation de dégât accrus fera davantage progresser votre force qu'une attaque classique.<br/><br/> 
Après apprentissage de cette compétence, vous la maitriserez à 50%. Lorsque vous la maitrisez à 80%, vous serez en mesure d'apprendre n'importe quelle compétence de niveau 1.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Compétences de Base'>Les compétences de base.</a></br/>
EOF;
            break;
        
        case 5:
            $str .= <<<EOF
TOURNOIEMENT est une compétence de niveau aspirant de l'école de la Garde d'Octobian. Elle permet de réaliser une attaque tournoyante visant toutes les cibles adjacentes au combattant, au prix de dégâts moindres en comparaison d'une attaque normale.<br/><br/>

Cette compétence est basée sur votre Force et votre Dextérité. Plus celles-ci seront élevées et plus les dégâts seront importants.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque+1 fonction de la catégorie d'arme équipée.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Garde d_Octobian'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            break;
        
        case 6:
            $str .= <<<EOF
AIGUISE LAME est une compétence qui vous permet d'augmenter les dégâts que procure une arme pour la prochaine fois ou vous vous en servez.
Cette compétence est particulièrement utile pour les personnages se battant avec une arme légère car elle peut être cumulée sur un même tour juste avant une attaque. 
Basé sur la rapidité de son utilisateur, cette compétence vous fera progresser en vitesse <br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step6#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 7:
            $str .= <<<EOF
ATTAQUE PERCANTE est une compétence de l'école de la Garde D'Octobian. Elle permet d'ignorer une partie de l'armure de son adversaire.<br/><br/>
Cette compétence est basée sur votre Dextérité et votre Force. Plus celles-ci seront élevées et plus la compétence sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step6#Ecole de l_Ombre'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            
            break;
        
        case 8:
            $str .= <<<EOF
LANCER DE BOLAS est une compétence de niveau aspirant de l'école de l'Ombre. Elle permet d'attaquer sa cible à une distance de deux cases au moyen d'un bolas. Le lanceur doit auparavant être equipé d'un bolas qui est une arme légère. Si l'attaque est réussie, la cible se retrouve à terre lui confèrant un malus de 30% sur toutes ses compétences de base. Elle ne peut alors plus se déplacer normalement mais doit ramper avec un coût de déplacement fixé à 5 PA, ou choisir de se libérer pour 8 PA. Si cette dernière se libère le bolas est laissé au sol.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le lancer sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 9:
            $str .= <<<EOF
COUP ASSOMMANT est une compétence de niveau 2 de l'école de la Garde d'Octobian. Elle permet au combattant d'étourdir la créature ciblée ce qui diminue sa Vitesse.<br/><br/>

Cette compétence est basée sur votre Force et votre Dextérité. Plus celles-ci seront élevées et plus le coup sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Garde d_Octobian'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            break;
        
        case 10:
            $str .= <<<EOF
COUP DE GRACE est une compétence de niveau 2 de l'école de la Garde d'Octobian. Elle permet au combattant de délivrer un Coup de Grâce infligeant à sa cible une blessure non guérissable par des moyens normaux. Avant de pouvoir être soignée la cible devra bénéficier du sortilège Souffle d'Athlan.<br/><br/>

Cette compétence est basée sur votre Force et votre Dextérité. Plus celles-ci seront élevées et plus le coup sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Garde d_Octobian'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            break;
        
        case 11:
            $str .= <<<EOF
DESARMER est une compétence de niveau 2 de l'école de l'Ombre. Elle permet de désarmer sa cible, sans lui infliger aucun dégât, mais l'obligeant à lâcher son arme qui tombe alors au sol.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus l'action sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 12:
            $str .= <<<EOF
AUTOREGENERATION est une compétence de niveau aspirant de l'école des Paladins de Tonak. En utilisant cette compétence, le Paladin active sa régénération ce qui lui redonnera quelques points de vie en contre-partie d'un peu de sa Force pendant 2 DLA.<br/><br/>

Cette compétence est basée sur votre Force et vos Points de Vie. Plus ceux-ci seront élevés et plus l'action sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 10 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 13:
            $str .= <<<EOF
APPEL DE LA LUMIERE est une compétence de niveau aspirant de l'école des Paladins de Tonak. En utilisant cette compétence, une vague de lumière enveloppe le Paladin et sa cible. A l'intérieur de cette aura les mouvements du Paladin sont rendus plus aisés ce qui lui permet de réaliser une attaque normale basée sur sa Maitrise de Magie plutot que sur sa Dextérité. Cette attaque sera particulièrement efficace contre les démons et les mort-vivants. <br/><br/>

Cette compétence est basée sur votre Force et votre Maitrise de la Magie. Plus celles-ci seront élevées et plus l'attaque sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 14:
            $str .= <<<EOF
AURA DE COURAGE est une compétence de niveau 2 de l'école des Paladins de Tonak. Tant qu'elle reste active, l'Aura de Courage confert un bonus d'Attaque à chaque membre d'un Groupe de Chasse, en contre-partie duquel le Paladin subit une perte de Points de Vie à chaque DLA. Le Paladin peut désactiver l'Aura de Courage gratuitement et à tout moment. <br/><br/>

Cette compétence est basée uniquement sur vos Points de Vie. Plus ceux-ci seront élevés et plus le bonus sera important.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'actions.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 15:
            $str .= <<<EOF
AURA DE RESISTANCE est une compétence de niveau 2 de l'école des Paladins de Tonak. Tant qu'elle reste active, l'Aura de Résistance confert un bonus d'Armure à chaque membre d'un Groupe de Chasse, en contre-partie duquel le Paladin subit un malus de Force. Le Paladin peut désactiver l'Aura de Résistance gratuitement et à tout moment.<br/><br/>

Cette compétence est basée sur votre Force et votre Maitrise de la Magie. Plus celles-ci seront élevées et plus le bonus sera important.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'actions.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 16:
            $str .= <<<EOF
EXORCISME DE L'OMBRE est une compétence de niveau 2 de l'école des Paladins de Tonak. En effectuant un Exorcisme de l'Ombre, le Paladin renvoie à ses adversaires encore dans sa vue une partie des dégâts que ceux-ci lui ont infligé au cours des 3 dernières DLA.<br/><br/>

Cette compétence est basée sur vos Points de Vie et votre Maitrise de la Magie. Plus ceux-ci seront élevés et plus l'exorcisme sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'actions.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 17:
            $str .= <<<EOF
VOL A LA TIRE est une compétence de niveau aspirant de l'école de l'Ombre. Elle permet de délester sa victime d'une grande partie de son or, si vous ne vous faites pas prendre par les gardes.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le vol sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 18:
            $str .= <<<EOF
TIR LOINTAIN est une compétence de niveau aspirant de l'école des Archers de Krima. Elle permet de tirer une flèche à une portée de 4 cases. Le malus de distance reste identique à celui d'un tir à 3 cases soit 20%. Cette compétence nécessite d'avoir un Arc équipé ainsi que des flèches dans son équipement.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le tir sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque+1 fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 19:
            $str .= <<<EOF
TIR GENANT est une compétence de niveau aspirant de l'école des Archers de Krima. Elle permet de gêner le déplacement d'un adversaire en lui tirant dans les jambes. La cible verra alors le coût de ses déplacements augmenté de 1 pour la DLA en cours et la suivante. Cette compétence nécessite d'avoir un Arc équipé ainsi que des flèches dans son équipement.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le tir sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 20:
            $str .= <<<EOF
FLECHE DE NEGATION est une compétence de niveau aspirant de l'école des Archers de Krima. Elle permet de tirer une flèche enchantée diminuant la Maitrise de Magie de la cible pour la DLA en cours et la suivante. De plus la flèche inflige des dégâts normaux à la cible. Cette compétence nécessite d'avoir un Arc équipé ainsi que des flèches dans son équipement.<br/><br/>

Cette compétence est basée sur votre Dextérité, votre Vitesse et votre Maitrise de la Magie. Plus celles-ci seront élevées et plus le tir sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 21:
            $str .= <<<EOF
COURSE CELESTE est une compétence de niveau 2 de l'école des Archers de Krima. Elle permet de fixer à 1 le coût de déplacement de l'archer, quelque soit le terrain, pour la DLA en cours ainsi que la suivante.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 2 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 22:
            $str .= <<<EOF
FLECHE ENFLAMMEE est une compétence de niveau 2 de l'école des Archers de Krima. Elle permet de tirer une flèche enflammée uniquement sur un batiment pour le mettre en feu et lui infliger des dégâts supplémentaires. Cette compétence nécessite d'avoir un Arc équipé ainsi que des flèches dans son équipement.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le tir sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 23:
            $str .= <<<EOF
LARCIN est une compétence de niveau 2 de l'école de l'Ombre. Elle permet, si l'action passe la défense de la cible, de faire en sorte qu'un des objets, au hasard, présents dans son inventaire tombe au sol si elle en vient à succomber lors de la DLA en cours.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus la compétence sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 24:
            $str .= <<<EOF
PROTECTION est une compétence de niveau aspirant de l'école des Paladins de Tonak. Jusqu'à sa désactivation, une Protection réussie permettra au Paladin de subir <b>la plupart</b> des attaques dirigées contre la cible protégée. En s'interposant le Paladin accepte de ne pas se défendre contre les coups de son attaquant, mais sa foi lui octroie alors un bonus magique d'armure. Il n'est possible de s'interposer que si la cible à protéger se trouve sur une case adjacente.<br/><br/>

Cette compétence est basée sur votre Maitrise de la Magie et vos Points de Vie. Plus ceux-ci seront élevés et plus l'action sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Paladins de Tonak'>L'Ecole des Paladins de Tonak.</a></br/>
EOF;
            break;
        
        case 25:
            $str .= <<<EOF
PROJECTION est une compétence de niveau 2 de l'école de la Garde d'Octobian. Elle permet au combattant de délivrer une attaque de Projection portant un coup et éjectant sa cible à deux cases à l'opposé de lui.<br/><br/>

Cette compétence est basée sur votre Force et votre Dextérité. Plus celles-ci seront élevées et plus le coup sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Garde d_Octobian'>L'Ecole de la Garde d'Octobian.</a></br/>
EOF;
            break;
        
        case 26:
            $str .= <<<EOF
VOLEE DE FLECHES est une compétence de niveau 2 de l'école des Archers de Krima. Elle permet de tirer une volée de flèches en enchainant trois tirs successifs sur trois cibles potentiellement différentes. Cette compétence nécessite d'avoir un Arc équipé ainsi que des flèches dans son équipement.<br/><br/>

Cette compétence est basée sur votre Dextérité et votre Vitesse. Plus celles-ci seront élevées et plus le tir sera efficace.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque+1 fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Archers de Krima'>L'Ecole des Archers de Krima.</a></br/>
EOF;
            break;
        
        case 27:
            $str .= <<<EOF
EMBUSCADE est une compétence de niveau 2 de l'école de l'Ombre. Elle permet au personnage de devenir invisible aux yeux de ceux qui ne se trouvaient pas à 5 cases ou moins de lui à ce moment. Il peut alors se déplacer pour 5 PA tout en restant invisible. Cependant, toute autre action de sa part le fera réapparaitre.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Ecole de l_Ombre'>L'Ecole de l'Ombre.</a></br/>
EOF;
            break;
        
        case 28:
            $str .= <<<EOF
BOTTE D'ESTOC est une compétence de base. Elle permet au combattant de surprendre son adversaire en le prenant de court. Pour effectuer une botte d'estoc, il est nécessaire d'avoir une griffe équipée dans sa main gauche.<br/>
Il est à noter que vous trouvez les griffes dans le rayon armure dans les échoppes.<br/><br/>

Cette compétence est basée sur votre Vitesse. Plus elle sera élevée et plus le bonus aux dégâts sera important.<br/><br/>

Après apprentissage de cette compétence, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera un Coût d'Attaque fonction de la catégorie d'arme équipée.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step7#Compétences de Base'>Les compétences de base.</a></br/>
EOF;
            break;
        
        default:
            $str .= "En cours";
            break;
    }
    $str .= "</td></tr>\n";
    $MAIN_BODY->add($str);
    $MAIN_PAGE->render();
}
 
