<?php

class CQArchive extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQArchive($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_POST["Reply"])) {
            $err = $this->reply($db);
        }
        
        if (isset($_POST["ReplyToAll"])) {
            $err = $this->replyToAll($db);
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = "";
        
        if (isset($_GET["archive"])) {
            $repertoryId = $_GET["archive"];
            $idRepertory = $repertoryId;
        } else {
            $dbrep1 = new DBCollection("SELECT id FROM NameRepertory WHERE id_Player=" . $id . " order by name_Repertory asc ", $db);
            if ($dbrep1->count() == 0) {
                $repertoryId = - 1;
                $idRepertory = - 1;
            } else {
                $repertoryId = $dbrep1->get("id");
                $idRepertory = $repertoryId;
            }
        }
        
        if (isset($_POST["Important"])) {
            $err = $this->importantMail($db);
        }
        
        if (isset($_GET["priority"])) {
            $err = $this->importantMail($db);
        }
        
        if (isset($_GET["delete"])) {
            $err = $this->deleteMail($db);
        }
        
        if (isset($_POST["Del"])) {
            $err = $this->deleteMail($db);
        }
        
        if (isset($_POST["FlagRead"])) {
            $err = $this->FlagReadMail($db);
        }
        
        if (isset($_POST["Move"])) {
            $move = 1;
            $mv = $this->getCheckedDelMailCond("id");
        } else {
            $move = 0;
            $mv = "";
        }
        
        if (isset($_POST["Shift"])) {
            if ($_POST["CHOOSE_REPERTORY_ID"] == 0) // on vérifie qu'un répertoire à bien été sélectionner
                $err = localize("Aucun répertoire sélectionné. Veuillez en choisir un dans la liste.");
            else {
                $dbr = new DBCollection("SELECT * FROM NameRepertory WHERE id = " . $_POST["CHOOSE_REPERTORY_ID"] . " and id_Player=" . $id, $db);
                $norep = $dbr->count();
                if ($norep == 0) // on vérifie qu'un répertoire à bien été créé
                    $err = localize("Ce répertoire n'existe pas. Veuillez utiliser un existant.");
                else {
                    if (! isset($_POST["select"]) || $_POST["select"] == "") {
                        $err = localize("Veuillez sélectionner au moins un message.");
                    } else {
                        $playerid = $curplayer->get("id");
                        $err = $this->moveRepertory($_POST["select"], $playerid, $db);
                        $err = localize("Message(s) déplacé(s) avec succès vers un autre répertoire.");
                    }
                }
            }
        }
        
        if (isset($_POST["DeleteRepertory"]))
            $del = 1;
        else
            $del = 0;
            
            // Si post continu alors le répertoire sélectionner par le joueur sera supprimé
        if (isset($_POST["Continue"])) {
            $repertoryId = $_POST["action"];
            if ($repertoryId == 0)
                $err = localize("Aucun répertoire sélectionner. Veuillez en choisir un dans la liste.");
            else {
                $dbrep = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $id . " AND id=" . $repertoryId, $db);
                $repId = $dbrep->get("id");
                $dba = new DBCollection("SELECT * FROM MailArchive WHERE id_repertory='$repId' AND id_Player=" . $id . " ", $db);
                while (! $dba->eof()) {
                    $deleteRep = $dba->get("id");
                    $dba->next();
                    $dbd = new DBCollection("DELETE FROM MailArchive WHERE id_repertory='$deleteRep' AND id_Player=" . $id . " ", $db,0,0,false);
                }
                $dbdrep = new DBCollection("DELETE FROM NameRepertory WHERE id='$repId' AND id_Player=" . $id . " ", $db,0,0,false);
                $err = localize("Répertoire effacé avec succès.");
            }
        } elseif (isset($_POST["Back"])) // si non on retourne sur la méssagerie
            $del = 0;
            
            // On test le post "créer un répertoire" et si le nom du répertoire est non null
        if (isset($_POST["CreateRepertory"]) && $_POST["nameRepertory"] != NULL) {
            if ($_POST["nameRepertory"] == NULL) // on vérifie qu'un nom de répertoire à bien été entrer
                $err = localize("!! Aucun nom définit.");
            
            else {
                $dbr = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $id . " and name_Repertory='" . quote_smart($_POST["nameRepertory"]) . "'", $db);
                if ($dbr->count() > 0) { // on vérifie qu'un nom de répertoire soit libre
                    $err = localize("Ce nom existe déjà. Entrez-en un nouveau.");
                } else {
                    $nameRep = new NameRepertory();
                    $nameRep->set('name_Repertory', $_POST["nameRepertory"]);
                    $nameRep->set('id_Player', $id);
                    $nameRep->set("date", gmdate("Y-m-d h:m:s"));
                    $nameRep->updateDB($db);
                    $err = localize("Répertoire créé avec succès.");
                }
            }
        }
        
        $target = "/conquest/conquest.php?center=archive&archive=" . $repertoryId;
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=search' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "</form>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span> \n";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= " </table>";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        
        $dbrep = new DBCollection("SELECT id,id_Player,name_Repertory FROM NameRepertory WHERE id_Player=" . $id . " order by name_Repertory asc", $db);
        
        $dba = new DBCollection("SELECT * FROM MailArchive", $db);
        $count = $dba->count();
        
        if ($dbrep->count() > 0 && $idRepertory != - 2) {
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbgtitle'><b>" . localize('Liste des répertoires que vous avez créés:') . "</b>\n";
            $str .= "</td></tr>\n";
            $str .= "<tr><td class='mainbgtitle'>";
            while (! $dbrep->eof()) {
                $str .= "<a href='../conquest/conquest.php?center=archive&archive=" . $dbrep->get("id") . "' class='tabmenu'>" . $dbrep->get("name_Repertory") . "</a>|\n";
                $dbrep->next();
            }
            $str .= "<a href='../conquest/conquest.php?center=archive&archive=-2' class='tabmenu'>Configuration des Archives</a>|\n";
            $str .= "</td></tr>\n";
            $str .= "</table>";
            
            if ($move == 0) {
                $str .= "<table class='maintable centerareawidth'>";
                $str .= "<tr>";
                $str .= "<td class='mainbglabel' width='30px' align='center'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
                $str .= "<td class='mainbglabel' width='165px' align='center'>" . localize('Destinataire(s)') . "</td>\n";
                $str .= "<td class='mainbglabel' width='150px' align='center'>" . localize('Expéditeur') . "</td>\n";
                $str .= "<td class='mainbglabel' width='60px'  align='center'>" . localize('Non lu') . "</td>\n";
                $str .= "<td class='mainbglabel' width='75px' align='center'>" . localize('Important') . "</td>\n";
                $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Titre') . ": </td>\n";
                $str .= "<td class='mainbglabel' width='140px' align='center'>" . localize('Date') . ": </td>\n";
                $str .= "</tr>";
                
                $dbm = new DBCollection(
                    "SELECT MailArchive.id AS idMailArchive,MailArchive.id_MailBody,MailBody.title,MailBody.recipient,
			MailArchive.new,MailArchive.id_Player\$receiver,MailArchive.id_Player,MailArchive.id_repertory,MailArchive.date,MailArchive.important,
			Player.name, Player.racename, Player.id AS idplayer FROM MailArchive LEFT JOIN Player ON id_Player\$sender=Player.id 
			LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE id_repertory=" . $idRepertory . " AND MailArchive.id_Player=" . $id . " ORDER BY date DESC", $db); // AND MailArchive.id_Player\$receiver=".$id."
                $NoMailArchive = $dbm->count();
                if ($NoMailArchive > 0) {
                    while (! $dbm->eof()) {
                        $id = $dbm->get("idMailArchive");
                        
                        $time = gmstrtotime($dbm->get("date"));
                        $date = date("Y-m-d H:i:s", $time);
                        
                        $player = "";
                        if ($dbm->get("name") != "")
                            $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                        else
                            $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") . "</a>";
                        
                        $title = "";
                        if ($dbm->get("title") != "")
                            $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("id_MailBody") . "&mail=3\" class='none popupify'>" . $dbm->get("title") . "</a>";
                        
                        if ($dbm->get("important") == "1")
                            $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                        else
                            $important = "";
                        
                        if ($dbm->get("new") == "1")
                            $new = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                        else
                            $new = "";
                        
                        $str .= "<tr>";
                        $str .= "<td class='mainbgtitle' width='30px' align='center'><input name='check[]' type='checkbox' value='" . $dbm->get('idMailArchive') . "'/>\n</td>";
                        $str .= "<td class='mainbgtitle stylepc' width='165px' align='center'>" . $dbm->get("recipient") . "</td>\n";
                        $str .= "<td class='mainbgtitle stylepc' width='150px' align='center'>" . $player . "</td>\n";
                        $str .= "<td class='mainbgtitle' width='60px'  align='center'>" . $new . "</td>\n";
                        $str .= "<td class='mainbgtitle' width='80px'  align='center'>" . $important . "</td>\n";
                        $str .= "<td class='mainbgtitle' width='200px' align='center'>" . $title . "</a></td>\n";
                        $str .= "<input name='title" . $id . "' type='hidden' value='" . localize("Re: ") . $dbm->get("title") . "' />\n";
                        $str .= "<td class='mainbgtitle' width='140px' align='center'>" . $date . "</td>\n";
                        $dbm->next();
                        $str .= "</tr>";
                    }
                    $str .= "</table>";
                    $str .= "<table>";
                    $str .= "<tr>";
                    $str .= "<td><input id='Important' type='submit' name='Important' value='" . localize("Important") . "' />";
                    $str .= "<input id='Reply' type='submit' name='Reply' value='" . localize("Répondre") . "' />";
                    $str .= "<input id='ReplyToAll' type='submit' name='ReplyToAll' value='" . localize("Répondre à tous") . "' />";
                    $str .= "<input id='Move' type='submit' name='Move' value='" . localize("Deplacer") . "' />";
                    $str .= "<input id='FlagRead' type='submit' name='FlagRead' value='" . localize("Tout Marquer comme lu") . "' />";
                    $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Effacer") . "' /></td>";
                    $str .= "</tr>";
                    $str .= "</table>";
                } else {
                    $str .= "<table class='maintable centerareawidth'>";
                    $str .= "<tr><td class='mainbglabel'>Vous n'avez archivé aucun message dans ce répertoire</td></tr>";
                    $str .= "</table>";
                }
            } elseif ($move == 1) {
                $str .= "<table class='maintable centerareawidth'>";
                $str .= "<tr><td class='mainbgtitle'>Sélectionner un répertoire pour déplacer vos messages</td></tr>";
                $str .= "<tr><td class='mainbgtitle' align='center'><select class='selector cqattackselectorsize' name='CHOOSE_REPERTORY_ID'>";
                $item = array();
                $dbrep = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $id, $db);
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un répertoire --") . "</option>";
                while (! $dbrep->eof()) {
                    $item[] = array(
                        $dbrep->get("id") => localize($dbrep->get("name_Repertory"))
                    );
                    $dbrep->next();
                }
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value)
                        $str .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $str .= "</select></td></tr>";
                $str .= "<tr><td><input id='Shift' type='submit' name='Shift' value='" . localize("Déplacer vos messages vers ce répertoire") . "'/></td></tr>";
                $str .= "<td><input name='select' type='hidden' value='" . $mv . "' /></td>";
                $str .= "</table>";
            }
        } else {
            $str .= "<table class='maintable centerareawidth'>";
            if ($dbrep->count() == 0) {
                $str .= "<tr><td class='mainbglabel'>!! Aucun répertoire existant. Veuillez en créer.</td></tr>";
                $str .= "</table>";
                $str .= "<table class='maintable centerareawidth'>";
                $str .= "<tr><td class='mainbgtitle'>Créer un répertoire:</td></tr>";
                $str .= "<tr><td class='mainbgtitle'><input type='text' name='nameRepertory'/>&nbsp;<input id='CreateRepertory' type='submit' name='CreateRepertory' value='" . localize("Créer un répertoire") . "'/></td></tr>";
                $str .= "</table>";
            } else {
                $str .= "<tr><td class='mainbgtitle'><b>" . localize('Liste des répertoires que vous avez créés:') . "</b>\n";
                $str .= "</td></tr>\n";
                $str .= "<td class='mainbgtitle'>";
                while (! $dbrep->eof()) {
                    $str .= "<a href='../conquest/conquest.php?center=archive&archive=" . $dbrep->get("id") . "' class='tabmenu'>" . $dbrep->get("name_Repertory") . "</a>|\n";
                    $dbrep->next();
                }
                $str .= "</td></tr>\n";
                $str .= "</table>";
                if ($del == 1) {
                    $str .= "<table class='maintable centerareawidth'>\n";
                    $repertory = $_POST["CHOOSE_REPERTORY_NAME"];
                    $repertoryId = $_POST["CHOOSE_REPERTORY_ID"];
                    $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point de supprimer le répertoire: " . $repertory . ".</br> 
				Cette Action aura pour effet d'effacer tous les messages archiver dans ce répertoire.</tr></td>";
                    $str .= "<tr><td><input id='Continue' type='submit' name='Continue' value='" . localize("Continuer") . "'/><input id='Back' type='submit' name='Back' value='" . localize("Retour") . "'/></td></tr>";
                    $str .= "<td><input name='action' type='hidden' value='" . $repertoryId . "' /></td>";
                    $str .= "</table>";
                } else {
                    $str .= "<table class='maintable centerareawidth'>";
                    $str .= "<tr><td class='mainbgtitle'>Sélectionner un répertoire dans la liste pour l'effacer.</td><td class='mainbgtitle'>Créer un répertoire:</td></tr>";
                    $str .= "<tr><td class='mainbgtitle' align='center'><select class='selector cqattackselectorsize' name='CHOOSE_REPERTORY_ID' onchange='document.getElementById(\"CHOOSE_REPERTORY_NAME\").value=this.options[this.selectedIndex].innerHTML;'>";
                    $item = array();
                    $dbrep = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $id, $db);
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un répertoire --") . "</option>";
                    while (! $dbrep->eof()) {
                        $item[] = array(
                            $dbrep->get("id") => localize($dbrep->get("name_Repertory"))
                        );
                        $dbrep->next();
                    }
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value)
                            $str .= "<option value='" . $key . "'>" . $value . "</option>";
                    }
                    $str .= "</select><input id='DeleteRepertory' type='submit' name='DeleteRepertory' value='" . localize("Effacer un répertoire") . "'/></td>";
                    $str .= "<td class='mainbgtitle' align='center'><input type='text' name='nameRepertory'/>&nbsp;<input id='CHOOSE_REPERTORY_NAME' name='CHOOSE_REPERTORY_NAME' type='hidden' value='' /><input id='CreateRepertory' type='submit' name='CreateRepertory' value='" . localize("Créer un répertoire") . "'/></td></tr>";
                    $str .= "</table>";
                }
            }
        }
        $str .= "</form>\n";
        
        return $str;
    }

    /* -------------------------------------------------------------------- Public function ------------------------------------------------------------------ */
    
    // Fonction pour archiver les messages ------------------------ Archive ---------------------------------------
    public function moveRepertory($move, $playerid, $db)
    {
        if (isset($_POST["CHOOSE_REPERTORY_ID"])) {
            $repertoryId = $_POST["CHOOSE_REPERTORY_ID"];
            $msgToArchive = $move;
            $dbma = new DBCollection("UPDATE MailArchive SET id_Repertory='$repertoryId' WHERE id_Player=" . $playerid . " AND " . $msgToArchive, $db, 0, 0, false);
            return localize("Message(s) déplacé(s) avec succès");
        }
    }
    
    // Fonction pour déclarer un message comme important ---------------------------- Important ---------------------------------
    public function importantMail($db)
    {
        $important = 0;
        if (isset($_GET["priority"]))
            $msgToImportant = "id=" . quote_smart($_GET["priority"]);
        else
            $msgToImportant = $this->getCheckedDelMailCond("id");
        
        if ($msgToImportant != "") {
            $dbi = new DBCollection("SELECT important FROM MailArchive WHERE " . $msgToImportant, $db);
            $important = $dbi->get("important");
            if ($important == 0) {
                $dbm = new DBCollection("UPDATE MailArchive SET important=1 WHERE " . $msgToImportant, $db, 0, 0, false);
                return localize("Message(s) marqué(s) comme important");
            } else {
                $dbm = new DBCollection("UPDATE MailArchive SET important=0 WHERE " . $msgToImportant, $db, 0, 0, false);
                return localize("Importance supprimé");
            }
        } else {
            return localize("Aucun message marquer comme important");
        }
        $str .= "<div class='profile'>\n";
    }
    
    // Fonction pour déclarer un message comme lu ---------------------------- Marquer un ou des meesages comme lu ------------------------------------------
    public function FlagReadMail($db)
    {
        $msgToRead = $this->getCheckedDelMailCond("id");
        
        $dbm = new DBCollection("UPDATE MailArchive SET new=0 ", $db, 0, 0, false);
        return localize("Message(s) marquer comme lu");
        $str .= "<div class='profile'>\n";
    }
    
    // fonction pour supprimer les messages ----------------------------------- Delete ------------------------------------------
    public function deleteMail($db)
    {
        if (isset($_GET["priority"])) {
            $msgToDelete = "MailArchive.id=" . quote_smart($_GET["priority"]);
        } else {
            $msgToDelete = $this->getCheckedDelMailCond("MailArchive.id");
        }
        
        if ($msgToDelete != "") {
            $curplayer = $this->curplayer;
            $db = $this->db;
            $id = $curplayer->get("id");
            $dbmb = new DBCollection("UPDATE MailBody LEFT JOIN MailArchive ON MailBody.id=MailArchive.id_MailBody SET cpt=cpt-1 WHERE " . $msgToDelete, $db, 0, 0, false);
            $dbm = new DBCollection("DELETE FROM MailArchive WHERE id_Player=$id AND " . $msgToDelete, $db,0,0,false);
            $dbmbd = new DBCollection("DELETE FROM MailBody WHERE cpt=0", $db, 0, 0, false);
            return localize("Message(s) effacé(s)");
        } else {
            return localize("Aucun message effacé");
        }
        $str .= "<div class='profile'>\n";
    }
    
    // fonction qui surveille les sélections ---------------------------------------- Checked Del Mail Cond ------------------------------------------------
    public function getCheckedDelMailCond($condName)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . $val;
                else
                    $msg .= " OR " . $condName . "=" . $val;
                $first = 0;
            }
        }
        return $msg;
    }
    
    // fonction qui surveille les sélections avec en paramètre la variable $title ------------------------------ Checked Mail Cond --------------------
    public function getCheckedMailCond($condName, &$title)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . quote_smart($val);
                else
                    $msg .= " OR " . $condName . "=" . quote_smart($val);
                $first = 0;
                $title = quote_smart($_POST["title" . $val]);
            }
        }
        return $msg;
    }
    // ----------------------------------------------------------- Reply -------------------------------------------------------
    public function reply($db)
    {
        $condToReply = $this->getCheckedMailCond("MailArchive.id", $title);
        if ($condToReply != "") {
            $dbm = new DBCollection("SELECT Player.name,MailArchive.id FROM MailArchive LEFT JOIN Player ON MailArchive.id_Player\$sender=Player.id WHERE " . $condToReply . " GROUP BY Player.name", $db);
            $first = 1;
            $names = "";
            while (! $dbm->eof()) {
                if ($first)
                    $names = $dbm->get("name");
                else
                    $names .= "," . $dbm->get("name");
                
                $first = 0;
                $dbm->next();
            }
            
            redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
            return "";
        }
        return localize("Vous devez d'abord choisir un message");
    }
    // ------------------------------------------------------- Reply To All -------------------------------------------
    public function replyToAll($db)
    {
        global $Conquest;
        
        $condToReply = $this->getCheckedMailCond("MailArchive.id", $title);
        if ($condToReply != "") {
            $dbm = new DBCollection("SELECT recipient,name FROM MailBody LEFT JOIN MailArchive ON MailBody.id=id_MailBody LEFT JOIN Player ON Player.id=MailBody.id_Player WHERE " . $condToReply, $db);
            $first = 1;
            $arr = array();
            while (! $dbm->eof()) {
                $splited = explode(",", $dbm->get("recipient"));
                foreach ($splited as $name)
                    $arr[$name] = 1;
                $arr[$dbm->get("name")] = 1;
                $dbm->next();
            }
            
            $player = $this->curplayer;
            
            unset($arr[$player->get("name")]);
            $names = "";
            foreach ($arr as $name => $val) {
                if ($first)
                    $names = $name;
                else
                    $names .= "," . $name;
                
                $first = 0;
            }
            redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
            return "";
        }
        return localize("Vous devez d'abord choisir un message");
    }
}

?>
