<?php

class CQArrest extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQArrest($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $dbide = new DBCollection("SELECT MAX(id) FROM Event WHERE id_Player\$Src=" . $curplayer->get("id"), $db);
        $dbe = new DBCollection("SELECT * FROM Event WHERE id=" . $dbide->get("MAX(id)"), $db);
        
        if ($dbe->get("typeEvent") == ABILITY_DISARM_EVENT_SUCCESS)
            $disarm = true;
        else
            $disarm = false;
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $str .= "<table class='maintable'><tr><td  class='mainbgtitle' width='750px'>";
        if ($disarm)
            $str .= "Vous venez de désarmer votre adversaire, dans ce cas précis, vous n'avez pas la possibilité de refuser l'arrestation</td></tr> <tr><td  class='mainbgtitle' width='750px'>";
        $str .= "<select class='selector cqattackselectorsize' name='CHOICE'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Que souhaitez vous faire  --") . "</option>";
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND name='Arrestation'", $db);
        if ($dbc->get("level") == 1) {
            $str .= "<option value='1'> Accepter l'arrestion et aller en prison </option>";
            if (! $disarm)
                $str .= "<option value='2'> Refuser l'arrestation </option>";
        }
        if ($dbc->get("level") == 2) {
            $str .= "<option value='3'> Rendre l'argent et payer l'amende de 20PO </option>";
            $str .= "<option value='4'> Rendre l'argent et être conduit en prison </option>";
            $str .= "<option value='2'> Refuser l'arrestation </option>";
        }
        
        $str .= "</select>";
        $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
        $str .= "<input name='action' type='hidden' value='" . ARREST . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</td></tr></table>";
        $str .= "</form>";
        
        return $str;
    }
}

?>