<?php

class CQMenuAuth extends HTMLObject
{

    public $nacridan;

    public function CQMenuAuth($nacridan, $db)
    {
        $this->nacridan = $nacridan;
        $this->db = $db;
    }

    public function toString()
    {
        $str = "<div class='menu'>";
        $str .= "<dl class='menuright'>";
        
        $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Mod Menu') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu1'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=pollpanel'>" . localize("Gestion des sondages") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=questpanel'>" . localize("Gestion des quêtes") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=xprewardpanel'>" . localize("Récompenses en PX des anims") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=newspanel'>" . localize("Gestion des news") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=serverpanel'>" . localize("Gestion du serveur") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=disabled'>" . localize("Persos Désactivés") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=globalMP'>" . localize("MP à tous") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=addDescription'>" . localize("Ajout de description") . "</a></li>\n";
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        $str .= "<dt onClick=\"showSubMenu('smenu10');\">" . localize('Test - persos') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu10'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addAP'>" . localize("Ajouter des PA.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addMoney'>" . localize("Ajouter des PO.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=levelup'>" . localize("Level up") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=newDLA'>" . localize("Nouvelle DLA") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createExploitation'>" . localize("Créer une exploitation.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=changeModifier'>" . localize("Changer carac.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=deleteChapelMalus'>" . localize("Supprimer malus Chapelle") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=instantTP'>" . localize("Téléportation.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createMonster'>" . localize("Créer un monstre.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=cloneMonster'>" . localize("Cloner un monstre.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=playIAMonster'>" . localize("Jouer Un monstre par IA.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=calcSPath'>" . localize("Calculer le chemin le plus court.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addAbilityToPNJ'>" . localize("Donner une comp à un perso.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addMagicSpellToPNJ'>" . localize("Donner un sort à un perso.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addTalentToPNJ'>" . localize("Donner un savoir-faire à un perso.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveItemToPNJ'>" . localize("Donner un objet à un perso.") .
             "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveMoneyToPNJ'>" . localize("Donner des PO à un perso.") .
             "</a></li>\n";
        // $str.="<li><a href='../conquest/conquest.php?center=view&bottom=accessBDD&".getGoogleConquestKeywords()."'>".localize("Changer une valeur.")."</a></li>\n";
        
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        $str .= "<dt onClick=\"showSubMenu('smenu11');\">" . localize('Test - objets') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu11'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveItemToPNJ'>" . localize("Créer un objet.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=renameObject'>" . localize("Renommer un objet.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=createParchment'>" . localize("Créer un parchemin personnalisé.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=emptyBag'>" . localize("Vider Sac.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=destroyObject'>" . localize("Détruire un Object au sol.") .
             "</a></li>\n";
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        $str .= "<dt onClick=\"showSubMenu('smenu12');\">" . localize('Test - bâtiments') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu12'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createBuilding'>" . localize("Créer un bâtiment.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=renameBuilding'>" . localize("Renommer un bâtiment.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=destroyBuilding'>" . localize("Détruire un bâtiment.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=openOrCloseDoors'>" . localize("Ouvrir ou fermer les portes.") .
             "</a></li>\n";
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        $str .= "<dt onClick=\"showSubMenu('smenu13');\">" . localize('Test - autres') . "</dt>\n";
        $str .= "<dd style='display: none;' id='smenu13'>\n";
        $str .= "<ul>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=runAI'>" . localize("Faire tourner l'IA") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=monsterProfile'>" . localize("Gestion des illus et descriptions.") . "</a></li>\n";
        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=terraformCase'>" . localize("Terraformation d'une case.") .
             "</a></li>\n";
        $str .= "</ul>\n";
        $str .= "</dd>\n";
        
        $str .= "</dl>\n";
        $str .= "</div>\n";
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }
}
?>
