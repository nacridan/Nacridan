<?php

class CQStore extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQStore($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        
        if (($curplayer->get("inbuilding") && $curplayer->get("ap") < STORE_OBJECT_INBUILDING_AP) || (! $curplayer->get("inbuilding") && $curplayer->get("ap") < STORE_OBJECT_AP)) {
            $str = "<div><table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour ranger un objet.");
            $str .= "</td></tr></table></div>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='80px'>";
            $str .= localize("Ranger") . " </td><td><select id='OBJECT_ID' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE state='Carried' AND weared='No' AND id_Player=" . $curplayer->get("id"), $db, 0, 0);
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
            
            $item[] = array(
                localize("-- Sac équipements --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("id_Equipment\$bag") == 0) {
                    if ($dbp->get("id_BasicEquipment") > 499)
                        $item[] = array(
                            localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    else
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                }
                $dbp->next();
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 1, $db)) {
                $item[] = array(
                    localize("-- Sac de gemmes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 2, $db)) {
                $item[] = array(
                    localize("-- Sac de matières premières --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 3, $db)) {
                $item[] = array(
                    localize("-- Sac d'herbes --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                $item[] = array(
                    localize("-- Ceinture --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                $item[] = array(
                    localize("-- Carquois --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                $item[] = array(
                    localize("-- Trousse à outils --") => - 1
                );
                $dbp->first();
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    
                    $dbp->next();
                }
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class=\"group\" label=\"" . $key . "\" />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td>";
            
            $str .= "<td align='center' rowspan=2><input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . STORE_OBJECT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr>";
            
            // Affichage des différentes possibilités de rangements
            $str .= "<tr class='mainbgtitle'> <td  width='80px'> dans </td><td> <select class='selector cqattackselectorsize' name='INV_ID'>";
            $str .= "<option value='1'>-- Sac d'équipement --</option>";
            
            // Sac de gemmes
            if (playerFactory::getIdBagByNum($id, 1, $db))
                $str .= "<option value='2'>-- Sac de gemmes --</option>";
                
                // Sac de matièrs premières
            if (playerFactory::getIdBagByNum($id, 2, $db))
                $str .= "<option value='3'>-- Sac de matières premières --</option>";
                
                // Sac d'herbes
            if (playerFactory::getIdBagByNum($id, 3, $db))
                $str .= "<option value='4'>" . localize("-- Sac d'herbes --") . "</option>";
                
                // Ceinture
            if (playerFactory::getIdBagByNum($id, 4, $db))
                $str .= "<option value='5'>" . localize("-- Ceinture --") . "</option>";
                
                // Carquois
            if (playerFactory::getIdBagByNum($id, 5, $db))
                $str .= "<option value='6'>" . localize("-- Carquois --") . "</option>";
                // Trousse à outils
            if (playerFactory::getIdBagByNum($id, 6, $db))
                $str .= "<option value='7'>" . localize("-- Trousse à outils --") . "</option>";
            $str .= "</select>";
            
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
