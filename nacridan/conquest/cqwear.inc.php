<?php

class CQWear extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQWear($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        if ($curplayer->get("ap") < WEAR_EQUIPMENT_AP && $curplayer->get("incity") == 0) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour vous équiper.");
            $str .= "</td></tr></table>";
        } else {
            if (! isset($_POST["action"])) {
                if ($curplayer->get("incity") == 1)
                    $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act&center=equip" . "' target='_self'>";
                else
                    $str = "<form name='form'  method='POST' target='_self'>";
                
                $str .= "<table class='maintable bottomareawidth' ><tr><td class='mainbgtitle' >";
                $str .= localize("Mettre") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
                
                $gap = 0;
                $dbp = new DBCollection("SELECT Equipment.id,Equipment.name,Equipment.level, Equipment.extraname,Equipment.id_Equipment\$bag, Equipment.id_BasicEquipment FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE state='carried' AND wearable=\"Yes\" AND weared=\"No\" AND progress=100 AND (templateProgress= 0 or templateProgress=100) AND id_Player=" . $id, $db, 0, 0);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisir un équipement --") . "</option>";
                
                $item[] = array(
                    localize("-- Sac équipements --") => - 1
                );
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == 0) {
                        if ($dbp->get("id_BasicEquipment") > 499)
                            $item[] = array(
                                localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        else
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                    }
                    $dbp->next();
                }
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                    $item[] = array(
                        localize("-- Trousse à outils --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select></td><td class='mainbgtitle' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . WEAR_EQUIPMENT . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                if ($_POST["OBJECT_ID"] == 0) {
                    $str = "<table class='maintable bottomareawidth' ><tr><td class='mainbgtitle'>";
                    $str .= localize("L'équipemement que vous avez choisi n'est pas valide !") . "</td>";
                    $str .= "</tr></table>";
                } else {
                    $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act&center=equip" . "' target='_self'>";
                    $str .= "<table class='maintable bottomareawidth' ><tr><td class='mainbgtitle' width='550px'>";
                    
                    $dbp = new DBCollection("SELECT E2.name,E2.extraname,E2.level,E2.id FROM Equipment AS E1 LEFT JOIN Equipment AS E2 ON E1.id_Player=E2.id_Player LEFT JOIN EquipmentType AS ET1 ON E1.id_EquipmentType=ET1.id LEFT JOIN EquipmentType AS ET2 ON E2.id_EquipmentType=ET2.id WHERE E1.id=" . quote_smart($_POST["OBJECT_ID"]) . " AND E1.id_Player=" . $id . " AND E2.weared=\"Yes\" AND (ET1.mask & ET2.mask)", $db, 0, 0);
                    
                    if ($dbp->eof())
                        $str .= localize("Cette action vous coûtera {nbpa} PA. Continuer ?", array(
                            "nbpa" => WEAR_EQUIPMENT_AP
                        )) . " :</td><td class='mainbgtitle'>";
                    
                    else {
                        $str .= localize("Equipement(s) en conflit(s) :") . "<br/>" . localize("(qui seront enlevé(s))") . "</td><td class='mainbgtitle'>";
                        while (! $dbp->eof()) {
                            if ($dbp->get("name") != "")
                                $str .= localize($dbp->get("name")) . " " . localize($dbp->get("extraname")) . " " . localize("Niveau") . " " . $dbp->get("level") . "<br/>";
                            else
                                $str .= localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . "<br/>";
                            
                            $dbp->next();
                        }
                        $str .= "</td><td class='mainbgtitle'>" . localize("Continuer ?") . "</td><td class='mainbgtitle'>";
                    }
                    
                    $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "<input name='OBJECT_ID' type='hidden' value='" . quote_smart($_POST["OBJECT_ID"]) . "' />";
                    $str .= "<input name='action' type='hidden' value='" . WEAR_EQUIPMENT . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</td></tr></table>";
                    $str .= "</form>";
                }
            }
        }
        return $str;
    }
}
?>
