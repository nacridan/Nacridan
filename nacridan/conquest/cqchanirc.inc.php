<?php

class CQChanIRC extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQChanIRC($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $str = "<table class='maintable centerareawidth'>\n";
        $str .= "<tr class='mainbgtitle'>";
        $str .= "<td colspan='2' class='mainbgtitle'>\n<b><h1> Chat IRC </h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		Vous avez la possibilité de rejoindre le chat IRC de nacridan en suivant <a class='stylepc' href='http://webchat.quakenet.org/?channels=nacridan&uio=d4' onclick='window.open(this.href); return false;'>ce lien</a>.
		<br /><br /></td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<ul><li>Pour se connecter, il suffit de cliquer sur le lien, de vous choisir un pseudonyme pour vous identifier et de cliquer sur \"join chat\". Il n'y a ni besoin d'inscription, ni de compte, ni de mot de passe. C'est un chat tout public.</li><br />
		<li>Les règles habituelles d'un chat tout public s'imposent bien entendu, à savoir : pas de grossièretés, un discours sans langage SMS, pas de propos offensants, etc. Un peu de bon sens devrait suffire pour que nous puissions tous communiquer sans accrochage !</li><br />
		<li>C'est l'occasion de discuter avec d'autres joueurs de Nacridan, de partager des idées, de discuter du développement en direct avec les développeurs, etc...</li></ul>
		<br /></td>";
        $str .= "</tr>";
        $str .= "</table>";
        
        return $str;
    }
}

?>
