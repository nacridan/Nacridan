<?php

class CQOption extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQOption($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function getATBHour()
    {
        $time = gmstrtotime($this->curplayer->get("nextatb")) - date("I") * 3600;
        $str = date("Y-m-d H:i:s", $time);
        
        // list($day,$hour)=preg_split(" ",$str);
        list ($day, $hour) = explode(" ", $str);
        return $hour;
    }

    public function getATBDay()
    {
        $time = gmstrtotime($this->curplayer->get("nextatb")) - date("I") * 3600;
        $day = $this->localizedday(date("j M Y", $time));
        return $day;
    }

    public function localizedday($date)
    {
        $eng_date_array = explode(" ", $date);
        
        $eng_date_array['num'] = $eng_date_array[0];
        unset($eng_date_array[0]);
        $eng_date_array['month'] = $eng_date_array[1];
        unset($eng_date_array[1]);
        $eng_date_array['year'] = $eng_date_array[2];
        unset($eng_date_array[2]);
        
        $local_date_array_db = array(
            'month' => array(
                'Jan' => localize("jan"),
                'Feb' => localize("feb"),
                'Mar' => localize("mar"),
                'Apr' => localize("apr"),
                'May' => localize("may"),
                'Jun' => localize("jun"),
                'Jul' => localize("jul"),
                'Aug' => localize("aug"),
                'Sep' => localize("sep"),
                'Oct' => localize("oct"),
                'Nov' => localize("nov"),
                'Dec' => localize("dec")
            )
        );
        
        $local_date_array['num'] = $eng_date_array['num'] . " ";
        
        foreach ($local_date_array_db['month'] as $eng_month => $local_month)
            if ($eng_month == $eng_date_array['month'])
                $local_date_array['month'] = $local_month . " ";
        
        $local_date_array['year'] = $eng_date_array['year'] . " ";
        
        $local_date = implode("", $local_date_array);
        return $local_date;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $str = "";
        $err = "";
        
        if (isset($_GET["type"])) {
            $type = quote_smart($_GET["type"]);
        } else {
            $type = "pc";
        }
        $errmdp = "";
        $errmdp2 = "";
        $errmdp3 = "";
        $errmdp4 = "";
        
        if (isset($_POST["deleteChar"])) {
            
            $dbp = new DBCollection("SELECT id FROM Player WHERE status='PC' AND id_Member=" . $curplayer->get("id_Member"), $db);
            if ($dbp->count() == 1)
                $dbm = new DBCollection("UPDATE Member SET newplayer=1 WHERE id=" . $curplayer->get("id_Member"), $db, 0, 0, false);
            else
                $dbm = new DBCollection("UPDATE Member SET maxNbPlayers=(maxNbPlayers - 1) WHERE id=" . $curplayer->get("id_Member"), $db, 0, 0, false);
                
                // Relatif au village et au bâtiment
            $dbb = new DBCollection("UPDATE Building SET id_Player=0 WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbc = new DBCollection("UPDATE City SET id_Player=0, money=0, captured=(case when captured=4 then 3 else 0 end) WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, 
                false);
            $dbb = new DBCollection("DELETE FROM Building WHERE (id_BasicBuilding=11 or id_BasicBuilding> 14) AND id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            
            // Nettoyage de la bdd
            $dbt = new DBCollection("DELETE FROM Template WHERE id_Equipment IN (SELECT id FROM Equipment WHERE id_Player=" . $curplayer->get("id") . ")", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Detail WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Modifier WHERE id=" . $curplayer->get("id_Modifier"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=" . $curplayer->get("id_Upgrade"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Player WHERE id=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Ability WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM MagicSpell WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Talent WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM BM WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Caravan WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            
            // Suppression des player controlés et des BM infligés
            $dbt = new DBCollection("DELETE FROM Player WHERE id_Owner=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $curplayer->get("id"), $db, 0, 0, false);
            // pour l'escorte, y'a pas de l'id_Owner
            $dbt = new DBCollection(
                "DELETE FROM Player WHERE id_Member=" . $curplayer->get("id_Member") . " AND id_Mission IN (SELECT id FROM Mission WHERE id_Player=" . $curplayer->get("id") . ")", 
                $db, 0, 0, false);
            
            $dbt = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Guest=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Player=" . $curplayer->get("id") . "", $db, 0, 0, false);
            
            // Suppression des mails - Tester les alias
            $dbt = new DBCollection("DELETE FROM MailSend WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM MailContact WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM MailContact WHERE id_Player\$friend=" . $curplayer->get("id"), $db, 0, 0, false);
            
            // Suppression des Alliances
            $dbt = new DBCollection("DELETE FROM PJAlliancePJ WHERE id_Player\$src=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM PJAlliancePJ WHERE id_Player\$dest=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM PJAlliance WHERE id_Player\$src=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM TeamAlliancePJ WHERE id_Player=" . $curplayer->get("id") . "", $db, 0, 0, false);
            $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Player=" . $curplayer->get("id") . "", $db, 0, 0, false);
            
            // Si chef d'un ordre
            $dbs = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $curplayer->get("id_Team") . "", $db);
            if ($dbs->count() > 0 && $dbs->get("id_Player") == $curplayer->get("id")) {
                // Envoi d'un message au membre de l'ordre
                $dbu = new DBCollection("SELECT * FROM Player WHERE id_Team=" . $curplayer->get("id_Team"), $db);
                if ($dbu->count() > 0) {
                    while (! $dbu->eof()) {
                        $dest[$dbu->get("name")] = $dbu->get("name");
                        $dbu->next();
                    }
                    require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                    MailFactory::sendSingleDestMsg($curplayer, localize("Information"), localize("L'ordre auquel vous apparteniez a été dissous."), $dest, $type, $err, $db, - 1, 0);
                    
                    $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Team=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM TeamRankInfo WHERE id_Team=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Team=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM TeamBody WHERE id_Team=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$dest=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$src=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM Team WHERE id=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                    $dbu = new DBCollection("UPDATE Player SET id_Team=0 WHERE id_Team=" . $curplayer->get("id_Team") . "", $db, 0, 0, false);
                }
            }
            
            // Si chef d'un groupe de chasse
            $dbs = new DBCollection("SELECT * from FighterGroup WHERE id=" . $curplayer->get("id_FighterGroup") . "", $db);
            $dest2 = array();
            if ($dbs->count() > 0 && $dbs->get("id_Player") == $curplayer->get("id")) {
                
                // Envoi d'un message au membre du GdC
                $dbu = new DBCollection("SELECT * FROM Player WHERE id_FighterGroup=" . $curplayer->get("id_FighterGroup"), $db);
                if ($dbu->count() > 0) {
                    while (! $dbu->eof()) {
                        $dest2[$dbu->get("name")] = $dbu->get("name");
                        $dbu->next();
                    }
                    require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                    MailFactory::sendSingleDestMsg($curplayer, localize("Information"), localize("Le groupe de chasse auquel vous apparteniez a été dissous."), $dest2, $type, $err, 
                        $db, - 1, 0);
                    
                    $dbd = new DBCollection("DELETE FROM FighterGroupMsg WHERE id_Fighter=" . $curplayer->get("id_FighterGroup") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM FighterGroup WHERE id=" . $curplayer->get("id_FighterGroup") . "", $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM FighterGroupMsg WHERE id_FighterGroup=" . $curplayer->get("id_FighterGroup") . "", $db, 0, 0, false);
                    $dbu = new DBCollection("UPDATE Player SET id_FighterGroup=0 WHERE id_FighterGroup=" . $curplayer->get("id_FighterGroup") . "", $db, 0, 0, false);
                }
            }
            
            $this->nacridan->sess->destroy();
            $this->nacridan->auth->logout();
            echo "<h1>" . localize("Votre personnage a correctement été effacé, vous devez vous reconnecter, si c'était votre dernier personnage vous devrez en recréer un.") .
                 "</h1>";
            exit(0);
            /*
             * $str="<form name='form' method='POST' action='".CONFIG_HOST."' target='_self' enctype='multipart/form-data'>\n";
             * $str="<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
             * $str.=localize("Votre compte a correctement été effacé.");
             * $str.="<input type='submit' name='thatsallfolks' value='".localize("Good Bye")."'/>";
             * $str.="</td></tr></table></form>";
             */
        }
        
        if (isset($_POST["updateView"])) {
            if (! $this->nacridan->isRepostForm()) {
                $view2d = 0;
                if (isset($_POST["view2d"]))
                    $view2d = 1;
                $this->nacridan->auth->auth["view2d"] = $view2d;
                $dbu = new DBCollection("UPDATE MemberOption SET view2d=" . $view2d . " WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db, 0, 0, false);
                $errmdp4 = "<span class='mainerror'>(" . localize("Modification(s) effectuée(s)") . ")</span>";
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateRose"])) {
            if (! $this->nacridan->isRepostForm()) {
                $windrose = 0;
                if (isset($_POST["windRose"]))
                    $windrose = 1;
                $dbu = new DBCollection("UPDATE MemberOption SET windRose=" . $windrose . " WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db, 0, 0, false);
                $errmdp4 = "<span class='mainerror'>(" . localize("Modification(s) effectuée(s)") . ")</span>";
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateMusic"])) {
            if (! $this->nacridan->isRepostForm()) {
                $music = 0;
                if (isset($_POST["music"]))
                    $music = 1;
                $dbu = new DBCollection("UPDATE MemberOption SET music=" . $music . " WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db, 0, 0, false);
                $errmdp4 = "<span class='mainerror'>(" . localize("Modification(s) effectuée(s)") . ")</span>";
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateAvatar"])) {
            if (! $this->nacridan->isRepostForm() && $_FILES['avatar']['name'] != "") {
                $tmp_name = $_FILES['avatar']['tmp_name'];
                $name = $_FILES['avatar']['name'];
                $size = $_FILES['avatar']['size'];
                $typefile = $_FILES['avatar']['type'];
                $erreur = $_FILES['avatar']['error'];
                
                $path = getPathFromPlayerName($curplayer->get("name"));
                
                // Get new sizes
                list ($width, $height) = getimagesize($tmp_name);
                
                if ($width != 0 && $height != 0) {
                    $newwidth = 252;
                    $newheight = 302;
                    
                    $ratioA = $width / $height;
                    $ratioB = $newwidth / $newheight;
                    
                    if ($ratioA > $ratioB) {
                        $newwidth2 = 252;
                        $newheight2 = $newwidth2 / $ratioA;
                    } else {
                        $newheight2 = 302;
                        $newwidth2 = $newheight * $ratioA;
                    }
                    
                    // Load
                    $thumb = imagecreatetruecolor($newwidth2, $newheight2);
                    
                    $typeimage = exif_imagetype($tmp_name);
                    
                    switch ($typeimage) {
                        case IMAGETYPE_GIF:
                            $source = @imagecreatefromgif($tmp_name);
                            break;
                        case IMAGETYPE_JPEG:
                            $source = @imagecreatefromjpeg($tmp_name);
                            break;
                        case IMAGETYPE_PNG:
                            $source = @imagecreatefrompng($tmp_name);
                            break;
                        case IMAGETYPE_BMP:
                            $source = @imagecreatefrombmp($tmp_name);
                            break;
                        default:
                            $source = "";
                            break;
                    }
                    
                    if ($source != "") {
                        // Resize
                        // echo $newwidth2." ".$newheight2."\n";
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth2, $newheight2, $width, $height);
                        exec("rm " . HOMEPATH . "/pics/PJ/" . $path . $curplayer->get("id") . "*.jpg");
                        // Output
                        imagejpeg($thumb, HOMEPATH . "/pics/PJ/" . $path . $curplayer->get("id") . time() . ".jpg", 90);
                        
                        $detail = new Detail();
                        
                        $detail->load($curplayer->get("id_Detail"), $db);
                        
                        $detail->set("pic", $curplayer->get("id") . time() . ".jpg");
                        $detail->updateDB($db);
                        // $str="<div class='mainbgtitle'> ALORS ".HOMEPATH."/pics/PJ/".$path.$curplayer->get("id")."*.jpg"."</div>";
                    }
                } else {
                    $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
                }
            }
        }
        if (isset($_POST["updateAvatarVue2D"])) {
            if (! $this->nacridan->isRepostForm()) {
                $imageNew = $_POST["avatar2Dchoice"];
                if ($imageNew == 'img_defaut' || (strpos($imageNew, $curplayer->get("id")) === FALSE && $imageNew != $curplayer->get("id"))) {
                    $imageNew = "";
                }
                $curplayer->set("picture", quote_smart($imageNew));
                $curplayer->updateDB($db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        if (isset($_POST["updateDesc"])) {
            if (! $this->nacridan->isRepostForm()) {
                
                $detail = new Detail();
                $detail->load($curplayer->get("id_Detail"), $db);
                $detail->set("body", $newbody = unHTMLTags($_POST["desc"]));
                $detail->updateDB($db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["switchDLA"])) {
            $str = "<form name='form'  method='POST' target='_self'>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            $str .= localize("Déclaler votre DLA de") . " <select id='hour' class='selector' name='HOUR'>";
            
            for ($i = 0; $i < 12; $i ++) {
                $hour[] = array(
                    "+" . $i . localize(" H") => $i
                );
            }
            
            foreach ($hour as $arr) {
                foreach ($arr as $key => $value) {
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select> ";
            
            $str .= "<select id='min' class='selector' name='MIN'>";
            for ($i = 0; $i <= 59; $i ++) {
                $min[] = array(
                    "+" . $i . localize(" min") => $i
                );
            }
            
            foreach ($min as $arr) {
                foreach ($arr as $key => $value) {
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . ATBSHIFT . "' />";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        
        if (isset($_POST["action"])) {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            
            $time = gmstrtotime($curplayer->get("nextatb")) - date("I") * 3600;
            $time += $_POST["MIN"] * 60 + $_POST["HOUR"] * 3600;
            $date = date("Y-m-d H:i:s", $time);
            
            $str .= localize("Décaler votre DLA pour cette nouvelle heure ? : ") . $date;
            $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='HOUR' type='hidden' value='" . quote_smart($_POST["HOUR"]) . "' />";
            $str .= "<input name='MIN' type='hidden' value='" . quote_smart($_POST["MIN"]) . "' />";
            $str .= "<input name='action' type='hidden' value='" . ATBSHIFT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr>";
            $str .= "<tr><td class='mainbgtitle'>Décaler les DLA des tous vos personnages à cette heure là ?</td>";
            $str .= "<td><input type='checkbox' name='SHIFT_ALL_CHARACTERS'  /></td></tr>";
            $str .= "</table>";
            $str .= "</form>";
        }
        
        if ($str == "") {
            $target = "/conquest/conquest.php?center=option";
            
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self' enctype='multipart/form-data'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('O P T I O N S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= "<b><h3>" . $curplayer->get('name') . "</h3></b></td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $str .= "<table class='maintable centerareawidth'><tr><td colspan='2' class='mainbglabel'>" . localize("DLA") . "</td></tr>\n";
            $str .= "<tr><td colspan='2' class='mainbgtitle'>" . localize("Heure : ") . "" . date("H:i:s") . "</tr><td>";
            $str .= "<tr><td class='mainbgtitle'> Prochaine <b>DLA</b> : " . $this->getATBDay();
            $str .= "<b>" . $this->getATBHour() . "</b></td>";
            $str .= "<td class='mainbgtitle'><input type='submit' name='switchDLA' value='" . localize("Decaler sa DLA") . "' /><td/></tr></table>";
            
            if ($curplayer->get('status') == 'PC') {
                $detail = new Detail();
                if ($curplayer->get("id_Detail") != 0)
                    $detail->load($curplayer->get("id_Detail"), $db);
                
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Avatar") . "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle'><table><tr><td>";
                if ($detail->get("pic") != "") {
                    
                    $str .= "<img src='" . CONFIG_HOST . "/pics/PJ/" . getPathFromPlayerName($curplayer->get("name")) . $detail->get("pic") . "' width='80px' height='96px' />\n";
                } else {
                    $str .= "<img src='" . CONFIG_HOST . "/pics/character/undef.jpg' />\n";
                }
                
                $str .= "</td><td class='mainbgtitle'>" . localize("Format recommandé : (252 x 302)") . "<br/><br/><input type='file' name='avatar' size='30'>";
                $str .= "<input type='hidden' name='max_file_size' value='50000'>\n";
                $str .= "<tr></table>\n";
                $str .= "<font color='#ff0000'><b>attention</b></font> <font color='#ffffff'>" .
                     localize("L'utilisation d'images pouvant heurter la morale pourra donner suite à l'annulation de votre compte sans préavis") . ".<br/>\n";
                $str .= localize("L'image ne doit pas être protégée en copyright ou protégée par une marque déposée.") . "<br/>\n";
                $str .= localize("les images dont la taille dépasse 50 kb seront refusées.") . "</font><br/><br/>";
                $str .= "<input type='submit' name='updateAvatar' value='" . localize("Mettre à jour votre Avatar") . "'>\n";
                
                $str .= "</td></tr>";
                $str .= "</table><br/><br/>\n";
                
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel' colspan='2'>" . localize("Avatar Vue 2D") . "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle'>";
                
                $currentPic = $curplayer->get("id_BasicRace") . ($curplayer->get("gender") == "F" ? "f" : "") . ".png";
                
                if ($curplayer->get("picture") != "") {
                    $str .= "<img src='" . CONFIG_HOST . "/pics/map/players/customized/" . $curplayer->get("picture") . ".png' width='72px' height='72px' />\n";
                    $currentPic = $curplayer->get("picture") . ".png";
                } else {
                    $str .= "<img src='" . CONFIG_HOST . "/pics/map/players/" . $curplayer->get("id_BasicRace") . ($curplayer->get("gender") == "F" ? "f" : "") .
                         ".png' width='72px' height='72px' />\n";
                }
                
                $str .= "</td><td class='mainbgtitle'>Image actuelle</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle' width='100px'>";
                $str .= "<input type='radio' name='avatar2Dchoice' value='img_defaut' id='avatar2D_defaut' style='position: absolute' " . ($currentPic == $curplayer->get(
                    "id_BasicRace") . ($curplayer->get("gender") == "F" ? "f" : "") . ".png" ? " checked='checked'" : "") . "/>";
                $str .= "<img src='" . CONFIG_HOST . "/pics/map/players/" . $curplayer->get("id_BasicRace") . ($curplayer->get("gender") == "F" ? "f" : "") .
                     ".png' width='72px' height='72px' />\n";
                $str .= "</td><td class='mainbgtitle'>Image par défaut</td></tr>\n";
                if (file_exists("../pics/map/players/customized/" . $curplayer->get('id') . ".png")) {
                    $str .= "<tr><td class='mainbgtitle' width='100px'>";
                    $str .= "<input type='radio' name='avatar2Dchoice' value='" . $curplayer->get('id') . "' id='avatar2D_id' style='position: absolute' " .
                         ($currentPic == $curplayer->get('id') . ".png" ? " checked='checked'" : "") . " />";
                    $str .= "<img src='" . CONFIG_HOST . "/pics/map/players/customized/" . $curplayer->get('id') . ".png' width='72px' height='72px' />\n";
                    $str .= "</td><td class='mainbgtitle'>Image " . $curplayer->get('id') . ".png</td></tr>\n";
                }
                $i = 1;
                while (file_exists("../pics/map/players/customized/" . $curplayer->get('id') . "-" . $i . ".png")) {
                    $str .= "<tr><td class='mainbgtitle' width='100px'>";
                    $str .= "<input type='radio' name='avatar2Dchoice' value='" . $curplayer->get('id') . "-" . $i . "' id='avatar2D_id_" . $i . "' style='position: absolute'  " .
                         ($currentPic == $curplayer->get('id') . "-" . $i . ".png" ? " checked='checked'" : "") . "/>";
                    $str .= "<img src='" . CONFIG_HOST . "/pics/map/players/customized/" . $curplayer->get('id') . "-" . $i . ".png' width='72px' height='72px' />\n";
                    $str .= "</td><td class='mainbgtitle'>Image " . $curplayer->get('id') . "-" . $i . ".png</td></tr>\n";
                    $i ++;
                }
                $str .= "<tr><td class='mainbgtitle' colspan='2'>";
                $str .= "<input type='submit' name='updateAvatarVue2D' value='" . localize("Mettre à jour votre Avatar Vue 2D") . "'>\n";
                $str .= "</td></tr>";
                $str .= "</table><br/><br/>\n";
                
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Description") . "</td></tr>\n";
                $body = new Detail();
                if ($curplayer->get("id_Detail") != 0)
                    $body->load($curplayer->get("id_Detail"), $db);
                $str .= "<tr><td><textarea name='desc' cols=80 rows=15>" . $body->get("body") . "</textarea></td></tr>";
                $str .= "</table>\n";
                $str .= "<input type='submit' name='updateDesc' value='" . localize("Mettre à jour votre description") . "' /><br/><br/><br/>";
                
                $str .= "<br/><br/>\n";
                
                // ------------------------- VUE 2D--------------------------------
                
                $dbs = new DBCollection("SELECT view2d FROM Member LEFT JOIN MemberOption ON Member.id=id_Member WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db);
                
                if (! $dbs->eof()) {
                    
                    $view2d = $dbs->get("view2d") ? "checked" : "";
                    
                    $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Vue") . " " . $errmdp4 . "</td></tr>\n";
                    $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='view2d' " . $view2d . " value='1'>" .
                         localize("Utiliser la vue 2D à la place de la vue traditionnelle") . "</td></tr>";
                    $str .= "</table>\n";
                    $str .= "<input type='submit' name='updateView' value='" . localize("Mettre à jour") . "' /><br/><br/>";
                }
                
                // --------------------------Rose des vents ---------------------------------------------------
                
                $dbr = new DBCollection("SELECT windRose FROM Member LEFT JOIN MemberOption ON Member.id=id_Member WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db);
                if (! $dbr->eof()) {
                    
                    $windrose = $dbr->get("windRose") ? "checked" : "";
                    $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Rose des vents") . " " . $errmdp4 . "</td></tr>\n";
                    $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='windRose' " . $windrose . " value='0'>" .
                         localize("Masquer/Afficher la rose des vents sur la vue 2D") . "</td></tr>";
                    $str .= "</table>\n";
                    $str .= "<input type='submit' name='updateRose' value='" . localize("Mettre à jour") . "' /><br/><br/>";
                }
                
                // --------------------------Musique ---------------------------------------------------
                
                $dbr = new DBCollection("SELECT music FROM Member LEFT JOIN MemberOption ON Member.id=id_Member WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db);
                if (! $dbr->eof()) {
                    
                    $music = $dbr->get("music") ? "checked" : "";
                    $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Thèmes musicaux") . " " . $errmdp4 . "</td></tr>\n";
                    $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='music' " . $music . " value='0'>" . localize("Activer/Désactiver les thèmes musicaux.") .
                         "</td></tr>";
                    $str .= "</table>\n";
                    $str .= "<input type='submit' name='updateMusic' value='" . localize("Mettre à jour") . "' /><br/><br/>";
                }
                
                // ------------------------- SUPPRESSION 1 SEUL PERSO --------------------------------
                
                $str .= "<table class='maintable centerareawidth'><tr><td colspan=2 class='mainbglabel'>" . localize("Supprimer ce personnage") . "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle mainerror'><b>ATTENTION CETTE OPÉRATION EST IRRÉVERSIBLE !!! De plus cette action réduit le nombre total de personnage possible de 1, sauf si c'est votre dernier. </b> </td></tr> ";
                $str .= "</table>";
                $str .= "<input type='submit' name='deleteChar' value='" . localize("Supprimer ce personnage") .
                     "' onclick=\"if (window.confirm('Êtes vous certain de vouloir supprimer ce personnage ?')){return true;} else {return false;}\" />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Ce joueur ne peut pas être personnalisé");
                $str .= "</td></tr></table>\n";
            }
        }
        return $str;
    }
}
?>
