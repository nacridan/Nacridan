<?php

class CQNormalAttack extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQNormalAttack($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("ap") < ATTACK_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser une attaque.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            $str .= localize("Attaquer") . " <select id='selector_id' class='selector cqattackselectorsize' name='TARGET_ID'>";
            
            $gap = 0;
            
            $item = array();
            
            $dbnpc = new DBCollection("SELECT * FROM Player WHERE status='NPC' AND incity=0 AND hidden=0 AND disabled=0 AND map=" . $map . " AND ABS(x-" . $xp . ")<=" . $gap . " AND ABS(y-" . $yp . ")<=" . $gap, $db, 0, 0);
            $item[] = array(
                localize("-- PNJ sur votre zone --") => - 1
            );
            while (! $dbnpc->eof()) {
                if ($dbnpc->get("id") != $id) {
                    $item[] = array(
                        localize($dbnpc->get("racename")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
                    );
                }
                $dbnpc->next();
            }
            
            $dbp = new DBCollection("SELECT * FROM Player WHERE status='PC' AND incity=0 AND hidden=0 AND disabled=0 AND map=" . $map . " AND ABS(x-" . $xp . ")<=" . $gap . " AND ABS(y-" . $yp . ")<=" . $gap, $db, 0, 0);
            
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
            $item[] = array(
                localize("-- Personnage(s) sur votre zone --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("id") != $id) {
                    $item[] = array(
                        $dbp->get("name") => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1) {
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    } else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='37' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
