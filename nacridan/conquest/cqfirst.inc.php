<?php

class CQFirst extends HTMLObject
{

    public $str;

    public function CQFirst($nacridan)
    {
        $str = "<div class='content'>\n";
        $str .= "<div id='cqaction' class='mainmsg centerareawidthmsg'>\n";
        $str .= localize("Bienvenue dans le monde de Nacridan !") . "<br/><br/>";
        
        $str .= "<b>" . localize("Vous venez de ramasser un parchemin détaillant le lieu d'un trésor, vous le rangez dans votre équipement !") . "</b><br/><br/>";
        
        $str .= localize("Si vous ne savez pas par où commencer, vous pouvez toujours aller consutlter votre équipement pour en savoir plus sur ce fameux parchemin.") . "<br/><br/><br/>";
        
        $str .= localize("Conseil : Si vous êtes perdu, allez faire un tour rapide dans les règles. Cela vous prendra 5 minutes et vous pourrez commencer votre aventure plus sereinement.") . "<br/><br/><br/>";
        $str .= localize("PS: il n'est pas obligé de lire toutes les règles pour se faire une idée du jeu.");
        
        $str .= "</div>";
        $str .= "</div>";
        
        $this->str = $str;
    }

    public function toString()
    {
        return $this->str;
    }
}
?>