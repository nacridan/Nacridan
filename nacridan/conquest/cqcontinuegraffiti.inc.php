<?php

class CQContinueGraffiti extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQContinueGraffiti($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        if ($curplayer->get("state") != "prison") {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Cette action n'est pas disponible.");
            $str .= "</td></tr></table>";
        } else {
            
            $dbe = new DBCollection("SELECT * FROM Graffiti WHERE terminé=0 AND id_Building=" . $curplayer->get("inbuilding") . " AND id_Player=" . $curplayer->get("id"), $db);
            
            if ($curplayer->get("ap") < CONTINUE_GRAFF_AP) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour continuer à graver sur le mur.");
                $str .= "</td></tr></table>";
            } elseif ($dbe->count() == 0) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Il n'y a aucune gravure à continuer.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr><td  class='mainbgtitle' >";
                $str .= localize("Continuer : ") . "<select id='Graff' class='selector cqattackselectorsize' name='GRAFF_ID'>";
                
                // $dbp=new DBCollection("SELECT * FROM Graffiti WHERE Graffiti.id_Building=".$curplayer->get("inbuilding")." AND id_Player=".$curplayer->get("id"),$db);
                
                $graffs = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un de vos textes --") . "</option>";
                while (! $dbe->eof()) {
                    $dbid = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("id_Player"), $db);
                    $graffs[] = array(
                        localize($dbid->get("name")) . " a gravé : " . localize($dbe->get("text")) => $dbe->get("id")
                    );
                    $dbe->next();
                }
                
                foreach ($graffs as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select><br /><label>Entrez 1 à 10 caractères à graver : </label>";
                $str .= "<input type ='text' name='GRAF_MSG' maxlength='10' />";
                $str .= "<br /><label>Graffiti terminé ? Oui : </label><input type ='checkbox' value='1' name='GRAF_FINI' />";
                
                $str .= "</td><td class='mainbgtitle' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . CONTINUE_GRAFFITI . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}

?>
