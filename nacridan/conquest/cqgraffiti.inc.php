<?php

class CQGraffiti extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGraffiti($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        if ($curplayer->get("state") != "prison") {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Cette action n'est pas disponible.");
            $str .= "</td></tr></table>";
        } else {
            if ($curplayer->get("ap") < GRAFF_AP) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour graver quelque chose sur le mur.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr><td  class='mainbgtitle' >";
                $str .= localize("Graffiti") . " <br /><label>Entrez 1 à 10 caractères à graver : </label>";
                
                $str .= "<input type ='text' name='GRAF_MSG' maxlength='10' />";
                $str .= "<br /><label>Graffiti terminé ? Oui : </label><input type ='checkbox' value='1' name='GRAF_FINI' />";
                
                $str .= "</select></td><td class='mainbgtitle' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . GRAFFITI . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}

?>
