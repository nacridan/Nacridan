<?php

class CQConsultGraffiti extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQConsultGraffiti($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        if ($curplayer->get("state") != "prison") {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Cette action n'est pas disponible.");
            $str .= "</td></tr></table>";
        } else {
            $dbe = new DBCollection("SELECT id FROM Graffiti WHERE id_Building=" . $curplayer->get("inbuilding"), $db);
            
            if ($curplayer->get("ap") < CONSULT_GRAFF_AP) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour déchiffrer ce qu'il semble écrit sur le mur.");
                $str .= "</td></tr></table>";
            } elseif ($dbe->count() == 0) {
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Il semble n'y avoir encore aucun graffiti.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr><td  class='mainbgtitle' >";
                $str .= localize("Lire les gravures");
                
                $str .= "</td><td class='mainbgtitle' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . CONSULT_GRAFFITI . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}

?>
