<?php

class CQGiveCity extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGiveCity($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("ap") < GIVE_XP_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour donner un village.");
            $str .= "</td></tr></table>";
        } else {
            if (! isset($_POST["action"])) {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'><tr><td>";
                
                // ----------------------
                
                $str .= "<select id='target' class='selector cqattackselectorsize' name='CITY_ID'>";
                
                $dbp = new DBCollection("SELECT id,name FROM Building WHERE id_Player=" . $id, $db, 0, 0);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez le village --") . "</option>";
                
                while (! $dbp->eof()) {
                    
                    $item[] = array(
                        $dbp->get("name") => $dbp->get("id")
                    );
                    $dbp->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                
                $str .= "</select></td><td>";
                
                // ---------------------------
                $str .= "<select id='target' class='selector cqattackselectorsize' name='TARGET_ID'>";
                
                $dbp = new DBCollection("SELECT id,name FROM Player WHERE incity=0 AND hidden=0 AND disabled=0 AND status='PC' AND map=" . $map . " AND ABS(x-" . $xp . ")<=" . GAP_XP . " AND ABS(y-" . $yp . ")<=" . GAP_XP, $db, 0, 0);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
                $item[] = array(
                    localize("-- Personnage(s) sur votre zone --") => - 1
                );
                while (! $dbp->eof()) {
                    if ($dbp->get("id") != $id) {
                        $item[] = array(
                            $dbp->get("name") => $dbp->get("id")
                        );
                    }
                    $dbp->next();
                }
                
                /*
                 * $dbp=new DBCollection("SELECT id_Player\$friend,name FROM MailContact LEFT JOIN Player ON id_Player\$friend=Player.id WHERE status='PC' AND id_Player=".$curplayer->get("id"),$db,0,0);
                 *
                 * //if($dbp->count()>0)
                 * $item[]= array( localize("-- Le(s) Contact(s) Enregistré(s)--") => -1);
                 * while(!$dbp->eof())
                 * {
                 * $item[]=array($dbp->get("name") => $dbp->get("id_Player\$friend"));
                 * $dbp->next();
                 * }
                 */
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                
                $str .= "</select></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . GIVE_CITY . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $dbp = new DBCollection("SELECT name FROM Player WHERE status='PC' AND id=" . quote_smart($_POST["TARGET_ID"]), $db, 0, 0);
                $dbb = new DBCollection("SELECT name FROM Building WHERE id=" . quote_smart($_POST["CITY_ID"]), $db, 0, 0);
                
                if ((! $dbp->eof()) && (! $dbb->eof())) {
                    $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    $str .= "<table class='maintable'><tr><td class='mainbgtitle'  width='550px'>";
                    
                    $str .= localize("Donner le village fortifié {city} à {name}. Continuer ?", array(
                        "city" => $dbb->get("name"),
                        "name" => $dbp->get("name")
                    )) . " :</td><td>";
                    
                    $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "<input name='TARGET_ID' type='hidden' value='" . quote_smart($_POST["TARGET_ID"]) . "' />";
                    $str .= "<input name='CITY_ID' type='hidden' value='" . quote_smart($_POST["CITY_ID"]) . "' />";
                    $str .= "<input name='action' type='hidden' value='" . GIVE_CITY . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</td></tr></table>";
                    $str .= "</form>";
                } else {
                    $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                    $str .= localize("Le destinataire n'est pas un joueur valide ou le village n'est pas un village valide.");
                    $str .= "</td></tr></table>";
                }
            }
        }
        return $str;
    }
}
?>
