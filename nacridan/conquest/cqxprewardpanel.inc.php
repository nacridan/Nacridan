<?php
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

class CQXPRewardPanel extends HTMLObject
{

    public $nacridan;

    public $players;

    public $curplayer;

    public $db;

    public $style;

    public function CQXPRewardPanel($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $curplayer = $this->curplayer;
        $id = 0;
        
        if (isset($_POST['createconfirm'])) // ------------------- DISTRIBUTION -----------------
{
            if ($_POST['createconfirm'] == 'yes') {
                require_once (HOMEPATH . "/class/Mission.inc.php");
                require_once (HOMEPATH . "/class/MissionReward.inc.php");
                
                $mission = new Mission();
                $mission->set("PlayerList", $_POST["ID_LIST"]);
                $mission->set("name", base64_decode($_POST["TITLE"]));
                $mission->set("content", base64_decode($_POST["CONTENT"]));
                $mission->set("date", gmdate("Y-m-d h:m:s"));
                $mission->set("id_BasicMission", 100); // Mission personnalisée avec MJ
                $mission->updateDB($db);
                
                $idarray = explode(",", $_POST['ID_LIST']);
                $N = count($idarray);
                $coeffArray = array(
                    1 => 0,
                    2 => 0.5,
                    3 => 1,
                    4 => 1.5,
                    5 => 2
                );
                $coeff = $coeffArray[$_POST['MARK']];
                $T = $_POST['DURATION']; // en jour
                
                $str = "<table class='maintable " . $this->style . "'>\n";
                
                foreach ($idarray as $participant_id) {
                    if (is_numeric($participant_id)) {
                        $participant = new Player();
                        $participant->load($participant_id, $db);
                        $Np = $participant->get('level');
                        $px = floor(floor($T / 4) * ($Np + 5) * $coeff) + 2 * $N;
                        $str .= "<tr class='mainbgbody' ><td colspan=3> " . $participant->get('name') . " (" . $participant_id . ") niv " . $Np;
                        $str .= ", <b> PX = " . $px . "</b></td></tr>";
                        $participant->set('xp', ($participant->get('xp') + $px));
                        PlayerFactory::checkLevelUp($participant, $db);
                        
                        $missionReward = new MissionReward();
                        $missionReward->set("idMission", $mission->get("id"));
                        $missionReward->set("xpStop", $px);
                        $missionReward->set("id_Player", $participant_id);
                        $missionReward->set("MissionDuration", $T);
                        $missionReward->set("MissionMark", $_POST['MARK']);
                        $missionReward->set("NumberOfParticipants", $N);
                        $missionReward->set("PlayerLevelAtThatTime", $Np);
                        $missionReward->updateDB($db);
                    }
                }
                
                $str .= "<tr><td class='mainbgbody'>La récompense a été distribuée.</td></tr>";
                $str .= "</table>";
            } else {
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>La récompense a été distribuée.</td></tr>";
                $str .= "</table>";
            }
            return $str;
        } elseif (isset($_POST['check'])) // --------------VERIFICATION DE L'ANNONCE
{
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody' colspan='3'>Cette récompense convient-elle ?</td></tr>";
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td width='100px'>" . gmdate("d-m-Y") . "</td>";
            $str .= "<td width='250px' >*** <b>" . stripslashes($_POST["title"]) . "</b> ***</td>";
            $str .= "</tr>";
            
            $content = stripslashes($_POST["content"]);
            $content = nl2br(bbcode($content));
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td colspan=3>" . $content . "</td>";
            $str .= "</tr>";
            
            if (is_numeric($_POST["duration"]) && is_numeric($_POST["mark"])) {
                
                $str .= "<tr class='mainbgtitle' ><td colspan=3>Liste des participants</td></tr>";
                $idarray = explode(",", $_POST['idlist']);
                $N = count($idarray);
                $coeffArray = array(
                    1 => 0,
                    2 => 0.5,
                    3 => 1,
                    4 => 1.5,
                    5 => 2
                );
                $coeff = $coeffArray[$_POST['mark']];
                $T = $_POST['duration'];
                
                foreach ($idarray as $participant_id) {
                    if (is_numeric($participant_id)) {
                        $participant = new Player();
                        $participant->load($participant_id, $db);
                        $Np = $participant->get('level');
                        $px = floor(floor($T / 4) * ($Np + 5) * $coeff) + 2 * $N;
                        $str .= "<tr class='mainbgbody' ><td colspan=3> " . $participant->get('name') . " (" . $participant_id . ") niv " . $Np;
                        $str .= ", <b> PX = " . $px . "</b></td></tr>";
                    }
                }
            }
            $str .= "<input type='hidden' name='TITLE' value='" . base64_encode($_POST['title']) . "'/>";
            $str .= "<input type='hidden' name='CONTENT' value='" . base64_encode($_POST['content']) . "'/>";
            $str .= "<input type='hidden' name='ID_LIST' value='" . $_POST['idlist'] . "'/>";
            $str .= "<input type='hidden' name='DURATION' value='" . $_POST['duration'] . "'/>";
            $str .= "<input type='hidden' name='MARK' value='" . $_POST['mark'] . "'/>";
            $str .= "<input type='hidden' name='createconfirm' value='yes'/>";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</table></form>";
            
            return $str;
        } elseif (isset($_POST['create'])) // ------- FORMULAIRE DE CREATION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='300' name='title'  id='title' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' ><label for='duration'>Durée de l'animation en jours, hors période de chasse. </label><input type='text' maxlength='30' name='duration'  id='duration' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' ><label for='mark'>Note donnée par le MJ de 1 à 5. </label><input type='text' maxlength='30' name='mark'  id='mark' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' ><label for='idlist'>Liste des participants sous la forme id1,id2, id3 etc. : </label><input type='text' maxlength='2000' name='idlist'  id='idlist' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' align='left'><label for='content'> Description courte de la mission, au minimum mettre le lien vers le récit dans le forum : </label> </td></tr>";
            $str .= "";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='content' id='content' /> </textarea></td>";
            $str .= "</tr>";
            $str .= "<td><input type='submit' name='check' value='Créer'/></td></tr>";
            
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['deleteconfirm'])) // --------------------- SUPPRESSION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            if ($_POST['deleteconfirm'] == 'yes') {
                $newslist = unserialize(stripslashes($_POST['newslist']));
                foreach ($newslist as $id) {
                    $dbd = new DBCollection("DELETE FROM News WHERE id=" . $id, $db, 0, 0, false);
                }
                $str .= "<tr><td>Les annonces sélectionnées ont normalement été supprimées.</td></td>";
            } else
                $str .= "<td><td>Rien n'a été changé.</td></td>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['delete'])) // --------------- CONFIRMATION DE LA SUPPRESSION
{
            
            $newslist = unserialize(stripslashes($_POST['newslist']));
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td class='mainbgbody'>Êtes-vous sûr de vouloir annuler ces récompenses ?</td>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui, suppression.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='no' id='no' />" . "<label for='no'>Non, on garde.</label></td></tr>";
            $str .= "</table>";
            
            $newsidlist = array();
            
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>id</td>\n";
            $str .= "<td class='mainbglabel' width='100px' align='center'>Titre</td>\n";
            $str .= "<td class='mainbglabel' align='center'>Date</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            foreach ($newslist as $arr) {
                if (array_key_exists("news" . $arr["id"], $_POST)) {
                    $str .= "<tr><td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["title"] . " </td>\n";
                    $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                    $str .= "</tr>\n";
                    
                    $newsidlist[] = $arr["id"];
                }
            }
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='newslist' value=" . serialize($newsidlist) . "/>";
            $str .= "<input type='submit' name='ok' value='Ok'/>";
            $str .= "</form>";
            return $str;
        } else // ---------------- LISTE des Récompenses -------------
{
            
            $dbm = new DBCollection("SELECT * FROM Mission WHERE id_BasicMission=100", $db);
            
            $data = array();
            
            while (! $dbm->eof()) {
                
                $strPlayerList = "";
                $idarray = explode(",", $dbm->get('PlayerList'));
                foreach ($idarray as $participant_id) {
                    if (is_numeric($participant_id)) {
                        $participant = new Player();
                        $participant->load($participant_id, $db);
                        $strPlayerList .= $participant->get('name') . " (" . $participant_id . ") niv " . $participant->get('level') . ", <br>";
                    }
                }
                $data[] = array(
                    'id' => $dbm->get('id'),
                    "title" => $dbm->get('name'),
                    "date" => $dbm->get("date"),
                    "list" => $strPlayerList
                );
                $dbm->next();
            }
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='4' class='mainbgtitle'><b>" . localize('LISTES DES ANCIENNES RÉCOMPENSES') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('Title') . "</td>\n";
            $str .= "<td class='mainbglabel' align='center'>" . localize('Date') . "</td>\n";
            $str .= "<td class='mainbglabel' align='center'> Participants</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>";
            
            foreach ($data as $arr) {
                $str .= "<tr><td class='mainbgbody' width='27px' align='center'> <input type='checkbox' name='news" . $arr["id"] . "' id='news" . $arr["id"] . "' /> </td>";
                $str .= "<td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["title"] . " </td>\n";
                $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                $str .= "<td class='mainbgbody'  align='left'> " . $arr["list"] . " </td>";
                $str .= "</tr>\n";
            }
            $str .= "</table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td><input type='submit' name='delete' value='Supprimer'/></td>";
            
            $str .= "</table>";
            $str .= "<input type='hidden' name='newslist' value='" . serialize($data) . "'/>";
            
            $str .= "</form>";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<td><input type='submit' name='create' value='Créer'/></td></tr>";
            $str .= "</table>";
            $str .= "</form>";
            return $str;
        }
    }
}
?>

 
      
