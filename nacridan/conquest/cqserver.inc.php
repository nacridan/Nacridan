<?php

class CQServer extends HTMLObject
{

    public $nacridan;

    public $players;

    public $curplayer;

    public $db;

    public $style;

    public function CQServer($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $curplayer = $this->curplayer;
        $id = 0;
        
        $str = "<table class='maintable " . $this->style . "'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('STATUT IA') . "</b></td>\n";
        $str .= "</tr>\n";
        
        if (isset($_POST["turnoff"])) {
            
            exec("(cd ../AI;./nacridanTEST.php stop)  > ../AI/logfolder/logfile.txt &", $resoff, $erroff);
            if (! $erroff)
                $str .= "<tr><td>*** AI should be OFF *** (besoin de rafraichir pour voir l'effet)  </td></tr>";
            else
                $str .= "<tr><td>Erreur : " . $erroff . "  </td></tr>";
        }
        
        if (isset($_POST["turnon"])) {
            
            // exec("(cd ../AI;nohup ./nacridanTEST.php start) > logAI-V2.txt &",$reson,$erron);
            exec("(cd ../AI;./nacridanTEST.php start) 2>../AI/logfolder/nacridanAI.log >../AI/logfolder/logfile.txt &", $reson, $erron);
            
            if (! $erron)
                $str .= "<tr><td>*** AI should be ON ***  </td></tr>";
            else
                $str .= "<tr><td>Erreur : " . $erron . "  </td></tr>";
        }
        
        if (isset($_POST["rmnohup"])) {
            exec("cd ../AI/logfolder;rm logfile.txt");
            if (! file_exists("../AI/logfolder/logfile.txt"))
                $str .= "<tr><td>*** nohup should be REMOVED ***  </td></tr>";
            else
                $str .= "<tr><td>*** remove nohup  FAILED ***  </td></tr>";
        }
        
        if (isset($_POST["rmlogAI"])) {
            exec("cd ../AI/logfolder;rm nacridanAI.log");
            if (! file_exists("../AI/logfolder/nacridanAI.log"))
                $str .= "<tr><td>***  AI log should be REMOVED ***  </td></tr>";
            else
                $str .= "<tr><td>*** remove log AI FAILED ***  </td></tr>";
        }
        
        if (isset($_POST["rmlogmain"])) {
            exec("cd ../AI/logfolder;rm nacridan.log");
            if (! file_exists("../AI/logfolder/nacridan.log"))
                $str .= "<tr><td>***  Main log should be REMOVED ***  </td></tr>";
            else
                $str .= "<tr><td>*** remove Main DB FAILED ***  </td></tr>";
        }
        
        if (isset($_POST["rmlogfe"])) {
            exec("cd ../AI/logfolder;rm nacridanFE.log");
            if (! file_exists("../AI/logfolder/nacridanFE.log"))
                $str .= "<tr><td>***  Main log should be REMOVED ***  </td></tr>";
            else
                $str .= "<tr><td>*** remove Main FE FAILED ***  </td></tr>";
        }
        
        $status = "OFF";
        exec("cd ../AI;./status.sh", $res);
        foreach ($res as $lign)
            if ($lign >= 2)
                $status = "ON";
            else
                $status = "OFF";
        $str .= "<tr><td>IA V2 <b>" . $status . " !!!</b> </td></tr>";
        
     
        $str .= "</table>";
        
        $str .= "<table class='maintable " . $this->style . "'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('DOWNLOAD DES LOGS') . "</b>  </td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td>--------------------------------  </td></tr>";
        $str .= "<tr><td>ATTENTION faire clique droit télécharger, sinon affichage dans le navigateur, et les fichiers peuvent être lourd !!!  </td></tr>";
        $str .= "<tr><td>--------------------------------  </td></tr>";
        $str .= "<form name='form'  method=post target='_self'>\n";
        if (file_exists("../AI/logfolder/nacridanFE.log"))
            $str .= "<tr><td><a href='../AI/logfolder/nacridanFE.log'>Le log des erreurs fatales</a>  </td><td><input type='submit' name='rmlogfe' value='supprimer'/></td></tr>";
        else
            $str .= "<tr><td>Pas de log des erreurs fatales trouvé</td></tr>";
        
        if (file_exists("../AI/logfolder/nacridan.log"))
            $str .= "<tr><td><a href='../AI/logfolder/nacridan.log'>Le log base de donnée</a>  </td><td><input type='submit' name='rmlogmain' value='supprimer'/></td></tr>";
        else
            $str .= "<tr><td>Pas de log base de donnée trouvé  </td></tr>";
        
        if (file_exists("../AI/logfolder/nacridanAI.log"))
            $str .= "<tr><td><a href='../AI/logfolder/nacridanAI.log'>Le log de l'IA</a>  </td><td><input type='submit' name='rmlogAI' value='supprimer'/></td></tr>";
        else
            $str .= "<tr><td>Pas de log IA trouvé  </td></tr>";
        
        if (file_exists("../AI/logfolder/logfile.txt"))
            $str .= "<tr><td><a href='../AI/logfolder/logfile.txt'>Fichier nohup de l'IA</a>   </td> <td><input type='submit' name='rmnohup' value='supprimer'/></td></tr>";
        else
            $str .= "<tr><td>Pas de nohup trouvé  </td></tr>";
        $str .= "</table>";
        $str .= "</form>";
        
        $str .= "<form name='form'  method=post target='_self'>\n";
        $str .= "<table class='maintable " . $this->style . "'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('AI CONTROL') . "</b></td>\n";
        $str .= "</tr>\n";
        
        $str .= "<tr> <td><input type='submit' name='turnoff' value='STOP IA'/></td></tr>";
        $str .= "<tr> <td><input type='submit' name='turnon' value='RUN IA'/></td></tr>";
        // $str.="<tr> <td><input type='submit' name='rmnohup' value='supprimer le nohup'/></td></tr>";
        $str .= "</table>";
        $str .= "</form>";
        
        return $str;
    }
}
?>

 
      
