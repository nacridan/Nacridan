<?php

class CQRecall extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQRecall($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $str .= "<table class='maintable'><tr><td  class='mainbgtitle' width='750px'>";
        $str .= "<select class='selector cqattackselectorsize' name='CHOICE'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Que souhaitez vous faire  --") . "</option>";
        $str .= "<option value='1'> Accepter le rappel pour la totalité de vos PA </option>";
        $str .= "<option value='2'> Refuser le rappel (0 pa) </option>";
        $str .= "</select>";
        $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
        $str .= "<input name='action' type='hidden' value='" . RECALL . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</td></tr></table>";
        $str .= "</form>";
        
        return $str;
    }
}

?>