<?php

class CQFormBasicMission extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQFormBasicMission($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        if (isset($_POST['createconfirm'])) // ------------------- CREATION -----------------
{
            if ($_POST['createconfirm'] == 'yes') {
                require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
                
                $idrace = base64_decode($_POST["idrace"]);
                $gender = base64_decode($_POST["gender"]);
                $body = base64_decode($_POST["descriptionMission"]);
                
                $dbplayers = new DBCollection("INSERT INTO EscortMissionRolePlay(description, id_Race, gender) VALUES('" . quote_smart($body) . "','" . quote_smart($idrace) . "','" . quote_smart($gender) . "')", $db, 0, 0, false);
                
                $str = "<table class='maintablebody'>\n";
                $str .= "<tr><td class='mainbgbody'>Le texte a été ajouté aux descriptions de missions d'escorte.</td></tr>";
                
                $str .= "</table>";
            } else {
                $str = "<table class='maintabletitle '>\n";
                $str .= "<tr><td class='mainbgbody'>La description n'a pas été ajoutée.</td></tr>";
                $str .= "</table>";
            }
            return $str;
        } elseif (isset($_POST['check'])) // --------------VERIFICATION DE L'ANNONCE
{
            $section = array();
            $idrace = array();
            $gender = array();
            $gender = stripslashes($_POST["gender"]);
            $idrace = stripslashes($_POST["idrace"]);
            
            $race = array(
                1 => "Humain",
                2 => "Elfe",
                3 => "Nain",
                4 => "Dorane"
            );
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintabletitle'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('AJOUT D\'UNE DESCRIPTION') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody'>Cette description convient-elle ? Attention la description va être rentrée dans la base de données et sera accessible pour tous les joueurs !<br /></td></tr>";
            $str .= "<tr><td>";
            $str .= "<div style='color: #999999'>";
            
            $str .= "<b>" . $race[$idrace] . "</b> <b>" . $gender . "</b><br/>";
            $str .= "<br/>";
            $str .= str_replace("\n", "<br/>", stripslashes($_POST["descriptionMission"])) . "<br/>";
            $str .= "<br/>";
            $descriptionMission = stripslashes(addslashes($_POST["descriptionMission"]));
            
            $str .= "</div>";
            $str .= "</td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='createconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='createconfirm' value='no' id='no' />" . "<label for='no'>Non.</label></td></tr>";
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='idrace' value='" . base64_encode($idrace) . "'/>";
            $str .= "<input type='hidden' name='gender' value='" . base64_encode($gender) . "'/>";
            $str .= "<input type='hidden' name='descriptionMission' value='" . base64_encode($descriptionMission) . "'/>";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</form>";
            
            return $str;
        } 

        elseif (isset($_POST['consult'])) // --------------CONSULTATION DE L'ANNONCE
{
            $descriptionid = $_POST["Description"];
            $dbmiss = new DBCollection("SELECT * FROM EscortMissionRolePlay WHERE id=" . $descriptionid, $db);
            $race = array(
                1 => "Humain",
                2 => "Elfe",
                3 => "Nain",
                4 => "Dorane"
            );
            $idrace = $dbmiss->get("id_Race");
            
            $str = "<table class='maintabletitle'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle' width='150px'><br /><b>PNJ concerné</b> :<br/> " . $race[$idrace] . " " . $dbmiss->get("gender") . "<br /><br /></td>";
            $str .= "<td class='mainbgtitle'>" . $dbmiss->get("description") . "</td>";
            $str .= "</tr>";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' width='100px' align='left'><label for='descriptionMission'> Nouvelle description </label> </td>";
            $str .= "<td class='mainbgbody' width='200px' align='center'> <textarea rows=6 cols=80 name='descriptionMission' id='descriptionMission' />" . $dbmiss->get("description") . " </textarea></td>";
            $str .= "</tr>";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgbody' width='200px' align='left'><label for='idrace'> Race du PNJ à escorter</label> </td>";
            $str .= "<td class='mainbgbody' width='300px' align='center'> <select id='idrace' class='selector cqattackselectorsize' name='idrace'>";
            $str .= "<option value=" . $idrace . " selected='selected'>" . $race[$idrace] . "</option>
		       <option value='1'>Humain</option>
			   <option value='2'>Elfe</option>
			   <option value='3'>Nain</option>
			   <option value='4'>Dorane</option></select></td>";
            $str .= "</tr>";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgbody' width='200px' align='left'><label for='gender'> Genre du PNJ à escorter</label> </td>";
            $str .= "<td class='mainbgbody' width='300px' align='center'> <select id='gender' class='selector cqattackselectorsize' name='gender'>";
            $str .= "<option value=" . $dbmiss->get("gender") . " selected='selected'>" . $dbmiss->get("gender") . "</option>
		       <option value='Femme'>Femme</option>
			   <option value='Homme'>Homme</option></select></td>";
            $str .= "</tr>";
            
            $str .= "<input id='description_id' name='description_id' type='hidden' value='" . $dbmiss->get("id") . "' />";
            $str .= "<tr><td><input type='submit' name='modif' value='Modifier'/></td>";
            $str .= "<td><a href=''><input type='button' value='Annuler'/></a></td></tr>";
            
            $str .= "</form>";
            $str .= "</table>";
            return $str;
        } 

        elseif (isset($_POST['modif'])) // --------------MODIFICATION DE L'ANNONCE
{
            $section = array();
            $idrace = array();
            $gender = array();
            $gender = stripslashes($_POST["gender"]);
            $idrace = stripslashes($_POST["idrace"]);
            $descriptionid = $_POST["description_id"];
            
            $race = array(
                1 => "Humain",
                2 => "Elfe",
                3 => "Nain",
                4 => "Dorane"
            );
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintabletitle'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('MODIFICATION D\'UNE DESCRIPTION') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody'>Cette description convient-elle ? Attention la description va être rentrée dans la base de données et sera accessible pour tous les joueurs !<br /></td></tr>";
            $str .= "<tr><td>";
            $str .= "<div style='color: #999999'>";
            
            $str .= "<b>" . $race[$idrace] . "</b> <b>" . $gender . "</b><br/>";
            $str .= "<br/>";
            $str .= str_replace("\n", "<br/>", stripslashes($_POST["descriptionMission"])) . "<br/>";
            $str .= "<br/>";
            $descriptionMission = stripslashes(addslashes($_POST["descriptionMission"]));
            
            $str .= "</div>";
            $str .= "</td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='modifconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='modifconfirm' value='no' id='no' />" . "<label for='no'>Non.</label></td></tr>";
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='idrace' value='" . base64_encode($idrace) . "'/>";
            $str .= "<input type='hidden' name='gender' value='" . base64_encode($gender) . "'/>";
            $str .= "<input type='hidden' name='descriptionMission' value='" . base64_encode($descriptionMission) . "'/>";
            $str .= "<input id='description_id' name='description_id' type='hidden' value='" . $descriptionid . "' />";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</form>";
            
            return $str;
        } 

        elseif (isset($_POST['modifconfirm'])) // --------------MODIFICATION DE L'ANNONCE
{
            
            require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
            
            $idrace = base64_decode($_POST["idrace"]);
            $gender = base64_decode($_POST["gender"]);
            $body = base64_decode($_POST["descriptionMission"]);
            $descriptionid = $_POST["description_id"];
            echo "<script>alert('" . $body . "');</script>";
            
            $dbplayers = new DBCollection("UPDATE EscortMissionRolePlay SET id_Race='" . quote_smart($idrace) . "', gender='" . quote_smart($gender) . "', description='" . quote_smart($body) . "' WHERE id=" . $descriptionid, $db, 0, 0, false);
            
            $str = "<table class='maintablebody'>\n";
            $str .= "<tr><td class='mainbgtitle'>Les modifications ont bien été prises en compte.</td></tr>";
            
            $str .= "</table>";
            
            return $str;
        } 

        else // ------- FORMULAIRE DE CREATION
{
            $str = "<table class='maintabletitle'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('AJOUT D\'UNE NOUVELLE DESCRIPTION') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgbody' width='200px' align='left'><label for='idrace'> Race du PNJ à escorter</label> </td>";
            $str .= "<td class='mainbgbody' width='300px' align='center'> <select id='idrace' class='selector cqattackselectorsize' name='idrace'>";
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez la race --") . "</option>
		       <option value='1'>Humain</option>
			   <option value='2'>Elfe</option>
			   <option value='3'>Nain</option>
			   <option value='4'>Dorane</option></select></td>";
            $str .= "</tr>";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgbody' width='200px' align='left'><label for='gender'> Genre du PNJ à escorter</label> </td>";
            $str .= "<td class='mainbgbody' width='300px' align='center'> <select id='gender' class='selector cqattackselectorsize' name='gender'>";
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez le genre --") . "</option>
		       <option value='Femme'>Femme</option>
			   <option value='Homme'>Homme</option></select></td>";
            $str .= "</tr>";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' width='100px' align='left'><label for='descriptionMission'> Contenu du message </label> </td>";
            $str .= "<td class='mainbgbody' width='200px' align='center'> <textarea rows=6 cols=40 name='descriptionMission' id='descriptionMission' /> </textarea></td>";
            $str .= "</tr>";
            
            $str .= "<tr><td><input type='submit' name='check' value='Ajouter'/></td></tr>";
            $str .= "</form>";
            
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CONSULTATION DES DESCRIPTIONS') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr><td class='mainbgtitle' ><select id='Description' class='selector cqattackselectorsize' name='Description'>";
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Descriptions disponibles --") . "</option>";
            
            $dbmiss = new DBCollection("SELECT * FROM EscortMissionRolePlay", $db);
            
            while (! $dbmiss->eof()) {
                $race = array(
                    1 => "Humain",
                    2 => "Elfe",
                    3 => "Nain",
                    4 => "Dorane"
                );
                $idrace = $dbmiss->get("id_Race");
                
                $item[] = array(
                    localize("(" . $race[$idrace] . ", " . $dbmiss->get("gender") . ") " . $dbmiss->get("description")) => $dbmiss->get("id")
                );
                
                $dbmiss->next();
            }
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td><input type='submit' name='consult' value='Consulter'/></td></tr>";
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        }
    }
}
?>
