<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];

Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$player = null;
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

if (isset($_GET["id"]))
    $_GET["id"] = quote_smart($_GET["id"]);

if (isset($_POST["search"])) {
    if (is_numeric($_POST["search"])) {
        $_GET["id"] = $_POST["search"];
    } else {
        $dbp = new DBCollection("SELECT id FROM City WHERE name='" . quote_smart($_POST["search"]) . "'", $db);
        if (! $dbp->eof()) {
            $_GET["id"] = $dbp->get("id");
        }
    }
}

if (isset($_GET["id"])) {
    
    $id = $_GET["id"];
    $city = new City();
    $city->load($id, $db);
    $str = "<table><tr><td style='vertical-align: top'><table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    
    $extra = "";
    if (isset($_GET["body"])) {
        $extra = "&body=1";
    }
    
    $str .= "<b><h1>" . localize($city->get('name')) . "</h1></b></td>\n";
    // $str.="<td align='right'>\n";
    // $str.="&nbsp;<br/><br/><form action='?id=".$id.$extra."' method='POST'>".localize("Recherche (n°/nom)")." <input name='search' type='text' size=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    // $str.="</tr>\n";
    $str .= "</table>";
    
    $picname = "cityunknown.jpg";
    
    if ($city->get("bigpic") != "" and file_exists("../pics/city/" . $city->get('bigpic')))
        $picname = $city->get('bigpic');
        
        // $str.="<img src='../pics/city/".$city->get("bigpic")."'>";
    $str .= "<table class='maintable' width='640px'>\n";
    $str .= "<tr><td class='mainbgbody' colspan=\"2\"><img src='../pics/city/" . $picname . "' width=640 height=480></td></tr>\n";
    if ($city->get('captured') >= 4) {
        $dbp = new DBCollection("SELECT id,name FROM Player WHERE id='" . $city->get('id_Player') . "'", $db);
        if ($dbp->count() > 0) {
            $str .= "<tr><td class='mainbglabel' width='25%'><h2>Gouverneur:</h2></td>";
            $str .= "<td class='mainbgbody'><h2><a href=\"profile.php?id=" . $dbp->get("id") . "\" class=\"stylemainpc popupify\">";
            $str .= $dbp->get("name") . "</a></h2></td></tr>\n";
        }
    }
    $str .= "<tr><td class='mainbgbody' colspan=\"2\">" . bbcode($city->get('description')) . "</td></tr>\n";
    $str .= "</table>";
    
    $MAIN_BODY->add($str);
    $MAIN_BODY->add("</td><td style='vertical-align: top'>");
    
    if ($curplayer->get("advert") == - 1) // ON VIRE LES PUB POUR LE MOMENT MAIS FAUDRA REFLECHIR A DES SYSTEMS D'ECHANGE De BANNIERES
        if (mt_rand(0, 9) != 0) // if(false)
{
            $MAIN_BODY->add(
                "<script language=\"javascript\" src=\"http://media.fastclick.net/w/get.media?sid=38368&m=3&tp=7&d=j&t=n\"></script>
<noscript><a href=\"http://media.fastclick.net/w/click.here?sid=38368&m=3&c=1\" target=\"_blank\">
<img src=\"http://media.fastclick.net/w/get.media?sid=38368&m=3&tp=7&d=s&c=1\"
width=160 height=600 border=1></a></noscript>");
        } else {
            $MAIN_BODY->add(
                "
<script type=\"text/javascript\"><!--
google_ad_client = \"pub-3725586809828932\";
google_ad_width = 160;
google_ad_height = 600;
google_ad_format = \"160x600_as\";
google_ad_type = \"text_image\";
//2007-07-23: InGamePopUp-Right-Skyscraper
google_ad_channel = \"9411983126\";" . getGoogleAdsPalette() . "google_ui_features = \"rc:6\";
//-->
</script>
<script type=\"text/javascript\"
  src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">
</script>");
            
            /*
             * $MAIN_BODY->add("<script language=\"JavaScript\" type=\"text/javascript\" src=\"http://ad.advertstream.com/adjs_r.php?what=zone:12806&inf=no\">
             * </script><noscript><a href=\"http://ad.advertstream.com/adsclick.php?what=zone:12806&inf=no&n=a7af095a\" target=\"_blank\"><img src=\"http://ad.advertstream.com/ads.php?what=zone:12806&inf=no&n=a7af095a\" border=\"0\" alt=\"\"></a></noscript>");
             */
        }
    
    $MAIN_BODY->add("</td></tr></table>");
    
    $google = "<script src='http://www.google-analytics.com/urchin.js' type='text/javascript'>
</script>
<script type='text/javascript'>
_uacct = 'UA-1166023-1';
urchinTracker();
</script>";
    
    $MAIN_BODY->add($google);
    $MAIN_BODY->add("<script src='" . CONFIG_HOST . "/javascript/astrack.js' type='text/javascript'></script>\n");
    $MAIN_PAGE->render();
}
