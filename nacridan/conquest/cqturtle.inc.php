<?php

class CQTurtle extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQTurtle($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        $state = $curplayer->get("state");
        
        $dbt = new DBCollection("SELECT Player.id as id, Player.x as x, Player.y as y, Player.room as room FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" . $curplayer->get("id") . " AND Player.id_BasicRace=263", $this->db);
        
        if ($state != "turtle") {
            if ($dbt->eof()) {
                $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Erreur : cette tortue n'existe plus");
                $str .= "</td></tr></table>";
            } elseif (distHexa($xp, $yp, $dbt->get("x"), $dbt->get("y")) > 1) {
                $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous êtes trop loin de cette tortue pour la chevaucher");
                $str .= "</td></tr></table>";
            } elseif ($curplayer->get("inbuilding") && ($curplayer->get("room") != $dbt->get("room"))) {
                $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous êtes trop loin de cette tortue pour la chevaucher");
                $str .= "</td></tr></table>";
            } elseif ($curplayer->get("ap") < RIDE_TURTLE_AP) {
                $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour chevaucher cette tortue.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Chevaucher cette tortue ?");
                $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='ID_TARGET' type='hidden' value='" . $dbt->get("id") . "' />";
                $str .= "<input name='action' type='hidden' value='" . RIDE_TURTLE . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        } else {
            if ($curplayer->get("ap") < TURTLE_UNMOUNT_AP) {
                $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour descendre.");
                $str .= "</td></tr></table>";
            } elseif (! $curplayer->get("inbuilding")) {
                require_once (HOMEPATH . "/lib/MapInfo.inc.php");
                $mapinfo = new MapInfo($this->db);
                $xp = $curplayer->get("x");
                $yp = $curplayer->get("y");
                $validzone = $mapinfo->getValidMap($xp - 1, $yp - 1, 2, $curplayer->get("map"));
                require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                $str .= "<table class='maintable'>";
                for ($j = 0; $j < 3; $j ++) {
                    
                    $str .= "<tr class='tr_move'>";
                    if ($j == 0) {
                        $str .= "<td width='175px' rowspan=3>&nbsp;";
                        $str .= "</td>";
                    }
                    for ($i = 0; $i < 3; $i ++) {
                        
                        if (BasicActionFactory::freePlace($xp - 1 + $i, $yp - 1 + $j, $curplayer->get("map"), $this->db))
                            $CoordEnable = "";
                        else
                            $CoordEnable = "disabled";
                        
                        if (! ($i == 0 && $j == 0) && ! ($i == 2 && $j == 2)) {
                            if ($validzone[$i][$j])
                                $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>(" . ($xp - 1 + $i) . ", " . ($yp - 1 + $j) . ")</div></td>";
                            else
                                $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>( - , - )</div></td>";
                        }
                    }
                    
                    if ($j == 2) {
                        $str .= "<td>";
                        $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                        $str .= "<input name='ID_TARGET' type='hidden' value='" . $dbt->get("id") . "' />";
                        $str .= "<input name='action' type='hidden' value='" . TURTLE_UNMOUNT . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</td>";
                    } else {
                        $str .= "<td>&nbsp;</td>";
                    }
                }
                
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Descendre de la tortue ?");
                $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='ID_TARGET' type='hidden' value='" . $dbt->get("id") . "' />";
                $str .= "<input name='action' type='hidden' value='" . TURTLE_UNMOUNT . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<input name='XYNEW' type='hidden' value='" . ($curplayer->get("x") . "," . $curplayer->get("y")) . "' />";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
