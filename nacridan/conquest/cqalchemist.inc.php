<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/BasicEquipment.inc.php");

class CQAlchemist extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQAlchemist($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        
        if (isset($_POST["Buy"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdBasic"]))
                    EquipFactory::buyBasicEquipment($this->curplayer, quote_smart($_POST["IdBasic"]), min(quote_smart($_POST["Level"]), $this->nacridan->getCity()->get("level")), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un équipement à acheter.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if ($err == "" && $str == "") {
            if (isset($_GET["type"])) {
                $type = quote_smart($_GET["type"]);
            } else {
                $type = 1;
            }
            
            if (isset($_GET["level"])) {
                $selectedlevel = min(quote_smart($_GET["level"]), $this->nacridan->getCity()->get("level"));
            } else {
                $selectedlevel = 0;
            }
            
            $eq = new BasicEquipment();
            $eqmodifier = new Modifier();
            
            $cond = "id_EquipmentType=8";
            
            $dbe = new DBCollection("SELECT zone," . $eq->getASRenamer("EQ", "EQ") . "," . $eqmodifier->getASRenamer("EQM", "EQM") . " FROM BasicEquipment AS EQ LEFT JOIN EquipmentType AS ET ON EQ.id_EquipmentType=ET.id LEFT JOIN Modifier_BasicEquipment AS EQM ON  id_Modifier_BasicEquipment=EQM.id WHERE " . $cond . " ORDER BY EQ.id_EquipmentType", $db);
            
            $target = "/conquest/conquest.php?center=alchemist";
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'><b>" . localize('A L C H I M I S T E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= "Potions ";
            $str .= "<select name=url onchange=window.location=this.options[selectedIndex].value>";
            for ($i = 0; $i <= $this->nacridan->getCity()->get("level"); $i ++) {
                if ($i == $selectedlevel)
                    $extra = "selected";
                else
                    $extra = "";
                
                $str .= "<option value='../conquest/conquest.php?center=alchemist&level=" . $i . "' " . $extra . ">" . localize('Niveau') . " " . $i . "</option>\n";
            }
            $str .= "</select></td></tr>\n";
            $str .= "</table>\n";
            
            $data = array();
            
            $array = array();
            while (! $dbe->eof()) {
                $eq->DBLoad($dbe, "EQ");
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($selectedlevel);
                $eqmodifier->initCharacStr();
                $checkbox = "<input type='radio' name='IdBasic' value='" . $eq->get("id") . "'>";
                $name = "<b>" . localize($eq->get("name")) . "</b><br/>";
                
                foreach ($eqmodifier->m_characLabel as $key) {
                    $tmp = $eqmodifier->getModifStr($key);
                    if ($tmp != "0") {
                        $name .= " " . localize($key) . ": " . $tmp . " |";
                    }
                }
                
                $price = floor((100 * ($selectedlevel * $selectedlevel + 1)) / $eq->get("frequency")) . " " . localize("PO");
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            $str .= "</table>\n";
            
            $str .= createTable(3, $array, array(), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("Nom et caractéristiques"),
                    "class='mainbglabel' width='70%' align='left'"
                ),
                array(
                    localize("Prix"),
                    "class='mainbglabel' width='25%' align='right'"
                )
            ), "class='maintable centerareawidth'");
            
            $str .= "<input id='Buy' type='submit' name='Buy' value='" . localize("Acheter") . "' />";
            $str .= "<input type='hidden' name='Level' value='" . $selectedlevel . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=alchemist' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
