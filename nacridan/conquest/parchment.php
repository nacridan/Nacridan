<?php

/**
 *Affiche le contenu du parchemin
 *
 * Définit une fonction par type de mission
 *
 *@author Nacridan
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<table><tr><td valign='top'>\n";

$str .= "<table class='maintable' width='660px'>\n";
$str .= "<tr>\n";
$str .= "<td colspan='2' class='mainbgtitle'><b>D E S C R I P T I O N</b>\n";
$str .= "</td>\n";
$str .= "</tr>\n";
$str .= "</table>";
$str .= "</table>";

if (isset($_GET["type"]) && isset($_GET["id"])) {
    $id = quote_smart($_GET["id"]);
    
    if ($_GET["type"] == "mission") // AFFICHAGE MISSION STANDARD
{
        $mission = new Mission();
        
        $dbm = new DBCollection("SELECT " . $mission->getASRenamer("Mission", "M") . ",Equipment.id FROM Equipment,Mission WHERE Equipment.id_Mission!=0 AND Equipment.id_Mission=Mission.id AND (Equipment.id_Player=" . $curplayer->get("id") . " OR Equipment.sell=1) AND Equipment.id=" . $id, $db, 0, 0);
        if (! $dbm->eof()) {
            // echo "je suis rentrer dans la condition if";
            $mission->DBLoad($dbm, 'M');
            $condition = new DBCollection("SELECT * FROM MissionCondition WHERE idMission=" . $mission->get("id"), $db);
            $reward = new DBCollection("SELECT * FROM MissionReward WHERE idMission=" . $mission->get("id"), $db);
            $str .= "<table class='maintable mainbgtitle' width='640px'>\n";
            $str .= "<tr><td>\n";
            
            $str .= "<b><h1>".$mission->get('name') . "</h1></b></td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $str .= "<table><tr><td valign='top'>\n";
            
            $str .= "<table class='maintable' width='620px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>D E S C R I P T I O N &nbsp; D E &nbsp; L A &nbsp; M I S S I O N</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr><td class='mainbgbody'>";
            $typeMission = $mission->get('id_BasicMission');
            
            if ($mission->get("id_Quest") == 0) {
                
                switch ($typeMission) {
                    case 1:
                        $str .= escortMission($mission, $condition, $reward, $curplayer);
                        break;
                    case 2:
                        $str .= releaseMission($mission);
                        break;
                    case 3:
                        $str .= HostelStandardMission($mission);
                        break;
                    case 4:
                        $str .= treasureMission($mission);
                        break;
                }
            } else
                $str .= InListMission($mission);
            $str .= "</td></tr></table>";
            
            $str .= "</table>";
        }
    } elseif ($_GET["type"] == "caravan") // AFFICHAGE MISSION COMMERCIALE
{
        $caravan = new Caravan();
        $dbm = new DBCollection("SELECT " . $caravan->getASRenamer("Caravan", "CAR") . ",Equipment.id FROM Equipment,Caravan WHERE Equipment.id_Caravan!=0 AND Equipment.id_Caravan=Caravan.id AND (Equipment.id_Player=" . $curplayer->get("id") . " OR Equipment.sell=1) AND Equipment.id=" . $id, $db, 0, 0);
        if (! $dbm->eof()) {
            $caravan->DBLoad($dbm, 'CAR');
            $str = "<table class='maintable mainbgtitle' width='640px'>\n";
            $str .= "<tr><td>\n";
            
            $str .= "<b><h1>Mission Commerciale</h1></b></td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $str .= "<table><tr><td valign='top'>\n";
            
            $str .= "<table class='maintable' width='640px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>D E S C R I P T I O N</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr><td class='mainbgbody'>";
            
            $dbtarget = new DBCollection("SELECT id_City,x,y FROM Building WHERE id=" . $caravan->get("id_endbuilding"), $db);
            $dbstart = new DBCollection("SELECT x,y FROM Building WHERE id=" . $caravan->get("id_startbuilding"), $db);
            $dist = distHexa($curplayer->get("x"), $curplayer->get("y"), $dbtarget->get("x"), $dbtarget->get("y"));
            
            $limitdate = strtotime($caravan->get("date"));
            $Mois = array(
                "janvier",
                "février",
                "mars",
                "avril",
                "mai",
                "juin",
                "juillet",
                "août",
                "septembre",
                "octobre",
                "novembre",
                "décembre"
            );
            $strdate = date("j", $limitdate) . " " . $Mois[(date("n", $limitdate) - 1)] . " à " . date("G", $limitdate) . "h" . date("i", $limitdate);
            
            $city = new City();
            $city->load($dbtarget->get("id_City"), $db);
            
            if ($city->get("type") == $city->get("name"))
                $cityname = "à un " . strtolower($city->get("type"));
            elseif ($city->get("type") == "Ville")
                $cityname = "à la " . strtolower($city->get("type")) . " de " . $city->get("name");
            else
                $cityname = "au " . strtolower($city->get("type")) . " de " . $city->get("name");
            
            $content = unserialize($caravan->get("content"));
            
            $str .= "<br />";
            $str .= "Vous avez organisé une caravane composée d'une tortue géante qui transporte les marchandises suivantes :";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<table class='maintable insidebuildingleftwidth'>";
            $str .= "<tr class='mainbglabel'><td> Type </td> <td> Niveau </td> <td> Quantité </td> <td> Prix (PO) </td> </tr>";
            
            $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $content["type1"], $db, 0, 0);
            $lvl = $content["level1"];
            $nb = $content["quantity1"];
            $price1 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
            
            $price1 = $price1 * $nb;
            $name = $dbbe->get("name");
            $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td><td> " . $nb . " </td> <td> " . $price1 . " </td> </tr>";
            
            $price2 = 0;
            $price3 = 0;
            
            if (isset($content["type2"])) {
                $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $content["type2"], $db, 0, 0);
                $lvl = $content["level2"];
                $nb = $content["quantity2"];
                $price2 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
                $price2 = $price2 * $nb;
                $name = $dbbe->get("name");
                $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td><td> " . $nb . " </td> <td> " . $price2 . " </td> </tr>";
            }
            
            if (isset($content["type3"])) {
                $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $content["type3"], $db, 0, 0);
                $lvl = $content["level3"];
                $nb = $content["quantity3"];
                $price3 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
                $price3 = $price3 * $nb;
                $name = $dbbe->get("name");
                $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td><td> " . $nb . " </td> <td> " . $price3 . " </td> </tr>";
            }
            
            $totalprice = $price1 + $price2 + $price3;
            $str .= "<tr class='mainbglabel'><td  colspan=3> Valeur totale du chargement </td><td> " . $totalprice . " </td></tr>";
            $str .= "<tr class='mainbglabel'><td  colspan=3> Valeur totale du chargement à la revente dans le comptoir commercial cible </td><td><b> " . $caravan->get("finalprice") . " </b></td></tr>";
            
            $str .= "</table>\n";
            $str .= "<br />";
            $str .= " Vous devez transporter ces marchandises " . $cityname . " situé en <b>(" . $city->get("x") . "/" . $city->get("y") . ")</b>. Une fois sur place, rendez-vous au comptoir commercial pour vendre vos marchandises et terminer la mission. La tortue doit se trouver dans les écuries du comptoir commercial pour que vous puissiez décharger la caravane.";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "Vous devez terminer la mission avant le " . $strdate . ". Il vous reste " . $dist . " lieues à parcourir, à vol d'oiseau.";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            $str .= "<br />";
            
            $str .= "<br />";
            $str .= "PS : pour contrôler la tortue vous devez vous installer à l'intérieur de sa carapace, avec les marchandises. Allez dans les écuries, puis montez dans la tortue, ensuite déplacez-vous normalement.";
            $str .= "Vous trouverez plus de détails dans <a class='parchment' href='#' onclick='javascript:detailedRules(\"" . CONFIG_HOST . "/i18n/rules/fr/formulas.php?formula_id=10\");'>les règles</a>.";
            $str .= "</td></tr></table>";
            $str .= "</table>";
        }
    }elseif ($_GET["type"] == "anim"){ // AFFICHAGE CONTENU PARCHO ANIMATION SANS MISSION
		$dbe = new DBCollection("SELECT id,name,description FROM Equipment WHERE (Equipment.id_Player=" . $curplayer->get("id") . " OR Equipment.sell=1) AND Equipment.id=" . $id, $db, 0, 0);
		$str = "<table class='maintable mainbgtitle' width='640px'>\n";
      $str .= "<tr><td>\n";
      
      $str .= "<b><h1>".$dbe->get('name')."</h1></b></td>\n";
      $str .= "</tr>\n";
      $str .= "</table>\n";
      
      $str .= "<table><tr><td valign='top'>\n";
      
      $str .= "<table class='maintable' width='640px'>\n";
      $str .= "<tr>\n";
      $str .= "<td colspan='2' class='mainbgtitle'><b>C O N T E N U</b>\n";
      $str .= "</td>\n";
      $str .= "</tr>\n";
      
      $str .= "<tr><td class='mainbgbody'>";
      $str .= bbcode($dbe->get("description"));
      $str .= "</td></tr></table>";
      $str .= "</table>";						
	 }
}

$MAIN_BODY->add($str);

$MAIN_PAGE->render();

function escortMission(&$mission, &$condition, &$reward, $curplayer)
{
    
    // $dbcond=new DBCollection("SELECT * FROM MissionCondition WHERE idMission=1",$db);
    $str = $mission->get("content") . "<br><br>";
    $str .= "<br> <center>------------------------</center><br><br>";
    
    $str .= "Vous devez escorter une personne entre deux points." . "<br/>";
    
    $coord = array();
    $cpt = 0;
    
    $coord[0] = array(
        "x" => $condition->get("positionXstart"),
        "y" => $condition->get("positionYstart")
    );
    $coord[1] = array(
        "x" => $condition->get("positionXstop"),
        "y" => $condition->get("positionYstop")
    );
    /*
     * foreach($mission->infos as $key)
     * {
     * list($x,$y)=explode(":",$key);
     * $coord[$cpt]=array("x" => $x, "y" => $y);
     * $cpt++;
     * }
     */
    if ($mission->get("Mission_Active")) {
        $limitdate = strtotime($mission->get("date"));
        $Mois = array(
            "janvier",
            "février",
            "mars",
            "avril",
            "mai",
            "juin",
            "juillet",
            "août",
            "septembre",
            "octobre",
            "novembre",
            "décembre"
        );
        $strdate = date("j", $limitdate) . " " . $Mois[(date("n", $limitdate) - 1)] . " à " . date("G", $limitdate) . "h" . date("i", $limitdate);
        $dist = distHexa($curplayer->get("x"), $curplayer->get("y"), $condition->get("positionXstop"), $condition->get("positionYstop"));
        $str .= "<b>Attention ! </b>Vous devez terminer la mission avant le " . $strdate . ", il vous reste " . $dist . " lieues à parcourir.<br/><br/>";
    } else
        $str .= "Quand la mission sera en cours, la date limite et la distance restante à parcourir s'affichera ici.<br/><br/>";
    
    for ($i = 0; $i <= 1; $i ++) {
        if ($i == 0)
            $str .= ("Point de départ") . " <b>(" . $coord[0]["x"] . "," . $coord[0]["y"] . ")</b><br/>";
        else
            $str .= ("Point d'arrivée") . " <b>(" . $coord[1]["x"] . "," . $coord[1]["y"] . ")</b>";
        
        $str .= "<div class='pmarged'>";
        $str .= ("Utilisez votre parchemin à cet endroit et vous recevrez :") . "<br/>";
        if ($i == 0) {
            $str .= ("- La personne que vous devrez escorter") . "<br/>";
            
            // $actions=$mission->actions[$coord[$i]["x"].":".$coord[$i]["y"]];
            // $str.="- ".$reward->get("moneyStart")." ".("PO")."<br/>";
            // $str.="- ".$reward->get("xpStart")." ".("PX")."<br/>";
        } else {
            // $str.=("- La personne que vous devrez escorter")."<br/>";
            
            // $actions=$mission->actions[$coord[$i]["x"].":".$coord[$i]["y"]];
            $str .= "- " . $reward->get("moneyStop") . " " . ("PO") . "<br/>";
            // $str.="- ".$reward->get("xpStop")." ".("PX")."<br/>";
        }
        /*
         * foreach($actions as $action)
         * {
         * switch($action["t"])
         * {
         * case 1:
         * $str.="- ".$action["v"]." ".("PO")."<br/>";
         * break;
         * case 2:
         * $str.="- ".$action["v"]." ".("PX")."<br/>";
         * break;
         * }
         * }
         */
        
        $str .= "</div><br/>";
    }
    $str .= ("Ps: Vous devrez déplacer et contrôler la personne à escorter comme si c'était votre propre personnage. (Elle devrait apparaître dans le selecteur en haut à droite de l'interface.). De plus, il vous faudra utiliser le parchemin au point d'arrivée, avec le personnage à escorter dans votre vue, pour terminer la mission avec succès.");
    return $str;
}

function releaseMission(&$mission)
{
    $str = ("Vous devez libérer une ville assiégée par des créatures.") . "<br/><br/>";
    $coord = array();
    $cpt = 0;
    foreach ($mission->infos as $key => $val) {
        list ($x, $y) = explode(":", $key);
        $coord[$cpt] = array(
            "x" => $x,
            "y" => $y
        );
        $cpt ++;
    }
    
    $str .= ("Position de la ville assiégée") . " <b>(" . $coord[0]["x"] . "," . $coord[0]["y"] . ")</b><br/>";
    
    $str .= "<div class='pmarged'>";
    $str .= ("Une fois créatures exterminées, utilisez votre parchemin à cet endroit et vous recevrez :") . "<br/>";
    $actions = $mission->actions[$coord[0]["x"] . ":" . $coord[0]["y"]];
    foreach ($actions as $action) {
        switch ($action["t"]) {
            case 1:
                $str .= "- " . $action["v"] . " " . ("PO") . "<br/>";
                break;
            case 2:
                $str .= "- " . $action["v"] . " " . ("PX") . "<br/>";
                break;
        }
    }
    
    $str .= "</div><br/>";
    return $str;
}

function treasureMission(&$mission)
{
    $db = DB::getDB();
    $str = ("Ce parchemin détaille le lieu d'un trésor.") . "<br/><br/>";
    $coord = array();
    $cpt = 0;
    foreach ($mission->infos as $key => $val) {
        list ($x, $y) = explode(":", $key);
        $coord[$cpt] = array(
            "x" => $x,
            "y" => $y
        );
        $cpt ++;
    }
    
    $str .= ("Lieu du trésor") . " <b>(" . $coord[0]["x"] . "," . $coord[0]["y"] . ")</b><br/>";
    $str .= "<div class='pmarged'>";
    $str .= ("Utilisez votre parchemin à cet endroit et vous recevrez :") . "<br/>";
    $actions = $mission->actions[$coord[0]["x"] . ":" . $coord[0]["y"]];
    foreach ($actions as $action) {
        switch ($action["t"]) {
            case 1:
                $str .= "- " . $action["v"] . " " . ("PO") . "<br/>";
                break;
            case 3:
                $eqid = "";
                foreach ($action["v"] as $eqname) {
                    if ($eqid == "")
                        $eqid = $mission->objs[$eqname];
                    else
                        $eqid .= " OR id=" . $mission->objs[$eqname];
                    
                    echo $eqid;
                    $dbequipment = new DBCollection("SELECT id,x,y,name,po,extraname,level,templateLevel FROM Equipment WHERE id=" . $eqid, $db, 0, 0);
                    
                    while (! $dbequipment->eof()) {
                        if ($dbequipment->get("name") != "") {
                            $str .= "- " . ($dbequipment->get("name"));
                            
                            if ($dbequipment->get("extraname") != "") {
                                $tlevel = $dbequipment->get("templateLevel");
                                $str .= " <span class='template'>" . ($dbequipment->get("extraname")) . "(" . $tlevel . ")</span>";
                            }
                            $str .= " (" . ("Niveau") . " " . $dbequipment->get("level") . ")<br/>";
                        }
                        $dbequipment->next();
                    }
                }
                break;
        }
    }
    $str .= "</div><br/>";
    $str .= ("ATTENTION: Après avoir utilisé votre parchemin à cet endroit, les objets apparaissent au sol (sauf les Pièces d'Or (PO)). Pensez à les ramasser s'ils vous intéressent.");
    return $str;
}

function HostelStandardMission(&$mission)
{
    $db = DB::getDB();
    $ML = $mission->get("Mission_level");
    $str = ("<u>Mission de niveau $ML</u><br/><br/>");
    $RP = $mission->get("RP_Mission");
    $str .= ($RP) . "<br/><br/><br/><br/>";
    $str .= ("RECAPITULATIF DE LA MISSION :") . "<br/><br/>";
    $str .= ("Vous devez escorter une personne entre deux points.") . "<br/><br/>";
    $coord = array();
    $cpt = 0;
    foreach ($mission->infos as $key => $val) {
        list ($x, $y) = explode(":", $key);
        $coord[$cpt] = array(
            "x" => $x,
            "y" => $y
        );
        $cpt ++;
    }
    
    for ($i = 0; $i <= 1; $i ++) {
        if ($i == 0)
            $str .= ("Point de départ") . " <b>(" . $coord[0]["x"] . "," . $coord[0]["y"] . ")</b><br/>";
        else
            $str .= ("Point d'arrivée") . " <b>(" . $coord[1]["x"] . "," . $coord[1]["y"] . ")</b>";
        
        $str .= "<div class='pmarged'>";
        $str .= ("Utilisez votre parchemin à cet endroit et vous recevrez :") . "<br/>";
        if ($i == 0)
            $str .= ("- La personne que vous devrez escorter") . "<br/>";
        
        $actions = $mission->actions[$coord[$i]["x"] . ":" . $coord[$i]["y"]];
        foreach ($actions as $action) {
            switch ($action["t"]) {
                case 1:
                    $str .= "- " . $action["v"] . " " . ("PO") . "<br/>";
                    break;
                case 2:
                    $str .= "- " . $action["v"] . " " . ("PX") . "<br/>";
                    break;
            }
        }
        
        $str .= "</div><br/>";
    }
    $str .= ("Ps: Vous devrez déplacer et contrôler la personne à escorter comme si c'était votre propre personnage. (Elle devrait apparaître dans le selecteur en haut à droite de l'interface.)");
    return $str;
}

function InListMission(&$mission)
{
    $db = DB::getDB();
    $condition = new MissionCondition();
    $idmission = $mission->get("id");
    
    $dbc = new DBCollection("SELECT " . $condition->getASRenamer("MissionCondition", "C") . " FROM MissionCondition WHERE idMission=" . $mission->get("id"), $db, 0, 0);
    $condition->DBLoad($dbc, 'C');
    
    $reward = new MissionReward();
    $dbr = new DBCollection("SELECT " . $reward->getASRenamer("MissionReward", "R") . " FROM MissionReward WHERE idMission=" . $mission->get("id"), $db, 0, 0);
    $reward->DBLoad($dbr, 'R');
    
    $quest = new Quest();
    $dbq = new DBCollection("SELECT " . $quest->getASRenamer("Quest", "Q") . " FROM Quest WHERE id=" . $mission->get("id_Quest"), $db, 0, 0);
    $quest->DBLoad($dbq, 'Q');
    
    $str = ("<b>Quête : " . $quest->get("name") . " </b><br/><br/>");
    $str .= ("<u>But de la quête :</u> <br/><br/>" . $quest->get("goal") . "<br/><br/>");
    $str .= ("Cette mission est la mission " . $mission->get("NumMission") . " sur les " . $quest->get("nbMission") . " missions que comporte cette quête.<br/><br/>");
    
    $ML = $mission->get("Mission_level");
    $str .= ("<u>Mission de niveau " . $ML . "</u><br/><br/>");
    $RP = $mission->get("RP_Mission");
    $str .= ($RP) . "<br/><br/><br/><br/>";
    
    $str .= ("RECAPITULATIF DE LA MISSION :") . "<br/><br/>";
    
    if (($condition->get("idPNJtoKill")) != 0)
        $str .= ("Utilisez le parchemin en x =" . $condition->get("positionXstop") . ", y=" . $condition->get("positionYstop") . " pour faire sortir le(s) monstre(s) de l'endroit ou il(s) se cache(nt). Une fois le(s) monstre(s) mort(s) utilisez le parchemin au même endroit pour recevoir la récompense.<br/><br/>");
    elseif (($condition->get("EnigmAnswer")) != '')
        $str .= ("Utilisez le parchemin en x =" . $condition->get("positionXstop") . ", y=" . $condition->get("positionYstop") . "vous devrez alors taper la réponse de l'énigme (un seul mot) pour recevoir la récompense.<br/><br/>");
    elseif ($reward->get("idEscort") != 0) {
        $str .= ("Utilisez le parchemin en x =" . $condition->get("positionXstart") . ", y=" . $condition->get("positionYstart") . " pour débuter la mission. <br/>");
        $str .= ("Vous y trouverez la personne à escorter.<br/><br/>");
        
        $str .= ("Utilisez le parchemin en x =" . $condition->get("EscortPositionX") . ", y=" . $condition->get("EscortPositionY") . " pour terminer la mission.<br/>");
        
        $str .= ("Vous devrez y avoir amené la personne à escorter pour que le parchemin fonctionne.<br/><br/>");
    } elseif ($reward->get("idEquipStart") != 0) {
        // $str.=("Utilisez le parchemin en x =".$condition->get("positionXstart").", y=".$condition->get("positionYstart")." pour débuter la mission. <br/>");
        $str .= ("L'objet à transporter se trouve normalement au sol, là où se trouvait ce parchemin.<br/><br/>");
        
        $str .= ("Utilisez le parchemin en x =" . $condition->get("EquipPositionX") . ", y=" . $condition->get("EquipPositionY") . " pour terminer la mission.<br/>");
        
        $str .= ("Vous devrez avoir déposé l'objet à transporté sur cette case et l'avoir déposé au sol pour que le parchemin fonctionne.<br/><br/>");
    } elseif (($condition->get("positionXstop")) != 0)
        $str .= ("Utilisez le parchemin en x =" . $condition->get("positionXstop") . ", y=" . $condition->get("positionYstop") . " pour trouver le trésor.<br/><br/>");
    
    if ($reward->get("moneyStart") != 0)
        $str .= ("Vous recevrez au point de départ " . $reward->get("moneyStart") . " PO.<br/>");
    
    if ($reward->get("xpStart") != 0)
        $str .= ("Vous recevrez au point de départ " . $reward->get("xpStart") . " PX.<br/>");
    
    if ($reward->get("moneyStop") != 0)
        $str .= ("Vous recevrez au point d'arrivé " . $reward->get("moneyStop") . " PO.<br/>");
    
    if ($reward->get("xpStop") != 0)
        $str .= ("Vous recevrez au point d'arrivé " . $reward->get("xpStop") . " PX.<br/>");
    
    if ($reward->get("idEquipStop") != 0)
        $str .= ("Vous recevrez au point d'arrivé un(e) " . $reward->get("NameEquipStop") . " " . $reward->get("ExtranameEquipStop") . ".<br/>");
    
    if ($reward->get("idEscort") != 0)
        $str .= ("<br/>Ps: Vous devrez déplacer et contrôler la personne à escorter comme si c'était votre propre personnage. (Elle devrait apparaître dans le selecteur en haut à droite de l'interface.)");
    if (($condition->get("EnigmAnswer")) != '')
        $str .= ("<br/>Ps: Tout est en minuscule.");
    return $str;
}
