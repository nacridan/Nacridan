<?php
require_once (HOMEPATH . "/class/BasicRace.inc.php");

class CQProfile extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQProfile($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function getCell($name, $curplayer, &$bm)
    {
        $str = "<td bgcolor=\"#3b302b\">" . $curplayer->getModifStr($name) . "</td><td bgcolor=\"#C2C0B0\">" . $bm[$name] . "</td><td bgcolor=\"#3b302b\">" .
             number_format($curplayer->getModifMean($name), 1, '.', '') . "</td><td bgcolor=\"#3b302b\">" . $curplayer->getModifMin($name) . "</td><td bgcolor=\"#3b302b\">" .
             $curplayer->getModifMax($name) . "</td>";
        return $str;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        if (isset($_GET["id"]) && $curplayer->get("authlevel") >= 3) {
            if (is_numeric($_GET["id"])) {
                $curplayer = new Player();
                $curplayer->externDBObj("Modifier");
                $curplayer->load($_GET["id"], $db);
                if ($curplayer->get("status") == "PC") {
                    $curplayer = $this->curplayer;
                } else {
                    $curplayer->getObj("Modifier")->initCharacStr();
                }
            }
        }
        
        $curplayer->externDBObj("BasicRace");
        $curplayer->loadExternDBObj($db);
        
        $modif = $curplayer->getObj("Modifier");
        
        $bm = array();
        foreach ($modif->m_characLabel as $label) {
            $bm[$label] = $modif->getModifStr($label . '_bm');
            if ($bm[$label] == "0") {
                $bm[$label] = "";
            }
        }
        
        $str = "<table class='maintable centerareawidth'>\n";
        $str .= "<tr><td class='mainbgtitle'>\n";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        $str .= "<table style=\"border-spacing: 0;\" ><tr><td valign='top'>\n";
        $str .= "<table class='maintable' width='300px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('NOM :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('name') . ' (' . $curplayer->get('id') .
             ')' . "</td></tr>\n";
        if ($curplayer->get("id_Team") > 0) {
            $dbrank = new DBCollection(
                "SELECT TeamRankInfo.name FROM TeamRank LEFT JOIN TeamRankInfo ON TeamRankInfo.id=id_TeamRankInfo WHERE id_Player=" . $curplayer->get("id") .
                     " AND TeamRank.id_Team=" . $curplayer->get("id_Team"), $db);
            $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('ALLÉGEANCE :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->getSub('Team', 'name') . "<br/>" .
                 $dbrank->get('name') . "</td></tr>\n";
        }
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('NIVEAU :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('level') . "</td></tr>\n";
        
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('CLASSE :') . "</b></td><td bgcolor='#3b302b'>" . localize($curplayer->getSub('BasicRace', 'class')) .
             "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('RACE :') . "</b></td><td bgcolor='#3b302b'>" . localize($curplayer->getSub('BasicRace', 'name')) .
             "</td></tr>\n";
        $gender = $curplayer->get('gender');
        if ($gender == "M")
            $gender = "Homme";
        else
            $gender = "Femme";
        
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('SEXE :') . "</b></td><td class='mainbgbody'>" . localize($gender) . "</td></tr>\n";
        if ($curplayer->get("id_BasicRace") == 5)
            $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('ARRESTATION(S) :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('nbkill') . "</td></tr>\n";
        else
            $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('MEURTRE(S) :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('nbkill') . "</td></tr>\n";
        if ($curplayer->get("modeAnimation") == 1)
            $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('PARTICULARITÉ :') . "</b></td><td bgcolor='#3b302b'><b>Mode animation actif</b></td></tr>\n";
        
        if ($curplayer->get("id_BasicRace") == 5)
            $str .= "<tr><td class='mainbglabel' width='120px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>" . localize('De force :') . "</b></td><td bgcolor='#3b302b'>" .
                 $curplayer->get('nbkillpc') . "</td></tr>\n";
        else
            $str .= "<tr><td class='mainbglabel' width='120px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>" . localize('Dont PJ :') . "</b></td><td bgcolor='#3b302b'>" .
                 $curplayer->get('nbkillpc') . "</td></tr>\n";
        
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('DÉCÈS :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('nbdeath') . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('SURSIS PRISON :') . "</b></td><td bgcolor='#3b302b'>" . floor($curplayer->get('prison') * 12) .
             " heures</td></tr>\n";
        $dbb = new DBCollection("SELECT id,x,y  FROM Building WHERE id=" . $curplayer->get("resurrectid"), $db);
        $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('RESURRECTION :') . "</b></td><td bgcolor='#3b302b'>" .
             ($dbb->count() > 0 ? "Temple situé en (X=" . $dbb->get("x") . "/Y=" . $dbb->get("y") . ")" : "Aucune") . "</td></tr>\n";
        $str .= "</table><br/>\n";
        $str .= "<table class='maintable' width='300px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('E X P É R I E N C E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='150px'><b>" . localize('PX :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('xp') . "</td></tr>\n";
        $level = $curplayer->get('level');
        $xpLevelUp = getXPLevelUp($level);
        $str .= "<tr><td class='mainbglabel' width='150px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>Niveau " . ($level + 1) . " :</b></td><td bgcolor='#3b302b'> " .
             (floor(($curplayer->get('xp') / $xpLevelUp) * 100)) . " %</td></tr>\n";
        
        $Nmar = floor($curplayer->get('merchant_level'));
        $str .= "<tr><td class='mainbglabel' width='150px'><b>" . localize('REPUTATION DE MARCHAND :') . "</b></td><td class='mainbgbody'>" . $Nmar . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='150px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>Niveau " . ($Nmar + 1) . " :</b></td><td bgcolor='#3b302b'> " .
             (($curplayer->get('merchant_level') - $Nmar) * 100) . " %</td></tr>\n";
        
        $str .= "<tr><td class='mainbglabel' width='150px'><b>" . localize('Pièce(s) d\'Or :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('money') . "</td></tr>\n";
        $str .= "</table><br/>\n";
        $str .= "<table class='maintable' width='300px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D I V E R S') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('PA :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('ap') . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('COORDONNÉES :') . "</b></td><td bgcolor='#3b302b'>X=" . $curplayer->get('x') . ',Y=' .
             $curplayer->get('y') . "</td></tr>\n";
        $str .= "</table>\n";
        $str .= "</td><td valign='top'> <img src='" . getImgSrc($curplayer, $this->db) . "'  class='profile_pic'></td></tr>\n";
        $str .= "</table><br/>\n";
        
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle' ><b>" . localize('C A R A C T É R I S T I Q U E S <br/> D E &nbsp &nbsp B A S E') . "</b></td>\n";
        $str .= "<td bgcolor='#505050' width='100px'><b>(" . localize('B / M') . ")</b></td>\n";
        $str .= "<td class='mainbgtitle' width='100px'><b>" . localize('M O Y E N N E') . "</b></td>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('M I N') . "</b></td>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('M A X') . "</b></td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('FORCE :') . '</b></td>' . $this->getCell('strength', $curplayer, $bm) . "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('DEXTERITE :') . '</b></td>' . $this->getCell('dexterity', $curplayer, $bm) . " </tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('VITESSE :') . '</b></td>' . $this->getCell('speed', $curplayer, $bm) . " </tr>       \n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('MAITRISE DE LA MAGIE :') . '</b></td>' . $this->getCell('magicSkill', $curplayer, $bm) . " </tr>\n";
        $str .= "</table><br/>\n";
        
        $dbup = new DBCollection("SELECT * FROM Upgrade WHERE id=" . $curplayer->get("id_Upgrade"), $db);
        if (! $dbup->eof()) {
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle centerareawidth' ><b>J A U G E S &nbsp &nbsp D' E V O L U T I O N</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbglabel' width='190px'><b>" . localize('V I E :') . "</b></td><td class='mainbgbody'>" . $dbup->get("hp") . "<td>  </tr>\n";
            $str .= "<tr><td class='mainbglabel' width='190px'><b>" . localize('FORCE :') . "</b></td><td class='mainbgbody'>" . $dbup->get("strength") . "<td> </tr>\n";
            $str .= "<tr><td class='mainbglabel' width='190px'><b>" . localize('DEXTERITE :') . "</b></td><td class='mainbgbody'>" . $dbup->get("dexterity") . "<td>  </tr>\n";
            $str .= "<tr><td class='mainbglabel' width='190px'><b>" . localize('VITESSE :') . "</b></td><td class='mainbgbody'>" . $dbup->get("speed") . "<td>  </tr>       \n";
            $str .= "<tr><td class='mainbglabel' width='190px'><b>" . localize('MAITRISE DE LA MAGIE :') . "</b></td><td class='mainbgbody'>" . $dbup->get("magicSkill") .
                 "<td>  </tr>\n";
            $str .= "</table><br/>\n";
        }
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle' ><b>" . localize('C A R A C T É R I S T I Q U E S <br/> I N T E R M E D I A I R E S') . "</b></td>\n";
        $str .= "<td bgcolor='#505050' width='100px'><b>(" . localize('B / M') . ")</b></td>\n";
        $str .= "<td class='mainbgtitle' width='100px'><b>" . localize('M O Y E N N E') . "</b></td>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('M I N') . "</b></td>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('M A X') . "</b></td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('ATTAQUE :') . '</b></td >' . $this->getCell('attack', $curplayer, $bm) . "</tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('DEFENSE :') . '</b></td>' . $this->getCell('defense', $curplayer, $bm) . " </tr>\n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('DÉGÂTS :') . '</b></td>' . $this->getCell('damage', $curplayer, $bm) . " </tr>       \n";
        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('ARMURE :') . '</b></td>' . $this->getCell('armor', $curplayer, $bm) . " </tr>\n";
        // $str.="<tr><td class='mainbglabel' width='130px'><b>".localize('RESISTANCE MAGIQUE :').'</b></td>'.$this->getCell('magicSkill',$curplayer,$bm)." </tr>\n";
        $str .= "</table>\n";
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id"), $this->db);
        $dbprot = new DBCollection("SELECT * FROM BM WHERE id_Player\$src=" . $curplayer->get("id") . " AND name='défendu'", $this->db);
        if (! $dbbm->eof() || ! $dbprot->eof()) {
            $str .= "<br/><table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('B O N U S / M A L U S') . "</b></td>\n";
            $str .= "</tr>\n";
            $bm = new StaticModifier();
            $bm->setAttach("StaticModifier_BM");
            $item = array();
            while (! $dbbm->eof()) {
                $bonusMalus = "";
                $life = "";
                if ($dbbm->get("id_StaticModifier_BM") != 0) {
                    $bm->load($dbbm->get("id_StaticModifier_BM"), $this->db);
                    $bm->updateFromBMLevel($dbbm->get("level"));
                    $bm->initCharacStr();
                    
                    foreach ($bm->m_characLabel as $label) {
                        
                        $value = $bm->getModifStr($label);
                        if ($value != "0")
                            $bonusMalus .= $value . " " . translateAtt($label) . ",";
                    }
                    
                    $bonusMalus = substr($bonusMalus, 0, - 1);
                    if ($dbbm->get("name") == "Aura de courage" && $dbbm->get("value") != 0)
                        $bonusMalus .= ", -" . $dbbm->get("value") . " PV à chaque nouvelle DLA";
                    $life = " (encore  " . $dbbm->get("life") . " DLA)";
                    if ($dbbm->get("life") == - 2 || $dbbm->get("life") == - 3)
                        $life = "";
                    
                    if ($dbbm->get("name") == "Infesté") {
                        $bonusMalus .= ", -" . $dbbm->get("level") . " PV à chaque nouvelle DLA";
                    }
                } elseif ($dbbm->get("name") == "Régénération") {
                    $bonusMalus = "+" . $dbbm->get("value") . " PV par dla";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Soleil de guérison") {
                    $bonusMalus = "+" . $dbbm->get("value") . "% de guérison automatique";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Bénédiction") {
                    $bonusMalus = "protection de niveau " . $dbbm->get("level") . " contre les malédictions";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Barrière enflammée") {
                    $bonusMalus = "inflige " . ($dbbm->get("value") / 2) . "D6 de dégât à tout attaquant au corps à corps";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Bulle de vie") {
                    $bonusMalus = "capable d'encaisser " . $dbbm->get("value") . " PV";
                    $life = "";
                } elseif ($dbbm->get("name") == "Course Celeste") {
                    $bonusMalus = "Déplacement à 1PA quelque soit le terrain";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "défendu") {
                    $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $this->db);
                    $bonusMalus = "Vous êtes défendu par " . $dbc->get("name");
                    $life = "";
                } elseif ($dbbm->get("name") == "Blessure gênante") {
                    $bonusMalus = "1 PA supplémentaire pour les déplacements";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Larçin") {
                    $bonusMalus = "Vous perdrez un objet de votre inventaire si vous mourrez.";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Exorcisme de l'ombre") {
                    $dbc = new DBCollection("SELECT id, name, racename FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $this->db);
                    if ($dbc->get("name") == "")
                        $name = $dbc->get("racename") . " (id:" . $dbc->get("id") . ")";
                    else
                        $name = $dbc->get("name");
                    
                    $bonusMalus = "Quantité d'énergie noire à libérer : " . $dbbm->get("value") . " sur " . $name;
                    $life = "";
                } elseif ($dbbm->get("name") == "Arrestation") {
                    $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $this->db);
                    $bonusMalus = "Sous l'arrestation  d'un " . $dbc->get("name");
                    $life = "";
                } elseif ($dbbm->get("name") == "En Prison") {
                    
                    $bonusMalus = "jusqu'au " . $dbbm->get("date");
                    $life = "";
                } elseif ($dbbm->get("name") == "Embuscade") {
                    if ($dbbm->get("id_Player\$src") == $dbbm->get("id_Player"))
                        $bonusMalus = "Vous êtes invisible pour tous ceux qui n'était pas là au moment d'utilisation de la compétence.";
                    else {
                        $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $this->db);
                        if ($dbc->count() > 0) {
                            $bonusMalus = $dbc->get("name") . " s'est embusqué sous vos yeux";
                        }
                    }
                    $life = "";
                } elseif ($dbbm->get("name") == "Sang de Lave") {
                    $bonusMalus = "perte de " . $dbbm->get("value") . " points de vie à chaque nouvelle DLA";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Blessure Profonde") {
                    $bonusMalus = "perte de " . $dbbm->get("value") . " points de vie à chaque nouvelle DLA";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Poison") {
                    $bonusMalus = "perte de " . $dbbm->get("value") . " points de vie à chaque nouvelle DLA";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                } elseif ($dbbm->get("name") == "Souffle de négation") {
                    $bonusMalus = "Invisible";
                } 

                elseif ($dbbm->get("name") == "Concentration") {
                    $ident = $dbbm->get("value");
                    if (floor($ident / 100) == 1)
                        $dbc = new DBCollection("SELECT name FROM BasicAbility WHERE id=" . ($ident - 100), $this->db);
                    if (floor($ident / 100) == 2)
                        $dbc = new DBCollection("SELECT name FROM BasicMagicSpell WHERE id=" . ($ident - 200), $this->db);
                    if (floor($ident / 100) == 3)
                        $dbc = new DBCollection("SELECT name FROM BasicTalent WHERE id=" . ($ident - 300), $this->db);
                    
                    $bonusMalus = "Vous bénéficiez de  " . $dbbm->get("level") . "% de maitrise supplémentaire dans l'action " . $dbc->get("name") .
                         " pour avoir déjà tenté de la réussir au cours de cette DLA";
                    $life = " (encore : " . $dbbm->get("life") . " DLA)";
                }
                
                $bonusMalus .= $life;
                $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize($dbbm->get("name")) . "</b></td><td class='mainbgbody'>" . $bonusMalus . "</td></tr>\n";
                $dbbm->next();
            }
            
            if (! $dbprot->eof()) {
                $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbprot->get("id_Player"), $this->db);
                $bonusMalus = "Vous défendez " . $dbc->get("name");
                $str .= "<tr><td class='mainbglabel' width='130px'><b> Protection </b></td><td class='mainbgbody'>" . $bonusMalus . "</td></tr>\n";
            }
            $str .= "</table>\n";
        }
        
        $dbc = new DBCollection(
            "SELECT * FROM Ability LEFT JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE id_Player=" . $curplayer->get("id") . " order by BasicAbility.name ", 
            $this->db);
        
        if (! $dbc->eof()) {
            $str .= "<br/><table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='" . ($dbc->count() >= 3 ? 6 : ($dbc->count() == 1 ? 2 : 4)) . "' class='mainbgtitle'><b>" . localize('C O M P É T E N C E (S)') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $i = 0;
            while (! $dbc->eof()) {
                while (! $dbc->eof()) {
                    if ($i % 3 == 0)
                        $str .= "<tr>";
                    
                    $str .= "<td class='mainbglabel' width='130px'><b>" . localize($dbc->get('name')) . "</b></td><td class='mainbgbody'>" . $dbc->get('skill') . "%</td>";
                    
                    if ($i % 3 == 2)
                        $str .= "</tr>\n";
                    
                    $i ++;
                    
                    $dbc->next();
                }
            }
            $str .= "</table>\n";
        }
        
        $dbc = new DBCollection(
            "SELECT * FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE id_Player=" . $curplayer->get("id") .
                 " order by BasicMagicSpell.level, BasicMagicSpell.name", $this->db);
        
        if (! $dbc->eof()) {
            $str .= "<br/><table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='" . ($dbc->count() >= 3 ? 6 : ($dbc->count() == 1 ? 2 : 4)) . "' class='mainbgtitle'><b>" . localize('S O R T I L È G E (S)') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $i = 0;
            while (! $dbc->eof()) {
                if ($i % 3 == 0)
                    $str .= "<tr>";
                
                $str .= "<td class='mainbglabel' width='130px'><b>" . localize($dbc->get('name')) . "</b></td><td class='mainbgbody'>" . $dbc->get('skill') . "%</td>";
                
                if ($i % 3 == 2)
                    $str .= "</tr>\n";
                
                $i ++;
                
                $dbc->next();
            }
            $str .= "</table>\n";
        }
        
        $dbc = new DBCollection(
            "SELECT * FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE id_Player=" . $curplayer->get("id") . " order by BasicTalent.name ", $this->db);
        
        if (! $dbc->eof()) {
            $str .= "<br/><table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='" . ($dbc->count() >= 3 ? 6 : ($dbc->count() == 1 ? 2 : 4)) . "' class='mainbgtitle'><b>" . localize('S A V O I R - F A I R E (S)') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $i = 0;
            while (! $dbc->eof()) {
                if ($i % 3 == 0)
                    $str .= "<tr>";
                
                $str .= "<td class='mainbglabel' width='130px'><b>" . localize($dbc->get('name')) . "</b></td><td class='mainbgbody'>" . $dbc->get('skill') . "%</td>";
                
                if ($i % 3 == 2)
                    $str .= "</tr>\n";
                
                $i ++;
                
                $dbc->next();
            }
            $str .= "</table>\n";
        }
        $dbc->first();
        $dbctal = new DBCollection(
            "select * from (SELECT BasicTalent.name as name,BasicEquipment.name as nameEquip FROM Talent INNER JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id 
		INNER JOIN Knowledge ON Knowledge.id_BasicTalent=BasicTalent.id and Talent.id_Player = Knowledge.id_Player INNER JOIN BasicEquipment on 
		BasicEquipment.id = Knowledge.id_BasicEquipment and ((Knowledge.id_BasicTalent = 13 and Knowledge.id_BasicEquipment >5) or Knowledge.id_BasicTalent <> 13) 
		WHERE Talent.id_Player=" . $curplayer->get("id") . "
        union SELECT BasicTalent.name as name, concat('Enchantement ',BasicTemplate.name,'/',BasicTemplate.name2) as nameEquip FROM Talent INNER JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id 
		inner join Knowledge ON Knowledge.id_BasicTalent=BasicTalent.id and Talent.id_Player = Knowledge.id_Player 
        INNER JOIN  BasicTemplate on BasicTemplate.id = Knowledge.id_BasicEquipment where Knowledge.id_Player = " . $curplayer->get("id") . ") tmp 
        order by name,nameEquip ", $this->db);
        
        if (! $dbctal->eof()) {
            $str .= "<br/><table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='" . ($dbc->count() >= 2 ? 4 : 2) . "' class='mainbgtitle'><b>" . localize('S A V O I R - F A I R E (S) - D E T A I L (S)') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $i = 0;
            $previous = '';
            while (! $dbctal->eof()) {
                if ($dbctal->get("name") != $previous) {
                    $str .= "</td>";
                }
                if ($i % 2 == 0) {
                    if ($dbctal->get("name") != $previous) {
                        if ($previous != '')
                            $str .= "</tr>";
                        $str .= "<tr>";
                    }
                }
                if ($dbctal->get("name") != $previous) {
                    $i ++;
                }
                if ($dbctal->get("name") != $previous) {
                    $str .= "<td class='mainbglabel' width='25%'><b>" . localize($dbctal->get('name')) . "</b></td><td class='mainbgbody' width='25%'>";
                } else {
                    $str .= "<br/>";
                }
                $str .= "-&nbsp;" . $dbctal->get('nameEquip');
                if ($dbctal->get("name") != $previous) {
                    
                    $previous = $dbctal->get("name");
                }
                
                $dbctal->next();
            }
            $str .= "</table>\n";
        }
        
        return $str;
    }
}

?>
