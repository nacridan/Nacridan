<?php
/**
 * @package binarychoice.system.unix
 * @since 1.0.2
 */

// Log message levels
define('DLOG_TO_CONSOLE', 1);
define('DLOG_NOTICE', 2);
define('DLOG_WARNING', 4);
define('DLOG_ERROR', 8);
define('DLOG_CRITICAL', 16);

/**
 * Daemon base class
 *
 * Requirements:
 * Unix like operating system
 * PHP 4 >= 4.3.0 or PHP 5
 * PHP compiled with:
 * --enable-sigchild
 * --enable-pcntl
 *
 * @package binarychoice.system.unix
 * @author Michal 'Seth' Golebiowski <seth@binarychoice.pl>
 * @copyright Copyright 2005 Seth
 * @since 1.0.2
 */
declare(ticks = 1);

class Daemon
{

    /**
     * #@+
     *
     * @access public
     */
    /**
     * User ID
     *
     * @var int
     * @since 1.0
     */
    var $userID = 99;

    /**
     * Group ID
     *
     * @var integer
     * @since 1.0
     */
    var $groupID = 99;

    /**
     * Path to PID file
     *
     * @var string
     * @since 1.0.1
     */
    var $pidFileLocation = '/tmp/nacridanAITEST.pid';

    /**
     * Home path
     *
     * @var string
     * @since 1.0
     */
    var $homePath = '/';

    /**
     * #@-
     */
    
    /**
     * #@+
     *
     * @access private
     */
    /**
     * Current process ID
     *
     * @var int
     * @since 1.0
     */
    var $_pid = 0;

    /**
     * Is this process a children
     *
     * @var boolean
     * @since 1.0
     */
    var $_isChildren = false;

    /**
     * Is daemon running
     *
     * @var boolean
     * @since 1.0
     */
    var $_isRunning = false;

    /**
     * #@-
     */
    var $jobs;

    /**
     * Constructor
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function Daemon()
    {
        // error_reporting(0);
        set_time_limit(0);
        ob_implicit_flush();
        register_shutdown_function(array(
            &$this,
            'releaseDaemon'
        ));
    }

    /**
     * Starts daemon
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function start()
    {
        $this->_logMessage('Starting daemon');
        
        if (! $this->_daemonize()) {
            $this->_logMessage('Could not start daemon', DLOG_ERROR);
            
            return false;
        }
        
        $this->_logMessage('Running...');
        
        $this->_isRunning = true;
        
        $this->_init();
        $this->_doTask();
        
        while ($this->_isRunning) {
            $this->_scheduler();
        }
        
        return true;
    }

    /**
     * Stops daemon
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function stop()
    {
        $this->_logMessage('Stoping daemon');
        
        $this->_isRunning = false;
        // $this->releaseDaemon();
    }

    /**
     * Stops daemon
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function kill()
    {
        if (file_exists($this->pidFileLocation)) {
            $pid = (int) trim(file_get_contents($this->pidFileLocation));
            
            if ($pid !== false) {
                posix_kill($pid, SIGTERM);
            } else {
                $this->_logMessage('Could not access to PID file', DLOG_ERROR);
            }
        }
    }

    /**
     * Do task
     *
     * @access protected
     * @since 1.0
     * @return void
     */
    function _doTask()
    {
        // override this method
    }

    function _schedule($name, $job)
    {
        $job["date"] = time();
        $this->jobs[$name] = $job;
    }

    function _unschedule($name)
    {
        unset($this->jobs[$name]);
    }

    function _scheduler()
    {
        $time = time();
        $nexttime = 0;
        foreach ($this->jobs as $name => $job) {
            if ($job["date"] < $time) {
                call_user_func(array(
                    $this,
                    $job["fctname"]
                ), $job["params"]);
                $this->jobs[$name]["date"] = $time + $job["step"];
            }
            $nexttime = max($nexttime, $this->jobs[$name]["date"]);
        }
        
        foreach ($this->jobs as $name => $job) {
            $nexttime = min($nexttime, $job["date"]);
        }
        
        sleep($nexttime - $time);
    }

    /**
     * Logs message
     *
     * @access protected
     * @since 1.0
     * @return void
     */
    function _logMessage($msg, $level = DLOG_NOTICE)
    {
        // override this method
    }

    /**
     * Daemonize
     *
     * @access private
     * @since 1.0
     * @return void
     */
    function _daemonize()
    {
        // ob_end_flush();
        $this->_setIdentity();
        
        if ($this->_isDaemonAlreadyRunning()) {
            return false;
        }
        
        if (! $this->_fork()) {
            return false;
        }
        
        if (! posix_setsid()) {
            $this->_logMessage('Could not make the current process a session leader', DLOG_ERROR);
            
            return false;
        }
        
        if (! $fp = fopen($this->pidFileLocation, 'w')) {
            $this->_logMessage('Could not write to PID file', DLOG_ERROR);
            
            return false;
        } else {
            fputs($fp, $this->_pid);
            fclose($fp);
        }
        
        chdir($this->homePath);
        umask(0);
        
        pcntl_signal(SIGCHLD, array(
            &$this,
            'sigHandler'
        ));
        pcntl_signal(SIGTERM, array(
            &$this,
            'sigHandler'
        ));
        // pcntl_signal(SIGKILL, array(&$this, 'sigHandler'));
        pcntl_signal(SIGINT, array(
            &$this,
            'sigHandler'
        ));
        
        return true;
    }

    /**
     * Cheks is daemon already running
     *
     * @access private
     * @since 1.0
     * @return bool
     */
    function _isDaemonAlreadyRunning()
    {
        if (file_exists($this->pidFileLocation)) {
            $oldPid = file_get_contents($this->pidFileLocation);
            
            if ($oldPid !== false && posix_kill(trim($oldPid), 0)) {
                $this->_logMessage('Daemon already running with PID: ' . $oldPid, (DLOG_TO_CONSOLE | DLOG_ERROR));
                
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Forks process
     *
     * @access private
     * @since 1.0
     * @return bool
     */
    function _fork()
    {
        $this->_logMessage('Forking...');
        
        $pid = pcntl_fork();
        
        if ($pid == - 1) {
            $this->_logMessage('Could not fork', DLOG_ERROR);
            
            return false;
        } else 
            if ($pid) {
                $this->_logMessage('Killing parent');
                
                exit();
            } else {
                $this->_isChildren = true;
                $this->_pid = posix_getpid();
                return true;
            }
    }

    /**
     * Sets identity of a daemon and returns result
     *
     * @access private
     * @since 1.0
     * @return bool
     */
    function _setIdentity()
    {
        posix_setgid($this->groupID);
        if (! posix_setgid($this->groupID) || ! posix_setuid($this->userID)) {
            $this->_logMessage('Could not set identity', DLOG_WARNING);
            
            return false;
        } else {
            return true;
        }
    }

    /**
     * Signals handler
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function sigHandler($sigNo)
    {
        switch ($sigNo) {
            case SIGTERM: // Shutdown
                $this->_logMessage('Shutdown signal');
                // $this->releaseDaemon();
                exit();
                break;
            
            case SIGINT: // Shutdown
                $this->_logMessage('Shutdown signal');
                // $this->releaseDaemon();
                exit();
                break;
            
            case SIGKILL: // Kill
                $this->_logMessage('Kill signal');
                // $this->releaseDaemon();
                exit();
                break;
            
            case SIGCHLD: // Halt
                $this->_logMessage('Halt signal');
                // $this->releaseDaemon();
                while (pcntl_waitpid(- 1, $status, WNOHANG) > 0);
                break;
        }
    }

    /**
     * Releases daemon pid file
     * This method is called on exit
     *
     * @access public
     * @since 1.0
     * @return void
     */
    function releaseDaemon()
    {
        if ($this->_isChildren && file_exists($this->pidFileLocation)) {
            $this->_logMessage('Releasing daemon');
            unlink($this->pidFileLocation);
        }
    }
}
?>
