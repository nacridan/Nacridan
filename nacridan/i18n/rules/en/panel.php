<div class='subtitle'>Et sinon je fais comment pour agir ?</div>

<p class='rule_pg'>
	Voici quelques explications sur l'interface : <br /> (positionnez la
	souris sur la zone qui vous intéresse)
</p>
<br />
<img src="<?echo CONFIG_HOST;?>/pics/rules/panelfr.jpg"
	usemap="#panelmap" style="margin-left: 50px; border: 0" alt="" />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<map id="panelmap" name="panelmap">
	<area shape="circle" coords="378,25,24" onmouseout="hideTip()"
		onmouseover="showTip('zone1')" alt="" />
	<area shape="circle" coords="60,92,24" onmouseout="hideTip()"
		onmouseover="showTip('zone2')" alt="" />
	<area shape="circle" coords="379,327,24" onmouseout="hideTip()"
		onmouseover="showTip('zone3')" alt="" />
	<area shape="circle" coords="636,164,24" onmouseout="hideTip()"
		onmouseover="showTip('zone4')" alt="" />
	<area shape="rect" coords="131,138,309,183" onmouseout="hideTip()"
		onmouseover="showTip('zoneb')" alt="" />
	<area shape="rect" coords="131,346,314,390" onmouseout="hideTip()"
		onmouseover="showTip('zonea')" alt="" />
</map>

<div class="absdiv" style='left: 500px; top: 150px;' id='zone1'>
	Cette zone permet de basculer entre les différents "modes"<br />
	cliquez sur :<br /> <br /> <b>Conquête :</b> pour vous battre, vous
	déplacer, agir sur le monde.<br /> <b>Diplomatie :</b> pour gérer
	"votre" groupe, vos amis et ennemis.<br /> <b>Économie :</b> pour
	acheter et vendre des objets avec les autres joueurs.<br /> <b>Messages
		:</b> pour envoyer des messages aux autres joueurs.<br /> <b>Règles :</b>
	pour voir les règles.<br /> <b>Options :</b> pour modifier les options.<br />
</div>

<div class="absdiv" style='left: 200px; top: 250px;' id='zone2'>
	Ce menu dépend du "mode" défini en (1).<br /> En mode <b>Conquète</b>,
	par exemple, c'est ici<br /> que vous pourrez décider de vous déplacer,<br />
	ramasser des objets au sol et vous équiper,<br /> attaquer etc. Une
	fois l'action lancée il faudra<br /> généralement aller à la ZONE A
	pour finir l'action.
</div>

<div class="absdiv" style='left: 500px; top: 450px;' id='zone3'>
	Cette zone a un but informatif sur votre personnage : <br /> <br /> <b>Carte
		:</b> permet de connaître la position de votre personnage sur l'île.<br />
	<b>Vue :</b> permet de consulter l'environnement proche de votre
	personnage.<br /> <b>Profil :</b> permet de consulter ses
	caractéritiques détaillées.<br /> <b>Équipement :</b> permet de voir
	son équipement.<br /> <b>Évènements :</b> permet de consulter
	l'historique de ses évênements.<br />
</div>

<div class="absdiv" style='left: 320px; top: 290px;' id='zone4'>
	Cette zone regroupe l'ensemble des caractéristiques<br /> de base de
	votre personnage de manière résumée.<br /> Vous y trouverez :<br /> <br />
	Vos caractéristiques de base, les bonus malus (B/M)<br /> en cours, les
	coordonnées actuelles de votre personnage,<br /> le nombre de points
	d'expérience (PX) et pièces<br /> d'or (PO) accumulées etc.<br />
</div>


<div class="absdiv" style='left: 400px; top: 520px;' id='zonea'>
	C'est généralement dans cette zone que vous devrez <br /> spécifier les
	"aboutissants" de votre action. <br /> Par exemple définir où vous
	déplacer, qui attaquer,<br /> quel objet ramasser, quelle potion
	utiliser etc.<br />
</div>

<div class="absdiv" style='left: 400px; top: 300px;' id='zoneb'>
	C'est dans cette zone que vous seront communiqués les<br /> résulats
	des actions entreprises.<br />
</div>


<script type='text/javascript'>
var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

</script>
