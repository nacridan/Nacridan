<div class='subtitle'>On peut faire quoi sur l'île ?</div>
<p class='rule_pg'>
	Il existe trois modes d'action dans le jeu. Le mode conquête, économie
	et diplomatie.<br /> <br /> <b>1-)</b> Les actions disponibles dans le
	mode conquête sont :
</p>
<ul>
	<li>Se déplacer.</li>
	<li>S'améliorer.</li>
	<li>Ramasser un objet.</li>
	<li>Poser un objet.</li>
	<li>S'équiper.</li>
	<li>Se déséquiper.</li>
	<li>Utiliser un objet magique.</li>
	<li>Attaquer.</li>
	<li>Donner des Points d'expérience (PX).</li>
	<li>Donner des Pièces d'Or (PO).</li>
</ul>
<p class='rule_pg'>
	<b>2-)</b> Dans le mode économie vous aurez un accès aux actions :
</p>
<ul>
	<li>Acheter un objet</li>
	<li>Mettre en vente un objet.</li>
</ul>
<p class='rule_pg'>
	<b>3-)</b> Et enfin dans le mode diplomatie vous aurez un accès à
	l'ensemble des actions permettant de constituer ou de rejoindre un
	"Ordre", c'est à dire de regrouper un ensemble de personnes partageant
	les mêmes convictions que vous.
</p>
<br />
<br />
<br />
<br />
<br />
<br />