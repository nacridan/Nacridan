<img style="float: right; margin-left: 30px"
	src="<?echo CONFIG_HOST;?>/pics/rules/arrival.jpg" alt="" />
<div class='subtitle'>Nacridan : Jeu de rôle au tour par tour :</div>

<p class='rule_pg'>
	<b>Nacridan</b> est un jeu de rôle au tour par tour où chaque tour dure
	<b>12 heures</b>. Pour chaque tour, vous avez <b>8 points d'action</b>
	(PA) à dépenser pour vous déplacer, attaquer, jeter des sorts etc. Une
	fois que vos points d'action sont épuisés, vous devez attendre le début
	du prochain tour de jeu délimité par la <b>Date Limite d'Action</b>
	(DLA) afin d'avoir à nouveau la totalité de vos PA. Ce jeu ne vous
	prendra que quelques minutes par jour pour être actif et respecté sur
	Nacridan.
</p>
<p class='rule_pg'>
	Une fois votre inscription terminée, vous pourrez incarner un ou une <b>Humain</b>,
	<b>Elfe</b>, <b>Nain</b> ou <b>Dorane</b> et devenir un véritable
	acteur dans le monde de Nacridan. Vous pourrez incarner un guerrier et
	partir, seul ou avec des amis, à la recherche d'objets magiques en
	affrontant de puissantes créatures, en devenant guérrier, magicien, et
	en faisant respecter vos propres valeurs avec , éventuellement, l'appui
	d'autres joueurs partageant vos convictions.
</p>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />