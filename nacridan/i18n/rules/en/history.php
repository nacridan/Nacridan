<table>
	<tr>
		<td width="600px" valign="top">

			<div class='subtitle'>L'histoire de Nacridan</div> Une vieille
			légende raconte qu'il y a fort longtemps des races majeures vivaient
			sur un continent partagé en son milieu par un désert. L'immensité du
			continent fit que l'ensemble des races s'étaient réparties le
			territoire et vivaient en relative harmonie. Les échanges entre les
			deux rives étaient assurés par de gigantesques vers des sables
			apprivoisés. Mais, passé l'heure de l'exploration et des échanges
			commerciaux, les contacts entre les deux rives sont devenus de plus
			en plus rares et, en quelques générations les voyageurs des sables
			disparurent ainsi que leurs gigantesques montures apprivoisées. Le
			côté Est du continent a continué à vivre pendant plusieurs siècles
			dans la paix et la prospérité. <br /> <br /> Mais un jour, une armée
			d'étranges guerriers sombres équipés de puissantes armes arriva de
			l'ouest par le désert. La paix venait de prendre fin. Forts de leurs
			armes, ils s'accaparèrent les ressources des villages dans le but
			d'asservir les populations. Personne n'a jamais vraiment su qui était
			à la tête de cette armée mais devant cette terrible menace les
			populations encore libres prirent la fuite par la mer dans une totale
			ignorance de leur destinée. Certaines d'entres elles accostèrent sur
			une île appelée : Nacridan.

		</td>
	</tr>
</table>