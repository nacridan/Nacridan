
<script type='text/javascript'>
var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

</script>


<div class='subtitle'>Comment on fait pour se faire respecter sur
	Nacridan ?</div>

<p class='rule_pg'>Sur Nacridan, vous serez probablement amené à vous
	battre. Que vous soyez l'attaquant ou le défenseur tout combat se
	déroule selon les deux phases suivantes. (Si vous êtes novice ne vous
	attardez pas trop sur les formules qui suivent, elles sont simplement
	données à titre d'information pour ceux qui voudraient appronfondir
	leur compréhension des combats)</p>
<br />


<table width="100%">
	<tr>
		<td bgcolor="#ffff99" align="center" colspan="4"><b>Phase 1 :</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td valign='top' style='padding-left: 20px'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?></td>
		<td align="justify" style='padding-right: 20px; padding-left: 20px'><b>Vous
				êtes l'assaillant</b><br /> Vous jetez la totalité des Dés de votre
			caractéristique d'attaque. Au score obtenu s'ajoute les Bonus/Malus
			liés aux armes portées. Le Jet d'Attaque sera : <br /> <br />
			<table>
				<tr>
					<td>
<?php echo "<img src='".CONFIG_HOST."/pics/rules/DicesIcon.gif' align='middle' alt=\"\"/>"; ?></td>
					<td><span class="rule_big">+</span></td>
					<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Bonus_MalusIcon.gif' align='middle' alt=\"\"/>"; ?></td>
					<td><span class="rule_big">&nbsp;= JA</span></td>
				</tr>
			</table></td>


		<td valign='top' style='padding-left: 20px'><?php echo "<img src='".CONFIG_HOST."/pics/rules/ShieldIcon.gif' alt=\"\"/>"; ?></td>
		<td align="justify" style='padding-right: 20px; padding-left: 20px'><b>Vous
				êtes le défenseur</b><br /> Vous jetez la totalité des Dés de votre
			caractéristique d'esquive. Au score obtenu s'ajoute les Bonus/Malus
			liés aux armures portées. Le Jet d'Esquive sera :<br /> <br />
			<table>
				<tr>
					<td>
<?php echo "<img src='".CONFIG_HOST."/pics/rules/DicesIcon.gif' align='middle' alt=\"\"/>"; ?></td>
					<td><span class="rule_big">+</span></td>
					<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Bonus_MalusIcon.gif' align='middle' alt=\"\"/>"; ?></td>
					<td><span class="rule_big">&nbsp;= JE</span></td>
				</tr>
			</table></td>

	</tr>
</table>
<br />
<div align="center">
	<b> Celui qui obtient le meilleur score prend l'initiative dans la
		phase suivante et capitalise la différence [JA - JE]</b>
</div>
<br />
<br />
<br />
<br />

<table width="100%">
	<tr>
		<td bgcolor="#ffff99" align="center" colspan="4"><b>Phase 2 :</b></td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td align="justify" colspan="4"
			style="padding-left: 20px; padding-right: 20px;">Le joueur qui a
			gagné l'initiative dans la première phase jette un dé à 100 faces et
			la réussite ou l'échec de sa manoeuvre sont déterminés de la manière
			suivante</td>
	</tr>
</table>


<!--
<map name="panelmap">
<area shape="rect" coords="0,0,430,100" onMouseOut="hideTip()" onMouseOver="showTip('zone1')">
</map>

<div class="absdiv" style='left:50px; top:330px;' id='zone1'>
      <b>Détails sur le calcul des zones </b><br>

      <ul>
      <li> La zone <b>b </b>correspond à la chance de réussite de l'action du joueur qui a gagné l'initiative dans la phase 1.</li>

      <li> La zone <b>c</b> s'ajoute à la zone <b>b</b> 
      en comptabilisant les points capitalisés dans la phase 1 et augmente 
      vos chances de réussite de 10 fois la différence des scores 
      de la phase 1, soit <b>[JA &#8211; JE] x 10</b>. </li>

      <li>La zone <b>a</b> est calculée de m&ecirc;me comme 5 fois la différence des 
      scores de la phase 1, soit <b>[JA &#8211; JE] x 5</b>. </li>
      
      <li>La zone <b>d</b> est égale au reste pour aller &agrave; <b>100</b>.</li>
      </ul>

</div>

<div align="center" width="100%"><?php echo "<img src='".CONFIG_HOST."/pics/rules/100facesJauge.gif' border=0 align='middle' usemap='#panelmap'/>";?></div>

-->

<table width="100%">
	<tr>
		<td>&nbsp;</td>
		<td valign='top' style='padding-left: 20px'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?></td>
		<td align="justify" width="300px"
			style='padding-right: 20px; padding-left: 20px'><b>Cas 1, l'attaquant
				a l'initiative :</b>
			<ul>
				<li>Ses chances de toucher son adversaire sont de : <b>50% + [JA -
						JE] * [80/NB dé d'attaque]</b></li>
				<li>Ses chances de toucher par un coup critique (c'est à dire de
					faire 50% de dégâts en plus) sont de : <b>[JA - JE] * [80/NB dé
						d'attaque]</b>
				</li>
				<li>Un jet compris entre 96 et 100 équivaut toujours à une attaque
					raté de manière critique.</li>
			</ul></td>

		<td valign='top' style='padding-left: 20px'><?php echo "<img src='".CONFIG_HOST."/pics/rules/ShieldIcon.gif' alt=\"\"/>"; ?></td>
		<td align="justify" style='padding-right: 20px; padding-left: 20px'><b>Cas
				2, le défenseur a l'initiative :</b>
			<ul>
				<li>Ses chances d'esquiver son adversaire sont de : <b>50% + [JA -
						JE] * [80/NB dé d'attaque] </b></li>
				<li>Ses chances d'esquiver de manière parfaite (c'est à dire ne pas
					subir le malus en esquive occasionné par l'attaque) sont de : <b>[JA
						- JE] * [80/NB dé d'attaque]</b>
				</li>
				<li>Un jet compris entre 96 et 100 équivaut toujours à une esquive
					raté de manière critique.</li>

			</ul></td>

	</tr>
</table>


<b>Malus :</b>
<p class='rule_pg'>
	Tout joueur ayant subi une attaque qui n'a pas été esquivée de manière
	parfaite, ou qui n'a pas été ratée de manière critique, voit son
	esquive diminuer de -1D6 par attaque subie (Ce malus disparaît lors de
	l'activation d'une DLA).<br />
</p>
<br />
<br />
<hr />
<br />
<br />
<b>Exemple de Combat 1:</b>
<p class='rule_pg'>
	Tanyss attaque un Garde Noir :<br /> (Tanyss a 10D6 en attaque contre
	9D6 en esquive pour son adversaire)<br /> <br /> Le jet d'attaque de
	Tanyss est de : 32 <br /> Le jet d'esquive du Garde Noir est de : 30 <br />
	<br /> Tanyss a eu le meilleur score et il prend l'initiative dans le
	combat pour une différence de <b>2</b> points.<br /> Il a 66% de chance
	de toucher son adversaire. (66% = [ 50 + <b>2</b> * (80/10) ] % )<br />
	Il a de plus, 16% de chance de toucher son adversaire par un coup
	critique. (16% = [ <b>2</b> * (80/10) ] % )<br /> <br /> Il jette son
	dé 100 et obtient : 72% !<br /> <br /> Il rate donc son attaque.<br />
	<br />
</p>


<b>Exemple de Combat 2:</b>
<p class='rule_pg'>
	Tanyss attaque un Garde Noir :<br /> Son jet d'attaque est de : 32<br />
	Le jet d'esquive du Garde Noir est de : 28<br /> <br /> Tanyss a eu le
	meilleur score et il prend l'initiative dans le combat pour une
	différence de <b>4</b> points.<br /> Il a 82% de chance de toucher son
	adversaire. (82% = [ 50 + <b>4</b> * (80/10) ] % )<br /> Il a de plus
	32% de chance de toucher son adversaire par un coup critique. (32% = [
	<b>4</b> * (80/10) ] % )<br /> <br /> Il jette son dé 100 et obtient :
	12% !<br /> <br /> Il touche son adversaire par un coup critique (car
	son score est inférieur aux 32% nécessaires) ! <br /> Tanyss fait un
	jet sur sa caractéristique dégâts et obtient : 22.<br /> Comme Tanyss a
	réalisé un coup critique il occasionne 50% de dégâts en plus ce qui
	donne au final : 33.<br /> L'armure du Garde Noir est de 8, ce dernier
	ne subira donc une perte que de 33-8 soit 25 points de vie.<br />
</p>
<br />
<br />
<hr />
<br />
<br />
<div class='subtitle'>Et si on meurt ça fait quoi ?</div>

<p class='rule_pg'>
	Sur Nacridan, vous serez probablement à mourrir tôt ou tard. Mais
	rassurez vous, la mort n'est pas définitive pour votre personnage. En
	effet, La seule chose que l'on perd à la mort (pour le moment) ce sont
	les Pièces d'Or (PO) et deux fois le niveau de votre personnage en PX.
	En ce qui concerne les PO, La perte dépendra de ce que votre personnage
	avait en poche au cours des 10 dernières DLA.<br /> Ramasser un petit
	paquet de PO au sol et se faire éclater à la DLA d'après occassionne
	une perte maxi de l'argent venant d'être ramassé. En revanche si on
	arrive à garder cette somme pendant 10 DLA ou plus, alors la perte en
	PO ne sera plus que de 10%.<br /> Lors d'une mort, votre personnage
	perdra donc entre 10% et 100% de sa cagnotte qu'il laissera sur le lieu
	de sa mort. Ensuite il réapparaîtra instantanément dans une zone plus
	ou moins proche avec la moitié de ses points de vie.

</p>
<br />

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />