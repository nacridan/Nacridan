<?php
require_once ("../../../conf/config.ini.php");
$tabImage = array(
    "",
    "Butt_Rules_Princ_",
    "Butt_Rules_Gene_",
    "Butt_Rules_Inter_",
    "Butt_Rules_Act_",
    "Butt_Rules_Combats_",
    "Butt_Rules_Exp_",
    "Butt_Rules_Compet_",
    "Butt_Rules_Sorti_",
    "Butt_Rules_Equip_",
    "Butt_Rules_Vill_",
    "Butt_Rules_Equip_"
);

$tabImage2 = array(
    "",
    "Butt_Rules_Sav_",
    "Butt_Rules_Vill_",
    "Butt_Rules_Fortress_",
    "Butt_Rules_Hist_",
    "Butt_Rules_Tuto_",
    "Butt_Rules_Mind_",
    "Butt_Rules_Beast_"
);

$page = "";

if (isset($_GET["page"])) {
    $page = $_GET["page"];
}
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">';
echo '<head>';

echo '<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>';
echo '<title>Nacridan</title>';
echo '<link rel="stylesheet" type="text/css" href="' . Cache::get_cached_file('/css/nacridanMain.css').'" />';
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo "<style type=\"text/css\">\n";
for ($i = 1; $i <= 9; $i ++) {
    echo ".rule_step" . $i . " {display:block; height: 25px;\n width: 100px;\n background:  url(../../../pics/rules/Regle_Menu/" . $tabImage[$i] . "Off.jpg) no-repeat; \n}\n";
    echo ".rule_step" . $i . ":hover {\n background: url(../../../pics/rules/Regle_Menu/" . $tabImage[$i] . "Roll.jpg) no-repeat;)\n}\n";
}

for ($i = 1; $i <= 7; $i ++) {
    echo ".rule_step1" . $i . " {display:block; height: 25px;\n width: 100px;\n background:  url(../../../pics/rules/Regle_Menu/" . $tabImage2[$i] . "Off.jpg) no-repeat; \n}\n";
    echo ".rule_step1" . $i . ":hover {\n background: url(../../../pics/rules/Regle_Menu/" . $tabImage2[$i] . "Roll.jpg) no-repeat;)\n}\n";
}

?>
</style>
</head>

<body class='rule_bg'>
	<div class="rule_content">
<?php
echo "<a href='" . CONFIG_HOST . "' class='rule_home' ></a>\n";
// echo "<img src='".CONFIG_HOST."/pics/rules/Rules_TopBanner_Nacrid.jpg'/>";
// echo "<img src='".CONFIG_HOST."/pics/rules/Rules_TopBanner_Regles.jpg'/>";
for ($i = 1; $i <= 9; $i ++) {
    echo "<img src='" . CONFIG_HOST . "/pics/rules/Regle_Menu/" . $tabImage[$i] . "Roll.jpg' style='display:none' alt='' />";
}

echo "<ul class='rule_list_item' style='position:absolute; top: 100px'>";
for ($i = 1; $i <= 9; $i ++) {
    if ($page == "step" . $i) {
        echo "<li class='rule_item'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Menu/" . $tabImage[$i] . "On.jpg' alt=\"\" /></li>";
    } else {
        echo "<li class='rule_item'><a href='rules.php?page=step" . $i . "' class='rule_step" . $i . "'></a></li>";
    }
}
echo "</ul>";

echo "<div style='position:absolute; top: 125px;'><img src='" . CONFIG_HOST . "/pics/rules/Rules_mid_stripe.jpg' alt=\"\"/></div>";

echo "<ul class='rule_list_item' style='position:absolute; top: 125px'>";
for ($i = 1; $i <= 7; $i ++) {
    if ($page == "step1" . $i) {
        echo "<li class='rule_item'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Menu/" . $tabImage2[$i] . "On.jpg' alt=\"\" /></li>";
    } else {
        echo "<li class='rule_item'><a href='rules.php?page=step1" . $i . "' class='rule_step1" . $i . "'></a></li>";
    }
}
echo "</ul>";

echo "<div class='rule_center' style='position:absolute; top: 150px'>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/GdeToile_TextureOmbre.jpg' alt=\"\"/></div>";
echo "<div class='rule_center_content'>";
switch ($page) {
    case "step1":
        include "bases.php";
        break;
    case "step2":
        include "gene.php";
        break;
    case "step3":
        include "panel.php";
        break;
    case "step4":
        include "action.php";
        break;
    case "step5":
        include "fight.php";
        break;
    case "step6":
        include "xp.php";
        break;
    case "step7":
        include "ability.php";
        break;
    case "step8":
        include "magicspell.php";
        break;
    case "step9":
        include "equip.php";
        break;
    case "step11":
        include "talent.php";
        break;
    case "step12":
        include "villages_villes.php";
        break;
    case "step13":
        include "fortress.php";
        break;
    case "step14":
        include "history.php";
        break;
    case "step15":
        include "tutorials.php";
        break;
    case "step16":
        include "mindfullness.php";
        break;
    case "step17":
        include "beasts.php";
        break;
    default:
        include "bases.php";
        
        break;
}
echo "</div>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Bottom.jpg' alt=\"\"/></div>";
echo "</div>";

$google = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68594660-1', 'auto');
  ga('send', 'pageview');

</script>";

echo $google;
?>
</div>
</body>

</html>
