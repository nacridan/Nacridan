<?php
require_once ("../../../conf/config.ini.php");
echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 0; $i < 100; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i . "\");return false;' > ";
}
// class='stylepc'

?>

<script type='text/javascript'>
var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

</script>



<div class='subtitle'>Introduction</div>

<p class='rule_pg'>Sur Nacridan, vous serez probablement amené à vous
	battre. Si vous êtes novice ne vous attardez pas trop sur les formules
	qui suivent, elles sont simplement données à titre d'information pour
	ceux qui voudraient approfondir leur compréhension des combats.</p>
<p class='rule_pg'>Il n'est pas possible de se trouver sur la même case
	qu'un autre personnage. La distance d'une case est donc considérée
	comme du contact. Le combat standard se fait au contact. Mais il existe
	plusieurs façons de se battre à distance (arc, bolas, magie). Près des
	villes de départ, le meurtre de PJ est interdit, de même que l'attaque
	des bâtiments ou des tortues géantes. Si vous tuez un PJ, les gardes
	interviendront et vous mettront en prison.</p>
<p class='rule_pg'>Toutes les formules présentées ci-dessous sont
	valables uniquement pour des attaques standard, sans utilisation de
	compétences ou de sortilèges.</p>
<br />

<div class='subtitle'>Attaque avec une arme de contact</div>
<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?>
Il existe beaucoup d'armes de contact dans nacridan. Elles sont regroupées en 3 catégories : les armes légères qui requièrent 7PA pour attaquer, les armes intermédiaires qui demandent 8PA et les armes lourdes qui permettent d'attaquer pour 9PA. Le type d'arme que vous utilisez influence grandement vos valeurs de combat. 
En effet, chaque type d'arme est associé à une caractéristique de base : la vitesse pour les armes légères, la dextérité pour les armes intermédiaires et la force pour les armes lourdes.
<br /> <br /> Lorsque vous attaquez une créature, vous allez lancer un
	certain nombre de dés qui dépend de vos caractéristiques de base et de
	l'arme utilisée, c'est ce que nous appellerons votre Jet d'Attaque
	(JAtt). La capacité d'attaquer est majoritairement déterminé par la
	dextérité de votre personnage. Comme le résume le tableau ci-dessous :
	votre JAtt est déterminé par un jet de tous vos dés de dextérité et un
	jet de la moitié des dés de la caractéristique associée à l'arme
	utilisée.
</p>
<br />
<div align='center'>
	<table class='enchanttable'>
		<tr>
			<th>Type d'Arme</th>
			<th>Dés utilisés pour votre JAtt</th>
		</tr>
		<tr>
			<td>Aucune</td>
			<td>Dextérité</td>
		</tr>
		<tr>
			<td>Légère</td>
			<td style='width: 190px;'>Dextérité + Vitesse/2</td>
		</tr>
		<tr>
			<td>Moyenne</td>
			<td>Dextérité + Dextérité/2</td>
		</tr>
		<tr>
			<td>Lourde</td>
			<td>Dextérité + Force/2</td>
		</tr>
	</table>
</div>

<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/ShieldIcon.gif' alt=\"\"/>"; ?>
Pour se défendre, le personnage ciblé va opposer un Jet de Défense (JDef) à votre JAtt. Le bouclier joue dans la défense le même rôle que l'arme dans l'attaque. Il existe de même trois types de bouclier liés, eux aussi, à une caractéristique de base : les petits boucliers sont associés à la vitesse, les boucliers moyens à la dextérité et les boucliers larges à la force.
<br /> <br /> La capacité à se défendre est majoritairement déterminée
	par la vitesse. Comme le résume le tableau ci-dessous, le JDef est
	déterminé par un jet de tous les dés de vitesse et de la moitié des dés
	de la caractéristique associée aux boucliers utilisés. Si le défenseur
	n'a pas de bouclier, le JDef est déterminé par un jet de tous les dés
	de vitesse et d'un quart des dés de la caractéristique associée à
	l'arme.
<div align='center'>
	<table class='enchanttable'>
		<tr>
			<th style='width: 150px;'>Bouclier</th>
			<th>Défense</th>
		</tr>
		<tr>
			<td>Ni bouclier ni arme</td>
			<td>Vitesse</td>
		</tr>
		<tr>
			<td>Petit</td>
			<td style='width: 190px;'>Vitesse + Vitesse/2</td>
		</tr>
		<tr>
			<td>Moyen</td>
			<td>Vitesse + Dextérité/2</td>
		</tr>
		<tr>
			<td>Large</td>
			<td>Vitesse + Force/2</td>
		</tr>
	</table>
	<br /> <br />Cas avec une arme mais sans bouclier:<br />
	<table class='enchanttable'>
		<tr>
			<th>Arme</th>
			<th>Défense</th>
		</tr>
		<tr>
			<td>Légère</td>
			<td style='width: 190px;'>Vitesse + Vitesse/4</td>
		</tr>
		<tr>
			<td>Moyenne</td>
			<td>Vitesse + Dextérité/4</td>
		</tr>
		<tr>
			<td>Lourde</td>
			<td>Vitesse + Force/4</td>
		</tr>
	</table>
</div>

</p>
<br />

<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?>
Si le JAtt est supérieur au JDef de la cible alors vous l'avez touché et vous allez lui infliger des dégâts. <br />
	La capacité à infliger des dégâts dans le combat au corps à corps est
	majoritairement déterminée pas la force. Le jet dégât (JDeg) est
	déterminé en lançant 80% de vos dés de force. <br /> <br /> La
	différence entre le JAtt et le JDef va également intervenir. Plus cette
	différence est grande et plus vous avez surpassé votre adversaire dans
	l'opposition et plus vous vous engagez dans le coup porté, c'est ce que
	nous appellerons le bonus de dégât. Ce bonus est calculé en divisant la
	différence du JAtt et du JDef par 4 : Bonus de dégât = (JAtt - JDef)/4
	arrondi à l'entier inférieur. Les dégâts totaux infligés sont déterminé
	en ajoutant le JDeg et le bonus de dégât. <br /> <br /> La perte de vie
	engendrée par le coup est finalement calculée en retirant aux dégâts
	infligés l'amure de la cible.
</p>
<br />

<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/Bonus_MalusIcon.gif' alt=\"\"/>"; ?>

Au cours de votre aventure, vos caractéristiques vont beaucoup évoluer, soit de façon permanente grâce à votre progression, soit de façon temporaire dépendant de l'état de votre personnage et de votre équipement.
Les variations de caractéristique dues à l'état de votre personnage et de l'équipement porté sont appelés <b>
		bonus/malus </b>. Ces bonus/malus ne sont pas forcément exprimés en
	D6, ils peuvent être fixe. <br /> <br /> Plus haut, nous avons omis
	volontairement de parler de ces bonus/malus fixe qui interviennent
	pourtant dans les différents jets et nous avons parlé que de jet de
	dés. Ici, nous allons voir comment les bonus/malus fixes entre en jeu.
	<br /> <br /> Plus haut, nous avons donc défini trois caractéristiques
	intermédiaires : l'attaque, la défense et les dégâts comme vous pouvez
	le retrouver dans votre profil montré dans l'interface. Les bonus/malus
	fixes peuvent intervenir vos caractéristiques de base ou sur les
	caractéristiques intermédiaires. Il est facile de comprendre le
	fonctionnement d'un bonus intervenant sur les caractéristiques
	intermédaires. <br /> <br /> Un bon exemple est le bonus apporté par
	votre arme. En plus de déterminer la caractéristisque que vous allez
	utiliser pour obtenir votre JAtt, il se peut que votre arme vous
	apporte un bonus fixe dans ce jet. Une dague de niveau 3 par exemple
	confère un bonus de 2,5 points d'attaque. Cette valeur sera donc ajouté
	à votre JAtt et plus haut lorsque nous avons parlé de JAtt ou d'autres
	jets, nous avons en fait considérer ce jet avec les bonus/malus
	associés. <br /> <br /> Les bonus sur les caractéristiques de base sont
	peut-être plus difficiles à comprendre. Un enchantement sur une pièce
	d'équipement ou une malédiction peut entrainer un bonus/malus sur une
	caractéristique de base. Il faudra donc prendre en compte ce malus pour
	chacun des jets faisant intervenir cette caractéristique. <br />
	Supposons que votre personnage porte une dague de niveau 3 (arme
	légère) et qu'il possède les caractéristiques suivantes :
<table width='700px' class='maintable'>
	<tr align='center' class='mainbglabel'>
		<td>Caractéristique</td>
		<td>Base</td>
		<td>BM</td>
		<td>Total</td>
	</tr>
	<tr align='center' class='mainbglabel'>
		<td>Dextérité</td>
		<td>6D6</td>
		<td>1D6 + 3</td>
		<td>7D6 + 4</td>
	</tr>
	<tr align='center' class='mainbglabel'>
		<td>Vitesse</td>
		<td>9D6</td>
		<td>-4</td>
		<td>9D6 -4</td>
	</tr>
</table>
<br />
La caractéristique intermédiaire d'attaque est déterminée, en se
référant à ce qu'il est dit plus haut, en ajoutant la totalité de ses
dés de dextérité ajouté à la moitié de ses dés de vitesse. En vérité
nous allons aussi prendre en compte les bonus/malus fixes de ces
dernières caractéristiques. Nous obtenons donc :
<br />
<br />
<table width='700px' class='maintable'>
	<tr align='center' class='mainbglabel'>
		<td>Caractéristique</td>
		<td>Base</td>
		<td>BM</td>
		<td>Total</td>
	</tr>
	<tr align='center' class='mainbglabel'>
		<td>Attaque</td>
		<td>11,5D6 +2</td>
		<td>+2,5</td>
		<td>11,5D6 + 4,5</td>
	</tr>
</table>
<br />
11,5D6 + 2 est obtenu en ajoutant la Dextérité : 7D6+4 avec la moitié de
la vitesse (9D6 - 4)/2 = 4,5D6 - 2.
<br />
<br />
<br>
<b> Qu'est-ce que la motié d'un D6 </b>
<br />
Jeter 11,5D6 signifie que vous allez jeter 11D6 et que vous avez une
chance sur deux de jeter un 12ème D6.
<br />
<br />

<div class='subtitle'>Tir à l'arc</div>
<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?>
Il existe deux types d'arc : l'arc court et l'arc long. L'arc court offre un faible bonus de dégât mais ne requiert que 7pa pour attaquer tandis que l'arc long offre un gros bonus de dégât mais demande 8PA pour être utilisé.
Le tir à l'arc standard permet de cibler une créature située jusqu'à trois cases de distance. <br />
	La caractéristique associée à l'arc est la dextérité. Votre JAtt est
	donc donné en jetant une fois et demi vos valeurs de dextérité. <br />
	<br /> Ce JAtt est réduit en fonction de la distance qui vous sépare à
	la cible. Au contact vous ne subissez aucun malus, à 2 cases de
	distance, vous subissez un malus de 10%, enfin à trois cases le malus
	subit est de 20%.
<table align='center' class='enchanttable'>
	<tr>
		<th>Distance</th>
		<th>Attaque</th>
	</tr>
	<tr>
		<td>Une case</td>
		<td>3*Dextérité/2</td>
	</tr>
	<tr>
		<td>Deux cases</td>
		<td style='width: 190px;'>3*Dextérité/2 - 10%</td>
	</tr>
	<tr>
		<td>Trois cases</td>
		<td>3*Dextérité/2 - 20%</td>
	</tr>
</table>
</p>
<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/ShieldIcon.gif' alt=\"\"/>"; ?>
La défense de la cible s'effectue de la même manière que lors d'une attaque au contact. Elle est donc déterminée par un jet de vitesse ajouté à un jet de la moitié de la caractéristique associée au bouclier utilisé.
<br />
</p>
<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?>
Les dégâts infligés par le tir à l'arc ne dépendent pas de votre force. Ils sont déterminés par la différence entre votre JAtt et le JDef de la cible, c'est ce que nous avons appelé le bonus de dégât dans la partie précédente.
Pour le tir à l'arc, ce bonus de dégâts est calculé par la formule complexe que vous pouvez consulter  <?php echo $name[99];?> ici </a>.
	<br /> <br />Pour déterminer les dégâts totaux infligés à la cible, il
	faut ajouter à ce bonus de dégâts les bonus octroyés par l'arc et la
	flèche utilisés. En effet, vous ne pouvez pas tirer à l'arc sans
	flèche. Il en existe deux types : les flèches torsadés qui confèrent un
	bonus d'attaque et les flèches barbelées qui confèrent un bonus de
	dégât. <br /> <br />Finalement, la perte de points de vie subie par la
	cible est, comme dans le cas d'une attaque au contact, donnée en
	retirant l'armure de la cible aux dégâts infligés.
</p>
<br />

<div class='subtitle'>Les attaques magiques</div>
<p class='rule_pg'><?php echo "<img src='".CONFIG_HOST."/pics/rules/SwordIcon.gif' alt=\"\"/>"; ?>
Il y quatre armes magiques dans le monde de Nacridan : le sceptre, le bâton magique, le bâton d'Aether et le bâton d'Athlan. Chacune de ces armes est également considérée comme une arme lourde et répondra comme telle si vous l'utilisez pour effectuer une attaque au contact. 
<br /> <br /> Au début de l'aventure, votre personnage, même s'il porte
	une arme magique, n'est pas capable d'effectuer d'attaque magique. Il
	lui faudra d'abord apprendre un sortilège. <br /> <br /> Il existe 4
	sortilèges considérés comme une attaque magique : Toucher Brûlant,
	Poing du Démon, Boule de feu et Brasier. Les autres sorts, même s'ils
	peuvent cibler l'adversaire ont leur propre fonctionnement qui ne fait
	pas intervenir l'attaque magique. <br /> Pour ces sortilèges, le JAtt
	est déterminé de la même façon dépendant de votre dextérité et du fait
	d'utiliser une arme magique ou non. On retrouve le calcul dans le
	tableau suivant :
	
<table align='center' class='enchanttable'>
	<tr>
		<th>Type d'Arme</th>
		<th>Dés utilisés pour votre JAtt</th>
	</tr>
	<tr>
		<td>Aucune / Arme non magique</td>
		<td>Dextérité</td>
	</tr>
	<tr>
		<td>Arme magique</td>
		<td>Dextérité + Dextérité/2</td>
	</tr>
	<table />
	</p>
	<br />
	<p class='rule_pg'>Le JDef de la cible reste inchangé et les dégâts
		infligés dépendent du sort utilisé.</p>
	<br />

	<div class='subtitle'>Influence sur les jauges de progression</div>

	<b>Attaque physique : Vous avez réussi à toucher votre adversaire </b>
	<br />
	<table width='700px' class='maintable'>
		<tr align='center' class='mainbglabel'>
			<td>Caractéristique</td>
			<td>Force</td>
			<td>Dextérité</td>
			<td>Vitesse</td>
			<td>Magie</td>
			<td>Vie</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme lourde</td>
			<td>4</td>
			<td>5</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme intermédiaire</td>
			<td>2</td>
			<td>6</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme légère</td>
			<td>1</td>
			<td>4</td>
			<td>2</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme magique</td>
			<td>1</td>
			<td>4</td>
			<td>0</td>
			<td>3</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arc</td>
			<td>0</td>
			<td>8</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
	</table>
	<br />
	<b>Attaque physique : Vous n'avez pas réussi à toucher votre adversaire
	</b>
	<br />
	<table width='700px' class='maintable'>
		<tr align='center' class='mainbglabel'>
			<td>Caractéristique</td>
			<td>Force</td>
			<td>Dextérité</td>
			<td>Vitesse</td>
			<td>Magie</td>
			<td>Vie</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme lourde</td>
			<td>2</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme intermédiaire</td>
			<td>0</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme légère</td>
			<td>0</td>
			<td>0</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme magique</td>
			<td>0</td>
			<td>2</td>
			<td>0</td>
			<td>2</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arc</td>
			<td>0</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>0</td>
		</tr>
	</table>
	</p>

	<b> Attaque physique : Vous avez réussi à défendre : </b>
	<br />
	<table width='700px' class='maintable'>
		<tr align='center' class='mainbglabel'>
			<td>Caractéristique</td>
			<td>Force</td>
			<td>Dextérité</td>
			<td>Vitesse</td>
			<td>Magie</td>
			<td>Vie</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Sans bouclier / griffe</td>
			<td>0</td>
			<td>0</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Arme lourde à deux main</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Petit Bouclier</td>
			<td>0</td>
			<td>0</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Bouclier moyen</td>
			<td>0</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
		</tr>
		<tr align='center' class='mainbglabel'>
			<td align='center'>Bouclier large</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
		</tr>
	</table>
	<br />
	</p>

	<b>Attaque physique : Vous n'avez pas réussi à défendre </b>
	<br /> Si vous n'avez pas réussi à défendre, alors quel que soit votre
	type de bouclier, votre jauge de vie progressera alors de 6 points et
	votre jauge de vitesse d'un point.
	<br />
	<br />

	<b>Opposition magique </b>
	<br /> Si vous défendez avec succès une opposition magique alors votre
	jauge de maîtrise de la magie progressera de 3 points. Si vous échouez
	dans l'opposition magique alors votre jauge de vie progressera de 6
	points et votre jauge de maîtrise de la magie d'un point.
	</p>
	<br />