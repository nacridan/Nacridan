
<p class='rule_pg'>
	<b>ATTENTION</b>, ceci est un texte provisoire. Il sera retravaillé par
	la suite, notamment en suivant les retours de la communauté.
</p>

<div class='subtitle'>Introduction</div>

<p class='rule_pg'>Comme tous les jeux, Nacridan a simplement pour but
	la détente et la bonne humeur. Après de nombreuses années de pratique
	de ce genre de jeu, nous savons que l'on peut parfois rencontrer des
	difficultés à apprécier pleinement le jeu quand tout ne se passe pas
	comme prévu. Or, il est certain que sur un long temps de jeu des
	événements désagréables vont survenir, par exemple une suite d'actions
	ratées, une discussion tendue avec d'autres joueurs, la mort d'un
	personnage, etc. Nous proposons ici quelques bases qui permettent de
	diminuer le côté désagréable de ces événements, voire même de les
	apprécier.</p>

<p class='rule_pg'>Il est connu que le théâtre peut contribuer au
	développement personnel des acteurs. Il est bien moins connu que le jeu
	de rôle induit les mêmes effets, mais c'est pourtant le cas, pour peu
	que le joueur soit intéressé par cet aspect.</p>


<div class='subtitle'>Le jeu de rôle</div>
<p class='rule_pg'>Le jeu de rôle est avant tout un jeu. Si vous vous
	apercevez qu'il devient un fardeau, alors il vaut mieux arrêter ou
	réfléchir à un moyen de changer cette perception. De plus, en gardant à
	l'esprit que ce n'est qu'un jeu, il peut être plus facile de
	relativiser : votre personnage vient de se faire voler, c'est gênant
	dans le jeu, mais rien n'a disparu dans la vie réelle. Et vous avez
	bien de la chance, car de nombreuses personnes se font effectivement
	voler dans le monde réel. Les situations désagréables dans le jeu
	constituent un moyen parmi d'autres pour apprécier pleinement la chance
	de notre situation dans le monde réel. On a facilement tendance à
	oublier que malgré les quelques problèmes de notre vie quotidienne,
	elle reste dans la plupart des cas bien meilleure que celle de millions
	de gens.</p>

<p class='rule_pg'>Le jeu de rôle est également un rôle, que vous pouvez
	ou non jouer pleinement. Bien sûr, pour jouer un rôle le mieux
	possible, il faut s'immerger dans l'univers en question, se sentir dans
	la peau du personnage de manière à pouvoir imaginer naturellement des
	répliques et des réactions "RP". Cependant, il faut toujours se
	souvenir que votre personnage n'est pas vous. Plus vous ferez la
	distinction entre votre personnage et vous-même, mieux vous jouerez
	votre rôle, car il ne sera pas parasité par des éléments IRL. De la
	même façon, quelles que soient les actions et paroles des autres
	personnages, il faut essayer de les interpréter comme les actes et
	paroles du personnage et pas du joueur. En faisant cela vous éviterez
	de garder rancune à quelqu'un pour un vol, meurtre etc. Il a juste
	"joué le rôle" du méchant. En général, cette distinction entre joueur
	et personnage vous ramène dans un esprit de jeu, et vous écrivez
	naturellement des répliques légères, voire comiques à un certain degré.
	Ainsi vous appréciez mieux le jeu, et ceux que vous croisez également.
</p>


<div class='subtitle'>Les vertus du Tour-par-Tour</div>
<p class='rule_pg'>Les jeux de rôles en temps réel comme Guild Wars ou
	Warcraft rendent plus difficile cette séparation entre personnage et
	joueur. En effet vous êtes happé par le flot des actions, tout va très
	vite et le recul est plus difficile. Dans un jeu au tour-par-tour comme
	Nacridan, vous avez en général le temps de relire votre réponse. Si un
	comportement ou des paroles vous heurtent, vous avez la possibilité d'y
	réfléchir un peu avant de répondre.</p>

<p class='rule_pg'>De plus, les tour-par-tour sont bien plus en accord
	avec une vie épanouie que les temps réel, car ils laissent beaucoup
	plus de temps IRL. Faites tout de même attention, la dépendance est à
	peu près aussi forte. Par exemple, si vous sentez que vous délaissez
	une personne importante IRL juste pour jouer votre tour, alors il est
	temps de faire une pause.</p>

<div class='subtitle'>Observer les émotions</div>

<p class='rule_pg'>Pour ceux qui aiment l'introspection, ou qui ont des
	difficultés à gérer leurs émotions, un univers virtuel comme celui de
	Nacridan est un parfait terrain d'essai. D'abord parce que c'est un
	espace "protégé" : si vous dérapez émotionnellement à cause du jeu,
	vous prononcerez peut-être des paroles acides à travers votre
	personnage, au pire vous abimerez votre ordinateur, mais la communauté
	reste protégée de vos actes physiques.</p>

<p class='rule_pg'>Ensuite, le côté "tour-par-tour" laisse de l'espace
	pour prendre conscience de notre état émotionnel. Dans la vie réelle,
	ou bien dans un jeu en temps réel, quand nous baignons dans l'agitation
	environnante, la plupart du temps nous ne pensons pas à observer ce
	qu'il se passe en nous. Dans un tour-par-tour, la structure même du jeu
	nous empêche de nous perdre dans l'agitation. Si nous jouons en étant
	tranquillement installés chez nous, alors nous avons la liberté de
	d'observer notre état intérieur, si cela nous intéresse.</p>

<p class='rule_pg'>De plus, le fait que "ce n'est qu'un jeu" aide à
	calmer les émotions désagréables puisqu'on peut relativiser les
	résultats de nos actions. Par exemple, si on rate dix fois de suite une
	action que l'on a neuf chances sur dix de réussir, cela peut créer de
	l'énervement. Mais on peut aussi noter que c'est un sacré record, et
	l'énervement se transformera alors en sourire amusé, surtout si on
	partage le "record" avec la communauté.</p>

<p class='rule_pg'>Enfin, un univers virtuel permet de goûter à des
	émotions difficilement accessibles dans la réalité. On peut devenir un
	roi, un guerrier légendaire, ou un tueur en série. En s'immergeant dans
	le monde virtuel, on ressent les émotions liées à l'évolution de notre
	personnage. On peut en profiter pour observer l'attachement que nous
	avons vis-à-vis de notre personnage. En général plus le personnage est
	puissant, ou connu, et plus nous sommes attachés à sa position sociale.
	C'est alors un excellent terrain d'entrainement pour observer que cette
	position sociale, cette richesse ou cette puissance n'est pas forcément
	lié à notre bonheur. C'est seulement un bonus agréable. Si nous
	arrivons finalement à être joyeux et paisible lorsque notre personnage
	perd tout, alors il y a de forte chance pour cette stabilité intérieure
	ait une influence positive dans la vie réelle.</p>



<div class='subtitle'>Respirer et sourire. =)</div>

<p class='rule_pg'>
	Voici un exemple d'exercice simple qui aide à apprécier le moment
	présent :</br> L'idée est de suivre la respiration pendant quelques
	secondes, en ajoutant une autosuggestion sur l'inspiration et sur
	l'expiration. </br> 1) Sur l'inspiration vous prenez conscience de
	votre corps physique, le contact des pieds sur le sols, des muscles
	tendus, etc. Pour aider cette observation vous pensez : "Conscience du
	corps", ou simplement "Conscience". Sur l'expiration, vous relâchez
	tout votre corps. Pour vous aider vous pouvez sourire aux différentes
	parties de votre corps et penser "Détente du corps" ou "Détente" ou
	encore "Sourire". </br> 2) Après avoir pratiqué le point 1) quelques
	secondes ou quelques minutes vous pouvez changer de sujet de
	concentration. Par exemple, vous pouvez observer la chance que vous
	avez d'être en vie dans le moment présent. Sur l'inspiration vous
	pensez "Moment Présent", sur l'expiration vous pensez "Moment
	Merveilleux". Ou plus simplement "Présent" sur l'inspiration et
	"Merveilleux" sur l'expiration.
</p>

<p class='rule_pg'>Ces petites méditations sont très flexibles, vous
	pouvez adapter les autosuggestions à vos besoins. Vous pouvez les
	pratiquer n'importe où et n'importe quand : au travail, dans le métro,
	dans la voiture, le soir avant de dormir, etc.</p>


<div class='subtitle'>Pour en savoir plus.</div>


<p class='rule_pg'>
	Il existent de nombreuses voies pour apprendre à calmer son stress, à
	se connaître mieux et à interagir plus facilement avec le monde. Voici
	quelques pistes testées par l'esprit curieux de l'un des fondateurs de
	Nacridan 2. <br> <b>Des exemples de pratiques modernes</b><br> - <a
		href="http://nvc-europe.org/SPIP/">La communication non-violente</a>
	(CNV) : un protocole de discussion conçu pour la gestion des conflits à
	travers la compréhension de l'autre et de soi-même.<br> -
	Mindfulness-Based Stress Reduction (MBSR) : développées dans un cadre
	médical aux Etats-Unis pour lutter contre le stress. Pour une première
	approche sur la "Pleine Conscience", le lecteur curieux est invité à
	lire <a href="Pleine-Conscience.pdf">ce document</a>. C'est un rapport
	de TPE qui fait le tour des différents aspects : définition, influence
	sur le cerveau, utilisation actuelle dans le monde. Il présente
	également une expérience intéressante de l'application de ces pratiques
	dans une classe de CE2.<br> <br> <b>Des exemples de pratiques
		traditionnelles non dévotionnelles</b><br> - La Pleine Conscience dans
	le bouddhisme zen : une des sources d'inspiration pour la création de
	la MBSR. La communauté du <a href="http://villagedespruniers.net/ ">Village
		des Pruniers</a> est un bon exemple de lieu de pratique et
	d'enseignement pour cette voie.<br> - Le Yoga, pas au sens moderne,
	mais au sens traditionnel : l'étude en profondeur des Yoga Sutra de
	Patanjali, par exemple avec la <a
		href="http://epyoga.org/spip.php?page=revue-yoga-livre&id_article=9 ">traduction
		de Frans Moore</a>. L'étude du yoga inclut bien-sûr le yoga postural
	vulgarisé en occident, à étudier en adaptant en douceur à ses
	capacités, de préférence avec un professeur. <br> <br>

</p>


<!--
<p class='rule_pg'>
Pour une première approche sur la "Pleine Conscience", le lecteur curieux est invité à lire <a href="Pleine-Conscience.pdf" >ce document</a>. C'est un rapport de TPE qui fait le tour des différents aspects : définition, influence sur le cerveau, utilisation actuelle dans le monde. Il présente également une expérience intéressante de l'application de ces pratiques dans une classe de CE2.
</p>

<p class='rule_pg'>
J'écris ce dernier paragraphe en mon nom propre, et pas au nom de l'équipe'
Il existe de très nombreuses possibilités pour approfondir le développement personnel. Je présente ici des centres ou pratiques que j'ai testé, ce n'est donc pas une liste exhaustive. Parmi les cadres modernes et en dehors de toute religion, il y a par exemple la <a href='http://nvc-europe.org/SPIP/'>Communication Non-Violente</a>. Parmi les cadres religieux qui développent l'ouverture du coeur et de l'esprit sans dérive sectaire il y a en France : </br>
- dans la tradition Bouddhiste, le monastère du <a href='http://www.villagedespruniers.net/'>Village des Pruniers</a>, une première approche peut se faire à travers les livres de Thich Nhat Hanh, le moine vietnamien fondateur de ce monastère. </br>
- dans la tradition chrétienne, le monastère Taizé qui est principalement réservé au jeunes. Il offre une version très ouverte des enseignements du Christ.</br>
Ces deux monastères sont de grands centres internationaux. Je ne connais pas d'équivalent musulman ou hindouiste, même si ces deux traditions possèdent également des branches douces. 
</p>


<br/>
<p class='rule_pg'>
<div style='text-align:right;'>Enjoy the game !</br></br>
Aé Li</div>
</p>
-->
<br />
<br />
<br />
