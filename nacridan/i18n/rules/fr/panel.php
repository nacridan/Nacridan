



<div class='subtitle'>Et sinon je fais comment pour agir ?</div>

<p class='rule_pg'>

	Voici quelques explications sur l'interface : <br /> (positionnez la
	souris sur la zone qui vous intéresse)
</p>
<br />
<img src="<?echo CONFIG_HOST;?>/pics/rules/panel-example2.jpg"
	usemap="#panelmap" style="margin-left: -20px; border: 0" alt="" />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<!-- 
<map id="panelmap" name="panelmap">
<area shape="circle" coords="378,25,24" onmouseout="hideTip()" onmouseover="showTip('zone1')" alt=""/>
<area shape="circle" coords="60,92,24" onmouseout="hideTip()" onmouseover="showTip('zone2')" alt=""/>
<area shape="circle" coords="379,327,24" onmouseout="hideTip()" onmouseover="showTip('zone3')" alt=""/>
<area shape="circle" coords="636,164,24" onmouseout="hideTip()" onmouseover="showTip('zone4')" alt=""/>
<area shape="rect" coords="100,100,700,400" onmouseout="hideTip()" onmouseover="showTip('zoneb')" alt=""/>

<area shape="rect" coords="100,100,700,400" onmouseout="hideTip()" onmouseover="showTip('zoneb')" alt=""/>
<area shape="rect" coords="131,346,314,390" onmouseout="hideTip()" onmouseover="showTip('zonea')" alt=""/>

</map>
-->

<div
	style='position: absolute; left: 5px; top: 190px; width: 156px; height: 307px; background-image: url(../../../pics/rules/interfaceframe1.png);'
	id='panel1' onmouseout="hideTip()" onmouseover="showTip('zone2')"></div>
<div
	style='position: absolute; left: 165px; top: 160px; width: 596px; height: 36px; background-image: url(../../../pics/rules/interfaceframe2.png);'
	id='panel2' onmouseout="hideTip()" onmouseover="showTip('zone1')"></div>
<div
	style='position: absolute; left: 760px; top: 190px; width: 133px; height: 309px; background-image: url(../../../pics/rules/interfaceframe3.png);'
	id='panel3' onmouseout="hideTip()" onmouseover="showTip('zone4')"></div>
<div
	style='position: absolute; left: 170px; top: 200px; width: 572px; height: 290px; background-image: url(../../../pics/rules/interfaceframe4.png);'
	id='panel4' onmouseout="hideTip()" onmouseover="showTip('zoneb')"></div>
<div
	style='position: absolute; left: 170px; top: 500px; width: 580px; height: 37px; background-image: url(../../../pics/rules/interfaceframe5.png);'
	id='panel5' onmouseout="hideTip()" onmouseover="showTip('zone3')"></div>
<div
	style='position: absolute; left: 750px; top: 530px; width: 121px; height: 99px; background-image: url(../../../pics/rules/interfaceframe6.png);'
	id='panel6' onmouseout="hideTip()" onmouseover="showTip('zone6')"></div>
<div
	style='position: absolute; left: 15px; top: 540px; width: 208px; height: 89px; background-image: url(../../../pics/rules/interfaceframe7.png);'
	id='panel7' onmouseout="hideTip()" onmouseover="showTip('zone7')"></div>
<div
	style='position: absolute; left: 225px; top: 540px; width: 473px; height: 86px; background-image: url(../../../pics/rules/interfaceframe8.png);'
	id='panel8' onmouseout="hideTip()" onmouseover="showTip('zonea')"></div>


<div class="absdiv" style='left: 500px; top: 200px;' id='zone1'>
	Cette zone permet de basculer entre les différents "modes"<br />
	cliquez sur :<br /> <br /> <b>Conquête :</b> pour vous battre, vous
	déplacer, agir sur le monde.<br /> <b>Économie :</b> pour acheter et
	vendre des objets avec les autres joueurs.<br /> <b>Diplomatie :</b>
	pour gérer "votre" groupe, vos amis et ennemis.<br /> <b>Royaume :</b>
	pour vous renseigner sur le royaume dans lequel vous êtes.<br /> <b>Carte
		:</b> pour voir la carte et ses options (routes,villages,etc).<br />
</div>

<div class="absdiv" style='left: 200px; top: 250px;' id='zone2'>
	Menu <b>Action</b><br /> Ce menu dépend de l'onglet sélectionné <br />
	dans le menu du haut.<br /> En mode "Conquête", par exemple, c'est ici<br />
	que vous pourrez lancer différentes actions : <br /> ramasser des
	objets au sol et vous équiper,<br /> attaquer, etc.
</div>

<div class="absdiv" style='left: 200px; top: 250px;' id='zone3'>
	Cette zone a un but informatif sur votre personnage : <br /> <br /> <b>Vue
		:</b> permet de consulter l'environnement proche de votre personnage.<br />
	<b>Profil :</b> permet de consulter ses caractéritiques détaillées.<br />
	<b>Équipement :</b> permet de voir son équipement.<br /> <b>Évènements
		:</b> permet de consulter l'historique de ses évênements.<br /> <b>Options
		:</b> permet de modifier la description et l'illustration de votre
	personnage.
</div>

<div class="absdiv" style='left: 320px; top: 290px;' id='zone4'>
	Cette zone regroupe l'ensemble des caractéristiques<br /> de votre
	personnage de manière résumée.<br /> Vous y trouverez :<br /> <br />
	Vos caractéristiques d'attaque, <br> les coordonnées actuelles de votre
	personnage,<br /> le nombre de points d'expérience (PX) et pièces<br />
	d'or (PO) accumulées etc.<br /> C'est en bas de cette zone que vous
	avez un lien pour vous déconnecter du jeu.
</div>

<div class="absdiv" style='left: 320px; top: 290px;' id='zone6'>
	Cette zone regroupe des onglets dont l'usage est <br> moins fréquent
	pour la plupart des joueurs :<br> - La messagerie interne du jeu, pour
	communiquer avec <br> les gens en dehors de votre groupe de chasse.<br>
	- La gestion de votre compte : email automatiques, mot de passe, etc.<br>
	- Les règles du jeu : la réponse à la plupart de vos questions sur le
	jeu.<br>
</div>

<div class="absdiv" style='left: 320px; top: 290px;' id='zone7'>Un lien
	vers le forum et la tribune. La tribune permet de communiquer plus
	facilement dans le groupe de chasse. Ce panneau est cliquable seulement
	si vous êtes membre d'un groupe de chasse.</div>


<div class="absdiv" style='left: 400px; top: 520px;' id='zonea'>
	C'est généralement dans cette zone que vous devrez <br /> spécifier les
	"aboutissants" de votre action. <br /> Par exemple définir où vous
	déplacer, qui attaquer,<br /> quel objet ramasser, quelle potion
	utiliser etc.<br /> C'est aussi dans cette zone que s'affiche le menu<br>
	contextuel de la vue 2D : quand vous cliquez sur <br> une case, sa
	description s'affiche ici.
</div>

<div class="absdiv" style='left: 400px; top: 300px;' id='zoneb'>
	C'est la zone qui affiche la vue de votre personnage. <br> C'est aussi
	dans cette zone que vous seront communiqués<br> les résulats des
	actions entreprises.<br>
</div>

<div class='subtitle'>Profil</div>
<p class='rule_pg'>
	Le profil récapitule toutes les informations relatives à votre
	personnage. <br /> <br>
</p>



<script type='text/javascript'>
var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

</script>

