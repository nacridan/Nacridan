$subject = "[Nacridan] Inscription réussie"; $message = "Bienvenue dans
le monde de Nacridan \n\n\n\n"; $message .= "Voici vos identifiants pour
vous connecter sur Nacridan : \n\n"; $message .= "- Votre Pseudo
.........: ".$login."\n\n"; $message .= "- Votre Mot de Passe ...:
".$password."\n\n\n\n\n\n"; $message .= "Vous pouvez dés à present créer
votre personnage en vous connectant sur ".CONFIG_ROOT."\n\n\n\n\n\n";
$message .= "Bonne chance ;-)\n\n\n\n"; $message .=
"-------------------------------------\n"; $message .= "Rappel : Vous
vous êtes engagé à respecter les quelques restrictions imposées par la
charte de bonne conduite ".CONFIG_HOST."/i18n/disclaimer/index.php\n";
$message .= "-------------------------------------\n"; $messagehtml =
"Bienvenue dans le monde de Nacridan
<br />
<br />
<br />
<br />
"; $messagehtml .= "Voici vos identifiants pour vous connecter sur
Nacridan :
<br />
<br />
"; $messagehtml .= "- Votre Pseudo .........: ".$login."
<br />
<br />
"; $messagehtml .= "- Votre Mot de Passe ...: ".$password."
<br />
<br />
<br />
<br />
<br />
<br />
"; $messagehtml .= "Vous pouvez dés à present créer votre personnage en
vous connectant sur
<a href='".CONFIG_ROOT."'>Nacridan</a>
<br />
<br />
<br />
<br />
<br />
<br />
"; $messagehtml .= "Bonne chance ;-)
<br />
<br />
<br />
<br />
"; $messagehtml .= "-------------------------------------
<br />
"; $messagehtml .= "Rappel : Vous vous êtes engagé à respecter les
quelques restrictions imposées par la
<a href='".CONFIG_HOST."/i18n/disclaimer/index.php'>charte</a>
de bonne conduite.
<br />
"; $messagehtml .= "-------------------------------------
<br />
"; $result ="Bravo, votre inscription s'est déroulée parfaitement.
<br />
Vous allez recevoir d'ici peu vos identifiants pour vous connecter sur
Nacridan.
<br />
<br />
PS:
<br />
<span class='attackevent'><b>Si vous ne recevez pas de message,<br />
		pensez à vérifier votre boîte de courrier indésirable (SPAM) si vous
		en possédez une.
</b></span>
"; $resultfail ="Erreur, votre inscription n'a pas pu se dérouler
parfaitement.
<br />
<br />
PS:
<br />
Votre adresse E-mail n'a pas réussi à être contactée
<br />
";
