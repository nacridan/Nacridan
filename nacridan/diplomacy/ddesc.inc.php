<?php

class DDesc extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DDesc($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $str = "";
        
        $team = new Team();
        if (($idteam = $curplayer->get("id_Team")) == 0) {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=desc' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>\n";
            $str .= "<b><h1>" . localize("Vous ne faites partie d'aucun Ordre.") . "</h1></b></td></tr>\n";
            $str .= "</table></form>\n";
        } else {
            if (isset($_POST["validate"])) {
                if (! $this->nacridan->isRepostForm()) {
                    require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                    if (isset($_POST["desc"]))
                        TeamFactory::modifyDesc($curplayer, quote_smart(unHTMLTags($_POST["desc"])), $err, $db);
                    else
                        TeamFactory::modifyDesc($curplayer, quote_smart(unHTMLTags($_POST[""])), $err, $db);
                }
            } else {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=desc' . "' target='_self'>\n";
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>\n";
                $team = $curplayer->getObj("Team");
                $str .= "<b><h1>" . $team->get("name") . "</h1></b></td></tr>";
                $str .= "</table>\n";
                
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Description") . "</td></tr>\n";
                $body = new TeamBody();
                $body->load($team->get("id_TeamBody"), $db);
                $str .= "<tr><td><textarea name='desc' cols=80 rows=15>" . $body->get("body") . "</textarea></td></tr>";
                $str .= "</table>\n";
                $str .= "<input type='submit' name='validate' value='" . localize("Valider les changements") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=desc' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        
        return $str;
    }
}

?>