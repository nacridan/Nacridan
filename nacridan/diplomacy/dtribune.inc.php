<?php

class DTribune extends HTMLObject
{

    private $tribune;

    public function __construct(NacridanModule $nacridan, PDO $db, $type)
    {
        $factory = new TribuneFactory();
        $this->tribune = $factory->get($nacridan, $db, $type);
    }

    public function toString()
    {
        return $this->tribune->toString();
    }

    public function getUnreadMessagesInfos()
    {
        return $this->tribune->getUnreadMessagesInfos();
    }

    public function getShortTitle()
    {
        return $this->tribune->getShortTitle();
    }
}

?>