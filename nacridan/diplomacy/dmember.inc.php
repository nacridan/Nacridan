<?php

class DMember extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DMember($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $str = "";
        
        if (isset($_POST["validate"]) || isset($_POST["validate2"]) || isset($_POST["Ban"]) || isset($_POST["Edit"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["validate"])) {
                    require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                    if (isset($_POST["check"]))
                        TeamFactory::banTeamMember($curplayer, quote_smart($_POST["check"]), $err, $db);
                }
                
                if (isset($_POST["validate2"])) {
                    require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                    if (isset($_POST["check"]))
                        TeamFactory::changeMemberGrade($curplayer, quote_smart($_POST["check"]), quote_smart($_POST), "RANK_", $err, $db);
                }
                
                if (isset($_POST["Ban"])) {
                    if (isset($_POST["check"])) {
                        $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=member' . "' target='_self'>\n";
                        $str .= "<table class='maintable' width='620px'>\n";
                        $str .= "<tr><td class='mainbgtitle' width='620px'>" . localize("Êtes vous sur de vouloir bannir les personnes sélectionnés ?") . "</td></tr></table>";
                        
                        $where = getSQLCondFromArray(quote_smart($_POST["check"]), "id", "OR");
                        $dbt = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.level FROM Player WHERE " . $where, $db);
                        $arrayMember = array();
                        while (! $dbt->eof()) {
                            if ($dbt->get("id") == $curplayer->getSub("Team", "id_Player")) {
                                $str .= "<table width='620px'><tr><td class='mainbgtitle'>" . localize("Attention, il est impossible de bannir le fondateur de l'Ordre.") .
                                     "</td></tr></table>";
                            } else {
                                $name = "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("name") . "</a> (" .
                                     $dbt->get("racename") . ")";
                                
                                // $grade=$dbt->get("trname");
                                
                                $arrayMember[] = array(
                                    "0" => array(
                                        "<input type='checkbox' name='check[]' checked value='" . $dbt->get("id") . "'>",
                                        "class='mainbgbody' align='center'"
                                    ),
                                    "1" => array(
                                        $dbt->get("id"),
                                        "class='mainbgbody' align='left'"
                                    ),
                                    "2" => array(
                                        $name,
                                        "class='mainbgbody' align='left'"
                                    ),
                                    "3" => array(
                                        $dbt->get("level"),
                                        "class='mainbgbody' align='center'"
                                    )
                                );
                            }
                            $dbt->next();
                        }
                        $str .= createTable(4, $arrayMember, 
                            array(
                                array(
                                    localize("Membres à Bannir"),
                                    "class='mainbgtitle' colspan=4 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    "",
                                    "class='mainbglabel' width='5%' align='center'"
                                ),
                                array(
                                    localize("id"),
                                    "class='mainbglabel' width='10%' align='left'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel' width='40%' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='15%' align='center'"
                                )
                            ), "class='maintable centerareawidth'");
                        
                        $str .= "<input type='submit' name='back' value='" . localize("Annuler") . "' />";
                        $str .= "<input type='submit' name='validate' value='" . localize("Continuer") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</form>";
                    } else {
                        $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
                    }
                }
                
                if (isset($_POST["Edit"])) {
                    if (isset($_POST["check"])) {
                        $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=member' . "' target='_self'>\n";
                        
                        $where = getSQLCondFromArray(quote_smart($_POST["check"]), "Player.id", "OR");
                        $dbt = new DBCollection(
                            "SELECT Player.id,Player.name,Player.racename,Player.level,TeamRankInfo.name AS trname,TeamRankInfo.id AS trid FROM Player LEFT JOIN Team ON Team.id=Player.id_Team LEFT JOIN TeamRank ON TeamRank.id_Player=Player.id LEFT JOIN TeamRankInfo ON TeamRankInfo.id=TeamRank.id_TeamRankInfo WHERE " .
                                 $where . " AND Team.id=" . $curplayer->get("id_Team") . " ORDER BY TeamRankInfo.num ASC, TeamRank.id ASC", $db);
                        
                        $dbrinfo = new DBCollection("SELECT * FROM TeamRankInfo WHERE id_Team=" . $curplayer->get("id_Team") . " ORDER BY num", $db);
                        
                        $arrayMember = array();
                        
                        while (! $dbt->eof()) {
                            $name = "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("name") . "</a> (" .
                                 $dbt->get("racename") . ")";
                            
                            $grade = "<select class='cqselector' name='RANK_" . $dbt->get("id") . "'>";
                            $dbrinfo->first();
                            while (! $dbrinfo->eof()) {
                                if ($dbrinfo->get("name") == $dbt->get("trname"))
                                    $grade .= "<option value='" . $dbrinfo->get("id") . "' selected='selected'>" . $dbrinfo->get("name") . "</option>";
                                else
                                    $grade .= "<option value='" . $dbrinfo->get("id") . "'>" . $dbrinfo->get("name") . "</option>";
                                $dbrinfo->next();
                            }
                            $grade .= "</select>";
                            
                            $arrayMember[] = array(
                                "0" => array(
                                    "<input type='hidden' name='check[]' value='" . $dbt->get("id") . "'>",
                                    "class='mainbgbody' align='center'"
                                ),
                                "1" => array(
                                    $dbt->get("id"),
                                    "class='mainbgbody' align='left'"
                                ),
                                "2" => array(
                                    $name,
                                    "class='mainbgbody' align='left'"
                                ),
                                "3" => array(
                                    $grade,
                                    "class='mainbgbody' align='left'"
                                ),
                                "4" => array(
                                    $dbt->get("level"),
                                    "class='mainbgbody' align='center'"
                                )
                            );
                            $dbt->next();
                        }
                        
                        $str .= createTable(5, $arrayMember, 
                            array(
                                array(
                                    localize("Membres"),
                                    "class='mainbgtitle' colspan=5 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    "",
                                    "class='mainbglabel' width='5%' align='center'"
                                ),
                                array(
                                    localize("id"),
                                    "class='mainbglabel' width='10%' align='left'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel' width='40%' align='left'"
                                ),
                                array(
                                    localize("Race"),
                                    "class='mainbglabel' width='30%' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='15%' align='center'"
                                )
                            ), "class='maintable centerareawidth'");
                        
                        $str .= "<input type='submit' name='back' value='" . localize("Annuler") . "' />";
                        $str .= "<input type='submit' name='validate2' value='" . localize("Valider les changements") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</form>";
                    } else {
                        $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
                    }
                }
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "" && $str == "") {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=member' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            
            $team = new Team();
            
            $dbtri = new DBCollection(
                "SELECT case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL . " then p2.id_Team else p1.id_Team end as id_Team, case when p1.id_BasicRace = " .
                     ID_BASIC_RACE_FEU_FOL . " then p2.id else p1.id end as idPlayerForRank FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                     ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id=" . $curplayer->get("id"), $db);
            $idTeamMember = $dbtri->get("id_Team");
            $idPlayerForRank = $dbtri->get("idPlayerForRank");
            if ($idTeamMember == 0) {
                $str .= "<b><h1>" . localize("Vous ne faites partie d'aucun Ordre.") . "</h1></b></td></tr>\n";
            } else {
                $team->load($idTeamMember, $db);
                $str .= "<b><h1>" . $team->get("name") . "</h1></b></td></tr>";
            }
            
            $str .= "</tr></table>\n";
            
            if ($team->get("name") != "") {
                $dbrank = new DBCollection(
                    "SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" .
                         $idPlayerForRank, $db);
                
                $modify3 = 0;
                $modify4 = 0;
                if (! $dbrank->eof()) {
                    if ($dbrank->get("auth3") == "Yes" || $curplayer->getSub("Team", "id_Player") == $curplayer->get("id")) {
                        $modify3 = 1;
                    }
                    if ($dbrank->get("auth4") == "Yes" || $curplayer->getSub("Team", "id_Player") == $curplayer->get("id")) {
                        $modify4 = 1;
                    }
                }
                
                $dbt = new DBCollection(
                    "SELECT p1.id,p1.id_BasicRace,case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                         " then concat(p1.racename,' de ',p2.name) else p1.name end as nameModified,p1.map,p1.x,p1.y,p1.racename,p1.level,TeamRankInfo.name AS trname,TeamRankInfo.id AS trid FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                         ID_BASIC_RACE_FEU_FOL .
                         ") LEFT JOIN Team ON Team.id=p1.id_Team or Team.id=p2.id_Team LEFT JOIN TeamRank ON TeamRank.id_Player=p1.id or TeamRank.id_Player=p2.id LEFT JOIN TeamRankInfo ON TeamRankInfo.id=TeamRank.id_TeamRankInfo WHERE p1.id_Team=" .
                         $idTeamMember . " or p2.id_Team =" . $idTeamMember . " ORDER BY TeamRankInfo.num ASC, TeamRank.id ASC", $db);
                $arrayMember = array();
                while (! $dbt->eof()) {
                    $name = '';
                    if ($dbt->get('id_BasicRace') != ID_BASIC_RACE_FEU_FOL) {
                        $name  = Envelope::render($dbt->get('nameModified'));
                    }
                    $name .= "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("nameModified") . "</a> (" .
                         $dbt->get("racename") . ")";
                    
                    $grade = $dbt->get("trname");
                    
                    if (($modify3 || $modify4) && $dbt->get("id_BasicRace") != ID_BASIC_RACE_FEU_FOL)
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $dbt->get("id") . "'>";
                    else
                        $checkbox = "&nbsp;";
                    
                    if ($dbrank->get("num") < 20) {
                        $xval = $dbt->get("x");
                        $yval = $dbt->get("y");
                        $mapval = $dbt->get("map");
                        $distance = distHexa($curplayer->get("x"), $curplayer->get("y"), $dbt->get("x"), $dbt->get("y"));
                    } else {
                        $xval = "-";
                        $yval = "-";
                        $mapval = "-";
                        $distance = "-";
                    }
                    
                    $arrayMember[] = array(
                        "0" => array(
                            $checkbox,
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("level"),
                            "class='mainbgbody' align='center'"
                        ),
                        "2" => array(
                            $name,
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $grade,
                            "class='mainbgbody' align='left'"
                        ),
                        "4" => array(
                            $xval,
                            "class='mainbgbody' align='right'"
                        ),
                        "5" => array(
                            $yval,
                            "class='mainbgbody' align='right'"
                        ),
                        "6" => array(
                            $distance,
                            "class='mainbgbody' align='center'"
                        ),
                        "7" => array(
                            $mapval,
                            "class='mainbgbody' align='right'"
                        )
                    );
                    $dbt->next();
                }
                $str .= createTable(7, $arrayMember, 
                    array(
                        array(
                            localize("Membres"),
                            "class='mainbgtitle' colspan=7 align='left'"
                        )
                    ), 
                    array(
                        array(
                            "",
                            "class='mainbglabel' width='4%' align='center'"
                        ),
                        array(
                            localize("Niveau"),
                            "class='mainbglabel' width='8%' align='center'"
                        ),
                        array(
                            localize("Nom"),
                            "class='mainbglabel' width='40%' align='left'"
                        ),
                        array(
                            localize("Rang"),
                            "class='mainbglabel' width='30%' align='left'"
                        ),
                        array(
                            localize("X"),
                            "class='mainbglabel' width='9%' align='right'"
                        ),
                        array(
                            localize("Y"),
                            "class='mainbglabel' width='9%' align='right'"
                        ),
                        array(
                            localize("Distance"),
                            "class='mainbglabel' width='9%' align='center'"
                        ),
                        array(
                            localize("Région"),
                            "class='mainbglabel' width='9%' align='right'"
                        )
                    ), "class='maintable centerareawidth'");
                if ($modify3)
                    $str .= "<input id='Ban' type='submit' name='Ban' value='" . localize("Bannir") . "' />";
                if ($modify4)
                    $str .= "<input id='Edit' type='submit' name='Edit' value='" . localize("Modifier le(s) Rang(s)") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=member' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
