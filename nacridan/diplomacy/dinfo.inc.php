<?php

class DInfo extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DInfo($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $idplay = $curplayer->get("id");
        $str = "<table class='maintable centerareawidth'>\n";
        $str .= "<tr><td class='mainbgtitle'>\n";
        $str .= "<b><h1>" . localize("Alliances") . "</h1></b></td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $idteam = $curplayer->get("id_Team");
        
        $str .= "<table><tr valign='top'><td>";
        $str .= $this->display_diplo("alliance personnelle", array(
            "main" => ($idteam != 0) ? 'halfcenterareawidth' : 'halfcenterareawidth',
            "ally" => "self_ally",
            "ennemy" => "self_ennemy"
        ), array(
            "id" => $idplay,
            "rfield" => "id_Player\$src",
            "queries" => array(
                array(
                    "link" => "alliegance",
                    "class" => "mainbgbody",
                    "table" => "PJAlliance",
                    "field" => "id_Team",
                    "mode" => "Team",
                    "linkclass" => "styleall"
                ),
                array(
                    "link" => "profile",
                    "class" => "mainbgbody",
                    "table" => "PJAlliancePJ",
                    "field" => "id_Player\$dest",
                    "mode" => "Player",
                    "linkclass" => "stylepc"
                )
            )
        ));
        
        if ($idteam != 0) {
            $str .= "</td><td>";
            
            $team = new Team();
            $team->load($idteam, $db);
            $str .= $this->display_diplo("Alliance de votre ordre", array(
                "main" => "halfcenterareawidth",
                "ally" => "guild_ally",
                "ennemy" => "guild_ennemy"
            ), array(
                "id" => $idteam,
                "rfield" => "id_Team\$src",
                "queries" => array(
                    array(
                        "link" => "alliegance",
                        "class" => "mainbgbody",
                        "table" => "TeamAlliance",
                        "field" => "id_Team\$dest",
                        "mode" => "Team",
                        "linkclass" => "styleall"
                    ),
                    array(
                        "link" => "profile",
                        "class" => "mainbgbody",
                        "table" => "TeamAlliancePJ",
                        "field" => "id_Player",
                        "mode" => "Player",
                        "linkclass" => "stylepc"
                    )
                )
            ));
        }
        
        $str .= "</td></tr></table>";
        
        return $str;
    }

    function display_group($title, $type, $class, $info_queries)
    {
        $db = $this->db;
        $first = "<tr><td colspan='2' class='mainbgtitle $class'>$title</td></tr>";
        $id = $info_queries["id"];
        $rfield = $info_queries["rfield"];
        $str = $first;
        foreach ($info_queries["queries"] as $query) {
            $tbl = $query["table"];
            $mode = $query["mode"];
            $field = $query["field"];
            $class = $query["class"];
            $link = $query["link"];
            $linkclass = $query["linkclass"];
            
            $dbt = new DBCollection("SELECT $tbl.id,name,type, datediff(now(),datestart) as datestart,$field FROM $tbl LEFT JOIN $mode ON $mode.id=$tbl.$field WHERE $rfield='$id' and type='$type' order by datestart desc", $db);
            while (($dbt->m_errorno == 0) && ! $dbt->eof()) {
                $idrecord = $dbt->get($field);
                $str .= "<tr><td align='right' class='mainbglabel'>" . $this->display_date($dbt->get("datestart")) . "&nbsp;</td>
					<td class='$class'>
						<a	href='/conquest/$link.php?id=" . $idrecord . "'
							class='$linkclass popupify'>" . $dbt->get("name") . "</a>
					</td></tr>";
                $dbt->next();
                // $first = "&nbsp;";
            }
        }
        return $str;
    }

    function display_diplo($title, $classes, $info_queries)
    {
        $db = $this->db;
        return "<table class='maintable " . $classes["main"] . "'>\n" . "<tr><td class='mainbgtitle' colspan='2' class='mainbgtitle'><b>" . $title . "</b></td></tr>" . $this->display_group(localize("Alliés"), "Allié", $classes["ally"], $info_queries) . $this->display_group(localize("Ennemis"), "Ennemi", $classes["ennemy"], $info_queries) . "</table>\n";
    }

    function display_date($date)
    {
        $lastunit = array(
            "value" => 0,
            "text" => ""
        );
        $unit = array(
            "value" => 0,
            "text" => "récemment"
        );
        $units = array(
            array(
                "value" => 7,
                "text" => "semaine"
            ),
            array(
                "value" => 10,
                "text" => "an"
            )
        );
        $rest = $date;
        foreach ($units as $u) {
            $val = $u["value"];
            if (($rest = floor($rest / $val)) > 0) {
                $lastunit["value"] = $unit["value"] - ($rest * $val);
                $lastunit["text"] = $unit["text"];
                $unit["text"] = $u["text"] . (($rest > 1) ? 's' : '');
                $unit["value"] = $rest;
            } else {
                $u = $unit["value"];
                $lu = $lastunit["value"];
                return (($u > 0) ? $u : "") . " " . localize($unit["text"]) . (($lu > 0) ? " " . $lu . " " . localize($lastunit["text"]) : "");
            }
        }
    }
}
?>
