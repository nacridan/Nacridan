<?php

class DDissol extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DDissol($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        if (isset($_POST["dissol"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                TeamFactory::dissolTeam($curplayer, quote_smart($_POST["dissol"]), $err, $db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        } else {
            if ($curplayer->get("id_Team") > 0) {
                $str = "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Dissolution de votre Ordre") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=dissol' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='550px'>" . localize("Êtes vous sûr de vouloir dissoudre votre Ordre ?") . "</td>";
                $str .= "<td><input type='hidden' name='dissol' value='true' /><input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $err = localize("Vous ne faites partie d'aucun Ordre.");
            }
        }
        
        if ($err != "") {
            $str = "<table class='maintable' width='620px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
        }
        return $str;
    }
}
?>