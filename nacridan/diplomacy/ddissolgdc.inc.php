<?php

class DDissolGdC extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DDissolGdC($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        if (isset($_POST["dissol"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                $type = 2;
                GdCFactory::dissolGdC($curplayer, $type, $err, $db);
            } else
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        } else {
            if ($curplayer->get("id_FighterGroup") > 0) {
                $str = "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Dissolution de votre GdC") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=dissolgdc' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='550px'>" . localize("Êtes vous sûr de vouloir dissoudre votre groupe de chasse ?") . "</td>";
                $str .= "<td><input type='hidden' name='dissol' value='true' /><input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $err = localize("Vous ne faites partie d'aucun groupe de chasse.");
            }
        }
        
        if ($err != "") {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
            $str .= "<input type='submit' name='submibt' value='" . localize("Terminer") . "' />";
            $str .= "</form>";
        }
        return $str;
    }
}
?>