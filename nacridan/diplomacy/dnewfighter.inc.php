<?php

class DNewFighter extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DNewFighter($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $err = "";
        if (isset($_POST["Add"]) || isset($_POST["Del"]) || isset($_POST["Cancel"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["Add"])) {
                    require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                    $type = 2;
                    if (isset($_POST["field"]))
                        GdCFactory::validateJoinGdC($curplayer, quote_smart($_POST["field"]), $type, $err, $db);
                    // else
                    // $err="Vous devez sélectionner un personnage à accepter.";
                }
                if (isset($_POST["Del"])) {
                    require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                    $type = 2;
                    if (isset($_POST["field"]))
                        GdCFactory::refuseJoinGdC($curplayer, quote_smart($_POST["field"]), $type, $err, $db);
                }
                if (isset($_POST["Cancel"])) {
                    require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                    $type = 2;
                    if (isset($_POST["check"]))
                        GdCFactory::cancelInviteGdC($curplayer, quote_smart($_POST["check"]), $type, $err, $db);
                }
            } else
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        }
        
        if ($err == "") {
            
            /* ******************************************************* Invitation reçues *********************************************************** */
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=newfigther' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= "<b><h1> Groupe de chasse </h1></b></td></tr>";
            $str .= "</tr></table>\n";
            $dbt = new DBCollection("SELECT Player.id, Player.name, Player.level, Player.racename FROM Player LEFT JOIN FighterGroupAskJoin ON Player.id=FighterGroupAskJoin.id_Player WHERE id_Guest=" . $curplayer->get("id"), $db);
            $arrayMember = array();
            while (! $dbt->eof()) {
                $name = "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("name") . "</a>";
                $arrayMember[] = array(
                    "0" => array(
                        "<input id='inv' type='radio' name='field' value='" . $dbt->get("id") . "'>",
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $dbt->get("id"),
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $name,
                        "left"
                    ),
                    "3" => array(
                        $dbt->get("racename"),
                        "class='mainbgbody' align='left'"
                    ),
                    "4" => array(
                        $dbt->get("level"),
                        "class='mainbgbody' align='center'"
                    )
                );
                $dbt->next();
            }
            $str .= createTable(5, $arrayMember, array(
                array(
                    localize("Invitations reçues"),
                    "class='mainbgtitle' colspan=5 align='left'"
                )
            ), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("id"),
                    "class='mainbglabel' width='10%' align='left'"
                ),
                array(
                    localize("Nom du chef du groupe"),
                    "class='mainbglabel' width='40%' align='left'"
                ),
                array(
                    localize("Race"),
                    "class='mainbglabel' width='30%' align='left'"
                ),
                array(
                    localize("Niveau"),
                    "class='mainbglabel' width='15%' align='center'"
                )
            ), "class='maintable  centerareawidth'");
            
            $str .= "<input id='Add' type='submit' name='Add' value='" . localize("Accepter") . "' />";
            $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Refuser") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            // $str.="</form>";
            
            /* ******************************************************* Invitations envoyées *********************************************************** */
            // $str="<form method='POST' action='".CONFIG_HOST.'/diplomacy/diplomacy.php?center=newfigther'."' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            // $str.="<tr><td class='mainbgtitle'>\n";
            // $str.="<b><h1> Groupe de chasse </h1></b></td></tr>";
            // $str.="</tr></table>\n";
            $dbt = new DBCollection("SELECT Player.id, Player.name, Player.level, Player.racename FROM Player LEFT JOIN FighterGroupAskJoin ON Player.id=FighterGroupAskJoin.id_Guest WHERE id_Player=" . $curplayer->get("id"), $db);
            $arrayMember = array();
            while (! $dbt->eof()) {
                $name = "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("name") . "</a>";
                $arrayMember[] = array(
                    "0" => array(
                        "<input type='checkbox' name='check[]' value='" . $dbt->get("id") . "'>",
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $dbt->get("id"),
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $name,
                        "left"
                    ),
                    "3" => array(
                        $dbt->get("racename"),
                        "class='mainbgbody' align='left'"
                    ),
                    "4" => array(
                        $dbt->get("level"),
                        "class='mainbgbody' align='center'"
                    )
                );
                $dbt->next();
            }
            $str .= createTable(5, $arrayMember, array(
                array(
                    localize("Invitations envoyées"),
                    "class='mainbgtitle' colspan=5 align='left'"
                )
            ), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("id"),
                    "class='mainbglabel' width='10%' align='left'"
                ),
                array(
                    localize("Destinataire"),
                    "class='mainbglabel' width='40%' align='left'"
                ),
                array(
                    localize("Race"),
                    "class='mainbglabel' width='30%' align='left'"
                ),
                array(
                    localize("Niveau"),
                    "class='mainbglabel' width='15%' align='center'"
                )
            ), "class='maintable  centerareawidth'");
            
            $str .= "<input id='Cancel' type='submit' name='Cancel' value='" . localize("Annuler") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=memberGdC' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
            $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
            $str .= "</form>";
        }
        
        return $str;
    }
}

?>
