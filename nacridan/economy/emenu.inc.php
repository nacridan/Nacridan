<?php

class EMenu extends HTMLObject
{

    public $nacridan;

    public $db;

    public function EMenu($nacridan, $db)
    {
        $this->nacridan = $nacridan;
        $this->db = $db;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($this->db);
    }

    public function toString()
    {
        $sess = $this->nacridan->sess;
        $id = $sess->get("cq_playerid");
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $str = "<div class='menu'>";
        $str .= "<dl class='rightmenu'>";
        
        if ($sess->has("DLA" . $id)) {
            $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Économie') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu1'>\n";
            $str .= "<ul>\n";
            // $str.="<li><a href='../economy/economy.php?bottom=buy&view=1'>".localize("Acheter ({ap} PA)",array ("ap" => BUY_AP))."</a></li>\n";
            $str .= "<li><a href='../economy/economy.php?bottom=sale&view=2'>" . localize("Mettre un Objet en Vente ({ap} PA)", array(
                "ap" => SALE_AP
            )) . "</a></li>\n";
            // $str.="<li><a href='../economy/economy.php?bottom=buyhorse'>".localize("Acheter une monture ({ap} PA)",array ("ap" => BUY_AP))."</a></li>\n";
            // $str.="<li><a href='../economy/economy.php?bottom=salehorse'>".localize("Mettre sa monture en Vente ({ap} PA)",array ("ap" => SALE_AP))."</a></li>\n";
            
            $str .= "</ul>\n";
            $str .= "</dd>\n";
        }
        
        $str .= "</dl>\n";
        $str .= "</div>\n";
        
        return $str;
    }
}
?>
