<?php

class EBuy extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function EBuy($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        $building = $curplayer->get("inbuilding");
        $room = $curplayer->get("room");
        
        if (isset($_POST["submitfinal"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                EquipFactory::buyEquipment($curplayer, quote_smart($_POST["OBJECT_ID"]), $err, $db);
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "") {
            if ($curplayer->get("ap") < BUY_AP) {
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='600px'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour acheter un objet.");
                $str .= "</td></tr></table>";
            } elseif (isset($_POST["submitbt"])) {
                $dbe = new DBCollection("SELECT id,name,sell FROM Equipment WHERE id=" . quote_smart($_POST["OBJECT_ID"]), $db);
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "<tr><td class='mainbgtitle' width='600px'>";
                $str .= "Vous allez acheter un(e) " . $dbe->get("name") . " pour " . $dbe->get("sell") . " PO (et " . BUY_AP . " PA). Continuer ?";
                $str .= "<input id='submitbt' type='submit' name='submitfinal' value='Action' />";
                $str .= "<input name='OBJECT_ID' type='hidden' value='" . $_POST["OBJECT_ID"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table></form>";
            } else {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Acheter un objet") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
                
                $gap = 1;
                
                $dbp = new DBCollection("SELECT Equipment.name,Equipment.level,Equipment.level,Equipment.id,Equipment.extraname  FROM Player LEFT JOIN Equipment ON Equipment.id_Player=Player.id WHERE Player.status='PC' AND Player.id!=" . $id . " AND Player.inbuilding=" . $building . " AND Player.room=" . $room . " AND Player.disabled=0 AND Player.map=" . $map . " AND (abs(Player.x-" . $xp . ") + abs(Player.y-" . $yp . ") + abs(Player.x + Player.y-" . $xp . "-" . $yp . "))/2 <=" . $gap . " AND Equipment.sell!=0", $db);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisir un équipement --") . "</option>";
                while (! $dbp->eof()) {
                    if ($dbp->get("name") != "") {
                        $item[] = array(
                            localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    }
                    $dbp->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "'>";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select> ";
                $str .= "</td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        if ($err != "") {
            $str = "<table class='maintable' width='620px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
        }
        return $str;
    }
}
?>
