<?php

class ESale extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function ESale($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if (isset($_POST["submitbt"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                EquipFactory::saleEquipment($curplayer, quote_smart($_POST["OBJECT_ID"]), quote_smart($_POST["PRICE"]), $err, $db);
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "") {
            if ($curplayer->get("ap") < SALE_AP) {
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='600px'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour mettre un objet en vente.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Mettre en vente") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
                
                $dbp = new DBCollection("SELECT * FROM Equipment WHERE state='carried' AND weared='NO' AND sell=0 AND id_Player=" . $curplayer->get("id"), $db, 0, 0);
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
                
                $item[] = array(
                    localize("-- Sac équipements --") => - 1
                );
                while (! $dbp->eof()) {
                    if ($dbp->get("id_Equipment\$bag") == 0) {
                        if ($dbp->get("id_BasicEquipment") > 499)
                            $item[] = array(
                                localize($dbp->get("name")) . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        else
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                    }
                    $dbp->next();
                }
                
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                if ($id_Bag = playerFactory::getIdBagByNum($id, 1, $db)) {
                    $item[] = array(
                        localize("-- Sac de gemmes --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                
                if ($id_Bag = playerFactory::getIdBagByNum($id, 2, $db)) {
                    $item[] = array(
                        localize("-- Sac de matières premières --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                
                if ($id_Bag = playerFactory::getIdBagByNum($id, 3, $db)) {
                    $item[] = array(
                        localize("-- Sac d herbes --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                
                if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                    $item[] = array(
                        localize("-- Ceinture --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                
                if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                    $item[] = array(
                        localize("-- Carquois --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                    $item[] = array(
                        localize("-- Trousse à outils --") => - 1
                    );
                    $dbp->first();
                    while (! $dbp->eof()) {
                        if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                            $item[] = array(
                                localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                            );
                        
                        $dbp->next();
                    }
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "'>";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select> ";
                $str .= localize("à") . " <input type='text' name='PRICE' value'' maxlength=9 size=10/> " . localize("PO");
                
                $str .= "</td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . SALE_AP . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        if ($err != "") {
            $str = "<table class='maintable' width='620px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
        }
        return $str;
    }
}
?>
