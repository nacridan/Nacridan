<?php

class EBuyHorse extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function EBuyHorse($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        if (isset($_POST["submitbt"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                PNJFactory::buyHorse($curplayer, quote_smart($_POST["OBJECT_ID"]), $err, $db);
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "") {
            if ($curplayer->get("ap") < BUY_AP) {
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='600px'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour acheter un objet.");
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Acheter ") . " <select id='Object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
                
                $gap = 0;
                
                $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member!=" . $curplayer->get("id_Member") . " AND map=" . $map . " AND x=" . $xp . " AND y=" . $yp . " AND id_Member!=0 AND state!=0", $db);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une monture --") . "</option>";
                while (! $dbp->eof()) {
                    if ($dbp->get("racename") != "") {
                        $item[] = array(
                            localize($dbp->get("racename")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                        );
                    }
                    $dbp->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "'>";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select> ";
                $str .= "</td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . BUY_AP . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        if ($err != "") {
            $str = "<table class='maintable' width='620px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
        }
        return $str;
    }
}
?>
