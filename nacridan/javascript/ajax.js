function RPCPost(url,param) {
  if(window.XMLHttpRequest) // FIREFOX
    xmlhttp = new XMLHttpRequest();
  else if(window.ActiveXObject) // IE
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  else
    return false;

  xmlhttp.open("POST", url, false);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");     
  xmlhttp.send(param);

  if(xmlhttp.readyState == 4)
    return xmlhttp.responseText;
  else 
    return "";
}
