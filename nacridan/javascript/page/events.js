$(document).ready(function() {

    var searchIcon = $('#searchIcon');
    var searchForm = $('#search');

    /**
     * Date picker stuff
     */
    var altFormat = 'yy-mm-dd 00:00:00';
    $("#date_from").val($.datepicker.formatDate('dd/mm/yy',$.datepicker.parseDate(altFormat, $('#filter_date_from').val())));
    $("#date_from").datepicker({
        altField: '#filter_date_from',
        altFormat: altFormat
    });
    $("#date_to").val($.datepicker.formatDate('dd/mm/yy',$.datepicker.parseDate(altFormat, $('#filter_date_to').val())));
    $("#date_to").datepicker({
        altField: '#filter_date_to',
        altFormat: altFormat
    });

    $('#clear_date_from').on('click', function(e){
        e.preventDefault();
        $("#date_from, #filter_date_from").val('');
    });
    $("#date_from").on('change', function(e){
        if ($(this).val().trim() == '')
            $("#filter_date_from").val('');
    })

    $('#clear_date_to').on('click', function(e){
        e.preventDefault();
        $("#date_to, #filter_date_to").val('');
    });
    $("#date_to").on('change', function(e){
        if ($(this).val().trim() == '')
            $("#filter_date_to").val('');
    })



    /**
     * Hide or show the search form
     */
    $(searchIcon).on('click', function(e){
        e.preventDefault();
        var height = 0;
        var paddingTop = 0;
        var paddingBottom = 0;
        if (searchForm.height() == 0) {
            height = "135px";
            paddingTop = "10px";
            paddingBottom = "10px";
        }
        searchForm.animate({
            height: height,
            paddingTop: paddingTop,
            paddingBottom: paddingBottom
        }, 250, function(){
            if(searchForm.height() == 0) {
                searchIcon.removeClass('active');
                enableOrDisableOldFilter();
            } else {
                searchIcon.addClass('active');
                disableOldFilter();
            }
        })




    });

    /**
     * Enabled only when the search form is hidden AND the search is not used.
     */
    function enableOrDisableOldFilter() {
        if ($('#date_from').val() != '' || $('#date_to').val() != '' || $('#target').val() != ''
            || $('#search select').find(":selected").length > 0) {
            disableOldFilter();
        } else {
            enableOldFilter();
        }
    }

    function disableOldFilter() {
        $('#classic_filter_select').attr('disabled', 'disabled');
    }
    function enableOldFilter() {
        $('#classic_filter_select').removeAttr('disabled');
    }



});