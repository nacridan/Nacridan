$(document).ready(function() {

    /* Params */
    // Time in seconds to check new messages
    var checkNewMessagesTime = 30;
    // Number of chars needed to trigger a confirmation when following the "new messages" link.
    var numberOfCharToConfirmToQuit = 10;


    // Default scroll to bottom or last read message
    var content = $('#scrollable_content')[0];
    var unread = $('.messageUnread');
    if (unread.length) {
        $(content).animate({
            scrollTop: unread.position().top
        }, 450);
    } else {
        $(content).scrollTop(content.scrollHeight);
    }

    // Default focus to textarea
    var textarea = $('textarea#Body');
    textarea.focus();

    // Submit form on CTRL+ENTER
    textarea.keydown(function(event){
        if (event.which == 13 && event.ctrlKey) {
            event.preventDefault();
            $('#tribune_form').submit();
        }
    });

    // Confirmation for the "new messages" link
    var newMessages = $('#newMessages');
    newMessages.on('click', function(e){
        if (textarea.val().trim().length > numberOfCharToConfirmToQuit) {
            if (!window.confirm("Vous avez commencé à écrire. Voulez vous mettre à jour les messages ?\n\n/!\\ Votre message en cours sera effacé ! /!\\")) {
                e.preventDefault();
            }
        }
    });


    // Check regularly if new messages are there
    setInterval(function(){
        var type = $('#tribune_type').val();
        var url = '../ajax/unreadCheckFighterGroup.php?type='+type;
        $.getJSON(url, function( data ) {
            /* Managing unreads */
            if (data.unreads > 0) {
                $(newMessages).removeClass('hidden');
                $('#countMessages').text(data.unreads);
                var currentTrInMenu =  $('.mmenu .tr').filter(function() {
                    return $(this).data('type') == type;
                });
                currentTrInMenu.addClass('trnewmsg');
                if (data.unreads > 1) {
                    newMessages.find('.plural').removeClass('hidden');
                } else {
                    newMessages.find('.plural').addClass('hidden');
                }
            } else {
                $(newMessages).addClass('hidden');
            }

            /* Managing ATB */
            // If ATB is outdated, showing it
            var nextATB = $('.nextatb');
            if (data.isATBOutdated && !nextATB.hasClass('atboutdated')) {
                nextATB.replaceWith(data.ATBInfos);
            } else {
                // Updating the current date
                var hourSpan = $('.nextatb .hour');
                hourSpan.hide(function() {
                    hourSpan.text(data.currentDate);
                    hourSpan.show('slow');
                });
            }
        });
    }, checkNewMessagesTime*1000);

    // Watch for the author's change form
    $('a.changePers').on('click', function(event) {
        event.preventDefault();
        $(this).siblings('form.changePersForm').removeClass('hidden');
        $(this).addClass('hidden');
        $(this).siblings('a.name').addClass('hidden');
        $(this).siblings('a.message').addClass('hidden');
    });
    $('a.cancel').on('click', function(event) {
        event.preventDefault();
        var parent = $(this).parents('td.mainbglabel');
        $(parent).find('form.changePersForm').addClass('hidden');
        $(parent).find('a.changePers').removeClass('hidden');
        $(parent).find('a.name').removeClass('hidden');
        $(parent).find('a.message').removeClass('hidden');
    });


});