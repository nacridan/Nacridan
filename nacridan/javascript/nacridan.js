var currentmenu;
var timeout=0; //ne pas toucher, c'est pour d�clarer la variable
var delay = 300; // en milliseconde
var outmenu;

function isdefined( variable)
{
    return (typeof(window[variable]) == "undefined")?  false: true;
}

function showSubMenu(id) {
   var d = document.getElementById(id);
   if (d) 
   {

      if (d.style.display=='none') 
      {
          d.style.display='block';
          d.style.width='20em';       
      }
      else
      {
         d.style.display='none';
      }
   }
}

function trim(string)
{
return string.replace(/(^\s*)|(\s*$)/g,'');
} 

function undoublecoma(string)
{
return string.replace(/,(,| )*/,',');
}

function updateDiv(id,response){
  document.getElementById(id).innerHTML = response;
}

function setStyleObj(obj, style, value){
  obj.style[style]= value;
}

function setStyle(id, style, value){
	
	var obj=document.getElementById(id);
  obj.style[style]= value;
}


function setStyleCity(style,value){
	var vmap=document.getElementById('map01'); 

	if(vmap.style['visibility']=='visible')
	{
		var obj1=document.getElementById('smallcity01');
  	obj1.style[style]= value;
  	
  	var obj2=document.getElementById('smallcity02');
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
		var obj1=document.getElementById('smallcity01');
  	obj1.style[style]= 'hidden';
  	
  	var obj2=document.getElementById('smallcity02');
  	obj2.style[style]= value;
	}  
}

function setStyleCityCaptured(style,value){
	var vmap=document.getElementById('map01'); 

	if(vmap.style['visibility']=='visible')
	{
		var obj1=document.getElementById('smallcitycaptured01');
		obj1.style[style]= value;
  	
		/* var obj2=document.getElementById('smallcity02');
		obj2.style[style]= 'hidden'; */
	}
	else if(vmap.style['visibility']=='hidden')
	{
		var obj1=document.getElementById('smallcitycaptured01');
		obj1.style[style]= 'hidden';
		
		/* var obj2=document.getElementById('smallcity02');
		obj2.style[style]= value; */
	}  
}

function setStyleLevelMonster(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('levelmonstermap01');
	var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStyleMapTopo(style,value){
	document.getElementById('maptopo').style[style]= value;
}

function setStyleKingdom(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('kingdommap01');
	//var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	//obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	//obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStyleRoad(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('roadmap01');
	//var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	//obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	//obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStylePlayers(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('player01');
	var obj2=document.getElementById('player02');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
	}  
}

function setStyleTeam(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('teammember01');
	var obj2=document.getElementById('teammember02');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
	}  
}

function loadMap(idmap,currmap){
	var obj1=document.getElementById('map01');
	var obj2=document.getElementById('map02');
	var obj3=document.getElementById('maincity');
	var obj4=document.getElementById('redcross');
	
	if(idmap=='map01')
	{
  	obj1.style['visibility']= 'visible';
  	obj2.style['visibility']= 'hidden';
  	obj3.style['visibility']= 'visible';
  	
  	if(currmap==1)
  	{ 		
  		obj4.style['visibility']= 'visible';
  	}
  	else if(currmap==2)
  	{  		
  		obj4.style['visibility']= 'hidden';
  	}
  }
  else if(idmap=='map02')
  {
  	obj1.style['visibility']= 'hidden';
  	obj2.style['visibility']= 'visible';
  	obj3.style['visibility']= 'hidden';
  	
  	if(currmap==1)
  	{  		
  		obj4.style['visibility']= 'hidden';
  	}
  	else if(currmap==2)
  	{  		
  		obj4.style['visibility']= 'visible';
  	}  
  }
  var obj5=document.getElementById('levelmonstermap01');
  obj5.style['visibility']= 'hidden';
  var obj6=document.getElementById('levelmonstermap02');
  obj6.style['visibility']= 'hidden';
  var obj7=document.getElementById('maincity2');
  obj7.style['visibility']= 'hidden';
}



function updateInput(id, value){
  document.getElementById(id).value = value;
}

function loadPage(id,page)
{
   eval("window.parent."+id+".location.href = page");
   showSubMenu();
}

function setInnerHTML(id,value)
{
   document.getElementById(id).innerHTML=value;
}

function setClassName(id, classname) {
   document.getElementById(id).className = classname;
}

function profile(url) { 
     nameWindow = "profile"+Math.round(Math.random()*100000);  
     window.open (url, nameWindow, "height=850px,width=832px,left=0,top=0,scrollbars=yes,titlebar=yes,status=yes,toolbar=yes,resizable=1,menubar=yes,location=yes"); 
}              

function detailedRules(url) { 
     nameWindow = "profile"+Math.round(Math.random()*100000);  
     window.open (url, nameWindow, "height=750px,width=930px,left=0,top=0,scrollbars=yes,titlebar=yes,status=yes,toolbar=yes,resizable=1,menubar=yes,location=yes"); 
}

function openOpener(url) 
{ 
   window.opener.location=url;
   return false;
}

var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

function invertCheckboxes(formname)
{
  var elts = document.getElementsByName('check[]');

 
  if(elts != null)
  {
    var nb  =  elts.length;                 
    if (nb) {
      for (var i = 0; i < nb; i++) {
	elts[i].checked = !elts[i].checked;
      } // end for
    }
    else
      {
        elts.checked = !elts.checked;
      }
  }
  return true;
}

// AJAX

var tooltype;

function checkAlliance(name,destid)
{ 
  var param;
  param="name="+name;
  param+="&destid="+destid;
  var response=RPCPost("../ajax/team.php",param);
  updateDiv('ajaxResp',response);
  if(response.length>0)
    setStyle('ajaxResp', 'visibility', 'visible');
  else
    setStyle('ajaxResp', 'visibility', 'hidden');
  return true;
}

function checkAlliancePJ(name,destid)
{ 
  var param;
  param="name="+name;
  param+="&destid="+destid;
  var response=RPCPost("../ajax/pj.php",param);
  updateDiv('ajaxResp',response);
  if(response.length>0)
    setStyle('ajaxResp', 'visibility', 'visible');
  else
    setStyle('ajaxResp', 'visibility', 'hidden');
  return true;
}

function checkAllegiance(name,destid)
{
 checkAlliance(name,destid);	 
}


function submitTheForm(formid,hiddenid,val)
{
  obj=document.getElementById(hiddenid);
  obj.value=val;
  obj=document.getElementById(formid);
  obj.submit();
}

var detect = navigator.userAgent.toLowerCase();
var browser;
function checkIt(string)
{
	place = detect.indexOf(string) + 1;
	thestring = string;
	return place;
}

if (checkIt('konqueror'))
{
  browser = "Konqueror"
}
else if (checkIt('safari')) browser = "Safari";
else if (checkIt('omniweb')) browser = "OmniWeb";
else if (checkIt('opera')) browser = "Opera";
else if (checkIt('webtv')) browser = "WebTV";
else if (checkIt('msie')) browser = "Internet Explorer";
else if (checkIt('icab')) browser = "iCab";
else if (!checkIt('compatible'))
{
	browser = "Netscape Navigator";
	version = detect.charAt(8);
}
else browser = "An unknown browser";



var addLimit=10;
var nbadd=0;
    
function addDest(name,dest,msg)
{
	if (dest)
	{
		index=dest.selectedIndex;
		if(dest.value>0)
		{
			curDest=document.getElementById(name);
			curDest.value=trim(curDest.value);    
			value= dest.options[index].text;
			if ((pos = curDest.value.indexOf(value,0)) < 0)
			{
				if(nbadd<addLimit)
				{
					if(curDest.value.length!=0 && curDest.value.charAt(curDest.value.length)!=',')
   						curDest.value+=',';
 					curDest.value=curDest.value+value;
 					curDest.value=undoublecoma(curDest.value);
 					nbadd=nbadd+1;
				}
				else
				{
 					alert(msg);
				}
			}
		}
		dest.selectedIndex=0;
 	}
}


$( document ).ready(function() {

    /**
     * Links to profile which are going to be open in a popup
     */
    $('a.popupify').on('click', function(event){
        event.preventDefault();
        profile($(this).attr('href'));
    });

    /**
     * Links from a popup which are going to be open in the opener's window if it has one.
     */
    $('a.toOpener').on('click', function(event){
        if (window.opener != null) {
            event.preventDefault();
            openOpener($(this).attr('href'));
        }
    });

    /**
     * Autocomplete for Players name
     */
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
    var accentMap = {"Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"};
    var normalize = function( term ) {
        var ret = "";
        for ( var i = 0; i < term.length; i++ ) {
            ret += accentMap[ term.charAt(i) ] || term.charAt(i);
        }
        return ret;
    };


    $('.players-autocomplete').autocomplete({
        minLength: 0,
        source: function(request, response) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
            response( $.grep($PLAYERS_LIST, function( value ) {
                value = value.label || value.value || value;
                return matcher.test( value ) || matcher.test( normalize( value ) );
            }) );
        }

    });

    $('.players-autocomplete-multiple').autocomplete({
        minLength: 0,
        source: function(request, response) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex( extractLast( request.term ) ), "i" );
            response( $.grep($PLAYERS_LIST, function( value ) {
                value = value.label || value.value || value;
                return matcher.test( value ) || matcher.test( normalize( value ) );
            }) );

        },
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push( ui.item.value );
            // add placeholder to get the comma-and-space at the end
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
});
