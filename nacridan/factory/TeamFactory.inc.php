 <?php
require_once (HOMEPATH . "/class/Team.inc.php");

class TeamFactory
{

    static function createTeam($playerSrc, $name, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        
        if ($playerSrc->get("id_Team") > 0) {
            $err = localize("Tant que vous ferez partie de l'Ordre : <br/> <b>{name}</b> <br/> vous ne pourrez ni créer ni rejoindre un autre Ordre.", 
                array(
                    "name" => $playerSrc->getSub("Team", "name")
                ));
            return CREATE_TEAM_NAME_ERROR;
        }
        
        if ($playerSrc->get("ap") < CREATE_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points D'action PA.");
            return CREATE_TEAM_NOT_ENOUGH_AP;
        }
        
        if ($name == "") {
            $err = localize("Erreur : Ce nom n'est pas valide.");
            return CREATE_TEAM_NAME_ERROR;
        }
        
        $team = new Team();
        $datetime = time();
        $team->set("name", $name);
        $team->set("id_Player", $playerSrc->get("id"));
        $team->set("date", gmdate("Y-m-d H:i:s", $datetime));
        $idteam = $team->addDB($db);
        $team->set("id_TeamBody", $idteam);
        $team->updateDB($db);
        if ($team->errorNoDB()) {
            if ($team->errorNoDB() == 1062) {
                $err = localize("Erreur : Ce nom est déjà utilisé.");
            } else {
                $err = localize("Erreur : Impossible d'ajouter un nouvel Ordre.");
            }
            
            return CREATE_TEAM_NAME_ERROR;
        }
        
        $i = 1;
        $dbd = new DBCollection(
            "INSERT INTO TeamRankInfo (id_Team,name,num,auth1,auth2,auth3,auth4) VALUES (" . $idteam . ",'" . localize("Fondateur") . "'," . $i . ",'Yes','Yes','Yes','Yes')", $db, 
            0, 0, false);
        for ($i = 2; $i < 20; $i ++) {
            $dbd = new DBCollection("INSERT INTO TeamRankInfo (id_Team,name,num) VALUES (" . $idteam . ",'" . addslashes(localize("Grade")) . $i . "'," . $i . ")", $db, 0, 0, false);
        }
        $dbd = new DBCollection("INSERT INTO TeamRankInfo (id_Team,name,num) VALUES (" . $idteam . ",'" . addslashes(localize("A l'essai")) . "',20)", $db, 0, 0, false);
        
        $dbd = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
        
        $dbs = new DBCollection("SELECT id FROM TeamRankInfo WHERE id_Team=" . $idteam . " AND num=1", $db);
        $dbd = new DBCollection(
            "INSERT INTO TeamRank (id_Player,id_TeamRankInfo,id_Team,date) VALUES (" . $playerSrc->get("id") . "," . $dbs->get("id") . "," . $idteam . ",'" .
                 gmdate("Y-m-d H:i:s", $datetime) . "')", $db, 0, 0, false);
        $dbd = new DBCollection("INSERT INTO TeamBody (id,id_Team) VALUES (" . $idteam . "," . $idteam . ")", $db, 0, 0, false);
        
        $playerSrc->set("id_Team", $idteam);
        $playerSrc->set("ap", $playerSrc->get("ap") - CREATE_TEAM_AP);
        $playerSrc->setSub("Team", "id_TeamBody", $idteam);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $err = localize("Vous voilà maintenant à la tête d'un nouvel Ordre.");
        return CREATE_TEAM_NO_ERROR;
    }

    static function askJoinTeam($playerSrc, $name, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        $previous = "";
        if ($playerSrc->get("ap") < JOIN_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return JOIN_TEAM_NOT_ENOUGH_AP;
        }
        
        $dbt = new DBCollection("SELECT id,name from Team WHERE name='" . addslashes($name) . "'", $db);
        
        if (! $dbt->eof()) {
            $dbs = new DBCollection("SELECT id FROM TeamAskJoin WHERE id_Player=" . $playerSrc->get("id"), $db);
            $datetime = time();
            if (! $dbs->eof()) {
                $dbu = new DBCollection("UPDATE TeamAskJoin SET id_Team=" . $dbt->get("id") . ", date='" . gmdate("Y-m-d H:i:s", $datetime) . "' WHERE id=" . $dbs->get("id"), $db, 
                    0, 0, false);
                $previous = "<br/>" . localize("(Votre précédente demande concernant un autre Ordre a été par conséquent annulée).");
            } else {
                $dbi = new DBCollection(
                    "INSERT INTO TeamAskJoin (id_Player,id_Team,date) VALUES (" . $playerSrc->get("id") . "," . $dbt->get("id") . ",'" . gmdate("Y-m-d H:i:s", $datetime) . "')", 
                    $db, 0, 0, false);
            }
        } else {
            $err = localize("Erreur : Le nom de cet Ordre n'existe pas.");
            return JOIN_TEAM_NAME_ERROR;
        }
        
        $err = localize("Votre demande a été soumise aux fondateurs de l'Ordre.") . $previous;
        
        $playerSrc->set("ap", $playerSrc->get("ap") - JOIN_TEAM_AP);
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return JOIN_TEAM_NO_ERROR;
    }

    static function modifyDesc($playerSrc, $newbody, &$err, $db)
    {
        $err = "";
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if ($dbt->get("id_Player") == $playerSrc->get("id")) {
            $team = $playerSrc->getObj("Team");
            $dbu = new DBCollection("UPDATE TeamBody SET body=\"" . $newbody . "\" WHERE id='" . $team->get("id_TeamBody") . "'", $db, 0, 0, false);
            $err = localize("Les Modifications ont été réalisées avec succès.");
        } else {
            $err = localize("Erreur : Vous n'êtes pas le fondateur de cet Ordre, vous ne pouvez pas changer les Rangs.");
        }
    }

    static function addAlliance($playerSrc, $name, $type, &$err, $db, $tablename, $field1, $field2, $tablename2, $typeid)
    {
        $err = "";
        $typealliance = "";
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if (($dbt->count() > 0 && $dbt->get("id_Player") == $playerSrc->get("id")) || $typeid = "Player") {
            
            $dbt = new DBCollection("SELECT id,name from " . $tablename2 . " WHERE name='" . addslashes($name) . "'", $db);
            
            if (! $dbt->eof()) {
                switch ($type) {
                    case 1:
                        $typealliance = "Allié";
                        break;
                    case 0:
                        $typealliance = "Ennemi";
                        break;
                }
                if ($typealliance != "") {
                    if ($playerSrc->get("id_" . $typeid) != $dbt->get("id")) {
                        if ($typeid == "Team")
                            $id = $playerSrc->get("id_Team");
                        else
                            $id = $playerSrc->get("id");
                        
                        $dbCheck = new DBCollection("select * from " . $tablename . " where " . $field1 . " = " . $id . " and  " . $field2 . " = " . $dbt->get("id"), $db);
                        if ($dbCheck->count() > 0) {
                            $err = localize("Erreur : Vérifiez que cet ordre/personnage ne fasse pas déjà partie de vos alliances (Allié/Ennemi).");
                        } else {
                            $dbi = new DBCollection(
                                "INSERT INTO " . $tablename . " (" . $field1 . ", " . $field2 . ", type, datestart) VALUES (" . $id . "," . $dbt->get("id") . ",'" . $typealliance .
                                     "','" . gmdate("Y-m-d H:i:s") . "')", $db, 0, 0, false);
                            
                            $err = localize("Les Modifications ont été réalisées avec succès.");
                        }
                    } else {
                        $err = localize("Erreur: Vous ne pouvez pas définir d'alliance sur votre propre alliance ou vous-même.");
                    }
                } else {
                    $err = localize("Erreur : Le type d'alliance est incorrect.");
                }
            } else {
                $err = localize("Erreur : Cet Ordre n'existe pas.");
            }
        } else {
            $err = localize("Erreur : Vous n'êtes pas le fondateur de cet Ordre, vous ne pouvez pas changer les Rangs.");
        }
    }

    static function addTeamAlliance($playerSrc, $name, $type, &$err, $mode, $db)
    {
        if ($mode == "Team") {
            self::addAlliance($playerSrc, $name, $type, $err, $db, "TeamAlliance", "id_Team\$src", "id_Team\$dest", "Team", "Team");
        } else {
            self::addAlliance($playerSrc, $name, $type, $err, $db, "TeamAlliancePJ", "id_Team\$src", "id_Player", "Player", "Team");
        }
    }

    static function addPlayerAlliance($playerSrc, $name, $type, &$err, $mode, $db)
    {
        if ($mode == "Team") {
            self::addAlliance($playerSrc, $name, $type, $err, $db, "PJAlliance", "id_Player\$src", "id_Team", "Team", "Player");
        } else {
            self::addAlliance($playerSrc, $name, $type, $err, $db, "PJAlliancePJ", "id_Player\$src", "id_Player\$dest", "Player", "Player");
        }
    }

    static function removeAllianceTeamAuth($playerSrc, $name, &$err, $db, $tablename)
    {
        $err = "";
        $typealliance = "";
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if ($dbt->get("id_Player") == $playerSrc->get("id")) {
            foreach ($name as $key => $val) {
                $dbi = new DBCollection("DELETE FROM " . $tablename . " WHERE id=" . $val, $db, 0, 0, false);
            }
            $err = localize("Les Modifications ont été réalisées avec succès.");
        } else {
            $err = localize("Erreur : Vous n'êtes pas le fondateur de cet Ordre, vous ne pouvez pas effectuer cette opération.");
        }
    }

    static function removeAlliancePJAuth($playerSrc, $name, &$err, $db, $tablename)
    {
        $err = "";
        
        foreach ($name as $key => $val) {
            $dbi = new DBCollection("DELETE FROM " . $tablename . " WHERE id_Player\$src=" . $playerSrc->get("id") . " AND id=" . $val, $db, 0, 0, false);
        }
        $err = localize("Les Modifications ont été réalisées avec succès.");
    }

    static function removeTeamAlliance($playerSrc, $name, &$err, $mode, $db)
    {
        if ($mode == "Team") {
            self::removeAllianceTeamAuth($playerSrc, $name, $err, $db, "TeamAlliance");
        } else {
            self::removeAllianceTeamAuth($playerSrc, $name, $err, $db, "TeamAlliancePJ");
        }
    }

    static function removePlayerAlliance($playerSrc, $name, &$err, $mode, $db)
    {
        if ($mode == "Team") {
            self::removeAlliancePJAuth($playerSrc, $name, $err, $db, "PJAlliance");
        } else {
            self::removeAlliancePJAuth($playerSrc, $name, $err, $db, "PJAlliancePJ");
        }
    }

    static protected function getCondFromArray($arr, $fieldname, $cond)
    {
        $msg = "";
        $first = 1;
        
        foreach ($arr as $key => $val) {
            if ($first)
                $msg = $fieldname . "=" . $val;
            else
                $msg .= " " . $cond . " " . $fieldname . "=" . $val;
            $first = 0;
        }
        return $msg;
    }

    static function changeTeamGrade($playerSrc, $array, $rank, $check, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < MODIFY_RANK_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return;
        }
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if ($dbt->get("id_Player") == $playerSrc->get("id")) {
            for ($i = 1; $i <= 20; $i ++) {
                $namerank = $rank . $i;
                $nameauth = $check . $i;
                
                if (isset($array[$namerank])) {
                    $name = $array[$namerank];
                    
                    for ($j = 1; $j <= 7; $j ++) {
                        $authval[$j] = "\"No\"";
                    }
                    
                    if (isset($array[$nameauth])) {
                        foreach ($array[$nameauth] as $val) {
                            $authval[$val] = "\"Yes\"";
                        }
                    }
                    
                    $dbu = new DBCollection(
                        "UPDATE TeamRankInfo SET name=\"" . $name . "\",auth1=" . $authval[1] . ",auth2=" . $authval[2] . ",auth3=" . $authval[3] . ",auth4=" . $authval[4] .
                             ",auth5=" . $authval[5] . ",auth6=" . $authval[6] . ",auth7=" . $authval[7] . " WHERE num=" . $i . " AND id_Team=" . $dbt->get("id"), $db, 0, 0, false);
                }
            }
            
            $err = localize("Les Modifications ont été réalisées avec succès.");
        } else {
            $err = localize("Erreur : Vous n'êtes pas le fondateur de cet Ordre, vous ne pouvez pas changer les Rangs.");
        }
    }

    static function changeMemberGrade($playerSrc, $playeridarray, $gradeidarray, $prefix, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < MODIFY_RANK_MEMBER_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return;
        }
        
        $dbrank = new DBCollection(
            "SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" .
                 $playerSrc->get("id"), $db);
        
        if (! $dbrank->eof()) {
            if ($dbrank->get("auth4") == "Yes" || $playerSrc->getSub("Team", "id_Player") == $playerSrc->get("id")) {
                foreach ($playeridarray as $key => $val) {
                    $dbu = new DBCollection(
                        "UPDATE TeamRank SET id_TeamRankInfo=" . $gradeidarray[$prefix . $val] . " WHERE id_Team=" . $playerSrc->get("id_Team") . " AND id_Player=" . $val, $db, 0, 
                        0, false);
                }
                
                $err = localize("Les Modifications ont été réalisées avec succès.");
            } else {
                $err = localize("Erreur : Vous n'avez pas les droits nécessaires.");
            }
        } else {
            $err = localize("Erreur : Vous ne faites pas partie de cet Ordre.");
        }
    }

    static function banTeamMember($playerSrc, $playeridarray, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < MODIFY_RANK_MEMBER_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return;
        }
        
        $dbrank = new DBCollection(
            "SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" .
                 $playerSrc->get("id"), $db);
        
        if (! $dbrank->eof()) {
            if ($dbrank->get("auth3") == "Yes" || $playerSrc->getSub("Team", "id_Player") == $playerSrc->get("id")) {
                foreach ($playeridarray as $key => $val) {
                    $dbu = new DBCollection("DELETE FROM TeamRank WHERE id_Player=" . $val . " AND id_Team=" . $playerSrc->get("id_Team"), $db, 0, 0, false);
                    $dbu = new DBCollection("UPDATE Player SET id_Team=0 WHERE id=" . $val . " AND id_Team=" . $playerSrc->get("id_Team"), $db, 0, 0, false);
                }
                // require_once("../class/MailFactory.inc.php");
                // MailFactory::sendSingleMsgById($playerSrc,localize("Information"),localize("Vous avez été bani de votre ordre."),$playeridarray,$err2,$db,-1,0);
                $err = localize("Les Modifications ont été réalisées avec succès.");
            } else {
                $err = localize("Erreur : Vous n'avez pas les droits nécessaires.");
            }
        } else {
            $err = localize("Erreur : Vous ne faites pas partie de cet Ordre.");
        }
    }

    static function validateJoinTeam($playerSrc, $arr, &$err, $db, $autoUpdate = 1)
    {
        if (count($arr) == 0) {
            $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
            return;
        }
        
        $dbrank = new DBCollection(
            "SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" .
                 $playerSrc->get("id"), $db);
        
        if (! $dbrank->eof()) {
            if ($dbrank->get("auth1") == "Yes" || $playerSrc->getSub("Team", "id_Player") == $playerSrc->get("id")) {
                $cond = self::getCondFromArray($arr, "Player.id", "OR");
                
                $dbs = new DBCollection(
                    "SELECT Player.id,Player.name FROM Player LEFT JOIN TeamAskJoin ON Player.id=id_Player WHERE TeamAskJoin.id_Team=" . $dbrank->get("id_Team") . " AND " . $cond, 
                    $db);
                $dbr = new DBCollection("SELECT id FROM TeamRankInfo WHERE id_Team=" . $dbrank->get("id_Team") . " AND num=20", $db);
                $datetime = time();
                $dest = array();
                $destid = array();
                while (! $dbs->eof()) {
                    $dbd = new DBCollection(
                        "INSERT INTO TeamRank (id_Player,id_TeamRankInfo,id_Team,date) VALUES (" . $dbs->get("id") . "," . $dbr->get("id") . "," . $dbrank->get("id_Team") . ",'" .
                             gmdate("Y-m-d H:i:s", $datetime) . "')", $db, 0, 0, false);
                    $dest[$dbs->get("name")] = $dbs->get("name");
                    $destid[$dbs->get("id")] = $dbs->get("id");
                    $dbs->next();
                }
                $cond = self::getCondFromArray($destid, "Player.id", "OR");
                
                if ($cond != "") {
                    $dbd = new DBCollection(
                        "DELETE TeamAskJoin FROM Player LEFT JOIN TeamAskJoin ON Player.id=id_Player WHERE TeamAskJoin.id_Team=" . $dbrank->get("id_Team") . " AND " . $cond, $db, 0, 
                        0, false);
                    $dbd = new DBCollection("UPDATE Player SET id_Team=" . $dbrank->get("id_Team") . " WHERE " . $cond, $db, 0, 0, false);
                    
                    require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                    MailFactory::sendSingleDestMsg($playerSrc, localize("Demande d'Allégeance"), localize("Votre demande d'Allégeance a été acceptée."), $dest, 2, $err2, $db, - 1, 
                        0);
                    
                    $err = localize("Les nouvelles recrues seront prévenues par un message.");
                } else {
                    $err = localize("Erreur : configuration non possible !!!.");
                }
            } else {
                $err = localize("Erreur : Vous n'avez pas les droits nécessaires.");
            }
        } else {
            $err = localize("Erreur : Vous ne faites pas partie de cet Ordre.");
        }
    }

    static function refuseJoinTeam($playerSrc, $arr, &$err, $db, $autoUpdate = 1)
    {
        if (count($arr) == 0) {
            $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
            return;
        }
        
        $dbrank = new DBCollection(
            "SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" .
                 $playerSrc->get("id"), $db);
        
        if (! $dbrank->eof()) {
            if ($dbrank->get("auth2") == "Yes" || $playerSrc->getSub("Team", "id_Player") == $playerSrc->get("id")) {
                $cond = self::getCondFromArray($arr, "Player.id", "OR");
                
                $dbs = new DBCollection(
                    "SELECT Player.id,Player.name FROM Player LEFT JOIN TeamAskJoin ON Player.id=id_Player WHERE TeamAskJoin.id_Team=" . $dbrank->get("id_Team") . " AND " . $cond, 
                    $db);
                $datetime = time();
                $dest = array();
                $destid = array();
                while (! $dbs->eof()) {
                    $dest[$dbs->get("name")] = $dbs->get("name");
                    $destid[$dbs->get("id")] = $dbs->get("id");
                    $dbs->next();
                }
                $cond = self::getCondFromArray($destid, "Player.id", "OR");
                
                if ($cond != "") {
                    $dbd = new DBCollection(
                        "DELETE TeamAskJoin FROM Player LEFT JOIN TeamAskJoin ON Player.id=id_Player WHERE TeamAskJoin.id_Team=" . $dbrank->get("id_Team") . " AND " . $cond, $db, 0, 
                        0, false);
                    
                    require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                    MailFactory::sendSingleDestMsg($playerSrc, localize("Demande d'Allégeance"), localize("Votre demande d'Allégeance a été rejetée."), $dest, 2, $err2, $db, - 1, 
                        0);
                    
                    $err = localize("Les personnes refusées seront prévenues par un message.");
                }
            } else {
                $err = localize("Erreur : Vous n'avez pas les droits nécessaires.");
            }
        } else {
            $err = localize("Erreur : Vous ne faites pas partie de cet Ordre.");
        }
    }

    static function disavowTeam($playerSrc, $name, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < DISAVOW_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return DISAVOW_TEAM_NOT_ENOUGH_AP;
        }
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if ($dbt->get("id_Player") != $playerSrc->get("id")) {
            if (! $dbt->eof()) {
                $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Player=" . $playerSrc->get("id") . " AND id_Team=" . $playerSrc->get("id_Team") . "", $db, 0, 0, false);
                $playerSrc->set("id_Team", 0);
                $playerSrc->set("ap", $playerSrc->get("ap") - DISAVOW_TEAM_AP);
                if ($autoUpdate)
                    $playerSrc->updateDB($db);
            } else {
                $playerSrc->set("id_Team", 0);
                $playerSrc->set("ap", $playerSrc->get("ap") - DISAVOW_TEAM_AP);
                if ($autoUpdate)
                    $playerSrc->updateDB($db);
                
                $err = localize("Vous ne faites plus partie d'aucun ordre désormais.");
                return DISAVOW_TEAM_NAME_ERROR;
            }
            $err = localize("Vous ne faites plus partie d'aucun ordre désormais.");
            return DISAVOW_TEAM_NO_ERROR;
        } else {
            $err = localize("Vous êtes le fondateur de cet Ordre, vous ne pouvez que le dissoudre.");
            return DISAVOW_TEAM_NO_ERROR;
        }
    }

    static function dissolTeam($playerSrc, $name, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < DISAVOW_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return DISAVOW_TEAM_NOT_ENOUGH_AP;
        }
        
        $dbt = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $playerSrc->get("id_Team") . "", $db);
        
        if ($dbt->get("id_Player") == $playerSrc->get("id")) {
            if (! $dbt->eof()) {
                $playerSrc->set("id_Team", 0);
                $playerSrc->set("ap", $playerSrc->get("ap") - DISAVOW_TEAM_AP);
                
                $dbu = new DBCollection("UPDATE Player SET id_Team=0 WHERE id_Team=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Team=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamRankInfo WHERE id_Team=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Team=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamBody WHERE id_Team=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$dest=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$src=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamAlliancePJ WHERE id_Team\$src=" . $dbt->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM Team WHERE id=" . $dbt->get("id") . "", $db, 0, 0, false);
                
                if ($autoUpdate)
                    $playerSrc->updateDB($db);
            } else {
                $err = localize("Erreur : Le nom de cet Ordre n'existe pas.");
                return DISAVOW_TEAM_NAME_ERROR;
            }
            $err = localize("Vous ne faites plus partie d'aucun ordre désormais.");
            return DISAVOW_TEAM_NO_ERROR;
        } else {
            $err = localize("Vous n'êtes pas le fondateur de cet Ordre, vous ne pouvez pas le dissoudre.");
            return DISAVOW_TEAM_NO_ERROR;
        }
    }

    static function transfertTeam($playerSrc, $name, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        if ($playerSrc->get("ap") < TRANSFERT_TEAM_AP) {
            $err = localize("Erreur : Vous n'avez pas assez de Points d'Action PA.");
            return TRANSFERT_TEAM_NOT_ENOUGH_AP;
        }
        
        $team = $playerSrc->getObj("Team");
        
        if ($team->get("id_Player") == $playerSrc->get("id")) {
            $dbt = new DBCollection("SELECT id,id_Team from Player WHERE name='" . addslashes($name) . "'", $db);
            if (! $dbt->eof()) {
                if ($dbt->get("id_Team") == $team->get("id")) {
                    $err = localize($name . " est maintenant le fondateur de l'Ordre : " . $team->get("name"));
                    $dbu = new DBCollection("UPDATE Team SET id_Player='" . $dbt->get("id") . "' WHERE id='" . $team->get("id") . "'", $db, 0, 0, false);
                    return TRANSFERT_TEAM_NO_ERROR;
                } else {
                    $err = localize("Ce personnage ne fait pas partie de votre Ordre.");
                    return TRANSFERT_TEAM_ERROR;
                }
            } else {
                $err = localize("Ce personnage n'existe pas.");
                return TRANSFERT_TEAM_ERROR;
            }
        } else {
            $err = localize("Vous n'êtes le fondateur de cet Ordre, vous n'avez pas ce pouvoir.");
            return TRANSFERT_TEAM_ERROR;
        }
    }
}

?> }
  }
}

?>