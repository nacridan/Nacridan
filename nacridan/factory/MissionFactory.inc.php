<?php

/**
 *  
 * Définit la class MissionFactory, qui s'occupe de la création des Missions.
 * 
 *
 *@author Nacridan
 *@version 1.0
 *@package NacridanV1
 *@subpackage factory
 */

// ***************************************//
// TODO : Tout casser, et tout refaire.
// ***************************************//
require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
require_once (HOMEPATH . "/class/Mission.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");

class MissionFactory
{

    static function getRndParchment($level, $x, $y, $map, $player, &$db)
    {
        $equip = new Equipment();
        $vector = new VectorProba();
        $dbe = new DBCollection("SELECT * FROM BasicMission WHERE id=1", $db); // options à enlever WHERE id=1
        while (! $dbe->eof()) {
            $vector->pushBack($dbe->get("id"), $dbe->get("frequency"));
            $dbe->next();
        }
        
        $vector->setInterval(PREC);
        $index = $vector->searchUpperOrEqual(mt_rand(1, PREC));
        if ($index >= 0) {
            $ret = $vector->getKey($index);
            $dbe->first();
            while ($dbe->get("id") != $ret && ! $dbe->eof()) {
                $dbe->next();
            }
            
            // require_once(HOMEPATH."/factory/MissionFactory.inc.php");
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($db);
            
            // $ret=MissionFactory::genRndEscortParchment($equip,$level,$x,$y,$db);
            // $ret=MissionFactory::genRndTreasureParchment($equip,$level,$x,$y,$db);
            
            switch ($dbe->get("id")) {
                case 1:
                    $ret = MissionFactory::genRndEscortParchment($mapinfo, $equip, max(floor($level / 2), 1), $player, $x, $y, $map, $db);
                    break;
                case 2:
                    $ret = MissionFactory::genRndReleaseParchment($equip, $x, $y, $map, $db);
                    if ($ret == false)
                        return null;
                    break;
                case 4:
                    $ret = MissionFactory::genRndTreasureParchment($mapinfo, $equip, $level, $x, $y, $map, $db);
                    if ($ret == false)
                        return null;
                    break;
                default:
                    return null;
                    break;
            }
        } else {
            echo "getRndParchment ERROR";
            exit(- 1);
        }
        return $equip;
    }

    static function genRndEscortParchment(&$mapinfo, &$equip, $level, $player, $x, $y, $map, &$db)
    {
        $nb = 1; // mt_rand(1,1);
        
        $validzone = $mapinfo->getValidMap($x, $y, 20, $map);
        do {
            $content["xstart"] = abs(mt_rand(- 10, 10) + $x);
            $content["ystart"] = abs(mt_rand(- 10, 10) + $y);
        } while (! $validzone[$content["xstart"] + 10 - $x][$content["ystart"] + 10 - $y]);
        
        $validzone = $mapinfo->getValidMap($content["xstart"], $content["ystart"], 40, $map);
        
        do {
            $content["xstop"] = abs(mt_rand(- 20, 20) + $content["xstart"]);
            $content["ystop"] = abs(mt_rand(- 20, 20) + $content["ystart"]);
            /*
             * if($content["xstart"]==$content["xstop"] && $content["ystart"]==$content["ystop"])
             * {
             * $content["xstop"]+=1;
             * }
             */
        } while (! $validzone[$content["xstop"] + 20 - $content["xstart"]][$content["ystop"] + 20 - $content["ystart"]]);
        
        if (mt_rand(0, 1) == 0) {
            $content["postart"] = $level * 10 * $nb * floor((mt_rand(100, 200) / 100));
        } else {
            $content["pxstart"] = $level * 1 * $nb * floor((mt_rand(100, 200) / 100));
        }
        
        if (mt_rand(0, 1) == 0) {
            $content["postop"] = $level * 25 * $nb * floor((mt_rand(100, 300) / 100));
        } else {
            $content["pxstop"] = $level * 3 * $nb * floor((mt_rand(100, 300) / 100));
        }
        return MissionFactory::genEscortParchment($equip, $content, $level, $player, $nb, $db);
    }

    static function genEscortParchment(&$equip, $content, $level, $player, $nb, &$db)
    {
        $mission = new Mission();
        $condition = new MissionCondition();
        $reward = new MissionReward();
        $mission->set("id_BasicMission", 1);
        $mission->set("name", "Mission : Escorter");
        
        $curplayer = new Player();
        $curplayer->load($player, $db);
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        // Modif concernant l'ajout des descriptions d'escorte
        $dbcnt = new DBCollection("SELECT id FROM EscortMissionRolePlay", $db);
        $idmax = $dbcnt->count();
        $iddesc = rand(1, $idmax);
        $mission->set("id_EscortMissionRolePlay", $iddesc);
        
        $dbdesc = new DBCollection("SELECT description FROM EscortMissionRolePlay WHERE id=" . $iddesc, $db);
        
        $tempcontent = $dbdesc->get("description");
        
        $condition->set("positionXstart", $xp);
        $condition->set("positionYstart", $yp);
        
        $distmin = 21;
        $distmax = 55;
        $map = 1;
        
        // les limites 590, 230 sont là pour éviter l'île de l'arène
        $dbend = new DBCollection("SELECT id,id_City, x, y FROM Building WHERE 
        id!=" . $curplayer->get("inbuilding") . " 
        AND id_BasicBuilding=1 
        AND map=" . $map . "
        AND (((".$xp." < 590) AND (".$yp." > 230 )) OR (".$yp." < 230 )) 
        AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 >=" . $distmin . " 
        AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $distmax, $db, 0, 0);
        
        if ($dbend->count() == 0) {
            $stop = array(
                "x" => 252,
                "y" => 301
            );
            $tempcontent = "Pas de chance, finalement vous allez à Earok.";
        } else {
            $posarray = array();
            while (! $dbend->eof()) {
                $posarray[] = array(
                    "id" => $dbend->get('id'),
                    "x" => $dbend->get('x'),
                    "y" => $dbend->get('y')
                );
                $dbend->next();
            }
            
            $stop = $posarray[array_rand($posarray)];
        }
        
        // $mission->addCond($content["xstop"],$content["ystop"],$str);
        $condition->set("positionXstop", $stop["x"]);
        $condition->set("positionYstop", $stop["y"]);
        // $condition->set("EscortPositionX",$content["xstop"]);
        // $condition->set("EscortPositionY",$content["ystop"]);
        
        $dist = distHexa($xp, $yp, $stop["x"], $stop["y"]);
        $salary = floor(ESCORT_SALARY * $dist / 12);
        
        $reward->set("moneyStop", $salary);
        
        $mission->set("Mission_level", 2);
        $mission->set("Player_levelMin", 1);
        $mission->set("Player_levelMax", 100);
        
        $mission->set("content", $tempcontent);
        $mission->addDBr($db);
        
        $parchemin = new Equipment();
        // $parchemin->setObj("Mission",$mission,true);
        $idm = $mission->get("id");
        $parchemin->set("id_Player", $player);
        $parchemin->set("id_Mission", $idm);
        $parchemin->set("name", "Parchemin");
        $parchemin->set("id_BasicEquipment", 600);
        $parchemin->set("id_EquipmentType", 45);
        $parchemin->set("date", gmdate("Y-m-d H:i:s"));
        $parchemin->addDBr($db);
        
        // $idparchemin=new DBCollection("SELECT id FROM Equipment WHERE id_Mission !=0 AND id_Player=".$player."",$db);
        
        $condition->set("idMission", $idm);
        $reward->set("idMission", $idm);
        
        $condition->updateDB($db);
        $reward->updateDB($db);
        
        return true;
    }

    protected static function chooseReleaseCity($x, $y, $map, &$xc, &$yc, &$db)
    {
        $rnd = mt_rand(1, 1);
        $dbs = new DBCollection("SELECT id,ABS(x-" . $x . ")+ABS(y-" . $y . ") as dist,x,y FROM `City` WHERE map=" . $map . " AND captured=2 HAVING dist>0 ORDER by dist ASC", $db, $rnd, 1);
        
        if (! $dbs->eof()) {
            $xc = $dbs->get("x");
            $yc = $dbs->get("y");
            return $dbs->get("id");
        } else {
            return null;
        }
    }

    static function genRndReleaseParchment(&$equip, $x, $y, $map, &$db)
    {
        $cityId = MissionFactory::chooseReleaseCity($x, $y, $map, $xc, $yc, $db);
        if ($cityId != null) {
            return MissionFactory::genReleaseParchment($equip, $cityId, $map, $xc, $yc, $db);
        } else {
            return false;
        }
    }

    static function genReleaseParchment(&$equip, $id, $map, $x, $y, &$db)
    {
        $mission = new Mission();
        $mission->set("name", "Mission : Libérer une Ville");
        $mission->set("id_BasicMission", 2);
        $dbnpc = new DBCollection("SELECT id,level FROM Player WHERE incity=0 AND hidden=0 AND disabled=0 AND status='NPC' AND map=" . $map . " AND x=" . $x . " AND y=" . $y, $db, 0, 0);
        $postop = 0;
        $pxstop = 0;
        while (! $dbnpc->eof()) {
            $postop += (($dbnpc->get("level") * 2) + mt_rand(1, $dbnpc->get("level"))) * 4;
            $pxstop += $dbnpc->get("level") + floor(mt_rand(1, $dbnpc->get("level")) / 2);
            $dbnpc->next();
        }
        if ($postop == 0)
            return false;
        if ($pxstop == 0)
            return false;
        
        $mission->addAction($x, $y, 1, $postop);
        $mission->addAction($x, $y, 2, $pxstop);
        
        $str = "\$dbnpc=new DBCollection(\"SELECT id FROM Player WHERE incity=0 AND hidden=0 AND disabled=0 AND status='NPC' AND map=\".\$p1->get('map').\" AND x=\".\$p1->get('x').\" AND y=\".\$p1->get('y'),\$db,0,0,0);if(\$dbnpc->count()==0){\$dbc=new DBCollection(\"SELECT id,captured FROM City WHERE id=" . $id .
             "\",\$db,0,0,0);if(\$dbc->get(\"captured\"!=0)){\$success='true';\$dbc=new DBCollection(\"UPDATE City SET captured=0 WHERE id=" . $id . "\",\$db,0,0,0);}else\$success='null';}else\$success='false';";
        
        $mission->addCond($x, $y, $str);
        $mission->set("content", serialize($mission));
        $equip->setObj("Mission", $mission, true);
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 30);
        $equip->set("id_EquipmentType", 9);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        return true;
    }

    static function genRndTreasureParchment(&$mapinfo, &$equip, $level, $x, $y, $map, &$db)
    {
        $dist = mt_rand(1, 3);
        
        $validzone = $mapinfo->getValidMap($x, $y, 10 * $dist, $map);
        do {
            $content["x"] = abs(mt_rand(- 5 * $dist, 5 * $dist) + $x);
            $content["y"] = abs(mt_rand(- 5 * $dist, 5 * $dist) + $y);
        } while (! $validzone[$content["x"] - $x + (5 * $dist)][$content["y"] - $y + (5 * $dist)]);
        
        $content["po"] = $level * 10 * $dist;
        
        // if(mt_rand(0,3)==0)
        // {
        $content["equip"] = PNJFactory::getRndEquipmentFromLevel($level + $dist, $db, 2);
        // }
        
        return MissionFactory::genTreasureParchment($equip, $content, $x, $y, $db);
    }

    static function genStartParchment(&$equip, $po, $x, $y, &$db)
    {
        $content["x"] = $x;
        $content["y"] = $y;
        $content["po"] = $po;
        
        return MissionFactory::genTreasureParchment($equip, $content, $x, $y, $db);
    }

    static function genTreasureParchment(&$equip, $content, $x, $y, &$db)
    {
        $mission = new Mission();
        $mission->set("name", "Mission : Trésor");
        $mission->set("id_BasicMission", 4);
        
        if (isset($content["equip"])) {
            $mission->addObject("equip", $content["equip"], $db);
            $mission->addAction($content["x"], $content["y"], 3, array(
                "equip"
            ));
        }
        
        if (isset($content["po"]))
            $mission->addAction($content["x"], $content["y"], 1, $content["po"]);
        
        $str = "\$success='true';";
        
        $mission->addCond($content["x"], $content["y"], $str);
        $mission->set("content", serialize($mission));
        $equip->setObj("Mission", $mission, true);
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 30);
        $equip->set("id_EquipmentType", 9);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        return true;
    }

    static function genInListParchment($tabcaracs, &$db)
    {
        $caracs = unserialize($tabcaracs);
        
        $quest = new Quest();
        $quest->set("nbMission", $caracs['nbmissions']);
        $quest->set("name", stripslashes(base64_decode($caracs['questname'])));
        $quest->set("goal", stripslashes(base64_decode($caracs['questRP'])));
        $quest->set("Player_levelMin", max(1, $caracs["questlvl"] - 3 - floor(0.2 * $caracs["questlvl"])));
        $quest->set("Player_levelMax", $caracs["questlvl"] + 3 + floor(0.2 * $caracs["questlvl"]));
        $quest->set("In_List", 1);
        $quest->updateDB($db);
        
        $idnext = 0;
        
        for ($i = $caracs['nbmissions']; $i >= 1; $i --) {
            $mission = new Mission();
            
            $mission->set("name", stripslashes(base64_decode($caracs["name$i"])));
            
            $mission->set("NumMission", $i);
            /*
             * $mission->set("Player_levelMin",max(1,$caracs["lvl$i"]-10));
             * $mission->set("Player_levelMax",$caracs["lvl$i"]+10);
             * $mission->set("In_List",1);
             */
            $mission->set("RP_Mission", stripslashes(base64_decode($caracs["RP$i"])));
            $mission->set("id_Quest", $quest->get("id"));
            $mission->set("id_NextMission", $idnext);
            $mission->set("Mission_level", $caracs["lvl$i"]);
            $mission->updateDB($db);
            
            $reward = new MissionReward();
            $condition = new MissionCondition();
            $equip = new Equipment();
            
            switch ($caracs["type$i"]) {
                case 0:
                    MissionFactory::InListTreasure($caracs, $condition, $reward, $i, $db);
                    $mission->set("id_BasicMission", 4);
                    break;
                
                case 1:
                    MissionFactory::InListEscort($caracs, $condition, $reward, $i, $mission->get("id"), $db);
                    $mission->set("id_BasicMission", 1);
                    break;
                
                case 2:
                    MissionFactory::InListCarry($caracs, $condition, $reward, $i, $mission->get("id"), $db);
                    $mission->set("id_BasicMission", 6);
                    break;
                
                case 3:
                    MissionFactory::InListKill($caracs, $condition, $reward, $mission->get("id"), $i, $db);
                    $mission->set("id_BasicMission", 5);
                    
                    break;
                
                case 4:
                    MissionFactory::InListEnigm($caracs, $condition, $reward, $i, $db);
                    $mission->set("id_BasicMission", 7);
                    break;
                
                default:
                    return false;
            }
            $mission->updateDB($db);
            
            $condition->set("idMission", $mission->get("id"));
            
            if ($caracs["EquipWBasicId$i"] != 'none') {
                $basicequip = new BasicEquipment();
                $dbbe = new DBCollection("SELECT " . $basicequip->getASRenamer("BasicEquipment", "B") . " FROM BasicEquipment WHERE id=" . $caracs["EquipWBasicId$i"], $db, 0, 0);
                $basicequip->DBLoad($dbbe, 'B');
                
                $equipwinned = new Equipment();
                $equipwinned->set("name", $basicequip->get("name"));
                $equipwinned->set("level", $caracs["equipwlvl$i"]);
                if ($caracs["wextraname$i"] != '')
                    $equipwinned->set("extraname", stripslashes(base64_decode($caracs["wextraname$i"])));
                $equipwinned->set("id_BasicEquipment", $basicequip->get("id"));
                $equipwinned->set("id_EquipmentType", $basicequip->get("id_EquipmentType"));
                $equipwinned->set("date", gmdate("Y-m-d H:i:s"));
                $equipwinned->updateDB($db);
                
                $reward->set("idEquipStop", $equipwinned->get("id"));
                $reward->set("NameEquipStop", $basicequip->get("name"));
                $reward->set("ExtranameEquipStop", stripslashes(base64_decode($caracs["wextraname$i"])));
            }
            $reward->set("moneyStop", floor(150 + 10 * ($caracs["lvl$i"] - 1) ^ 2 + (1 / 4) * ($caracs["questlvl"] - 1) ^ 2 + 10 * $caracs["questlvl"] * $caracs["lvl$i"]));
            $reward->set("xpStop", floor(20 + 2 * ($caracs["lvl$i"] - 1) ^ 2 + 0.01 * ($caracs["questlvl"] - 1) ^ 2 + (1 / 2) * $caracs["questlvl"] * $caracs["lvl$i"]));
            $reward->set("idMission", $mission->get("id"));
            
            $condition->updateDB($db);
            $reward->updateDB($db);
            
            $equip->setObj("Mission", $mission, true);
            $equip->set("name", "Parchemin");
            $equip->set("id_BasicEquipment", 30);
            $equip->set("id_BasicMission", 5);
            $equip->set("id_EquipmentType", 9);
            $equip->set("date", gmdate("Y-m-d H:i:s"));
            // $equip->set("state","Ground");
            // $equip->set("x",279);
            // $equip->set("y",380);
            $equip->set("id_Mission", $mission->get("id"));
            
            $equip->updateDB($db);
            
            $idnext = $mission->get("id");
        }
        $quest->set("idFirstMission", $idnext);
        $quest->updateDB($db);
        
        return true;
    }

    static function InListTreasure($caracs, &$condition, &$reward, $i, &$db)
    {
        $condition->set("distanceX", $caracs["distanceX$i"]);
        $condition->set("distanceY", $caracs["distanceY$i"]);
        
        $condition->updateDB($db);
    }

    static function InListEscort($caracs, &$condition, &$reward, $i, $idm, &$db)
    {
        $condition->set("distanceX", $caracs["distanceX$i"]);
        $condition->set("distanceY", $caracs["distanceY$i"]);
        
        $npc = PNJFactory::getPNJFromLevel($caracs["idraceEscort$i"], 2, $db);
        $npc->set("hidden", 1);
        // $npc->set("name",stripslashes(base64_decode($caracs["EscortName$i"])));
        $npc->set("id_Mission", $idm);
        $npc->set("gender", $caracs["EscortGender$i"]);
        // $npc->set("status","PC");
        $npc->addDBr($db);
        
        $condition->set("idEscort", $npc->get("id"));
        $reward->set("idEscort", $npc->get("id"));
        
        $reward->updateDB($db);
        $condition->updateDB($db);
    }

    static function InListCarry($caracs, &$condition, &$reward, $i, $idm, &$db)
    {
        $condition->set("distanceX", $caracs["distanceX$i"]);
        $condition->set("distanceY", $caracs["distanceY$i"]);
        
        $basicequip = new BasicEquipment();
        $dbbe = new DBCollection("SELECT " . $basicequip->getASRenamer("BasicEquipment", "B") . " FROM BasicEquipment WHERE id=" . $caracs["EquipBasicId$i"], $db, 0, 0);
        $basicequip->DBLoad($dbbe, 'B');
        
        $equip = new Equipment();
        $equip->set("name", $basicequip->get("name"));
        if ($caracs["extraname$i"] != '')
            $equip->set("extraname", stripslashes(base64_decode($caracs["extraname$i"])));
        $equip->set("id_BasicEquipment", $basicequip->get("id"));
        $equip->set("id_Mission", $idm);
        $equip->set("id_EquipmentType", $basicequip->get("id_EquipmentType"));
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->updateDB($db);
        
        $condition->set("idEquip", $equip->get("id"));
        $reward->set("idEquipStart", $equip->get("id"));
        
        $reward->updateDB($db);
        $condition->updateDB($db);
    }

    static function InListKill($caracs, &$condition, &$reward, $idm, $i, &$db)
    {
        $condition->set("distanceX", $caracs["distanceX$i"]);
        $condition->set("distanceY", $caracs["distanceY$i"]);
        
        $npc = PNJFactory::getPNJFromLevel($caracs["idracePNJ$i"], $caracs["questlvl"] + rand(1, 5) + 2 * $caracs["lvl$i"] * floor($caracs["questlvl"] / 70), $db);
        $npc->set("hidden", 1);
        $npc->set("id_Mission", $idm);
        $npc->addDBr($db);
        $idt = $npc->get("id");
        $dbu = new DBCollection("UPDATE Player SET id_Team=" . $idt . " WHERE id_Mission=" . $idm, $db, 0, 0, false);
        
        for ($j = 0; $j < $caracs["nbcible$i"]; $j ++) {
            $npc = PNJFactory::getPNJFromLevel($caracs["idracePNJ$i"], $caracs["questlvl"] + rand(1, 5) + 2 * $caracs["lvl$i"] * floor($caracs["questlvl"] / 70), $db);
            $npc->set("hidden", 1);
            $npc->set("id_Mission", $idm);
            $npc->set("id_Team", $idt);
            $npc->addDBr($db);
        }
        
        $condition->set("idPNJtoKill", $npc->get("id"));
        
        $condition->updateDB($db);
    }

    static function InListEnigm($caracs, &$condition, &$reward, $i, &$db)
    {
        $condition->set("distanceX", $caracs["distanceX$i"]);
        $condition->set("distanceY", $caracs["distanceY$i"]);
        
        $condition->set("EnigmAnswer", $caracs["answer$i"]);
        
        $condition->updateDB($db);
    }

    static function genStandardEscortParchment1($map, $Player_x, $Player_y, $idPlayer, $levelMission, &$db)
    {
        $levelNPC = 2;
        $nb = 1;
        $maitreid = rand(1, 4);
        switch ($maitreid) {
            case 1:
                $maitre = " de l'armurier";
                break;
            case 2:
                $maitre = " de l'enchanteur";
                break;
            case 3:
                $maitre = " du menuisier";
                break;
            case 4:
                $maitre = " de l'alchimiste";
                break;
        }
        
        $name = "Escorter l'apprentis";
        $RP = "Alors que vous comtempliez pensivement votre verre vide, un enfant d'une dizaine d'années s'approche et tire sur votre manteau... Il vous explique qu'il doit partir en apprentissage auprès$maitre d'un village voisin mais que la région est trop dangereuse pour lui. Sa famille est donc prête à lui payer un garde du corps. Un des clients à qui vous parliez quelques minutes plus tôt lui a vanté votre fiabilité et son choix s'est donc finalement posé sur vous.";
        $mission = new Mission();
        $equip = new Equipment();
        $mission->set("id_BasicMission", 3);
        $mission->set("name", $name);
        $idrace = 1;
        
        $mission->set("Player_levelMin", 1);
        $mission->set("Player_levelMax", 1000);
        $mission->set("In_List", 0);
        $mission->set("Is_Standard", 1);
        $mission->set("RP_Mission", $RP);
        $mission->set("Mission_level", $levelMission);
        $mission->updateDB($db);
        
        $dbp = new DBCollection("SELECT City.id,City.name,City.level,City.x,City.y FROM City WHERE map=" . $map . " AND abs(City.x-" . $Player_x . ")<=50 AND abs(City.y-" . $Player_y . ")<=50 AND (abs(City.x-" . $Player_x . ")>0 OR abs(City.y-" . $Player_y . ")>0) ORDER BY City.x", $db, 0, 0);
        
        $listX = array();
        $listY = array();
        $listLevel = array();
        
        while (! $dbp->eof()) {
            if ($dbp->get("name") != "") {
                $listX[] = $dbp->get("x");
                $listY[] = $dbp->get("y");
                $listLevel[] = $dbp->get("level");
            }
            $dbp->next();
        }
        $cle = array_rand($listX);
        $xf = $listX[$cle];
        $yf = $listY[$cle];
        $lvl = $listLevel[$cle] * $listLevel[$cle];
        
        $content["xstart"] = $Player_x;
        $content["ystart"] = $Player_y;
        $content["postart"] = 0;
        $content["pxstart"] = 0;
        $content["postop"] = 5 * ($lvl + 1) * $levelMission * floor((mt_rand(100, 300) / 100));
        $content["pxstop"] = ($lvl + 1) * $levelMission * floor((mt_rand(50, 80) / 100));
        $content["xstop"] = $xf;
        $content["ystop"] = $yf;
        
        $arrayNpc = array();
        for ($i = 0; $i < $nb; $i ++) {
            $npc = PNJFactory::getPNJFromLevel($idrace, $levelNPC, $db);
            $npc->set("x", $content["xstart"]);
            $npc->set("y", $content["ystart"]);
            $npc->set("map", $map);
            
            // $npc->set("status","PC");
            $npc->set("id_Mission", $mission->get("id"));
            $npc->set("state2", 50);
            $mission->addObject("npc" . $i, $npc, $db);
            $arrayNpc[] = "npc" . $i;
        }
        
        if (isset($content["postart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 1, $content["postart"]);
        if (isset($content["pxstart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 2, $content["pxstart"]);
        
        $mission->addAction($content["xstart"], $content["ystart"], 4, $arrayNpc);
        $mission->addAction($content["xstart"], $content["ystart"], 6, $arrayNpc);
        
        if (isset($content["postop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 1, $content["postop"]);
        if (isset($content["pxstop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 2, $content["pxstop"]);
        
        $mission->addAction($content["xstop"], $content["ystop"], 5, $arrayNpc);
        
        $str = "";
        foreach ($arrayNpc as $npc) {
            $str .= "\$p1->get('x')==\$" . $npc . "->get('x') && \$p1->get('y')==\$" . $npc . "->get('y') && ";
        }
        $str = "if(" . substr($str, 0, - 4) . ")\$success='true';else\$success='false';";
        
        $mission->addCond($content["xstart"], $content["ystart"], "\$success='true';");
        $mission->addCond($content["xstop"], $content["ystop"], $str);
        $mission->set("content", serialize($mission));
        
        $mission->updateDB($db);
        
        $equip->setObj("Mission", $mission, true);
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 30);
        $equip->set("id_EquipmentType", 9);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("state", "Carried");
        $equip->set("id_Player", $idPlayer);
        
        $equip->set("id_Mission", $mission->get("id"));
        
        $equip->updateDB($db);
        return true;
    }

    static function genStandardEscortParchment2($map, $Player_x, $Player_y, $idPlayer, $levelMission, &$db)
    {
        $levelNPC = 4;
        $nb = 1;
        
        $name = "Escorter le messager";
        $RP = "Un jeune elfe semble observer votre conversation depuis un moment, il se lève soudain et vient vers vous d'un pas décidé. Il vous explique qu'il est au service d'un notable du village. Ce dernier désire avoir des nouvelles d'un ami habitant dans un village un peu éloigné et il doit donc s'y rendre. Mais le risque est bien grand pour un jeune elfe sans expérience du combat et son maître le comprend. Il lui a donc donner de quoi se payer un garde du corps pour ce voyage et vous semblez tout désigné...";
        $mission = new Mission();
        $equip = new Equipment();
        $mission->set("id_BasicMission", 3);
        $mission->set("name", $name);
        $idrace = 2;
        
        $mission->set("Player_levelMin", 1);
        $mission->set("Player_levelMax", 1000);
        $mission->set("In_List", 0);
        $mission->set("Is_Standard", 1);
        $mission->set("RP_Mission", $RP);
        $mission->set("Mission_level", $levelMission);
        $mission->updateDB($db);
        
        $dbp = new DBCollection("SELECT City.id,City.name,City.level,City.x,City.y FROM City WHERE map=" . $map . " AND abs(City.x-" . $Player_x . ")>=50 AND abs(City.y-" . $Player_y . ")>=10 AND abs(City.x-" . $Player_x . ")<=100 AND abs(City.y-" . $Player_y . ")<=50 ORDER BY City.x", $db, 0, 0);
        
        $listX = array();
        $listY = array();
        $listLevel = array();
        
        while (! $dbp->eof()) {
            if ($dbp->get("name") != "") {
                $listX[] = $dbp->get("x");
                $listY[] = $dbp->get("y");
                $listLevel[] = $dbp->get("level");
            }
            $dbp->next();
        }
        $cle = array_rand($listX);
        $xf = $listX[$cle];
        $yf = $listY[$cle];
        $lvl = $listLevel[$cle] * $listLevel[$cle];
        
        $content["xstart"] = $Player_x;
        $content["ystart"] = $Player_y;
        $content["postart"] = 0;
        $content["pxstart"] = 0;
        $content["postop"] = 8 * ($lvl + 1) * $levelMission * floor((mt_rand(100, 300) / 100));
        $content["pxstop"] = ($lvl + 1) * $levelMission * floor((mt_rand(100, 200) / 100));
        ;
        $content["xstop"] = $xf;
        $content["ystop"] = $yf;
        
        $arrayNpc = array();
        for ($i = 0; $i < $nb; $i ++) {
            $npc = PNJFactory::getPNJFromLevel($idrace, $levelNPC, $db);
            $npc->set("x", $content["xstart"]);
            $npc->set("y", $content["ystart"]);
            $npc->set("map", $map);
            
            // $npc->set("status","PC");
            $npc->set("id_Mission", $mission->get("id"));
            $npc->set("state2", 50);
            $mission->addObject("npc" . $i, $npc, $db);
            $arrayNpc[] = "npc" . $i;
        }
        
        if (isset($content["postart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 1, $content["postart"]);
        if (isset($content["pxstart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 2, $content["pxstart"]);
        
        $mission->addAction($content["xstart"], $content["ystart"], 4, $arrayNpc);
        $mission->addAction($content["xstart"], $content["ystart"], 6, $arrayNpc);
        
        if (isset($content["postop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 1, $content["postop"]);
        if (isset($content["pxstop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 2, $content["pxstop"]);
        
        $mission->addAction($content["xstop"], $content["ystop"], 5, $arrayNpc);
        
        $str = "";
        foreach ($arrayNpc as $npc) {
            $str .= "\$p1->get('x')==\$" . $npc . "->get('x') && \$p1->get('y')==\$" . $npc . "->get('y') && ";
        }
        $str = "if(" . substr($str, 0, - 4) . ")\$success='true';else\$success='false';";
        
        $mission->addCond($content["xstart"], $content["ystart"], "\$success='true';");
        $mission->addCond($content["xstop"], $content["ystop"], $str);
        $mission->set("content", serialize($mission));
        
        $mission->updateDB($db);
        
        $equip->setObj("Mission", $mission, true);
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 30);
        $equip->set("id_EquipmentType", 9);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("state", "Carried");
        $equip->set("id_Player", $idPlayer);
        $equip->set("id_Mission", $mission->get("id"));
        
        $equip->updateDB($db);
        return true;
    }

    static function genStandardEscortParchment3($map, $Player_x, $Player_y, $idPlayer, $levelMission, &$db)
    {
        $levelNPC = 1;
        $nb = 1;
        
        $name = "Escorter le vieux dorane";
        $RP = "Vous arrêtez soudain de parler, et laissant en plan la personne qui venait de vous offrir un verre, vous vous dirigez sans savoir pouquoi vers le coin le plus sombre de la salle où l'ombre dissimulait ce qui semble être le plus vieux dorane ayant jamais vécu sur les terre de Nacridan. Son regard demeure toutefois aussi pénétrant qu'une épée d'Aigle et vous comprenez que c'est à son ordre mental que vous venez d'obéir. Il vous explique qu'il voudrait se retirer dans un ermitage caché près d'un village à plusieurs dizaines de lieues de là pour finir ses jours. Toutefois, il n'est pas sûr d'avoir la force suffisante pour faire face aux monstres sur le chemin. Il affirme avoir suffisamment d'économies pour vous payer un salaire tout le long du voyage.";
        $mission = new Mission();
        $equip = new Equipment();
        $mission->set("id_BasicMission", 3);
        $mission->set("name", $name);
        $idrace = 4;
        
        $mission->set("Player_levelMin", 1);
        $mission->set("Player_levelMax", 1000);
        $mission->set("In_List", 0);
        $mission->set("Is_Standard", 1);
        $mission->set("RP_Mission", $RP);
        $mission->set("Mission_level", $levelMission);
        $mission->updateDB($db);
        
        $dbp = new DBCollection("SELECT City.id,City.name,City.level,City.x,City.y FROM City WHERE map=" . $map . " AND abs(City.x-" . $Player_x . ")>=100 AND abs(City.y-" . $Player_y . ")>=10 AND abs(City.x-" . $Player_x . ")<=150 AND abs(City.y-" . $Player_y . ")<=50 AND City.id<164 ORDER BY City.x", $db, 0, 0);
        
        $listX = array();
        $listY = array();
        $listLevel = array();
        
        while (! $dbp->eof()) {
            if ($dbp->get("name") != "") {
                $listX[] = $dbp->get("x");
                $listY[] = $dbp->get("y");
                $listLevel[] = $dbp->get("level");
            }
            $dbp->next();
        }
        $cle = array_rand($listX);
        $xf = $listX[$cle];
        $yf = $listY[$cle];
        $lvl = $listLevel[$cle] * $listLevel[$cle];
        
        $content["xstart"] = $Player_x;
        $content["ystart"] = $Player_y;
        $content["postart"] = 0;
        $content["pxstart"] = 0;
        $content["postop"] = 8 * ($lvl + 1) * $levelMission * floor((mt_rand(100, 300) / 100));
        $content["pxstop"] = ($lvl + 1) * $levelMission * floor((mt_rand(100, 200) / 100));
        ;
        $content["xstop"] = $xf - 2;
        $content["ystop"] = $yf;
        
        $arrayNpc = array();
        for ($i = 0; $i < $nb; $i ++) {
            $npc = PNJFactory::getPNJFromLevel($idrace, $levelNPC, $db);
            $npc->set("x", $content["xstart"]);
            $npc->set("y", $content["ystart"]);
            $npc->set("map", $map);
            
            // $npc->set("status","PC");
            $npc->set("id_Mission", $mission->get("id"));
            $npc->set("state2", 50);
            $mission->addObject("npc" . $i, $npc, $db);
            $arrayNpc[] = "npc" . $i;
        }
        
        if (isset($content["postart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 1, $content["postart"]);
        if (isset($content["pxstart"]))
            $mission->addAction($content["xstart"], $content["ystart"], 2, $content["pxstart"]);
        
        $mission->addAction($content["xstart"], $content["ystart"], 4, $arrayNpc);
        $mission->addAction($content["xstart"], $content["ystart"], 6, $arrayNpc);
        
        if (isset($content["postop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 1, $content["postop"]);
        if (isset($content["pxstop"]))
            $mission->addAction($content["xstop"], $content["ystop"], 2, $content["pxstop"]);
        
        $mission->addAction($content["xstop"], $content["ystop"], 5, $arrayNpc);
        
        $str = "";
        foreach ($arrayNpc as $npc) {
            $str .= "\$p1->get('x')==\$" . $npc . "->get('x') && \$p1->get('y')==\$" . $npc . "->get('y') && ";
        }
        $str = "if(" . substr($str, 0, - 4) . ")\$success='true';else\$success='false';";
        
        $mission->addCond($content["xstart"], $content["ystart"], "\$success='true';");
        $mission->addCond($content["xstop"], $content["ystop"], $str);
        $mission->set("content", serialize($mission));
        
        $mission->updateDB($db);
        
        $equip->setObj("Mission", $mission, true);
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 30);
        $equip->set("id_EquipmentType", 9);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("state", "Carried");
        $equip->set("id_Player", $idPlayer);
        $equip->set("id_Mission", $mission->get("id"));
        
        $equip->updateDB($db);
        return true;
    }
    
    // --------------- SUPPRESSION DES QUÊTES----
    static function deletequest($id_quest, &$db)
    {
        $dbm = new DBCollection("SELECT id FROM Mission WHERE id_Quest=" . $id_quest, $db, 0, 0);
        
        while (! $dbm->eof()) {
            $idm = $dbm->get("id");
            $dbp = new DBCollection("SELECT Player.id,Player.id_Modifier,Player.id_Upgrade FROM Player WHERE  Player.id_Mission=" . $idm, $db, 0, 0);
            while (! $dbp->eof()) {
                $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Modifier WHERE id=" . $dbp->get("id_Modifier"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=" . $dbp->get("id_Upgrade"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Player WHERE id=" . $dbp->get("id"), $db, 0, 0, false);
                
                $dbp->next();
            }
            $dbd = new DBCollection("DELETE FROM Equipment WHERE id_Mission=" . $idm, $db, 0, 0, false);
            $dbd = new DBCollection("DELETE FROM MissionCondition WHERE idMission=" . $idm, $db, 0, 0, false);
            $dbd = new DBCollection("DELETE FROM MissionReward WHERE idMission=" . $idm, $db, 0, 0, false);
            $dbd = new DBCollection("DELETE FROM Mission WHERE id=" . $idm, $db, 0, 0, false);
            
            $dbm->next();
        }
        
        $dbd = new DBCollection("DELETE FROM Quest WHERE id=" . $id_quest, $db, 0, 0, false);
        
        return true;
    }
}

?>
