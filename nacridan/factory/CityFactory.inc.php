<?php
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");

class CityFactory
{

    /* ******************************************************** FONCTIONS AUXILLIAIRES ************************************************ */
    static function getNameCity($id, $db)
    {
        switch ($id) {
            case 11:
                $name = "Nephy";
                break;
            case 24:
                $name = "Izandar";
                break;
            case 65:
                $name = "Landar";
                break;
            case 82:
                $name = "Krima";
                break;
            case 103:
                $name = "Djin";
                break;
            case 135:
                $name = "Earok";
                break;
            case 160:
                $name = "Dust";
                break;
            case 161:
                $name = "Artasse";
                break;
            case 169:
                $name = "Octobian";
                break;
            case 170:
                $name = "Tonak";
                break;
            default:
                $name = "village";
                break;
        }
        return $name;
    }

    /* ******************************************************** GESTION DES EQUIPEMENT DES ECHOPPES ************************************************ */
    static function fillStockEquipmentAllShop($db)
    {
        $echoppe = new Building();
        $dbc = new DBCollection("SELECT id FROM Building where name='Echoppe'", $db);
        while (! $dbc->eof()) {
            $dbe = new DBCollection("DELETE FROM Equipment WHERE id_Shop=" . $dbc->get("id"), $db, 0, 0, false);
            $echoppe->load($dbc->get("id"), $db);
            self::updateEquipmentShop($echoppe, $db);
            $dbc->next();
        }
    }

    static function updateEquipmentShop($echoppe, $db)
    {
        $type = array();
        $type[1] = "Armes";
        $type[2] = "Armures";
        $type[3] = "Outils";
        $type[4] = "Potions";
        $type[5] = "Matières";
        
        $cond = array();
        $cond[1] = "(id_EquipmentType<9 OR id_EquipmentType=28 OR id_EquipmentType=22)";
        $cond[2] = "(id_EquipmentType>8 AND id_EquipmentType<22)";
        $cond[3] = "(id_EquipmentType>39)";
        $cond[4] = "(id_EquipmentType=29)";
        $cond[5] = "(id_EquipmentType=30 OR id_EquipmentType=31 OR id_EquipmentType=32)";
        
        self::deleteTriple($echoppe, $db);
        
        for ($i = 1; $i < 6; $i ++) {
            
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE " . $cond[$i] . " AND id_Shop=" . $echoppe->get("id"), $db);
            
            for ($j = 0; $j < 2 + 2 * $echoppe->get("level") - $dbe->count(); $j ++)
                self::createEquipmentFromShop($echoppe, $db, $type[$i]);
        }
    }

    static function deleteTriple($echoppe, $db)
    {
        $dbe = new DBCollection("SELECT level, id_BasicEquipment, count(*) as nb FROM Equipment WHERE id_Shop=" . $echoppe->get("id") . " GROUP BY id_BasicEquipment, level", $db);
        while (! $dbe->eof()) {
            if ($dbe->get("nb") > 2) {
                $dbc = new DBCollection(
                    "SELECT id FROM Equipment WHERE id_Shop=" . $echoppe->get("id") . " AND id_BasicEquipment=" . $dbe->get("id_BasicEquipment") . " AND level=" . $dbe->get(
                        "level"), $db);
                $dbc->next();
                $dbc->next();
                while (! $dbc->eof()) {
                    $dbd = new DBCollection("DELETE FROM Equipment WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
                    $dbc->next();
                }
            }
            $dbe->next();
        }
    }

    static function createEquipmentFromShop($echoppe, $db, $type = 0)
    {
        $id = self::getNewEquipmentIdFromShop($echoppe, $db, $type);
        if ($id != 0) {
            
            $level = max(1, mt_rand($echoppe->get("level") - 1, $echoppe->get("level")));
            
            $equip = new Equipment();
            $equip->set("level", $level);
            $equip->set("id_Shop", $echoppe->get("id"));
            $equip->set("id_BasicEquipment", $id);
            $equip->set("date", gmdate("Y-m-d H:i:s"));
            $dbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id, $db);
            if ($dbe->get("id_EquipmentType") >= 28)
                $level = min(3, $level);
            $equip->set("id_EquipmentType", $dbe->get("id_EquipmentType"));
            $equip->set("name", $dbe->get("name"));
            $coef = 1;
            if ($equip->get("id_BasicEquipment") >= 200 && $equip->get("id_BasicEquipment") <= 206 && $equip->get("id_BasicEquipment") != 205)
                
                $coef = $equip->get("level");
            $equip->set("durability", $dbe->get("durability") * $coef);
            $equip->addDB($db);
        }
    }

    static function getNewEquipmentIdFromShop($echoppe, $db, $type)
    {
        
        // Chargement des ressource de la cité
        $city = new City();
        $city->load($echoppe->get("id_City"), $db);
        $res = $city->getArrayRes();
        switch ($type) {
            case "Armes":
                $cond = "(BasicEquipment.id_EquipmentType<8 OR BasicEquipment.id_EquipmentType=28 OR BasicEquipment.id_EquipmentType=22) AND ";
                $pick = array(
                    "iron" => $res["iron"],
                    "emerald" => $res["emerald"],
                    "wood" => $res["wood"]
                );
                break;
            
            case "Armures":
                $cond = "(BasicEquipment.id_EquipmentType>7 AND BasicEquipment.id_EquipmentType<21) AND ";
                $pick = array(
                    "linen" => $res["linen"],
                    "scale" => $res["scale"],
                    "leather" => $res["leather"],
                    "iron" => $res["iron"]
                );
                break;
            
            case "Outils":
                $cond = "(BasicEquipment.id_EquipmentType>39) AND ";
                $pick = array(
                    "iron" => $res["iron"],
                    "wood" => $res["wood"],
                    "leather" => $res["leather"]
                );
                break;
            
            case "Potions":
                $cond = "(BasicEquipment.id_EquipmentType=29) AND ";
                $pick = array(
                    "seed" => $res["seed"],
                    "leaf" => $res["leaf"],
                    "root" => $res["root"]
                );
                break;
            case "Matières":
                $cond = "(BasicEquipment.id_EquipmentType=30 OR BasicEquipment.id_EquipmentType=31 OR BasicEquipment.id_EquipmentType=32) AND ";
                $pick = $res;
                break;
            
            default:
                break;
        }
        
        // Selection d'une ressource de la cité compatible avec le type d'équipement sélectionné
        $vector = new VectorProba();
        foreach ($pick as $key => $val)
            $vector->pushBack($key, $val);
            
            // $vector->setInterval($vector->m_total);
        $index = $vector->getRandomElement();
        if ($index >= 0)
            $ret = $vector->getKey($index);
            
            // Choix d'un basicEquipment du type choisi (armes, armures, potion etc...) et de la ressources sélectionné plus haut.
        $basicEquip = array();
        $dbe = new DBCollection(
            "SELECT BasicEquipment.id AS id, BasicEquipment.frequency AS freq FROM BasicEquipment LEFT JOIN BasicMaterial ON BasicEquipment.id_BasicMaterial=BasicMaterial.id WHERE " .
                 $cond . " BasicMaterial.name='" . $ret . "'", $db);
        while (! $dbe->eof()) {
            $basicEquip[$dbe->get("id")] = $dbe->get("freq");
            $dbe->next();
        }
        
        // Test non nécessaire depuis la réforme des ressources normalement.
        if (sizeof($basicEquip) == 0)
            return 0;
        
        $vector2 = new VectorProba();
        foreach ($basicEquip as $key => $val)
            $vector2->pushBack($key, $val);
        
        $index = $vector2->getRandomElement();
        if ($index >= 0) {
            $ret = $vector2->getKey($index);
            // echo " id_equip :".$ret;
            return $ret;
        } else
            return 0;
    }

    /* ***************************************************************** Initialisation des ressources des cités ****************************** */
    static function initialiseRessourceAllcity($db)
    {
        $city = new City();
        for ($i = 2; $i < 165; $i ++) {
            $city->load($i, $db);
            self::initialiseRessourceCity($city, $db);
        }
    }

    static function initialiseRessourceCity($city, $db)
    {
        // Les potions
        $res = array(
            "seed" => 1,
            "leaf" => 1,
            "root" => 2
        );
        $vector = new VectorProba();
        foreach ($res as $key => $val) {
            $vector->pushBack($key, $val);
            $city->set($key, 0);
        }
        $total = 4;
        while ($total > 0) {
            $vector->setInterval(PREC);
            $index = $vector->searchUpperOrEqual(rand(1, PREC));
            if ($index >= 0) {
                $ret = $vector->getKey($index);
                $val = mt_rand(1, $total);
                $total = $total - $val;
                $city->set($ret, $city->get($ret) + $val);
            }
        }
        
        // Les armes
        $res = array(
            "iron" => 3,
            "emerald" => 1,
            "wood" => 2
        );
        $vector = new VectorProba();
        foreach ($res as $key => $val) {
            $vector->pushBack($key, $val);
            $city->set($key, 0);
        }
        $total = 6;
        while ($total > 0) {
            $vector->setInterval(PREC);
            $index = $vector->searchUpperOrEqual(rand(1, PREC));
            if ($index >= 0) {
                $ret = $vector->getKey($index);
                $val = mt_rand(1, $total);
                $total = $total - $val;
                $city->set($ret, $city->get($ret) + $val);
            }
        }
        
        // Les Armures
        $res = array(
            "ruby" => 1,
            "linen" => 2,
            "leather" => 2,
            "scale" => 2
        );
        $vector = new VectorProba();
        foreach ($res as $key => $val) {
            $vector->pushBack($key, $val);
            $city->set($key, 0);
        }
        $total = 6;
        while ($total > 0) {
            $vector->setInterval(PREC);
            $index = $vector->searchUpperOrEqual(rand(1, PREC));
            if ($index >= 0) {
                $ret = $vector->getKey($index);
                $val = mt_rand(1, $total);
                $total = $total - $val;
                $city->set($ret, $city->get($ret) + $val);
            }
        }
        
        $city->updateDB($db);
    }

    static function FixRessourceAllcity($db)
    {
        $city = new City();
        $dbc = new DBCollection("SELECT id FROM City WHERE wood + iron + leaf + linen + seed + root + scale + leather + emerald + ruby !=100", $db);
        while (! $dbc->eof()) {
            $city->load($dbc->get("id"), $db);
            self::initialiseRessourceCity($city, $db);
            $dbc->next();
        }
    }

    function cleanBddExpOnRoad($db)
    {
        $dbe = new DBCollection("SELECT * FROM Exploitation", $db);
        $nb = 0;
        while (! $dbe->eof()) {
            if (getLandType($dbe->get("x"), $dbe->get("y"), 1, $db) == "beatenearth") {
                $dbd = new DBCollection("DELETE FROM Exploitation WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
                $nb ++;
            }
            $dbe->next();
        }
        echo $nb;
    }

    /*
     * static function initialiseRessourceCity($city, $db)
     * {
     * $cityEtalon = new City();
     * $cityEtalon->load(1, $db);
     * $res = $cityEtalon->getArrayRes();
     *
     *
     *
     * $vector=new VectorProba();
     * foreach($res as $key => $val)
     * {
     * $vector->pushBack($key,$val);
     * $city->set($key, 0);
     * }
     *
     * $total = 10;
     *
     * while($total > 0)
     * {
     * $vector->setInterval(PREC);
     * $index=$vector->searchUpperOrEqual(rand(1,PREC));
     * if($index>=0)
     * {
     * $ret=$vector->getKey($index);
     * $val = mt_rand(1,$total);
     * $total = $total - $val;
     * $city->set($ret, $val);
     * }
     * }
     * $city->updateDB($db);
     * }
     *
     */
    static function getBuildingPrice($id, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id, $db);
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=" . $dbc->get("id_BasicBuilding"), $db);
        
        $price = floor($dbb->get("price") * pow((3 / 2), ($dbc->get("level") - 1)));
        return $price;
    }

    static function getPriceToUpgradeBuilding($id, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id, $db);
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=" . $dbc->get("id_BasicBuilding"), $db);
        
        $price = floor($dbb->get("price") * pow(3 / 2, $dbc->get("level")));
        return $price;
    }

    static function getPriceToRepairBuilding($id, $db)
    {
        $price = self::getBuildingPrice($id, $db);
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id, $db);
        $price *= ($dbc->get("sp") - $dbc->get("currsp")) / $dbc->get("sp");
        if ($dbc->get("name") != "Rempart");
        $price *= 1 / 3;
        return ceil($price);
    }

    static function getNextSPBuilding($id, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id, $db);
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=" . $dbc->get("id_BasicBuilding"), $db);
        
        $solidity = $dbb->get("solidity");
        if ($solidity == "Fragile")
            $nextsp = 200 + 20 * pow($dbc->get("level") + 1, 2);
        elseif ($solidity == "Medium")
            $nextsp = 300 + 30 * pow($dbc->get("level") + 1, 2);
        elseif ($solidity == "Solid")
            $nextsp = 400 + 40 * pow($dbc->get("level") + 1, 2);
        
        return floor($nextsp);
    }

    static function getBuildingActionPrice($id_Building, $action, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Building, $db);
        // $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=".$dbc->get("id_BasicBuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $dba = new DBCollection("SELECT * FROM BuildingAction WHERE id=" . $action . " AND id_BasicBuilding=" . $dbc->get("id_BasicBuilding"), $db);
        
        $price = array();
        $profit = array();
        if ($dbh->get("captured") > 3)
            $profit = unserialize($dbc->get("profit"));
        else
            $profit[$dba->get("name")] = $dba->get("profit");
            
            // echo $dbh->get("captured")."-";
            // echo $dba->get("price");
        
        if ($dba->get("id") == BANK_DEPOSIT || $dba->get("id") == BANK_TRANSFERT)
            return $dba->get("price") - ($dbc->get("level") - 1) + $profit[$dba->get("name")];
        else
            return $dba->get("price") + $profit[$dba->get("name")];
    }

    static function getBuildingActionProfit($id_Building, $action, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Building, $db);
        // $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=".$dbc->get("id_BasicBuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $dba = new DBCollection("SELECT * FROM BuildingAction WHERE id=" . $action . " AND id_BasicBuilding=" . $dbc->get("id_BasicBuilding"), $db);
        
        $profit = array();
        if ($dbh->get("captured") > 3)
            $profit = unserialize($dbc->get("profit"));
        else
            $profit[$dba->get("name")] = $dba->get("profit");
        
        return $profit[$dba->get("name")];
    }

    static function getBuildingSP($id_BasicBuilding, $db)
    {
        $dbp = new DBCollection("SELECT * FROM BasicBuilding WHERE id=" . $id_BasicBuilding, $db);
        switch ($dbp->get("solidity")) {
            case "Fragile":
                $sp = FRAGILE;
                break;
            case "Medium":
                $sp = MEDIUM;
                break;
            case "Solid":
                $sp = SOLID;
                break;
            default:
                $sp = FRAGILE;
                break;
        }
        
        return $sp;
    }
}

?>
