<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamMsg extends DBObject
{

    function TeamMsg()
    {
        parent::DBObject();
        $this->m_tableName = "TeamMsg";
        $this->m_className = "TeamMsg";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>