<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicAbility extends DBObject
{

    function BasicAbility()
    {
        parent::DBObject();
        $this->m_tableName = "BasicAbility";
        $this->m_className = "BasicAbility";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["ident"] = "";
        $this->m_attr["timeAttack"] = "";
        $this->m_attr["modifierPA"] = "";
        $this->m_attr["school"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["usable"] = "";
        $this->init();
    }
}

?>
