<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Graffiti extends DBObject
{

    function Graffiti()
    {
        parent::DBObject();
        $this->m_tableName = "Graffiti";
        $this->m_className = "Graffiti";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Building"] = "";
        $this->m_attr["date_Graff"] = "";
        $this->m_attr["text"] = "";
        $this->m_attr["terminé"] = "";
        $this->init();
    }
}

?>
