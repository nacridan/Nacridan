<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailSend extends DBObject
{

    function MailSend()
    {
        parent::DBObject();
        $this->m_tableName = "MailSend";
        $this->m_className = "MailSend";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_MailBody"] = "";
        $this->m_attr["important"] = "";
        $this->m_attr["id_repertory"] = "";
        $this->init();
    }
}

?>