<?php

/**
 *  Define the Building class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BuildingAction extends DBObject
{

    function Building()
    {
        parent::DBObject();
        $this->m_tableName = "Building";
        $this->m_className = "Building";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["price"] = "";
        $this->m_attr["profit"] = "";
        $this->m_attr["ap"] = "";
        $this->m_attr["id_BasicBuilding"] = "";
        $this->m_attr["taxable"] = "";
        
        $this->init();
    }
}

?>
