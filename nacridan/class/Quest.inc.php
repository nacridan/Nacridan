<?php

/**
 *  Define the Quest class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Quest extends DBObject
{

    function Quest()
    {
        parent::DBObject();
        $this->m_tableName = "Quest";
        $this->m_className = "Quest";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["nbMission"] = "";
        $this->m_attr["idFirstMission"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["goal"] = "";
        $this->m_attr["In_List"] = "";
        $this->m_attr["Player_levelMin"] = "";
        $this->m_attr["Player_levelMax"] = "";
        $this->init();
    }
}

?>
