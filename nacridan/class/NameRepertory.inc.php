<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class NameRepertory extends DBObject
{

    function NameRepertory()
    {
        parent::DBObject();
        $this->m_tableName = "NameRepertory";
        $this->m_className = "NameRepertory";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["name_Repertory"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>