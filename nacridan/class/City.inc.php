<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class City extends DBObject
{

    public $m_characLabel;

    function City()
    {
        parent::DBObject();
        $this->m_tableName = "City";
        $this->m_className = "City";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["money"] = "";
        $this->m_attr["captured"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["bigpic"] = "";
        $this->m_attr["description"] = "";
        $this->m_attr["emerald"] = "";
        $this->m_attr["ruby"] = "";
        $this->m_attr["wood"] = "";
        $this->m_attr["linen"] = "";
        $this->m_attr["iron"] = "";
        $this->m_attr["leather"] = "";
        $this->m_attr["scale"] = "";
        $this->m_attr["root"] = "";
        $this->m_attr["seed"] = "";
        $this->m_attr["leaf"] = "";
        $this->m_attr["door"] = "";
        $this->m_attr["accessTemple"] = "";
        $this->m_attr["loyalty"] = "";
        $this->m_attr["putsch"] = "";
        $this->init();
        
        $this->m_characLabel = array(
            "emerald",
            "ruby",
            "wood",
            "linen",
            "iron",
            "leather",
            "scale",
            "root",
            "seed",
            "leaf",
            "door"
        );
    }

    function getArrayRes()
    {
        $res = array();
        for ($i = 0; $i < 10; $i ++) {
            $res[$this->m_characLabel[$i]] = $this->m_attr[$this->m_characLabel[$i]];
        }
        
        return $res;
    }
}
?>
