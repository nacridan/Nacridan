<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamRank extends DBObject
{

    function TeamRank()
    {
        parent::DBObject();
        $this->m_tableName = "TeamRank";
        $this->m_className = "TeamRank";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_TeamRankInfo"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>