<?php

/**
 *  Define the Building class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Building extends DBObject
{

    function Building()
    {
        parent::DBObject();
        $this->m_tableName = "Building";
        $this->m_className = "Building";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["id_BasicBuilding"] = "";
        $this->m_attr["id_City"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["value"] = "";
        $this->m_attr["school"] = "";
        $this->m_attr["sp"] = "";
        $this->m_attr["currsp"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["progress"] = "";
        $this->m_attr["repair"] = "";
        $this->m_attr["money"] = "";
        $this->m_attr["profit"] = "";
        $this->init();
    }
}

?>
