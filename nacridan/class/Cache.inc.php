<?php

/**
 * Class Cache
 *
 * The class responsible for caching utilities.
 */
class Cache
{
    /**
     * The the URL of the file with latest cache date.
     *
     * @param $location string The file relative path
     * @return string The file URL with cache information.
     */
    public static function get_cached_file($location)
    {
        $filepath = HOMEPATH . $location;
        $fileHost = CONFIG_HOST . $location;
        if (file_exists($filepath)) {
            $time = filemtime($filepath);
            $fileHost .= '?'.$time;
        } else {
            feLogMessage('The file "'.$filepath.'" hasn\'t been found.');
        }
        return $fileHost;
    }

} 