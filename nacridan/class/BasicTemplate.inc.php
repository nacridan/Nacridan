<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicTemplate extends DBObject
{

    function BasicTemplate()
    {
        parent::DBObject();
        $this->m_tableName = "BasicTemplate";
        $this->m_className = "BasicTemplate";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_realDBName["id_Modifier"] = "id_Modifier_BasicTemplate";
        $this->m_attr["id_Modifier"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["name2"] = "";
        foreach ($this->m_realDBName as $key => $val) {
            $this->m_realAPIName[$val] = $key;
        }
        $this->init();
    }
}

?>