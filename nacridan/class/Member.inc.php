<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Member extends DBObject
{

    function Member()
    {
        parent::DBObject();
        $this->m_tableName = "Member";
        $this->m_className = "Member";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["login"] = "";
        $this->m_attr["password"] = "";
        $this->m_attr["authlevel"] = "";
        $this->m_attr["lang"] = "";
        $this->m_attr["utc"] = "";
        $this->m_attr["newplayer"] = "";
        $this->m_attr["maxNbPlayers"] = "";
        $this->m_attr["display"] = "";
        $this->m_attr["disabled"] = "";
        $this->m_attr["firstlog"] = "";
        $this->m_attr["lastlog"] = "";
        $this->init();
    }
}

?>
