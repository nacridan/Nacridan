<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class PJAlliancePJ extends DBObject
{

    function PJAlliancePJ()
    {
        parent::DBObject();
        $this->m_tableName = "PJAlliancePJ";
        $this->m_className = "PJAlliancePJ";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player\$src"] = "";
        $this->m_attr["id_Player\$dest"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["datestart"] = "";
        $this->m_attr["datestop"] = "";
        $this->init();
    }
}

?>