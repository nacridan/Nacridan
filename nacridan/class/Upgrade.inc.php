<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Upgrade extends DBObject
{

    function Upgrade()
    {
        parent::DBObject();
        $this->m_tableName = "Upgrade";
        $this->m_className = "Upgrade";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["hp"] = "";
        $this->m_attr["strength"] = "";
        $this->m_attr["dexterity"] = "";
        $this->m_attr["speed"] = "";
        $this->m_attr["magicSkill"] = "";
        $this->init();
    }
}

?>