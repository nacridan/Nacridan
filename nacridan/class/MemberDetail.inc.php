<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MemberDetail extends DBObject
{

    function MemberDetail()
    {
        parent::DBObject();
        $this->m_tableName = "MemberDetail";
        $this->m_className = "MemberDetail";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Member"] = "";
        $this->m_attr["email"] = "";
        $this->m_attr["address"] = "";
        $this->m_attr["postcode"] = "";
        $this->m_attr["city"] = "";
        $this->m_attr["country"] = "";
        $this->m_attr["state"] = "";
        $this->m_attr["phone"] = "";
        $this->m_attr["firstname"] = "";
        $this->m_attr["lastname"] = "";
        $this->m_attr["sex"] = "";
        $this->m_attr["height"] = "";
        $this->m_attr["weight"] = "";
        $this->m_attr["aircolor"] = "";
        $this->m_attr["eyecolor"] = "";
        $this->m_attr["born"] = "";
        $this->m_attr["job"] = "";
        $this->m_attr["smoker"] = "";
        $this->init();
    }
}

?>