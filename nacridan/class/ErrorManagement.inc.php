<?php

class ErrorManagement
{

    const DBLOGFILE = "nacridan.log";

    const FELOGFILE = "nacridanFE.log";

    static function dbLogMessage($msg)
    {
        /*
         * if (DEBUG_MODE == 1) {
         * $fp = fopen(self::DBLOGFILE, "a");
         * $now = DateTime::createFromFormat('U.u', microtime(true));
         * fwrite($fp, $now->format("m-d-Y H:i:s") . $msg . "\n");
         * fclose($fp);
         * }
         */
        if (DEBUG_MODE == 1) {
            $fp = fopen(LOG_FOLDER_PATH . self::DBLOGFILE, "a");
            fwrite($fp, gmdate("Y/m/d H:i:s ") . $msg . "\n");
            fclose($fp);
        }
        redirect(CONFIG_HOST . "/main/errordefaultpage.php");
    }

    static function dbLogMessageDebug($msg)
    {
        $fp = fopen(LOG_FOLDER_PATH . self::DBLOGFILE, "a");
        fwrite($fp, gmdate("Y/m/d H:i:s ") . $msg . "\n");
        fclose($fp);
    }

    static function feLogMessage($msg)
    {
        $fp = fopen(LOG_FOLDER_PATH . self::FELOGFILE, "a");
        fwrite($fp, gmdate("Y/m/d H:i:s ") . $msg . "\n");
        fclose($fp);
    }

    static function debug_string_backtrace_error()
    {
        ob_start();
        debug_print_backtrace();
        $trace = ob_get_contents();
        ob_end_clean();
        
        // Remove first item from backtrace as it's this function which
        // is redundant.
        $trace = preg_replace('/^#0\s+' . __FUNCTION__ . "[^\n]*\n/", '', $trace, 1);
        
        // Renumber backtrace items.
        
        $trace = preg_replace_callback('/^#(\d+)/m', function ($m)
        {
            return '\'#\' . (' . ($m[1] - 1) . ')';
        }, $trace);
        
        return $trace;
    }

    static function nacridanErrorHandler($errno, $errstr, $errfile, $errline)
    {
        $errRpt = error_reporting();
        // echo $errno;
        if (($errno & $errRpt) != $errno)
            return;
        $msg = "[$errno] $errstr (@line $errline in file $errfile).";
        nacridanFatalError($msg);
    }

    static function nacridanExceptionHandler($exception)
    {
        self::feLogMessage(ErrorManagement::debug_string_backtrace_error());
        redirect(CONFIG_HOST . "/main/errordefaultpage.php");
    }

    static function nacridanFatalError($msg)
    {
        self::feLogMessage($msg);
        self::feLogMessage(ErrorManagement::debug_string_backtrace_error());
        redirect(CONFIG_HOST . "/main/errordefaultpage.php");
    }
}
?>