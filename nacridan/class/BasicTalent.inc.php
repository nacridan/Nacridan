<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicTalent extends DBObject
{

    function BasicTalent()
    {
        parent::DBObject();
        $this->m_tableName = "BasicTalent";
        $this->m_className = "BasicTalent";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["ident"] = "";
        $this->m_attr["PA"] = "";
        $this->m_attr["type"] = "";
        $this->init();
    }
}

?>
