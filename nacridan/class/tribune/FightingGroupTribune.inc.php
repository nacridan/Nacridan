<?php

/**
 * Class FightingGroupTribune
 *
 * Tribune du Groupe de Chasse.
 */
class FightingGroupTribune extends Tribune
{

    protected function getType()
    {
        return Tribune::TYPE_FIGHTING_GROUP;
    }

    protected function getGroupMessageTableName()
    {
        return 'FighterGroupMsg';
    }

    protected function getGroupTableName()
    {
        return 'FighterGroup';
    }

    protected function getPlayerReadTimeField()
    {
        return 'time_FighterGroupMsg_read';
    }

    protected function getGroupForeignKeyField()
    {
        return 'id_FighterGroup';
    }

    protected function getTitle()
    {
        return 'La Tribune du Groupe de Chasse';
    }

    public function getShortTitle()
    {
        return 'Groupe de Chasse';
    }

    protected function getTribuneTimeReadable()
    {
        return TRIBUNE_TIME_READABLE;
    }

    protected function getTribuneMaxTimeReadable()
    {
        return TRIBUNE_MAX_TIME_READABLE;
    }

    protected function getUnallowedMessage()
    {
        $str  = '<p class="important">Ceci est un chat persistant dédié aux membres d\'un même groupe de chasse.<br/>';
        $str .= 'Vous devez faire partie d\'un GdC (groupe de chasse) pour en profiter.</p>';
        $str .= '<p class="important"><b>Rappel</b> : <i>Diplomatie &#8212;> GdC &#8212;> Inviter</i></p>';
        $str .= '</div>';
        return $str;
    }

} 