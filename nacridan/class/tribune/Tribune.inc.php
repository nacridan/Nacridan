<?php

/**
 * Class Tribune
 *
 * La classe principale pour gérer les tribunes.
 *
 * Pour créer un nouveau type de tribune :
 * Voir class/tribune/howto.php
 *
 * Vocabulaire :
 * - Group : L'unité pour laquelle la tribune est accessible (ex. Le groupe de chasse / L'ordre)
 */
abstract class Tribune {

    const TYPE_FIGHTING_GROUP = 1;
    const TYPE_TEAM = 2;

    public $db;
    public $curplayer;
    public $nacridan;


    public function __construct(NacridanModule $nacridan, PDO $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($this->db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $lastId = 0;
        $str = '';
        $sessPlayers = $this->nacridan->loadSessPlayers();

        // Envois du message si besoin
        if (!empty($_POST["Body"])) {
            $lastId = $this->sendMessage($_POST["Body"]);
        }

        // Changement d'informatikons
        $this->checkToChangeAuthor($sessPlayers);

        $str .= '<div id="newMessages" class="hidden"><a href="">Vous avez <span id="countMessages">0</span> nouveau<span class="plural">x</span> message<span class="plural">s</span>.</a></div>';
        $str .= '<div id="scrollable_content">';
        $str .= '<h1><b>'.$this->getTitle().'</b></h1>';
        $str .= '<p id="irc_explained">Un <a class="stylepc" href="http://webchat.quakenet.org/">chat</a> IRC est également à votre disposition. ';
        $str .= 'Plus de détails pour vous connecter dans ';
        $str .= '<a class="stylepc" href="' . CONFIG_HOST . '/i18n/rules/fr/rules.php?page=step2#comtools">cette section</a> ';
        $str .= 'des règles.</p>';


        $idPlayer = $this->getIdPlayer($curplayer);
        list($lastReadMsg, $idGroup) = $this->getGroupInfos($idPlayer, $db);
        $lastReadTime = gmstrtotime($lastReadMsg);
        $lastReadDate = date("Y-m-d H:i:s", $lastReadTime);


        // On prend tous les messages vieux de plus de `getTribuneMaxTimeReadable()` secondes a partir de la date de dernier message non lu.
        // Maximum getTribuneMaxTimeReadable() secondes
        $time = max($lastReadTime - $this->getTribuneMaxTimeReadable(), time() - $this->getTribuneMaxTimeReadable());
        $cal = gmdate("Y-m-d H:i:s", $time);

        if ($idGroup > 0) {
            $dbm = $dbm = new DBCollection(
                "SELECT f.*, p1.id as idPlayer, p1.id_BasicRace, case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                " then concat(p1.racename,' de ',p2.name) else p1.name end as nameModified FROM ".$this->getGroupMessageTableName()." f left outer join Player p1 on p1.id = f.id_Player left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                ID_BASIC_RACE_FEU_FOL . ") WHERE f.date > '" . $cal . "' AND f.".$this->getGroupForeignKeyField()."=" . $idGroup . ' ORDER BY f.date ASC ', $db);

            new DBCollection(
                'UPDATE Player SET '.$this->getPlayerReadTimeField().'="'.gmdate('Y-m-d H:i:s').'" WHERE '.$this->getGroupForeignKeyField().'=' . $idGroup . ' and id_Member=' . $curplayer->get("id_Member") . ' ', $db,
                0, 0, false);

            $listSessionPlayersOfSameGroup = $this->getListOfSessionPlayersOfSameGroup($sessPlayers, $idGroup);
            $tableHeaderInfos = array();
            while (! $dbm->eof()) {

                $body = $dbm->get("body");
                $date = date("Y-m-d H:i:s", gmstrtotime($dbm->get("date")));

                // To know if the message is unread
                $readClass = '';
                if ($date > $lastReadDate && $dbm->get('id') != $lastId) {
                    $readClass = ' messageUnread';
                }

                // We get all the infos of the message to render
                $infos = $this->getAuthorInfos($dbm, $listSessionPlayersOfSameGroup, $date);

                $tableHeaderInfos[] = array(
                    '0' => array(
                        $infos,
                        'class="mainbglabel'.$readClass.'" align="center"'
                    ),
                    '1' => array(
                        bbcode($body),
                        'class="maintable'.$readClass.'" align="left"'
                    )
                );

                $dbm->next();
            }

            $str .= createTable(2, $tableHeaderInfos, array(),
                array(
                    array(
                        '<b>Nom</b>',
                        'class="mainbglabel" align="center"'
                    ),
                    array(
                        '<b>'.localize("Message").'</b>',
                        'class="mainbglabel" id="messageLabel" align="center"'
                    )
                ), "class='maintable centerareawidth'", "formid", "order");

            $str .= '</td></tr></table>'."\n";
            $str .= '</div>';

            $str .= '<form method="POST" action="" target="_self" id="tribune_form">'."\n";
            $str .= '<input type="hidden" name="type" id="tribune_type" value="'.$this->getType().'"/>';
            $str .= '<div class="inputs">';
            $str .= '<textarea id="Body" name="Body"></textarea>';

            $str .= '<button id="Send" type="submit" name="Send">' . localize('Envoyer') . '<br/><span class="shortcut">(CTRL+ENTER)</span></button>';
            $str .= '</div>';
            $str .= "</form>";
        } else {
            $str .= $this->getUnallowedMessage();
        }

        return $str;
    }

    /**
     * Add the message to DB
     *
     * @param $message
     * @return array
     */
    public function sendMessage($message)
    {
        $dbtri = new DBCollection(
            "SELECT case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
            " then p2.".$this->getGroupForeignKeyField()." else p1.".$this->getGroupForeignKeyField()." end as ".$this->getGroupForeignKeyField()." FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
            ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id=" . $this->curplayer->get("id"), $this->db);
        $messageClass = $this->getGroupMessageTableName();
        $msg = new $messageClass();
        $msg->set($this->getGroupForeignKeyField(), $dbtri->get($this->getGroupForeignKeyField()));
        $msg->set("id_Player", $this->curplayer->get("id"));
        $msg->set("body", $message);
        $date = gmdate('Y-m-d H:i:s');
        $msg->set("date", $date);
        $lastId = $msg->addDB($this->db);
        return $lastId;
    }

    /**
     * Change author of message if authorized and if it is needed
     *
     * @param $sessPlayers
     * @return string
     */
    public function checkToChangeAuthor($sessPlayers)
    {
        $idPostToChange = filter_input(INPUT_POST, 'idPostToChange', FILTER_VALIDATE_INT);
        $changePlayer = filter_input(INPUT_POST, 'changePlayer', FILTER_VALIDATE_INT);
        if (!empty($idPostToChange) && !empty($changePlayer)) {
            $r = 'UPDATE '.$this->getGroupMessageTableName().' SET id_Player=' . $changePlayer . ' WHERE id=' . $idPostToChange . ' AND id_Player in (';
            $r .= implode(',', array_keys($sessPlayers));
            $r .= ')';
            new DBCollection($r, $this->db);
        }
    }

    /**
     * Get the player ID, or it's owner if he has one
     *
     * @return mixed
     */
    public function getIdPlayer()
    {
        if ($this->curplayer->get('id_Owner') == "0") {
            $idPlayer = $this->curplayer->get('id');
        } else {
            $idPlayer = $this->curplayer->get('id_Owner');
        }
        return $idPlayer;
    }

    /**
     * Get the infos of unreads messages
     *
     * @return array(
     *  'success' => bool,
     *  'id_Group' => int,
     *  'unreads' => int
     * )
     */
    public function getUnreadMessagesInfos() {
        // On compte tous les messages vieux de plus de `$this->getTribuneTimeReadable()` secondes a partir de la date de dernier message non lu.
        // Maximum $this->getTribuneMaxTimeReadable() secondes
        $maxTime = time() - $this->getTribuneMaxTimeReadable();
        $maxDate = gmdate("Y-m-d H:i:s", $maxTime);

        $idPlayer = $this->getIdPlayer();

        $request = 'SELECT p.'.$this->getGroupForeignKeyField().', SUM(p.'.$this->getPlayerReadTimeField().' < m.date) as unreads FROM Player p LEFT JOIN '.$this->getGroupTableName().' g ON p.'.$this->getGroupForeignKeyField().'=g.id
         LEFT JOIN '.$this->getGroupMessageTableName().' m ON g.id=m.'.$this->getGroupForeignKeyField().'
         WHERE (m.date > DATE_SUB(p.'.$this->getPlayerReadTimeField().', INTERVAL ' . $this->getTribuneTimeReadable() . ' SECOND)
         AND m.date > "'.$maxDate.'")
         AND p.id=' . $idPlayer;
        $dbtri = new DBCollection($request, $this->db);

        $return = array(
            'success' => $dbtri->count() > 0,
            'id_Group' => $dbtri->get($this->getGroupForeignKeyField()),
            'unreads' => (is_null($dbtri->get("unreads")) ? 0 : $dbtri->get("unreads"))
        );
        return $return;
    }

    /**
     * Render the HTML string for the message infos.
     *
     * @param $dbm
     * @param $listSessionPlayersOfSameGroup
     * @param $date
     * @return string
     */
    public function getAuthorInfos($dbm, $listSessionPlayersOfSameGroup, $date)
    {
        $posteur = $dbm->get("nameModified");
        $idPlayer = $this->getIdPlayer();

        $infos = '';
        // If the speaking player is not a monster and neither is the current player, we add a link to write to him.
        if ($dbm->get('id_BasicRace') < 100 && $this->curplayer->get('id_Owner') == "0" && $idPlayer != $dbm->get('idPlayer')) {
            $infos .= Envelope::render($posteur);
        }

        if ($dbm->get('idPlayer') == null) {
            $infos .= '<i>Un Disparu</i>';
        } else {
            $infos .= '<a href="../conquest/profile.php?id=' . $dbm->get('idPlayer') . '" class="popupify name">' . $posteur . '</a>';
        }
        if (count($listSessionPlayersOfSameGroup) > 1
            && array_key_exists($dbm->get('idPlayer'), $listSessionPlayersOfSameGroup)
        ) {
            // Si le message à été écrit il y a moins de 5minutes
            $infos .= '<a href="#" class="changePers" title="Changer l\'auteur de ce message" alt="Changer l\'auteur de ce message">';
            $infos .= '<img src="../pics/misc/change.png"/></a>';
            $infos .= '<form class="changePersForm hidden" method="POST" action="" target="_self">';
            $infos .= '<select name="changePlayer">';
            foreach ($listSessionPlayersOfSameGroup as $id => $name) {
                $infos .= '<option value="' . $id . '">' . $name . '</option>';
            }
            $infos .= '</select>';
            $infos .= '<input type="hidden" name="idPostToChange" value="' . $dbm->get("id") . '">';
            $infos .= '<button type="submit" class="changePlayer">Changer</button>';
            $infos .= '<a href="#" class="cancel"><img src="../pics/misc/redcross.gif"/></a>';
            $infos .= '</form>';
        }
        $infos .= '<br/>';
        $infos .= '<i class="date">' . $date . '</i>';
        return $infos;
    }

    /**
     * @param $sessPlayers
     * @param $db
     * @param $idGroup
     * @return array
     */
    public function getListOfSessionPlayersOfSameGroup($sessPlayers, $idGroup)
    {
        $listSessionPlayersOfSameGroup = array();
        $r = 'SELECT p.id, p.name, p.'.$this->getGroupForeignKeyField().', p.id_Owner, p.id, p.racename FROM Player p
                        WHERE p.id IN (' . implode(',', array_keys($sessPlayers)) . ')
                        AND (p.id_BasicRace < 100 OR p.id_BasicRace=' . ID_BASIC_RACE_FEU_FOL . ')';
        $dbplayers = new DBCollection($r, $this->db);
        while (!$dbplayers->eof()) {
            if ($dbplayers->get($this->getGroupForeignKeyField()) == $idGroup || $dbplayers->get('id_Owner') != "0") {
                if ($dbplayers->get('name') != '') {
                    $name = $dbplayers->get('name');
                } else {
                    $name = $dbplayers->get('racename') . ' de ' . $sessPlayers[$dbplayers->get('id_Owner')];
                }
                $listSessionPlayersOfSameGroup[$dbplayers->get('id')] = $name;
            }
            $dbplayers->next();
        }
        return $listSessionPlayersOfSameGroup;
    }

    /**
     * Get the group information.
     *
     * The parameter order of the return array does matter.
     *
     * @param $idPlayer
     * @param $db
     * @return array(lastReadMsg, idGroupForeignKey)
     */
    private function getGroupInfos($idPlayer, $db)
    {
        $dbinfo = new DBCollection('SELECT p.'.$this->getPlayerReadTimeField().' as lastReadMsg, p.'.$this->getGroupForeignKeyField().' FROM Player p WHERE p.id=' . $idPlayer, $db);
        return array($dbinfo->get('lastReadMsg'), $dbinfo->get($this->getGroupForeignKeyField()));
    }

    protected abstract function getType();

    /**
     * Gives the name of the SQL table where the messages are.
     *
     * @return mixed
     */
    protected abstract function getGroupMessageTableName();

    /**
     * Gives the name if the SQL table where
     *
     * @return mixed
     */
    protected abstract function getGroupTableName();

    protected abstract function getPlayerReadTimeField();

    protected abstract function getGroupForeignKeyField();

    protected abstract function getTitle();

    public abstract function getShortTitle();

    protected abstract function getUnallowedMessage();

    protected abstract function getTribuneTimeReadable();

    protected abstract function getTribuneMaxTimeReadable();
} 