<?php

class TribuneFactory {

    /**
     * Get the right tribune for a given type.
     *
     * @param $nacridan
     * @param $db
     * @param $type
     * @return FightingGroupTribune|TeamTribune
     * @throws Exception
     */
    public function get($nacridan, $db, $type)
    {
        if ($type == Tribune::TYPE_FIGHTING_GROUP) {
            return new FightingGroupTribune($nacridan, $db);
        } else if ($type == Tribune::TYPE_TEAM) {
            return new TeamTribune($nacridan, $db);
        } else {
            throw new Exception('There is no tribune of this type : "' . $type . '"');
        }
    }


} 