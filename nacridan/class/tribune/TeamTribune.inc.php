<?php

/**
 * Class TeamTribune
 *
 * Tribune de l'Ordre
 */
class TeamTribune extends Tribune
{

    protected function getType()
    {
        return Tribune::TYPE_TEAM;
    }

    protected function getGroupMessageTableName()
    {
        return 'TeamMsg';
    }

    protected function getGroupTableName()
    {
        return 'Team';
    }

    protected function getPlayerReadTimeField()
    {
        return 'time_TeamMsg_read';
    }

    protected function getGroupForeignKeyField()
    {
        return 'id_Team';
    }

    protected function getTitle()
    {
        return 'La Tribune d\'Ordre';
    }

    public function getShortTitle()
    {
        return 'Ordre';
    }

    protected function getTribuneTimeReadable()
    {
        return TRIBUNE_ORDER_TIME_READABLE;
    }

    protected function getTribuneMaxTimeReadable()
    {
        return TRIBUNE_ORDER_MAX_TIME_READABLE;
    }

    protected function getUnallowedMessage()
    {
        $str  = '<p class="important">Ceci est un chat persistant dédié aux membres d\'un même Ordre.<br/>';
        $str .= 'Vous devez faire partie d\'un Ordre pour en profiter.</p>';
        $str .= '</div>';
        return $str;
    }

} 