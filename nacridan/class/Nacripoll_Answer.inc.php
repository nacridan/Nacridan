<?php

/**
 *  Define the Nacripoll_Question class. The class that characterizes the polls
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV2
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Nacripoll_Answer extends DBObject
{

    function Nacripoll_Answer()
    {
        parent::DBObject();
        $this->m_tableName = "Nacripoll_Answer";
        $this->m_className = "Nacripoll_Answer";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";        
        $this->m_attr["id_Nacripoll_Question"] = "";
        $this->m_attr["text"] = "";
		$this->m_attr["votes"] = "";        
        $this->init();
    }
}

?>
