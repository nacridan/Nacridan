<?php

/**
 *  Define the BasicBuilding class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicBuilding extends DBObject
{

    function BasicBuilding()
    {
        parent::DBObject();
        $this->m_tableName = "BasicBuilding";
        $this->m_className = "BasicBuilding";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["pic"] = "";
        $this->m_attr["price"] = "";
        $this->m_attr["solidity"] = "";
        $this->init();
    }
}

?>
