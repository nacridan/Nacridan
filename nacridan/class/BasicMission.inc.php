<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicMission extends DBObject
{

    function BasicMission()
    {
        parent::DBObject();
        $this->m_tableName = "BasicMission";
        $this->m_className = "BasicMission";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["frequency"] = "";
        $this->m_attr["success"] = "";
        $this->init();
    }
}

?>