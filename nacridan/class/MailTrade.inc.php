<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailTrade extends DBObject
{

    function MailTrade()
    {
        parent::DBObject();
        $this->m_tableName = "MailTrade";
        $this->m_className = "MailTrade";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player\$receiver"] = "";
        $this->m_attr["id_Player\$sender"] = "";
        $this->m_attr["id_MailBody"] = "";
        $this->m_attr["new"] = "";
        $this->m_attr["title"] = "";
        $this->m_attr["important"] = "";
        $this->m_attr["id_repertory"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>