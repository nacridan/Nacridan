<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicEquipment extends DBObject
{

    function BasicEquipment()
    {
        parent::DBObject();
        $this->m_tableName = "BasicEquipment";
        $this->m_className = "BasicEquipment";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Modifier_BasicEquipment"] = "";
        $this->m_attr["id_EquipmentType"] = "";
        $this->m_attr["id_BasicMaterial"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["frequency"] = "";
        $this->m_attr["durability"] = "";
        
        $this->init();
    }
}

?>
