<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailAlias extends DBObject
{

    function MailAlias()
    {
        parent::DBObject();
        $this->m_tableName = "MailAlias";
        $this->m_className = "MailAlias";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["alias"] = "";
        $this->init();
    }
}

?>