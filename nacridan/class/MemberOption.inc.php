<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MemberOption extends DBObject
{

    function MemberOption()
    {
        parent::DBObject();
        $this->m_tableName = "MemberOption";
        $this->m_className = "MemberOption";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Member"] = "";
        $this->m_attr["fightMsg"] = "";
        $this->m_attr["mailMsg"] = "";
        $this->m_attr["deadMsg"] = "";
        $this->m_attr["view2d"] = "";
        $this->m_attr["windRose"] = "";
        $this->m_attr["music"] = "";
        $this->init();
    }
}

?>