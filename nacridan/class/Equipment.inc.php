<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Equipment extends DBObject
{

    function Equipment()
    {
        parent::DBObject();
        $this->m_tableName = "Equipment";
        $this->m_className = "Equipment";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Shop"] = "";
        $this->m_attr["id_BasicEquipment"] = "";
        $this->m_attr["id_EquipmentType"] = "";
        $this->m_attr["id_Mission"] = "";
        $this->m_attr["id_Caravan"] = "";
        $this->m_attr["id_Equipment\$bag"] = "";
        $this->m_attr["id_Warehouse"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["extraname"] = "";
        $this->m_attr["description"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["po"] = "";
        $this->m_attr["durability"] = "";
        $this->m_attr["sharpen"] = "";
        $this->m_attr["state"] = "";
        $this->m_attr["inbuilding"] = "";
        $this->m_attr["room"] = "";
        $this->m_attr["hidden"] = "";
        $this->m_attr["weared"] = "";
        $this->m_attr["sell"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["progress"] = "";
        $this->m_attr["templateProgress"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["date"] = "";
        $this->m_attr["collected"] = "";
        $this->m_attr["dropped"] = "";
        $this->m_attr["maudit"] = "";
        
        $this->init();
    }
}

?>
