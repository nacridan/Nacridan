<?php

class DBCollection
{

    public $m_curIndex;

    public $m_nbElement;

    public $m_query;

    public $m_records;

    public $m_param;

    public $m_sizeLimit;

    public $m_tableName;

    public $m_className;

    public $m_errormsg;

    public $m_errorno;

    public $m_id;

    function DBCollection($param = "", &$db, $start = 0, $sizelimit = 0, $isQuery = true)
    {
        $this->m_className = "DBCollection";
        $this->init($param, $db, $start, $sizelimit, $isQuery);
    }

    function init($param = "", &$db, $start = 0, $sizeLimit = 0, $isQuery = true)
    {
        $this->m_param = $param;
        $this->m_curIndex = - 1;
        $this->m_nbElement = 0;
        $this->m_sizeLimit = $sizeLimit;
        if (isset($db)) {
            $this->m_query = sprintf("%s", $this->m_param);
            if ($isQuery) {
                if ($this->m_sizeLimit != 0) {
                    $this->m_query = sprintf("%s LIMIT %s,%s", $this->m_param, $start, $this->m_sizeLimit);
                }
                $this->execQuery($this->m_query, $db);
            } else {
                $this->execDirect($this->m_query, $db);
            }
        } else {
            $name = $this->m_className;
            trigger_error("l'object DB de la classe $name = NULL", E_USER_ERROR);
        }
    }

    protected function execDirect($query, &$db)
    {
        $result = - 1;
        
        try {
            $result = $db->exec($query);
            
            // ErrorManagement::dbLogMessageDebug($query);
        } catch (exception $e) {}
        
        $this->m_errorno = $db->errorCode();
        
        if ($result === FALSE) {
            $this->m_nbElement = 0;
            $this->m_curIndex = - 1;
        } else {
            $this->m_nbElement = $result;
            $this->m_curIndex = - 1;
        }
        
        if (! ($this->m_errorno === '00000')) {
            $this->m_errormsg = $db->errorInfo();
            $errorMessage = $query . "\n";
            $errorMessage .= "Error SQLSTATE:" . $this->m_errormsg[0] . ", error driver:" . $this->m_errormsg[1] . ", Message:" . $this->m_errormsg[2] . "\n";
            $errorMessage .= ErrorManagement::debug_string_backtrace_error() . "\n";
            dbLogMessage($errorMessage);
            return - 1;
        }
        return 0;
    }

    protected function execQuery($query, &$db)
    {
        // echo $query;
        try {
            $stmt = $db->query($query);
            
            // ErrorManagement::dbLogMessageDebug($query);
            
            $this->m_records = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (exception $e) {}
        
        $this->m_errorno = $db->errorCode();
        
        if (! empty($this->m_records)) {
            $this->m_nbElement = count($this->m_records);
            $this->m_curIndex = 0;
        } else {
            $this->m_nbElement = 0;
            $this->m_curIndex = - 1;
        }
        
        if (! ($this->m_errorno === '00000')) {
            $this->m_errormsg = $db->errorInfo();
            $errorMessage = $query . "\n";
            $errorMessage .= "Error SQLSTATE:" . $this->m_errormsg[0] . ", error driver:" . $this->m_errormsg[1] . ", Message:" . $this->m_errormsg[2] . "\n";
            $errorMessage .= ErrorManagement::debug_string_backtrace_error() . "\n";
            dbLogMessage($errorMessage);
            return - 1;
        }
        return 0;
    }

    function errorNoDB()
    {
        return ! ($this->m_errorno === '00000');
    }

    function errorMsgDB()
    {
        return $this->m_errormsg;
    }

    function first()
    {
        if (isset($this->m_records)) {
            $this->m_curIndex = 0;
        } else {
            $name = $this->m_className;
            trigger_error("l'object m_records de la classe $name = NULL", E_USER_ERROR);
        }
    }

    function count()
    {
        // if (isset($this->m_records)) {
        return $this->m_nbElement;
        // } else {
        // $name = $this->m_className;
        // trigger_error("l'object m_records de la classe $name = NULL", E_USER_ERROR);
        // }
    }

    function get($var)
    {
        return $this->m_records[$this->m_curIndex][$var];
    }

    function eof()
    {
        if (isset($this->m_records)) {
            return (($this->m_curIndex > ($this->m_nbElement - 1)) || $this->m_nbElement == 0);
        } else {
            return true;
        }
    }

    function next()
    {
        if (isset($this->m_records)) {
            
            if ($this->eof()) {
                return false;
            }
            $this->m_curIndex += 1;
            return true;
        } else {
            $name = $this->m_className;
            trigger_error("l'object m_records de la classe $name = NULL", E_USER_ERROR);
        }
    }

    function getCurObject($type)
    {
        if (isset($this->m_records)) {
            $curObject = new $type();
            $curObject->DBLoad($this);
            return $curObject;
        } else {
            return null;
        }
    }
}

?>