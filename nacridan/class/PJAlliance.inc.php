<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class PJAlliance extends DBObject
{

    function PJAlliance()
    {
        parent::DBObject();
        $this->m_tableName = "PJAlliance";
        $this->m_className = "PJAlliance";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player\$src"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["datestart"] = "";
        $this->m_attr["datestop"] = "";
        $this->init();
    }
}

?>