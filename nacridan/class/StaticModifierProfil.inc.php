<?php
require_once (HOMEPATH . "/class/DBStaticModifier.inc.php");

class StaticModifierProfil extends DBStaticModifier
{

    public $m_characLabel;

    public $m_profil;

    public $m_profilCharac;

    function StaticModifierProfil()
    {
        parent::DBStaticModifier();
        $this->m_tableName = "StaticModifierProfil";
        $this->m_className = "StaticModifierProfil";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["attack"] = "";
        $this->m_attr["attackCost"] = "";
        $this->m_attr["attackValue"] = "";
        $this->m_attr["dodge"] = "";
        $this->m_attr["dodgeCost"] = "";
        $this->m_attr["dodgeValue"] = "";
        $this->m_attr["damage"] = "";
        $this->m_attr["damageCost"] = "";
        $this->m_attr["damageValue"] = "";
        $this->m_attr["armor"] = "";
        $this->m_attr["armorCost"] = "";
        $this->m_attr["armorValue"] = "";
        $this->m_attr["regen"] = "";
        $this->m_attr["regenCost"] = "";
        $this->m_attr["regenValue"] = "";
        $this->m_attr["hp"] = "";
        $this->m_attr["hpCost"] = "";
        $this->m_attr["hpValue"] = "";
        $this->m_attr["magicSkill"] = "";
        $this->m_attr["magicSkillCost"] = "";
        $this->m_attr["magicSkillValue"] = "";
        $this->m_attr["magicResist"] = "";
        $this->m_attr["magicResistCost"] = "";
        $this->m_attr["magicResistValue"] = "";
        $this->m_attr["time"] = "";
        $this->m_attr["timeCost"] = "";
        $this->m_attr["timeValue"] = "";
        $this->init();
        
        $this->m_characLabel = array(
            "attack",
            "dodge",
            "damage",
            "armor",
            "regen",
            "hp",
            "magicSkill"
        );
        $this->initCharac();
    }

    function getRndSheet(&$field, &$dice, &$value)
    {
        $index = $this->m_profil->searchUpperOrEqual(rand(1, 100));
        if ($index == - 1)
            return;
        
        $field = $this->m_profil->getKey($index);
        // if($field="armor")
        // print_r($this->m_profil->m_map);
        
        if ($field == "")
            return;
        
        $index = $this->m_profilCharac[$field]->searchUpperOrEqual(rand(1, 100));
        
        $dice = $this->m_profilCharac[$field]->getKey($index);
        $value = $this->getModif($field . "Value", $dice);
    }

    function initCharac()
    {
        $total = 0;
        $this->m_profil = new VectorProba();
        foreach ($this->m_attr as $key => $value) {
            if ($key != "id") {
                if ($value != "") {
                    $this->m_charac[$key] = explode("/", $value);
                } else {
                    $this->set($key, "////////");
                    $this->m_charac[$key] = explode("/", "////////");
                }
                
                if (strstr($key, "Cost") == false && strstr($key, "Value") == false) {
                    $total = 0;
                    $val = 0;
                    $this->m_profilCharac[$key] = new VectorProba();
                    
                    for ($i = 0; $i < DICE_CHARAC; $i ++) {
                        $val = $this->m_charac[$key][$i];
                        if ($val > 0) {
                            $total += $val;
                            $this->m_profilCharac[$key]->pushBack($i, $val);
                        }
                    }
                    
                    $this->m_profilCharac[$key]->setInterval(100);
                    $this->m_profil->pushBack($key, $total);
                }
            }
        }
        $this->m_profil->setInterval(100);
    }
}

?>