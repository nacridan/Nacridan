<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicMagicSpell extends DBObject
{

    function BasicMagicSpell()
    {
        parent::DBObject();
        $this->m_tableName = "BasicMagicSpell";
        $this->m_className = "BasicMagicSpell";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["ident"] = "";
        $this->m_attr["school"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["PA"] = "";
        $this->m_attr["usable"] = "";
        
        $this->init();
    }
}

?>