<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Paypal extends DBObject
{

    function Paypal()
    {
        parent::DBObject();
        $this->m_tableName = "Paypal";
        $this->m_className = "Paypal";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["txn_id"] = "";
        $this->m_attr["item_name"] = "";
        $this->m_attr["item_number"] = "";
        $this->m_attr["payment_status"] = "";
        $this->m_attr["mc_gross"] = "";
        $this->m_attr["mc_fee"] = "";
        $this->m_attr["mc_currency"] = "";
        $this->m_attr["receiver_email"] = "";
        $this->m_attr["payer_email"] = "";
        $this->m_attr["post_value"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>