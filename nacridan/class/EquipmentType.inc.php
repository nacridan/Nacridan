<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class EquipmentType extends DBObject
{

    function EquipmentType()
    {
        parent::DBObject();
        $this->m_tableName = "EquipmentType";
        $this->m_className = "EquipmentType";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["wearable"] = "";
        $this->m_attr["small"] = "";
        $this->m_attr["mask"] = "";
        $this->m_attr["zone"] = "";
        $this->init();
    }
}

?>