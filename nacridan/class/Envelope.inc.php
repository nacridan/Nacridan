<?php

/**
 * Class Envelope
 *
 * This is a utility class to render the "Send message to a player" functionality
 */
class Envelope
{
    /**
     * Renders an envelope in a link to the messaging.
     *
     * @param mixed $recipients A recipient or an array of recipients
     * @param int $imgDepth The depth to the image path.
     * @return string The HTML string to render.
     */
    public static function render($recipients, $text='', $linkClasses='maintextbody', $imgDepth=1) {
        $recipientsList = $recipients;
        $recipientsDisplay = $recipients;
        if (is_array($recipients)) {
            if (count($recipients) > 1) {
              $recipientsList = implode(',', $recipients);
              $lastRecipient =  array_pop($recipients);
              $recipientsDisplay  = implode(', ', $recipients);
              $recipientsDisplay .= ' et '.$lastRecipient;
            } else {
                $recipientsList = array_shift($recipients);
                $recipientsDisplay = $recipientsList;
            }
        }

        $html  = '<a href="../conquest/conquest.php?center=compose&to=' . urlencode($recipientsList) . '"';
        $html .= ' class="message-envelope '.$linkClasses.'"';
        $html .= ' title="Envoyer un message à ' . $recipientsDisplay . '">';
        $html .= '<img alt="Envoyer un message à ' . $recipientsDisplay . '" src="'.str_repeat('../', $imgDepth).'pics/misc/wrap.gif"/>';
        $html .= $text;
        $html .= '</a>';
        return $html;
    }

    /**
     * Render a size equal to the envelope to allow alignment between text which is preceded by envelope and text which is not.
     */
    public static function renderEmptySize() {
        return '<span class="message-envelope-empty"></span>';
    }
}