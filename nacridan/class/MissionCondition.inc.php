<?php

/**
 *  Define the MissionCondition class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MissionCondition extends DBObject
{

    function MissionCondition()
    {
        parent::DBObject();
        $this->m_tableName = "MissionCondition";
        $this->m_className = "MissionCondition";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["idMission"] = "";
        $this->m_attr["positionXstart"] = "";
        $this->m_attr["positionYstart"] = "";
        $this->m_attr["positionXstop"] = "";
        $this->m_attr["positionYstop"] = "";
        $this->m_attr["distanceX"] = "";
        $this->m_attr["distanceY"] = "";
        $this->m_attr["idEscort"] = "";
        $this->m_attr["EscortPositionX"] = "";
        $this->m_attr["EscortPositionY"] = "";
        $this->m_attr["idEquip"] = "";
        $this->m_attr["EquipPositionX"] = "";
        $this->m_attr["EquipPositionY"] = "";
        $this->m_attr["idPNJtoKill"] = "";
        $this->m_attr["EnigmAnswer"] = "";
        $this->init();
    }
}

?>
