<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Banned extends DBObject
{

    function Banned()
    {
        parent::DBObject();
        $this->m_tableName = "Banned";
        $this->m_className = "Banned";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Player\$target"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>