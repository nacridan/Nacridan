<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Template extends DBObject
{

    function Template()
    {
        parent::DBObject();
        $this->m_tableName = "Template";
        $this->m_className = "Template";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Equipment"] = "";
        $this->m_attr["id_BasicTemplate"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["pos"] = "";
        $this->init();
    }
}

?>