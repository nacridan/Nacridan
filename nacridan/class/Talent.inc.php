<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Talent extends DBObject
{

    function Talent()
    {
        parent::DBObject();
        $this->m_tableName = "Talent";
        $this->m_className = "Talent";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicTalent"] = "";
        $this->m_attr["skill"] = "";
        $this->init();
    }
}

?>