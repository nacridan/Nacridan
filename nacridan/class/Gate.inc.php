<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Gate extends DBObject
{

    function Gate()
    {
        parent::DBObject();
        $this->m_tableName = "Gate";
        $this->m_className = "Gate";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["npcBand"] = "";
        $this->m_attr["nbNPC"] = "";
        $this->m_attr["activation"] = "";
        $this->init();
    }

    function createGate($land, $levelbase, $mapinfo, &$db)
    {
        if ($land == null) {
            trigger_error("Gate:createNPC Type de terrain inconnu", E_USER_ERROR);
        }
        
        if ($this->m_attr["id"] != "") {
            $time = time();
            $time += rand(3600, 6 * 3600);
            
            $nbNPC = $this->get("nbNPC");
            $nb = rand(1, $nbNPC);
            
            $this->set("nbNPC", $nbNPC - $nb);
            
            $this->set("activation", gmdate("Y-m-d H:i:s", $time));
            $this->updateDB($db);
        } else {
            trigger_error("Gate ID is null, can't create NPC", E_USER_ERROR);
        }
        return true;
    }
}

?>
