<?php

/**
 *  Define the Building class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BuildingEvent extends DBObject
{

    function BuildingEvent()
    {
        parent::DBObject();
        $this->m_tableName = "BuildingEvent";
        $this->m_className = "BuildingEvent";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_City"] = "";
        $this->m_attr["id_BuildingAction"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicEvent"] = "";
        $this->m_attr["id_Building"] = "";
        $this->m_attr["price"] = "";
        $this->m_attr["profit"] = "";
        $this->m_attr["date"] = "";
        
        $this->init();
    }
}

?>
