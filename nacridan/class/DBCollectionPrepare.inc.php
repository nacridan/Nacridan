<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class DBCollectionPrepare extends DBCollection
{

    function DBCollectionPrepare($param = "", &$db, $start, $sizelimit, $isQuery, $arrayValue)
    {
        $this->m_className = "DBCollectionPrepare";
        $this->initPrepare($param, $db, $start, $sizelimit, $isQuery, $arrayValue);
    }

    function initPrepare($param = "", &$db, $start, $sizeLimit, $isQuery, $arrayValue)
    {
        $this->m_param = $param;
        $this->m_curIndex = - 1;
        $this->m_nbElement = 0;
        $this->m_sizeLimit = $sizeLimit;
        if (isset($db)) {
            $this->m_query = sprintf("%s", $this->m_param);
            if ($isQuery) {
                if ($this->m_sizeLimit != 0) {
                    $this->m_query = sprintf("%s LIMIT %s,%s", $this->m_param, $start, $this->m_sizeLimit);
                }
                $this->execQueryPrepare($this->m_query, $arrayValue, $db);
            } else {
                $this->execDirectPrepare($this->m_query, $arrayValue, $db);
            }
        } else {
            $name = $this->m_className;
            trigger_error("l'object DB de la classe $name = NULL", E_USER_ERROR);
        }
    }

    protected function execDirectPrepare($query, $arrayValue, &$db)
    {
        $result = - 1;
        
        try {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($arrayValue);
            
            // ErrorManagement::dbLogMessageDebug($query);
        } catch (exception $e) {}
        
        $this->m_errorno = $db->errorCode();
        
        if ($result === FALSE) {
            $this->m_nbElement = 0;
            $this->m_curIndex = - 1;
        } else {
            $this->m_nbElement = $result;
            $this->m_curIndex = - 1;
        }
        
        if (! ($this->m_errorno === '00000')) {
            $this->m_errormsg = $db->errorInfo();
            $errorMessage = $query . "\n";
            $errorMessage .= "Error SQLSTATE:" . $this->m_errormsg[0] . ", error driver:" . $this->m_errormsg[1] . ", Message:" . $this->m_errormsg[2] . "\n";
            $errorMessage .= ErrorManagement::debug_string_backtrace_error() . "\n";
            dbLogMessage($errorMessage);
            return - 1;
        }
        return 0;
    }

    protected function execQueryPrepare($query, $arrayValue, &$db)
    {
        // echo $query;
        try {
            $stmt = $db->prepare($query);
            $stmt->execute($arrayValue);
            // ErrorManagement::dbLogMessageDebug($query);
            
            $this->m_records = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (exception $e) {}
        
        $this->m_errorno = $db->errorCode();
        
        if (! empty($this->m_records)) {
            $this->m_nbElement = count($this->m_records);
            $this->m_curIndex = 0;
        } else {
            $this->m_nbElement = 0;
            $this->m_curIndex = - 1;
        }
        
        if (! ($this->m_errorno === '00000')) {
            $this->m_errormsg = $db->errorInfo();
            $errorMessage = $query . "\n";
            $errorMessage .= "Error SQLSTATE:" . $this->m_errormsg[0] . ", error driver:" . $this->m_errormsg[1] . ", Message:" . $this->m_errormsg[2] . "\n";
            $errorMessage .= ErrorManagement::debug_string_backtrace_error() . "\n";
            dbLogMessage($errorMessage);
            return - 1;
        }
        return 0;
    }
}

?>