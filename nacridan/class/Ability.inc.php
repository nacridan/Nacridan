<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Ability extends DBObject
{

    function Ability()
    {
        parent::DBObject();
        $this->m_tableName = "Ability";
        $this->m_className = "Ability";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicAbility"] = "";
        $this->m_attr["skill"] = "";
        $this->init();
    }
}

?>
