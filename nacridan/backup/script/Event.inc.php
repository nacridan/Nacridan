 class Event extends DBObject{ function getEvents() { global $_EVENT; }

static function sendEmail($type,$email,$killed,$name,&$res){

if(SMTPDOMAIN!="nacridan.com") return;

require_once(HOMEPATH."/lib/swift/Swift.php");
require_once(HOMEPATH."/lib/swift/Swift/Connection/SMTP.php");

switch($type) { case SPELL_FIREBALL: case ATTACK: if($killed) {
$subject="[Nacridan] Évènement de type mort"; $message = "Votre
Personnage (".$name.") est mort\n\n"; $message .= "Vous pouvez dés à
present consulter les détails sur ".CONFIG_ROOT." dans la section
'Évènements'\n\n\n\n"; $messagehtml = "Votre Personnage (".$name.") est
mort
<br />
<br />
"; $messagehtml .= "Vous pouvez dés à present consulter les détails sur
<a href='".CONFIG_ROOT."'>Nacridan</a>
dans la section 'Évènements'
<br />
<br />
<br />
<br />
"; } else { $subject="[Nacridan] Évènement de type combat"; $message =
"Votre Personnage (".$name.") a été impliqué dans un combat\n\n";
$message .= "Vous pouvez dés à present consulter les détails du combat
sur ".CONFIG_ROOT." dans la section 'Évènements'\n\n\n\n"; $messagehtml
= "Votre Personnage (".$name.") a été impliqué dans un combat
<br />
<br />
"; $messagehtml .= "Vous pouvez dés à present consulter les détails du
combat sur
<a href='".CONFIG_ROOT."'>Nacridan</a>
dans la section 'Évènements'
<br />
<br />
<br />
<br />
"; } $message .= "-------------------------------------\n"; $message .=
"Rappel : Si vous ne désirez plus recevoir cet E-Mail, allez dans la
section 'Options' puis 'membre' de votre compte pour désactiver l'envoi
automatique.\n"; $message .= "-------------------------------------\n";


$messagehtml .= "-------------------------------------
<br />
"; $messagehtml .= "Rappel : Si vous ne désirez plus recevoir cet
E-Mail, allez dans la section 'Options' puis 'membre' de votre compte
pour désactiver l'envoi automatique.
<br />
"; $messagehtml .= "-------------------------------------
<br />
"; break; default: return -1; break; } $to = $email; $mailer = new
Swift(new Swift_Connection_SMTP(SMTPHOST)); if ($mailer->isConnected())
//Optional { if(SMTPLOGIN!="" && SMTPPASS!="") {
require_once(HOMEPATH."/lib/swift/Swift/Authenticator/PLAIN.php");
$mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
$mailer->authenticate(SMTPLOGIN, SMTPPASS); } //Add as many parts as you
need here $mailer->addPart($message); $mailer->addPart($messagehtml,
'text/html'); $mailer->setCharset("UTF-8"); $mailer->send( $to,
"Nacridan
<noreply@".SMTPDOMAIN.".>", $subject ); $mailer->close(); } } static
function logAction(&$db,$action,&$param) {
require_once(HOMEPATH."/class/HFightMsg.inc.php");
require_once(HOMEPATH."/class/HGiveXPMsg.inc.php");
require_once(HOMEPATH."/class/HGiveMoneyMsg.inc.php"); if($action!=null)
{ if(isset($param["PLAYER_ID"])) { $idPlayer=$param["PLAYER_ID"];
$idMember=$param["MEMBER_ID"]; $racePlayer=$param["PLAYER_RACE"];
$event=new Event(); $event->set("id_Member",$idMember);
$event->set("id_Player\$src",$idPlayer);

$event->set("id_BasicRace\$src",$racePlayer);

$event->set("typeAction",$action); $datetime=gmdate("Y-m-d H:i:s");
$event->set("date",$datetime); /* if(isset($_SERVER['HTTP_VIA'])) {
if(strstr($_SERVER['HTTP_VIA'], "google")!=false) {
if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { $iparray =
explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
$_SERVER['REMOTE_ADDR']=$iparray[0]; } } } */
if(isset($_SERVER['REMOTE_ADDR']))
$event->set("ip",$_SERVER['REMOTE_ADDR']); $event->set("active",1);
$figthMsg=null; $deadMsg=null; $targetEmail=null;
if(isset($param["TARGET_ID"]) && !empty($param["TARGET_ID"])) { $dbs=new
DBCollection("SELECT fightMsg,deadMsg,email FROM Player LEFT JOIN Member
ON Member.id=Player.id_Member LEFT JOIN MemberOption ON
Member.id=MemberOption.id_Member LEFT JOIN MemberDetail ON
Member.id=MemberDetail.id_Member WHERE
Player.id=".$param["TARGET_ID"],$db); if(!$dbs->eof()) {
$deadMsg=$dbs->get("deadMsg"); $figthMsg=$dbs->get("fightMsg");
$targetEmail=$dbs->get("email"); } } switch ($action) { case MOVE:
$event->set("typeEvent",MOVE_EVENT); $event->addDB($db); break; case
TRAINING: $event->set("typeEvent",TRAINING_EVENT); $event->addDB($db);
break; case ENHANCE: $event->set("typeEvent",ENHANCE_EVENT);
$event->addDB($db); break; case SPELL_FIREBALL:
$idTarget=$param["TARGET_ID"]; $raceTarget=$param["TARGET_RACE"];
$killed=$param["TARGET_KILLED"]; if($killed==1 && $deadMsg==1) {
self::sendEmail($action,$targetEmail,$killed,$param["TARGET_NAME"],$res);
} if($killed!=1 && $figthMsg==1) {
self::sendEmail($action,$targetEmail,$killed,$param["TARGET_NAME"],$res);
} $event->set("id_Player\$dest",$idTarget);
$event->set("id_BasicRace\$dest",$raceTarget);

$serial=serialize($param); $fightmsg=new HMagicalFightMsg();
$fightmsg->set("body",$serial); $fightmsg->set("date",$datetime);
$fightmsg->set("id_Player\$src",$idPlayer);
$fightmsg->set("id_Player\$dest",$idTarget);

$idfight=$fightmsg->addDB($db); $event->set("type","HMagicalFightMsg");
$event->set("idtype",$idfight);
$event->set("typeEvent",$param["TYPE_ATTACK"]); $event->addDB($db);
break; case SPELL_REGEN:
$event->set("id_Player\$dest",$param["TARGET_ID"]);
$event->set("id_BasicRace\$dest",$param["TARGET_RACE"]);
$event->set("typeEvent",SPELL_REGEN_EVENT); $event->addDB($db); break;
case SPELL_HYPNO: $event->set("id_Player\$dest",$param["TARGET_ID"]);
$event->set("id_BasicRace\$dest",$param["TARGET_RACE"]);
$event->set("typeEvent",SPELL_HYPNO_EVENT); $event->addDB($db); break;
case SPELL_SHIELD: $event->set("id_Player\$dest",$param["TARGET_ID"]);
$event->set("id_BasicRace\$dest",$param["TARGET_RACE"]);
$event->set("typeEvent",SPELL_SHIELD_EVENT); $event->addDB($db); break;
case ABILITY_ENCIRCLE:
$event->set("id_Player\$dest",$param["TARGET_ID"]);
$event->set("id_BasicRace\$dest",$param["TARGET_RACE"]);
$event->set("typeEvent",ABILITY_ENCIRCLE_EVENT); $event->addDB($db);
break; case ABILITY_INSULT:
$event->set("id_Player\$dest",$param["TARGET_ID"]);
$event->set("id_BasicRace\$dest",$param["TARGET_RACE"]);
$event->set("typeEvent",ABILITY_INSULT_EVENT); $event->addDB($db);
break; case ABILITY_ESCAPE:
$event->set("typeEvent",ABILITY_ESCAPE_EVENT); $event->addDB($db);
break; case ABILITY_DAMAGE: case ABILITY_ATTACK: $param["ABILITY"]=1;
case ATTACK: $idTarget=$param["TARGET_ID"];
$raceTarget=$param["TARGET_RACE"]; $killed=$param["TARGET_KILLED"];

if($killed==1 && $deadMsg==1) {
self::sendEmail($action,$targetEmail,$killed,$param["TARGET_NAME"],$res);
} if($killed!=1 && $figthMsg==1) {
self::sendEmail($action,$targetEmail,$killed,$param["TARGET_NAME"],$res);
} $event->set("id_Player\$dest",$idTarget);

$event->set("id_BasicRace\$dest",$raceTarget);

$serial=serialize($param); $fightmsg=new HFightMsg();
$fightmsg->set("body",$serial); $fightmsg->set("date",$datetime);
$fightmsg->set("id_Player\$src",$idPlayer);
$fightmsg->set("id_Player\$dest",$idTarget);

$idfight=$fightmsg->addDB($db); $event->set("type","HFightMsg");
$event->set("idtype",$idfight);
$event->set("typeEvent",$param["TYPE_ATTACK"]); $event->addDB($db);
break; case PICK_UP_OBJECT: $event->set("typeEvent",PICK_UP_EVENT);
$event->addDB($db); break; case DROP_OBJECT:
$event->set("typeEvent",DROP_EVENT); $event->addDB($db); break; case
USE_OBJECT: $idTarget=$param["TARGET_ID"];
$raceTarget=$param["TARGET_RACE"];

$event->set("id_Player\$dest",$idTarget);
$event->set("id_BasicRace\$dest",$raceTarget);

$event->set("typeEvent",USE_OBJECT_EVENT); $event->addDB($db); break;

case GIVE_XP: $idTarget=$param["TARGET_ID"];
$raceTarget=$param["TARGET_RACE"];

$event->set("id_Player\$dest",$idTarget);
$event->set("id_BasicRace\$dest",$raceTarget);

$serial=serialize($param); $objectmsg=new HGiveXPMsg();
$objectmsg->set("body",$serial); $objectmsg->set("date",$datetime);
$objectmsg->set("id_Player\$src",$idPlayer);
$objectmsg->set("id_Player\$dest",$idTarget);

$idobject=$objectmsg->addDB($db); $event->set("type","HGiveXPMsg");
$event->set("idtype",$idobject); $event->set("typeEvent",GIVE_XP_EVENT);
$event->addDB($db); break; case GIVE_MONEY:
$idTarget=$param["TARGET_ID"]; $raceTarget=$param["TARGET_RACE"];

$event->set("id_Player\$dest",$idTarget);
$event->set("id_BasicRace\$dest",$raceTarget);

$serial=serialize($param); $objectmsg=new HGiveMoneyMsg();
$objectmsg->set("body",$serial); $objectmsg->set("date",$datetime);
$objectmsg->set("id_Player\$src",$idPlayer);
$objectmsg->set("id_Player\$dest",$idTarget);

$idobject=$objectmsg->addDB($db); $event->set("type","HGiveMoneyMsg");
$event->set("idtype",$idobject);
$event->set("typeEvent",GIVE_MONEY_EVENT); $event->addDB($db); break;

case USE_PARCHMENT: $event->set("typeEvent",USE_PARCHMENT_EVENT);
$event->addDB($db); break; case WEAR_EQUIPMENT: //$event->addDB($db);
break; case UNWEAR_EQUIPMENT: //$event->addDB($db); break; default:
break; } } } } }