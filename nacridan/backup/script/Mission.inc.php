<?php

class Mission extends DBObject{
{
  public $infos;
  public $conds;
  public $actions;
  public $objs;
  public $objsClass;
  
  public function addCond($x, $y, $cond)
{
    $this->infos[$x . ":" . $y] = 1;
    $this->conds[$x . ":" . $y] = array(
        "c" => $cond
    );
}
  
  public function addAction($x,$y,$type,$value)
  {
    $this->actions[$x.":".$y][]=array("t" => $type, "v" => $value);
  }
  
  public function addObject($name,$value,&$db)
  {
    if(is_object($value))
      {
	$this->objsClass[$name]=get_class($value);
      }
    else
      {
	$this->objsClass[$name]=null;
      }
    $value->set("hidden",1);
    $value->set("incity",1);
    $id=$value->addDBr($db);
    $this->objs[$name]=$id;
  }
  
  private function winMoney(&$playerSrc,$value)
  {
    $playerSrc->set("money",$playerSrc->get("money")+$value);
  }
  
  private function winXP(&$playerSrc,$value)
  {
    $playerSrc->set("xp",$playerSrc->get("xp")+$value);
  }
  
  private function winEquipments(&$playerSrc,$equipNames,$mid,&$db)
  {
    if(!is_array($equipNames))
      $equipNames=array($equipNames);

    foreach($equipNames as $equipName)
    {
      //, id_Player=".$playerSrc->get("id")."
      $dbp = new DBCollection("UPDATE Equipment SET state='Ground',x=".$playerSrc->get("x").",y=".$playerSrc->get("y").",hidden=0,incity=0,id_Mission=".$mid." WHERE id=".$this->objs[$equipName],$db,0,0,false);
    }
  }
  
  function winPlayers(&$playerSrc,$playerNames,$mid,&$db)
  {
    if(!is_array($playerNames))
      $playerNames=array($playerNames);
    
    foreach($playerNames as $playerName)
      {
	$dbp = new DBCollection("UPDATE Player SET hidden=0,incity=0,state2=1,id_Member=".$playerSrc->get("id_Member").",id_Player\$target2=".$playerSrc->get("id").",id_Mission=".$mid.",creation=\"".gmdate("Y-m-d H:i:00")."\" WHERE id=".$this->objs[$playerName],$db,0,0,false);
      }
  }

  function free($idMember,$objNames,$extra,&$db)
  {
    if(!is_array($objNames))
      $objNames=array($objNames);
    
    foreach($objNames as $objName)
      {
	$dbp = new DBCollection("UPDATE Player SET id_Member=".$idMember.",id_Mission=0".$extra." WHERE id=".$this->objs[$objName],$db,0,0,false);
      }
  }
  
  function delPlayers(&$playerSrc,$playerNames,&$db)
  {
    if(!is_array($playerNames))
      $playerNames=array($playerNames);
    
    foreach($playerNames as $playerName)
    {
      $player=new Player();
      $player->load($this->objs[$playerName],$db);
      $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=".$player->get("id"),$db,0,0,false);
      $dbt = new DBCollection("DELETE FROM Modifier WHERE id=".$player->get("id_Modifier"),$db,0,0,false);
      $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=".$player->get("id_Upgrade"),$db,0,0,false);
      $dbt = new DBCollection("DELETE FROM Player WHERE id=".$player->get("id"),$db,0,0,false);    
    }
  }
  
  public function solve(&$p1,$mid,$eqid,&$param,&$db)
    {
      $x=$p1->get("x");
      $y=$p1->get("y");
      if(isset($this->conds[$x.":".$y]))
	{
	  if(is_array($this->objs))
	    {
	      foreach($this->objs as $objname=>$id)
		{
		  if(strstr($this->conds[$x.":".$y]["c"],"\$".$objname))
		    {
		      $classname=$this->objsClass[$objname];
		      $$objname=new $classname();
		      $$objname->load($id,$db);
		    }
		}
	    }

          eval($this->conds[$x.":".$y]["c"]);

	  if($success=="true")
	    {
	      foreach($this->actions[$x.":".$y] as $action)
		{
		  switch($action["t"])
		    {
		    case WIN_MONEY:
		      $this->winMoney($p1,$action["v"],$db);
		      break;
		    case WIN_XP:
		      $this->winXP($p1,$action["v"],$db);
		      break;
		    case WIN_EQUIP:
		      $this->winEquipments($p1,$action["v"],0,$db);
		      break;
		    case ADD_PLAYER:
		      $this->winPlayers($p1,$action["v"],$mid,$db);
		      break;
		    case DEL_PLAYER:
		      $this->delPlayers($p1,$action["v"],$db);
		      break;
		    case WIN_PLAYER:
		      $this->free($p1->get("id_Member"),$action["v"],",state2=0,id_Mission=0,id_Player\$target2=0",$db);
		      break;
		    case FREE_PLAYER:
		      $this->free(0,$action["v"],",state2=0,id_Player\$target2=0",$db);
		      break;
		    default:
		      break;
		    }	    
		}  
              $p1->updateDB($db);   
	      unset($this->conds[$x.":".$y]);
              $this->set("id",$mid);
	      $this->set("content",serialize($this));
	      if(!empty($this->conds))
		{
		  $this->updateDB($db);
		}
	      else
		{
		  $dbd=new DBCollection("DELETE FROM Equipment WHERE id=".$eqid,$db,0,0,false);
		  $dbd=new DBCollection("DELETE FROM Mission WHERE id=".$mid,$db,0,0,false);
		  return "end";
		}
	      
	      return "true";
	    }
	  if($success=="null")
	    {
	      $dbd=new DBCollection("DELETE FROM Equipment WHERE id=".$eqid,$db,0,0,false);
	      $dbd=new DBCollection("DELETE FROM Mission WHERE id=".$mid,$db,0,0,false);	
	       return "null";
	    }
	  return "false";
	}
      else
        {
	  return "false";
	}
    }
}

?>