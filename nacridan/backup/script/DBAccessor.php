#!/usr/bin/php
<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
// DEFINE ("FOREIGN_KEY_SYNTAX","id_([^_\$]*)[_]?([^\$]*)(.*)"); //id_(list)tablename_optionnalDesc
// DEFINE ("TABLE_SYNTAX","([^_]*)(.*)");

DEFINE("CLASSPATH", "../class/");

class AccessorsGenerator
{

    public $m_foreignKeySyntax;

    public $m_tableSyntax;

    public $m_db;

    public $m_classHierarchyFile;

    public $m_tablesAndFields;

    public $m_tablesAndClass;

    public $m_inheritance;

    public $m_knownObject;

    function AccessorsGenerator($foreignKeySyntax, $tableSyntax, $classHierarchyFile, $db, $collectionPrefix)
    {
        $this->m_db = $db;
        $this->m_classHierarchyFile = $classHierarchyFile;
        $this->m_tableSyntax = $tableSyntax;
        $this->m_foreignKeySyntax = $foreignKeySyntax;
        $this->m_collectionPrefix = $collectionPrefix;
        
        $query = sprintf("show tables");
        $record1 = $this->m_db->Execute($query);
        
        while (! $record1->EOF) {
            $query = sprintf("describe %s", $record1->fields[0]);
            
            $record2 = $this->m_db->Execute($query);
            
            while (! $record2->EOF) {
                $this->m_tablesAndFields[$record1->fields[0]][] = $record2->fields[0];
                
                preg_match('/' . $this->m_tableSyntax . '/', $record1->fields[0], $tab);
                $this->m_tablesAndClass[$record1->fields[0]] = $tab[1];
                $record2->MoveNext();
            }
            $record1->MoveNext();
        }
        // print_r($this->m_tablesAndFields);
        // print_r($this->m_tablesAndClass);
    }

    protected function loadHierarchy()
    {
        $fd = fopen($this->m_classHierarchyFile, "r");
        while (! feof($fd)) {
            $buffer = fgets($fd, 4096);
            $arr = explode(":", $buffer);
            
            if (count($arr) == 2) {
                foreach ($this->m_tablesAndClass as $key => $val) {
                    if (preg_match('/' . rtrim($arr[1] . '/', "\n\r"), $key, $tab)) {
                        $this->m_inheritance[$val] = $arr[0];
                    }
                }
            } else {
                $arr = explode("//", $buffer);
                for ($i = 0; $i < count($arr) - 1; $i ++) {
                    $this->m_inheritance[rtrim($arr[1], "\n\r")] = $arr[0];
                }
            }
        }
        // print_r($this->m_inheritance);
        
        fclose($fd);
    }

    protected function hasTableRepresentation($name)
    {
        $has = 0;
        foreach ($this->m_tablesAndFields as $key => $val) {
            list ($table) = explode("_", $key);
            if ($table == $name) {
                $has = 1;
            }
        }
        
        return $has;
    }

    protected function printVirtualClasses()
    {
        $str = array();
        foreach ($this->m_inheritance as $key => $val) {
            if (! $this->hasTableRepresentation($val) && ! isset($this->m_knownObject[$val])) {
                if (file_exists($val . ".inc.php")) {
                    $class = file_get_contents($val . ".inc.php");
                    // eval($class);
                    $this->m_knownObject[$val] = "new";
                    $str[$val] = $class;
                } else {
                    foreach ($this->m_tablesAndClass as $key2 => $val2) {
                        if ($val == $val2 && ! isset($this->m_knownObject[$val])) {
                            $class = $this->printClass($val, "");
                            // eval($class);
                            $this->m_knownObject[$val] = "new";
                            $str[$val] = $class;
                        }
                    }
                }
            }
        }
        return $str;
    }

    public function isBlank($value)
    {
        return preg_match('/^[\n\t ]+$/', $value);
    }

    public function isAlpha($value)
    {
        return preg_match('/^[a-zA-Z]+$/', $value);
    }

    public function isNum($value)
    {
        return is_numeric($value);
    }

    private function extractFunctions($className, $class, &$constructor)
    {
        $type = array(
            "private",
            "protected",
            "public",
            "function"
        );
        $constructor = "";
        $str = "";
        $i = 0;
        
        while (! (($pos = strpos($class, "private", $i)) === false) || ! (($pos = strpos($class, "protected", $i)) === false) || ! (($pos = strpos($class, "public", $i)) === false) || ! (($pos = strpos($class, "function", $i)) === false)) {
            foreach ($type as $typefct) {
                if (($pos2 = strpos($class, $typefct, $i)) != false) {
                    if ($pos2 < $pos)
                        $pos = $pos2;
                }
            }
            
            $i = $pos; // +strlen("function");
            $sign = "";
            
            $j = $i;
            while ($class[$j] != "(" && $class[$j] != ";") {
                $sign .= $class[$j];
                $j ++;
            }
            // echo $i."////".$sign;
            if ($class[$j] == "(") {
                $i = $j;
                
                while (! $this->isBlank($class[$i])) {
                    $i --;
                }
                $i ++;
                $fctname = "";
                
                while ($class[$i] != "(") {
                    $fctname .= $class[$i];
                    $i ++;
                }
                
                $fctbody = "";
                
                while ($class[$i] != "{") {
                    $fctbody .= $class[$i];
                    $i ++;
                }
                
                $bloc = 1;
                $fctbody .= "{";
                
                do {
                    $i ++;
                    $fctbody .= $class[$i];
                    if ($class[$i] == "{")
                        $bloc ++;
                    if ($class[$i] == "}")
                        $bloc --;
                } while ($bloc > 0);
                
                $fctbody .= "\n";
                
                if ($fctname != $className)
                    $str .= "  " . $sign . " " . $fctbody;
                else
                    $constructor = "  " . $sign . " " . $fctbody;
            } else {
                $i = $j;
            }
        }
        return $str;
    }

    private function extractVars($class)
    {
        $type = array(
            "private",
            "protected",
            "public",
            "function"
        );
        $i = 0;
        $str = "";
        while (! (($pos = strpos($class, "private", $i)) === false) || ! (($pos = strpos($class, "protected", $i)) === false) || ! (($pos = strpos($class, "public", $i)) === false) || ! (($pos = strpos($class, "function", $i)) === false)) {
            foreach ($type as $typefct) {
                if (($pos2 = strpos($class, $typefct, $i)) != false) {
                    if ($pos2 < $pos)
                        $pos = $pos2;
                }
            }
            
            $i = $pos; // +strlen("function");
            $var = "  ";
            $i = $pos;
            while ($class[$i] != ";" && $class[$i] != "(") {
                $var .= $class[$i];
                $i ++;
            }
            
            if ($class[$i] == ";")
                $str .= $var . ";\n";
        }
        return $str;
    }

    private function mergeClass($className, $classAuto, $classManual)
    {
        $str = "";
        $fctAuto = $this->extractFunctions($className, $classAuto, $constructorAuto);
        $fctManual = $this->extractFunctions($className, $classManual, $constructorManual);
        $varsAuto = $this->extractVars($classAuto);
        $varsManual = $this->extractVars($classManual);
        
        $i = 0;
        while ($classAuto[$i] != "{") {
            $str .= $classAuto[$i];
            $i ++;
        }
        $str .= "{\n";
        
        $str .= $varsAuto . "\n";
        $str .= $varsManual . "\n";
        
        $i = 0;
        $str .= $this->mergeConstructor($constructorAuto, $constructorManual);
        $str .= $fctAuto . "\n";
        $str .= $fctManual;
        $str .= "}\n";
        return $str;
    }

    private function mergeConstructor($constructorAuto, $constructorManual)
    {
        if ($constructorManual == "")
            return $constructorAuto;
        
        $str = "";
        $i = 0;
        $j = 0;
        while ($constructorAuto[$i] != "{") {
            $str .= $constructorAuto[$i];
            $i ++;
        }
        $i ++;
        $str .= "{\n";
        while ($constructorManual[$j] != "{") {
            $j ++;
        }
        $j ++;
        
        // extract parent//
        $parentdef = "";
        do {
            $parentdef .= $constructorAuto[$i];
            $i ++;
        } while ($constructorAuto[$i] != ";");
        $parentdef .= ";";
        $i ++;
        
        while ($this->isBlank($constructorAuto[$i]))
            $i ++;
        
        $auto = "";
        $bloc = 1;
        do {
            $auto .= $constructorAuto[$i];
            $i ++;
            if ($constructorAuto[$i] == "{")
                $bloc ++;
            if ($constructorAuto[$i] == "}")
                $bloc --;
        } while ($bloc > 0);
        
        $bloc = 1;
        do {
            $j ++;
            $str .= $constructorManual[$j];
            if ($constructorManual[$j] == "{")
                $bloc ++;
            if ($constructorManual[$j] == "}")
                $bloc --;
        } while ($bloc > 0);
        $str .= "\n";
        
        $str = str_replace("##parent##", $parentdef, $str);
        $str = str_replace("##auto##", $auto . "\n", $str);
        
        return $str;
    }

    protected function printOtherClasses()
    {
        $str = array();
        foreach ($this->m_tablesAndClass as $key => $val) {
            if (! isset($this->m_knownObject[$val])) {
                if (file_exists($val . ".inc.php")) {
                    $classAuto = $this->printClass($val, $val);
                    $classManual = file_get_contents($val . ".inc.php");
                    $class = $this->mergeClass($val, $classAuto, $classManual);
                    // eval($class);
                    $this->m_knownObject[$val] = "new";
                    $str[$val] = $class;
                } else {
                    $class = $this->printClass($val, $val);
                    // eval($class);
                    $this->m_knownObject[$val] = "new";
                    $str[$val] = $class;
                }
            }
        }
        return $str;
    }

    protected function getTableNamePrototype($tableName)
    {
        foreach ($this->m_tablesAndFields as $key => $val) {
            preg_match('/' . $this->m_tableSyntax . '/', $key, $tab);
            if ($tab[1] == $tableName)
                return $key;
        }
    }

    protected function printClass($className, $tableName)
    {
        $str = "";
        if (isset($this->m_inheritance[$className])) {
            $str .= "require_once(HOMEPATH.\"/class/" . $this->m_inheritance[$className] . ".inc.php\");\n\n";
        }
        
        $str .= "class " . $className;
        if (isset($this->m_inheritance[$className])) {
            $str .= " extends " . $this->m_inheritance[$className] . "{\n\n";
        } else {
            $str .= "{\n\n";
        }
        
        $str .= "     function " . $className . "(){\n";
        $str .= "        parent::" . $this->m_inheritance[$className] . "();\n";
        $str .= "        \$this->m_tableName=\"" . $tableName . "\";\n";
        $str .= "        \$this->m_className=\"" . $className . "\";\n";
        if (isset($this->m_tablesAndClass[$className])) {
            $str .= "        \$this->m_extraName=\"\";\n";
        }
        
        if (! isset($this->m_tablesAndFields[$tableName])) {
            $tableName = $this->getTableNamePrototype($tableName);
        }
        
        if ($tableName && isset($this->m_tablesAndFields[$tableName])) {
            $fkreal = 0;
            foreach ($this->m_tablesAndFields[$tableName] as $key => $val) {
                $val = str_replace("\$", "\\\$", $val);
                if (ereg($this->m_foreignKeySyntax, $val, $tab)) {
                    if ($tab[2] != "") {
                        if ($tab[3] != "") {
                            $str .= "        \$this->m_realDBName[\"id_" . $tab[1] . $tab[3] . "\"]=\"" . $val . "\";\n";
                            $str .= "        \$this->m_attr[\"id_" . $tab[1] . $tab[3] . "\"]=\"\";\n";
                            $fkreal = 1;
                            // echo $className.":".$tab[1].":".$tab[2].":".$tab[3]."*\n";
                        } else {
                            $str .= "        \$this->m_realDBName[\"id_" . $tab[1] . "\"]=\"" . $val . "\";\n";
                            $str .= "        \$this->m_attr[\"id_" . $tab[1] . "\"]=\"\";\n";
                            
                            // echo $className.":".$tab[1].":".$tab[2].":".$tab[3]."\n";
                            $fkreal = 1;
                        }
                    } else {
                        $str .= "        \$this->m_attr[\"" . $val . "\"]=\"\";\n";
                    }
                } else {
                    $str .= "        \$this->m_attr[\"" . $val . "\"]=\"\";\n";
                }
            }
            if ($fkreal == 1) {
                $str .= "        foreach (\$this->m_realDBName as \$key => \$val){\n            ";
                $str .= "\$this->m_realAPIName[\$val]=\$key;\n        }\n";
            }
        }
        $str .= "        \$this->init();\n";
        $str .= "     }\n";
        
        $str .= "}\n\n";
        
        return $str;
    }

    function genClasses()
    {
        $str = array();
        $this->loadHierarchy();
        
        $str = array_merge($str, $this->printVirtualClasses());
        
        $str = array_merge($str, $this->printOtherClasses());
        
        return $str;
    }
}

class CollectionsGenerator extends AccessorsGenerator
{

    public $m_db;

    public $m_classHierarchyFile;

    public $m_tablesAndFields;

    public $m_tablesAndClass;

    public $m_collectionPrefix;

    public $m_inheritance;

    function CollectionsGenerator($classHierarchyFile, $db, $collectionPrefix)
    {
        $this->m_db = $db;
        $this->m_classHierarchyFile = $classHierarchyFile;
        $this->m_collectionPrefix = $collectionPrefix;
        
        $query = sprintf("show tables");
        $record1 = $this->m_db->Execute($query);
        
        while (! $record1->EOF) {
            $query = sprintf("describe %s", $record1->fields[0]);
            
            $record2 = $this->m_db->Execute($query);
            
            while (! $record2->EOF) {
                $this->m_tablesAndFields[$record1->fields[0]][] = $record2->fields[0];
                $this->m_tablesAndClass[$record1->fields[0]] = $this->m_collectionPrefix . $record1->fields[0];
                $record2->MoveNext();
            }
            $record1->MoveNext();
        }
    }

    protected function printOtherClasses()
    {
        $str = array();
        foreach ($this->m_tablesAndClass as $key => $val) {
            if (! isset($this->m_knownObject[$val])) {
                if (file_exists($val . ".inc.php")) {
                    $classAuto = $this->printClass($val, $key);
                    $classManual = file_get_contents($val . ".inc.php");
                    $class = $this->mergeClass($val, $classAuto, $classManual);
                    // eval($class);
                    $this->m_knownObject[$val] = "new";
                    $str[$val] = $class;
                } else {
                    $class = $this->printClass($val, $key);
                    // eval($class);
                    $this->m_knownObject[$val] = "new";
                    $str[$val] = $class;
                }
            }
        }
        return $str;
    }

    protected function printClass($className, $tableName)
    {
        list ($class) = explode("_", $tableName);
        $str = "class " . $className;
        if (isset($this->m_inheritance[$className])) {
            $str .= " extends " . $this->m_inheritance[$className] . "{\n\n";
        } else {
            $str .= "{\n\n";
        }
        
        $str .= "     function " . $className . "(\$param=\"\",&\$db,\$start=0,\$sizelimit=0){\n";
        $str .= "        \$this->m_tableName=\"" . $tableName . "\";\n";
        $str .= "        \$this->m_className=\"" . $class . "\";\n";
        $str .= "        \$this->init(\$param,\$db,\$start,\$sizelimit);\n";
        $str .= "     }\n";
        
        $str .= "}\n\n";
        
        return $str;
    }

    function genClasses()
    {
        $this->loadHierarchy();
        
        $str = $this->printVirtualClasses();
        
        return $str;
    }
}

$db = ADONewConnection(DRIVER);
$db->Connect(MYSQLHOST, LOGIN, PASSWD, DB);

$access = new AccessorsGenerator(FOREIGN_KEY_SYNTAX, TABLE_SYNTAX, "hierarchy.txt", $db, "DB");

$array = $access->genClasses();

$collection = new CollectionsGenerator("hierarchyCollection.txt", $db, "DB");

$array = array_merge($array, $collection->genClasses());

foreach ($array as $key => $value) {
    if (is_file(CLASSPATH . $key . ".inc.php"))
        unlink(CLASSPATH . $key . ".inc.php");
    
    $fp = fopen(CLASSPATH . $key . ".inc.php", 'w+');
    fwrite($fp, "<?\n" . $value . "\n?>");
    fclose($fp);
}

?>