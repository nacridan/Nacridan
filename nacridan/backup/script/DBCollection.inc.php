class DBCollection{ public $m_curIndex; public $m_query; public
$m_records; public $m_param; public $m_sizeLimit; public $m_tableName;
public $m_className; public $m_errormsg; public $m_errorno; public
$m_id; function DBCollection($param="",&$db,$start=0,$sizelimit=0){
$this->m_className="DBCollection";
$this->init($param,$db,$start,$sizelimit); } function
init($param="",&$db,$start=0,$sizeLimit=0) { $this->m_param=$param;
$this->m_curIndex=0; $this->m_sizeLimit=$sizeLimit; if(isset($db)){
if($this->m_sizeLimit==0) { $this->m_query=sprintf("%s",$this->m_param);
} else { $this->m_query=sprintf("%s LIMIT
%s,%s",$this->m_param,$start,$this->m_sizeLimit); }
$this->execQuery($this->m_query,$db,$this->m_records); } else {
$name=$this->m_className; trigger_error ("l'object DB de la classe $name
= NULL", E_USER_ERROR); } } function getTotalNbRows($db) {
if(isset($db)){ $this->m_query=sprintf("SHOW TABLE STATUS LIKE
\"%s\"",$this->m_tableName);
$this->execQuery($this->m_query,$db,$record); return
$record->fields["Rows"]; } else { $name=$this->m_className;
trigger_error ("l'object DB de la classe $name = NULL", E_USER_ERROR);
return -1; } } protected function execQuery($query,&$db,&$record=null) {
//echo $query; try { $record=$db->Execute($query); } catch (exception
$e) { } $this->m_errorno=$db->ErrorNo(); //dbLogMessage($query);

if($db->ErrorNo()) { $this->m_errormsg=$db->ErrorMsg();
dbLogMessage($query); dbLogMessage($this->m_errormsg); return -1; }
return 0; } function errorNoDB(){ return $this->m_errorno; } function
errorMsgDB(){ return $this->m_errormsg; } function first() {
if(isset($this->m_records)){ $this->m_records->MoveFirst();
$this->m_records->EOF=false; } else { $name=$this->m_className;
trigger_error ("l'object m_records de la classe $name = NULL",
E_USER_ERROR); } } function count() { if(isset($this->m_records)){
return $this->m_records->RecordCount(); } else {
$name=$this->m_className; trigger_error ("l'object m_records de la
classe $name = NULL", E_USER_ERROR); } } function get($var) { return
$this->m_records->fields[$var]; } function eof() {
if(isset($this->m_records)){ return $this->m_records->EOF; } else {
return true; } } function next() { if(isset($this->m_records)){

if(!$this->m_records->MoveNext()) { return false; } return true; } else
{ $name=$this->m_className; trigger_error ("l'object m_records de la
classe $name = NULL", E_USER_ERROR); } } function getCurObject($type){
if(isset($this->m_records)){ $curObject=new $type();
$curObject->DBLoad($this->m_records); return $curObject; } else { return
null; } } }

