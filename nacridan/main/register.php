<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/TplForm.inc.php");
// require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");

$lang = 'fr';

// |ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û

DEFINE("RX_ALPHA", "([a-z]|[A-Z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");
DEFINE("RX_UPPER_ALPHA", "([A-Z]|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");
DEFINE("RX_LOWER_ALPHA", "([a-z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î)");
DEFINE("RX_ALPHA_NUM", "([a-z]|[0-9]|[A-Z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");

//
// Compute a hash of $str
//
function pun_hash($str)
{
    return sha1($str);
}

//
// Try to determine the correct remote IP-address
//
function get_remote_address()
{
    $remote_addr = $_SERVER['REMOTE_ADDR'];
    
    // If we are behind a reverse proxy try to find the real users IP
    if (defined('FORUM_BEHIND_REVERSE_PROXY')) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // The general format of the field is:
            // X-Forwarded-For: client1, proxy1, proxy2
            // where the value is a comma+space separated list of IP addresses, the left-most being the farthest downstream client,
            // and each successive proxy that passed the request adding the IP address where it received the request from.
            $forwarded_for = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $forwarded_for = trim($forwarded_for[0]);
            
            if (@preg_match('%^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$%', $forwarded_for) || @preg_match(
                '%^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$%', 
                $forwarded_for))
                $remote_addr = $forwarded_for;
        }
    }
    
    return $remote_addr;
}

class Registration extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $db;

    public function Registration($master, $lang, $session = null)
    {
        $this->master = $master;
        $this->lang = $lang;
        $this->db = DB::getDB();
        // ------------- Main FORM
        $formname = "form";
        $this->mainForm = new TplForm($formname, $session);
        
        // $this->mainForm->addElement(array("name"=>"lastname","targetid"=>"lastname_err","targeterr"=>"err","targetcss"=>"info","type"=>"text","value"=>"","valid_regex"=>"^".RX_ALPHA."+(".RX_ALPHA."| |-)*$","errorclass"=>"error","targetmsg"=>localize(""),"valid_e"=>localize("Ce champ ne doit comporter que des lettres")));
        
        // $this->mainForm->addElement(array("name"=>"firstname","targetid"=>"firstname_err","targeterr"=>"err","targetcss"=>"info","type"=>"text","value"=>"","valid_regex"=>"^".RX_ALPHA."+(".RX_ALPHA."| |-)*$","errorclass"=>"error","targetmsg"=>localize(""),"valid_e"=>localize("Ce champ ne doit comporter que des lettres")));
        
        $this->mainForm->addElement(
            array(
                "name" => "login",
                "targetid" => "login_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^" . RX_ALPHA . "+(" . RX_ALPHA_NUM . "| ){2,}" . RX_ALPHA_NUM . "{1}$",
                "errorclass" => "error",
                "targetmsg" => "Ce pseudo est celui qui sera utilisé pour se connecter au jeu et dans <b>le forum</b>, ce n'est pas le nom de votre premier personnage, lequel sera choisi plus tard.<br/><br/>",
                "valid_e" => "Ce champ doit commencer par une lettre et doit comporter au moins 4 caractères"
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "email",
                "targetid" => "email_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^([a-z]|[A-Z]|[0-9]|\.|-|_)+@([a-z]|[A-Z]|[0-9]|\.|-|_)+\.([a-z]|[A-Z]|[0-9]){2,5}$",
                "errorclass" => "error",
                "targetmsg" => ('Attention, c\'est à cette adresse que vous sera envoyé votre mot de passe.<br/><br/>'),
                "valid_e" => ("Adresse E-mail non valide")
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "discheck",
                "targetid" => "discheck_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "checkbox",
                "value" => "accept",
                "exclude" => "false",
                "targetmsg" => "",
                "valid_e" => ("Vous n'avez pas accepté la charte.")
            ));
        
        // $onclickCaptcha = "document.getElementById(\"captcha\").src = \"../lib/securimage/securimage_show.php?\" + Math.random(); return false";
        // <br/><a href='#' onclick='".$onclickCaptcha."'>[ Choisir une autre image ]</a>
        // onclick='".$onclickCaptcha."'
        $this->mainForm->addElement(
            array(
                "name" => "country_feinte",
                "targetid" => "imgcode_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "errorclass" => "error",
                "targetmsg" => ("Entrez le code de validation ci-contre, en tenant compte des majuscules."),
                "valid_e" => ("Ce champ ne doit contenir que des lettres ou des chiffres")
            ));
        
        $o = array();
        $dbm = new DBCollection("SELECT * FROM Timezone ORDER BY name", $this->db);
        while (! $dbm->eof()) {
            $o[] = array(
                "label" => $dbm->get("name"),
                "value" => $dbm->get("name") . ";" . $dbm->get("utcOffset") . ";" . $dbm->get("utcOffsetDLS")
            );
            $dbm->next();
        }
        
        $info_days = array();
        for ($i = 1; $i <= 31; $i ++)
            $info_days[] = array(
                "label" => $i,
                "value" => $i
            );
        
        $info_months = array();
        for ($i = 1; $i <= 12; $i ++)
            $info_months[] = array(
                "label" => $i,
                "value" => $i
            );
        
        $year = date("Y");
        
        $info_years = array();
        for ($i = $year; $i >= $year - 100; $i --)
            $info_years[] = array(
                "label" => $i,
                "value" => $i
            );
        
        $o2 = array();
        $o2[] = array(
            "label" => ("Homme"),
            "value" => "M"
        );
        $o2[] = array(
            "label" => ("Femme"),
            "value" => "F"
        );
        
        $this->mainForm->addElement(array(
            "type" => "select",
            "name" => "day",
            "options" => $info_days
        ));
        
        $this->mainForm->addElement(array(
            "type" => "select",
            "name" => "month",
            "options" => $info_months
        ));
        
        $this->mainForm->addElement(
            array(
                "type" => "select",
                "name" => "year",
                "value" => 1980,
                "options" => $info_years
            ));
        
        /*
         * $this->mainForm->addElement(array("type"=>"select",
         * "name"=>"gender",
         * "value"=>"M",
         * "options"=>$o2));
         */
        $this->mainForm->addElement(
            array(
                "type" => "select",
                "name" => "zone",
                "options" => $o,
                "size" => 1,
                "valid_e" => ("Heure invalide"),
                "value" => "Europe/Paris;60;120",
                "extrahtml" => 'id="zone" onchange="javascript:sethour(\'hourzone\',this.options[this.selectedIndex].value);"'
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "submit",
                "type" => "submit",
                "value" => ("Valider mon Inscription"),
                "extrahtml" => "class=\"button\""
            ));
        $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
    }

    protected function passGen($max = 8, $min = 6)
    {
        $nbchar = mt_rand($min, $max);
        $chars = array(
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
        );
        
        $pass = "";
        
        for ($i = 0; $i < $nbchar; $i ++)
            $pass .= $chars[rand(0, count($chars) - 1)];
        
        return $pass;
    }

    protected function sendSubscriptionEmail($login, $password, $email, &$res)
    {
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        switch ($this->lang) {
            case "fr":
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/subscribeMsgFr.inc"));
                break;
            default:
                // eval(file_get_contents(HOMEPATH."/i18n/mailregister/subscribeMsgEn.inc"));
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/subscribeMsgFr.inc"));
                break;
        }
        
        $to = $email;
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        $succes = false;
        if ($mailer->isConnected()) // Optional
{
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->addPart($messagehtml, 'text/html');
            $mailer->setCharset("UTF-8");
            $succes = $mailer->send($to, "Nacridan <noreply@" . SMTPDOMAIN . ".>", $subject);
            
            $mailer->close();
        }
        if ($succes) {
            $res = $result;
        } else {
            $res = $resultfail;
        }
        return $succes;
    }

    public function step0()
    {
        
        // $str="<div class='attackevent'><b>La procédure d'inscription est en maintenance pour renforcer la sécurité. Elle sera disponible au plus tard samedi 17/01/2015 à 22h.</b></div><br/><br/>";
        $str = "<div class='registration_title'><b> - Nouvelle Inscription </b></div><br/><br/>";
        
        // $eng="<br/><a class='link' target='blank' href='".CONFIG_HOST."/i18n/disclaimer/en/disclaimer.php'> (english version)</a>";
        $eng = "";
        $form = "<table class='registration' width='590px'>";
        
        $form .= "<tr><td  >" . ("Pseudo") . "</td><td >" . $this->mainForm->getElement("login") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='login_err' " . $this->mainForm->getTargetCss("login") . ">" . $this->mainForm->getTargetMsg("login") . "</td></tr>\n";
        
        $form .= "<tr><td  >" . ("Date de Naissance") . "</td><td>" . $this->mainForm->getElement("day") . $this->mainForm->getElement("month") . $this->mainForm->getElement(
            "year") . "</td></tr>\n";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "<tr><td  >" . ("Adresse E-mail") . "</td><td>" . $this->mainForm->getElement("email") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='email_err' " . $this->mainForm->getTargetCss("email") . ">" . $this->mainForm->getTargetMsg("email") . "</td></tr>\n";
        $form .= "<tr><td  >" . ("Zone horaire") . "</td><td>" . $this->mainForm->getElement("zone") . "&nbsp;<span id='hourzone'></span></td></tr>\n";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        $form .= "<tr><td  >" . ("J'accepte la") . " <a class='link' target='blank' href='" . CONFIG_HOST . "/i18n/disclaimer/" . $this->lang . "/disclaimer.php'>" . ("charte") .
             "</a>" . $eng . "</td><td>" . $this->mainForm->getElement("discheck") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='discheck_err' " . $this->mainForm->getTargetCss("discheck") . ">" . $this->mainForm->getTargetMsg("discheck") . "</td></tr>\n";
        
        /*
         * $form.="<tr><td><img id='captcha' src='../lib/securimage/securimage_show.php' alt='CAPTCHA Image' /><br /></td>";
         * $form.="<td>".$this->mainForm->getElement("captcha_code")."</td></tr>\n";
         * $form.="<tr><td></td><td id='imgcode_err' ".$this->mainForm->getTargetCss("captcha_code").">".$this->mainForm->getTargetMsg("captcha_code")."</td></tr>\n";
         */
        // Rename captcha en country_feinte pour voir si les scripts de fishing sont basés sur les noms des champs
        $form .= "<tr><td><img id='country_feinte' src='../lib/securimage/securimage_show.php' alt='country_feinte' /><br /></td>";
        $form .= "<td>" . $this->mainForm->getElement("country_feinte") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='imgcode_err' " . $this->mainForm->getTargetCss("country_feinte") . ">" . $this->mainForm->getTargetMsg("country_feinte") . "</td></tr>\n";
        
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "</table><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $this->mainForm->getElement("submit") . "\n";
        
        return $str . $this->mainForm->getForm($form);
        
        // return $str;
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        
        // -new secure image
        include_once (HOMEPATH . "/lib/securimage/securimage.php");
        $securimage = new Securimage();
        
        $valid = $securimage->check($_POST['country_feinte']);
        
        $err_array = array();
        
        $validNick = true;
        $db = $this->db;
        $dbp = new DBCollection("SELECT login FROM Member WHERE login='" . $this->mainForm->get("login") . "'", $db);
        if (! $dbp->eof()) {
            $validNick = false;
            $this->mainForm->setTargetCss("login", "err");
            $this->mainForm->setTargetMsg("login", ("Ce pseudo est déjà utilisé !"));
        }
        
        $validEmail = true;
        
        $db = $this->db;
        $dbp = new DBCollection("SELECT email FROM MemberDetail WHERE email='" . quote_smart($this->mainForm->get("email")) . "'", $db);
        if (! $dbp->eof()) {
            $validEmail = false;
            $this->mainForm->setTargetCss("email", "err");
            $this->mainForm->setTargetMsg("email", ("Cette adresse E-mail a déjà été utilisée pour une inscription !"));
        }
        
        if (! $valid) {
            $this->mainForm->setTargetCss("country_feinte", "err");
            $this->mainForm->setTargetMsg("country_feinte", ("Code incorrect."));
        }
        
        if ($this->mainForm->validate() && $valid == true && $validNick == true && $validEmail == true && ! $this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            
            $password = $this->passGen();
            
            /**
             * ***************************************************************************
             */
            /**
             * ************** INSCRIPTION AUTOMATIQUE AU FORUM ***************************
             */
            /**
             * ***************************************************************************
             */
            
            try {
                $dbh = new PDO("mysql:host=" . MYSQLHOST . ";dbname=" . DBFORUM, LOGINFORUM, PASSWDFORUM);
                $initUTF8 = $dbh->prepare('SET NAMES UTF8');
                $initUTF8->execute();
                
                /**
                 * * prepare the SQL statement **
                 */
                $stmt = $dbh->prepare(
                    "INSERT INTO flux_users (group_id,username,password,email,language,style,registered,registration_ip) VALUES (:group_id, :username, :password, :email, :language, :style, :registered, :registration_ip)");
                
                /**
                 * * execute the prepared statement **
                 */
                $success = $stmt->execute(
                    array(
                        'group_id' => 4, // Membres standards
                        'username' => $this->mainForm->get("login"),
                        'password' => pun_hash($password),
                        'email' => $this->mainForm->get("email"),
                        'language' => 'French',
                        'style' => 'Nacridan',
                        'registered' => time(),
                        'registration_ip' => get_remote_address()
                    ));
                
                if ($success)
                    $str .= "- L'inscription au forum s'est bien déroulée. <br>";
                
                /**
                 * * close the database connection **
                 */
                $dbh = null;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
            
            /**
             * ***************************************************************************
             */
            /**
             * ************** FIN - INSCRIPTION AUTOMATIQUE AU FORUM *********************
             */
            /**
             * ***************************************************************************
             */
            
            /**
             * ***************************************************************************
             */
            /**
             * ************** INSCRIPTION AUTOMATIQUE AU SITE ANNEXE *********************
             */
            /**
             * ***************************************************************************
             */
            
            try {
                $dbh = new PDO("mysql:host=" . MYSQLHOST . ";dbname=" . DBGG, LOGINGG, PASSWDGG);
                $initUTF8 = $dbh->prepare('SET NAMES UTF8');
                $initUTF8->execute();
                
                /**
                 * * prepare the SQL statement **
                 */
                $stmt = $dbh->prepare("INSERT INTO user (username, email, password, vmoney, role, theme) VALUES (:login, :email, :password, 0, 0, 'Basic')");
                
                /**
                 * * execute the prepared statement **
                 */
                $success = $stmt->execute(
                    array(
                        'login' => $this->mainForm->get("login"),
                        'email' => $this->mainForm->get("email"),
                        'password' => $password
                    ));
                
                if ($success)
                    $str .= "- L'inscription au site annexe s'est bien déroulée. <br>";
                
                /**
                 * * close the database connection **
                 */
                $dbh = null;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
            
            /**
             * ***************************************************************************
             */
            /**
             * ************ FIN - INSCRIPTION AUTOMATIQUE AU SITE ANNEXE *****************
             */
            /**
             * ***************************************************************************
             */
            
            $member = new Member("", $db);
            $member->set("login", unHTMLTags($this->mainForm->get("login")));
            
            $member->set("password", $password);
            $member->set("lang", "fr");
            $member->set("newplayer", "1");
            $member->set("maxNbPlayers", "1");
            
            list ($zone, $offset, $offsetDLS) = explode(";", $this->mainForm->get("zone"));
            // $zone="Europe/paris";
            $member->set("utc", $zone);
            $member->set("firstlog", gmdate("Y-m-d H:i:s"));
            $member->set("lastlog", gmdate("Y-m-d H:i:s"));
            $id = $member->addDB($db);
            
            if ($id == - 1) {
                if ($member->errorNoDB() == 1062) {
                    $err = ("Ce pseudo est déjà utilisé.");
                } else {
                    $err = ("- Impossible d'ajouter un nouveau membre dans la base de donnée.") . " " . ("(envoyez un mail au webmaster)");
                }
                $str .= $err;
                $str .= "<br/><br/><input type='button' value='" . ("Recommencer") . "' onclick=\"window.location='" . CONFIG_HOST . "/main/register.php'\" />";
                $str .= "</div>";
            } else {
                if ($this->sendSubscriptionEmail($this->mainForm->get("login"), $password, $this->mainForm->get("email"), $res)) {
                    $memDetail = new MemberDetail("", $db);
                    $memDetail->set("id", $id);
                    $memDetail->set("id_Member", $id);
                    $memDetail->set("email", $this->mainForm->get("email"));
                    $memDetail->set("lastname", $this->mainForm->get("lastname"));
                    $memDetail->set("firstname", $this->mainForm->get("firstname"));
                    $memDetail->set("sex", $this->mainForm->get("gender"));
                    $memDetail->set("born", $this->mainForm->get("year") . "-" . $this->mainForm->get("month") . "-" . $this->mainForm->get("day"));
                    $id = $memDetail->addDB($db);
                    
                    $memOption = new MemberOption("", $db);
                    $memOption->set("id", $id);
                    $memOption->set("id_Member", $id);
                    $memOption->set("fightMsg", 1);
                    $memOption->set("mailMsg", 1);
                    $memOption->set("deadMsg", 1);
                    $memOption->set("view2d", 1);
                    $id = $memOption->addDB($db);
                    
                    $res = "<br/><br/>- L'inscription à Nacridan 2 a été effectuée avec succès, vous allez recevoir rapidement un mail avec vos identifiants. Lors de votre première connexion le site vous guidera dans la création de votre personnage. <br/> <br/>Bienvenue dans l'univers de Nacridan !";
                } else
                    $res = "<br/><br/>- Votre adresse email est injoignable, cela peut provenir d'un bug du site. Veuiller signaler le problème dans le forum ou via le formulaire de contact disponible en dessous de la fenêtre de connexion sur la page d'accueil.";
                
                $str .= $res;
                $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
                $str .= "</div>";
            }
            return $str;
        }
        $this->mainForm->setStep(0);
        $this->mainForm->resetValue("imgcode", "");
        
        if ($valid == false) {
            $this->mainForm->setTargetCss("imgcode", "err");
            $this->mainForm->setTargetMsg("imgcode", ("Code incorrect !"));
        }
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= ("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }

    public function toString()
    {
        /*
         * $str="<div class='registration'>";
         * $str.=("Nacridan 2 ouvre dimanche 3 novembre à 14h, revenez plus tard.");
         * $str.="</div>";
         *
         *
         * return $str;
         */
        $step = $this->mainForm->getStep($_POST);
        $ret = array();
        $time = "\n<script type='text/javascript'>var obj=document.getElementById('zone');sethour('hourzone',obj.options[obj.selectedIndex].value);</script>";
        switch ($step) {
            case 1:
                return $this->step1();
                break;
            case 0:
                return $this->step0() . $time;
                break;
            
            default:
                printf("empty page <br />\n");
                break;
        }
    }
}

class ConfirmMail extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $db;

    public function ConfirmMail($master, $lang, $session = null)
    {
        $this->master = $master;
        $this->lang = $lang;
        $this->db = DB::getDB();
        $formname = "form";
        $this->mainForm = new TplForm($formname, $session, CONFIG_HOST . "/main/register.php?confirmmail=true");
        
        $this->mainForm->addElement(
            array(
                "name" => "email",
                "targetid" => "email_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^([a-z]|[A-Z]|[0-9]|\.|-|_)+@([a-z]|[A-Z]|[0-9]|\.|-|_)+\.([a-z]|[A-Z]|[0-9]){2,5}$",
                "errorclass" => "error",
                "targetmsg" => ('Attention, cette adresse doit être celle que vous avez utilisé pour vous inscrire.'),
                "valid_e" => ("Adresse E-mail non valide")
            ));
        $this->mainForm->addElement(
            array(
                "name" => "submit",
                "type" => "submit",
                "value" => ("Demander le renvoi du mail d&rsquo;inscription"),
                "extrahtml" => "class=\"button\""
            ));
        $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
    }

    protected function sendSubscriptionEmail($login, $password, $email, &$res)
    {
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        switch ($this->lang) {
            case "fr":
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/subscribeMsgFr.inc"));
                break;
            default:
                // eval(file_get_contents(HOMEPATH."/i18n/mailregister/subscribeMsgEn.inc"));
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/subscribeMsgFr.inc"));
                break;
        }
        
        $to = $email;
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        $succes = false;
        if ($mailer->isConnected()) // Optional
{
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->addPart($messagehtml, 'text/html');
            $mailer->setCharset("UTF-8");
            $succes = $mailer->send($to, "Nacridan <noreply@" . SMTPDOMAIN . ".>", $subject);
            
            $mailer->close();
        }
        if ($succes) {
            $res = $result;
        } else {
            $res = $resultfail;
        }
        return $succes;
    }

    public function step0()
    {
        $str = "<div class='attackevent'><b>Attention </b><br/><div style='text-align: justify; padding-left: 20px; width: 80%'>Certaines boites mails peuvent considérer le mail confirmant votre inscription comme du spam. C'est notamment le cas avec <b>gmail</b> et <b>hotmail</b>.</div></div><br/><br/>";
        
        $str .= "<div class='registration_title'><b> - Demande de renvoi du Mail d'inscription</b></div><br/>";
        
        $str .= "<span class='attackevent'><b>" . ("Pensez à vérifier votre boîte de courrier indésirable (SPAM).") . "<br/>" . ("Il se peut que le mail d'inscription y soit déjà.") .
             "</b></span><br/><br/>";
        
        $form = "<table class='registration' width='590px'>";
        
        $form .= "<tr><td  width='230px'>" . ("Adresse E-mail utilisé lors de l'inscription") . "</td><td>" . $this->mainForm->getElement("email") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='email_err' " . $this->mainForm->getTargetCss("email") . ">" . $this->mainForm->getTargetMsg("email") . "</td></tr>\n";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        $form .= "</table><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $this->mainForm->getElement("submit") . "\n";
        
        return $str . $this->mainForm->getForm($form);
        $str .= "<table class='registration' width='590px'>";
        return $str;
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        
        $err_array = array();
        
        if ($this->mainForm->validate() && ! $this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $db = $this->db;
            $dbp = new DBCollection(
                "SELECT * FROM Member LEFT JOIN MemberDetail ON Member.id=MemberDetail.id_Member WHERE email='" . $this->mainForm->get("email") . "' AND newplayer=1", $db);
            $res = "";
            if (! $dbp->eof()) {
                $this->sendSubscriptionEmail($dbp->get("login"), $dbp->get("password"), $dbp->get("email"), $res);
                $str .= $res;
                $str .= "<br/><br/>";
            } else {
                $str .= ("Aucune demande d'inscription récente n'a été réalisée avec cette adresse E-mail. Vous avez peut-être mal orthographié votre adresse au moment de l'inscription.");
                $str .= "<br/><br/>";
            }
            
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            
            return $str;
        }
        
        $this->mainForm->setStep(0);
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= ("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }

    public function toString()
    {
        $step = $this->mainForm->getStep($_POST);
        $ret = array();
        
        switch ($step) {
            case 1:
                return $this->step1();
                break;
            case 0:
                return $this->step0();
                break;
            
            default:
                printf("empty page <br />\n");
                break;
        }
    }
}

class LostPassword extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $db;

    public function LostPassword($master, $lang, $session = null)
    {
        $this->master = $master;
        $this->lang = $lang;
        $this->db = DB::getDB();
        $formname = "form";
        $this->mainForm = new TplForm($formname, $session, CONFIG_HOST . "/main/register.php?lostpasswd=true");
        
        $this->mainForm->addElement(
            array(
                "name" => "email",
                "targetid" => "email_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^([a-z]|[A-Z]|[0-9]|\.|-|_)+@([a-z]|[A-Z]|[0-9]|\.|-|_)+\.([a-z]|[A-Z]|[0-9]){2,5}$",
                "errorclass" => "error",
                "targetmsg" => ('Attention, cette adresse doit être celle que vous avez utilisé pour vous inscrire.'),
                "valid_e" => ("Adresse E-mail non valide")
            ));
        $this->mainForm->addElement(
            array(
                "name" => "submit",
                "type" => "submit",
                "value" => ("Redemander mes identifiants de connection"),
                "extrahtml" => "class=\"button\""
            ));
        $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
    }

    protected function sendLostPasswordEmail($login, $password, $email, &$res)
    {
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        switch ($this->lang) {
            case "fr":
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/lostPasswdMsgFr.inc"));
                break;
            default:
                // eval(file_get_contents(HOMEPATH."/i18n/mailregister/subscribeMsgEn.inc"));
                eval(file_get_contents(HOMEPATH . "/i18n/mailregister/lostPasswdMsgFr.inc"));
                break;
        }
        
        $to = $email;
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        $succes = false;
        if ($mailer->isConnected()) // Optional
{
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->addPart($messagehtml, 'text/html');
            $mailer->setCharset("UTF-8");
            $succes = $mailer->send($to, "Nacridan <noreply@" . SMTPDOMAIN . ".>", $subject);
            
            $mailer->close();
        }
        if ($succes) {
            $res = $result;
        } else {
            $res = $resultfail;
        }
        return $succes;
    }

    public function step0()
    {
        $str = "<div class='registration_title'><b> - Mot de passe perdu</b></div><br/><br/>";
        
        $form = "<table class='registration' width='590px'>";
        
        $form .= "<tr><td  width='230px'>" . ("Adresse E-mail utilisé lors de l'inscription") . "</td><td>" . $this->mainForm->getElement("email") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='email_err' " . $this->mainForm->getTargetCss("email") . ">" . $this->mainForm->getTargetMsg("email") . "</td></tr>\n";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        $form .= "</table><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $this->mainForm->getElement("submit") . "\n";
        
        return $str . $this->mainForm->getForm($form);
        $str .= "<table class='registration' width='590px'>";
        return $str;
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        
        $err_array = array();
        
        if ($this->mainForm->validate() && ! $this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $db = $this->db;
            $dbp = new DBCollection("SELECT * FROM Member LEFT JOIN MemberDetail ON Member.id=MemberDetail.id_Member WHERE email='" . $this->mainForm->get("email") . "'", $db);
            $res = "";
            if (! $dbp->eof()) {
                $this->sendLostPasswordEmail($dbp->get("login"), $dbp->get("password"), $dbp->get("email"), $res);
            }
            
            if ($res == "") {
                $str .= ("Si cette adresse E-mail existe dans la base de données, les identifiants de connection du joueur associé vous seront envoyés par E-mail à cette même adresse.");
                $str .= "<br/><br/>";
                $str .= ("Si vous ne recevez pas de message,<br/> pensez à vérifier votre boîte de courrier indésirable (SPAM) si vous en possédez une.");
            } else
                $str .= $res;
            
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            
            return $str;
        }
        
        $this->mainForm->setStep(0);
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= ("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }

    public function toString()
    {
        $step = $this->mainForm->getStep($_POST);
        $ret = array();
        
        switch ($step) {
            case 1:
                return $this->step1();
                break;
            case 0:
                return $this->step0();
                break;
            
            default:
                printf("empty page <br />\n");
                break;
        }
    }
}

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

// Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "registration", true, '../i18n/messages/cache', $filename = "");

if (! isset($_GET["lostpasswd"]) && ! isset($_GET["confirmmail"])) {
    global $sess;
    $sess->start();
    $sess->set("register", true);
    $register = new Registration($MAIN_PAGE, $lang, $sess);
    $BOTTOMTitle->add(
        "<div class='register-warning'><b>Attention </b><br/>Certaines boites mails peuvent considérer le mail confirmant votre inscription comme du spam. Si vous ne recevez pas le mail pensez à vérifier votre dossier spam et vos options de filtre. Si vous trouvez ce mail dans le spam, pensez à le signaler en non-spam.</div>");
    
    $BOTTOMTitle->add($register);
} else
    $sess->set("register", false);

if (isset($_GET["lostpasswd"])) {
    $lost = new LostPassword($MAIN_PAGE, $lang, $sess);
    $BOTTOMTitle->add($lost);
}

if (isset($_GET["confirmmail"])) {
    $confirm = new ConfirmMail($MAIN_PAGE, $lang, $sess);
    $BOTTOMTitle->add($confirm);
}

// $BOTTOMMainArea->add("<br/><br/><br/><br/><br/><br/><br/><br/><br/>");
// $BOTTOMTitle->add('I N S C R I P T I O N');

$MAIN_PAGE->render();
// Translation::saveMessages();

?>
