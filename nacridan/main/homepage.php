<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

// require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

$db = DB::getDB();


$CENTER->add(
    "<a class='statsevent' href='" . CONFIG_HOST . "/include/login.inc.php'><img src='" . CONFIG_HOST . "/pics/new-homepage/play-button-parchment-with-frame.png' width='400' ></a>");

$BOTTOMTitle->add(
    "<span style=\"font-size: 1.5em\">Nacridan est un <em><b>jeu de rôle</b></em>.
			<br> Vous incarnez un aventurier dans un univers médiéval-fantastique.<BR/><BR/>");

$str = "C'est aussi un tour-par-tour à durée de tour fixe. Ainsi vous n'avez besoin que de <em><b>quelques minutes par jour</b></em> pour jouer, ce qui en fait un jeu idéal pour ceux qui cherchent un univers virtuel riche mais qui n'ont pas le temps pour les grands jeux en temps réel. 
<a class='statsevent' href='#description'>En savoir plus sur le cadre du jeu</a>
<BR/></span>";

$str .= "<a href='" . CONFIG_HOST . "/main/beastsamples.php'><img class='index-bottom-panels' src='" . CONFIG_HOST . "/pics/new-homepage/voir-bestiaire.png' ></a>";
$str .= "<a href='" . CONFIG_HOST . "/main/screenshots.php'><img class='index-bottom-panels' src='" . CONFIG_HOST . "/pics/new-homepage/voir-plateau.png' ></a>";
$str .= "<a href='" . CONFIG_HOST . "/main/stats.php'><img class='index-bottom-panels' src='" . CONFIG_HOST . "/pics/new-homepage/voir-stats.png' ></a>";
$str .= "<a href='" . CONFIG_HOST . "/main/association.php'><img class='index-bottom-panels' src='" . CONFIG_HOST . "/pics/new-homepage/en-savoir-plus.png' ></a>";

$str .= "<span id='description' style=\"font-size: 1.5em\"><br><b>Description du cadre.</b><br><br>

    Nacridan est un jeu de rôle où vos avatars évoluent dans un univers Médiéval Fantastique. Une multitude de profils sont possibles et le jeu est conçu pour favoriser les interactions entre joueurs, il sera donc toujours plus facile de jouer en collaborant avec les autres, même si une vie d’aventurier solitaire est tout à fait possible.<br><br>

    Une des particularités de ce monde réside dans la <b> fragilité de la notion de « propriété ».</b> Les guerres, le meurtre, le vol d'or, le vol d'armes, l’attaque de caravanes commerciales, la possibilité d’organiser un siège et d’attaquer la ville d’une faction rivale,… ne sont pas fréquents mais existent bel et bien ! Et toute une gamme de « profils de méchants » peut être jouée.<br><br>

Cela n'empêche pas les profils plus paisibles, bien que très capables de se défendre contre d’éventuelles agressions et de protéger leurs biens, d'être en nombre dominant ! Toute une série de carrières sont envisageables et on peut très bien se concentrer sur une vie de commerçant renommé, d’artisan réputé et incontournable pour obtenir du matériel de grande qualité, d’explorateur intrépide ou de fin chasseur de monstres.<br><br>

    Il est donc évident que plus on s’investit dans ce monde en accumulant des richesses (virtuelles), plus on cherche à constituer un royaume puissant, plus on tente de conquérir de territoires et de villes, plus on recherche la notoriété par les armes, le contrôle de vastes zones …. Plus on s’expose à la convoitise et plus on s’inscrit volontairement dans un « rôle-play » orienté PvP (Joueurs contre joueurs). Plus on possède, plus on risque évidement de perdre.<br><br>

    Une vie plus calme et bien moins orientée PvP est tout à fait possible aussi facilement. Il suffit de choisir un rôle-play moins voyant d’un point de vue « guerrier », d’éviter de trop impliquer son personnage dans la course au pouvoir, les guerres de territoires, les alliances,... Et de construire sa notoriété sur des aspects plus « pacifiques », où de choisir une vie plus discrète.<br><br>

    Il est important de bien appréhender cette réalité pour choisir judicieusement la vie que l’on souhaite pour son avatar et être bien conscient que ce qui se joue, c’est l’affrontement de personnages imaginaires dans un monde qui l’est tout autant. Il est important de relativiser, car nous sommes simplement là pour nous amuser et nous accorder un instant de détente. :-)<br><br>
    Une charte du jeu et des règles détaillées sont là pour encadrer tout ceci et garantir le respect de chacun.<br><br>

    Bon jeu à toutes et à tous!</span>";

$BOTTOMTitle->add($str);

$MAIN_PAGE->render();

?>
