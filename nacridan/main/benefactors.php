<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");

require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/indextemp.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");


$db = DB::getDB();



$str = "<div class='about'><div class='about-main'> Une part importante du financement de la V2 s'est faite grâce au site Ulule. <br/><br/> ";

$str .= "<a style='color: #585858;' href='http://fr.ulule.com/nacridan-2/'>Vers la page de la campagne Ulule</a><br>";


$str .= "<div class='about-main'> Un remerciement tout spécial à :<br/><br/> ";
$str .= "<b>Alicia de Lamb</b> et <b>Aksho</b>, pour leur extraordinaire générosité !<br><br>";

$str .= "<div class='about-main'> Un remerciement spécial à :<br/><br/> ";
$str .= "<b>Moa-Nho</b>, <b>Cereza</b> et <b>Dilvich</b> pour avoir permis au projet de sortir de la couveuse d'Ulule grâce à leur dons rapides.<br><br>";

$str .= "<div class='about-main'> Une centaine de personnes ont contribué financièrement au projet par des dons ou l'usage de la boutique. Un grand merci à eux, et en particulier à ceux qui se sont investis au début du projet : <br/><br/> <table class='creditswidth' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; ' rowspan=2>\n";
$str .= "Nanik, Ambrune, Ginglese, Carameline, Hellodi, Sylvanelle, Julespetitbidon, Schimoufi, Jérôme Bianquis,Anata, Profonde Reconnaissance du Coeur, Emdjii, Llyn, Nacridan (Simon), Kurien, Philecolo, Vyka, Kyrra, Zaza, Hain, Khazin, Mighty Duck, Nidaime, Chistophe Poissenot, Belette, Lord Thergal, Aé Li";
$str .= "</td></tr>\n";

// $str.="</table></div><br/><br/><br/><br/>\n";

$str .= "</table><br/><br/> Et peut-être un jour, vous-même ? ... ;-) <br/><br/>  </div>\n";
$str .= "<div style='color: #999999; '> Un oubli ? une mise-à-jour ? ... passez dans le forum pour le signaler. </div></div>";


$BOTTOMTitle->add("Nos soutiens financiers");
$BOTTOMMainArea->add($str);

$MAIN_PAGE->render();

?>
