<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

$BOTTOMTitle->add(
    "<div style='font-size: 1.5em; margin-top:50px;'>Woups ! Nous sommes désolé, vous avez trouvé un bug important.<br> Passez dans le forum pour le signaler, les développeurs ont les infos nécessaires pour le corriger. </div>");
$BOTTOMTitle->add(
    "<a class='statsevent' href='" . CONFIG_HOST . "/include/login.inc.php'><img src='" . CONFIG_HOST . "/pics/new-homepage/play-button-parchment-with-frame.png' width='400' ></a>");

$MAIN_PAGE->render();

?>
