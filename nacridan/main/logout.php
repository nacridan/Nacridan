<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/functions_forum.php");
page_load(array(
    "sess" => "Session",
    "auth" => "Auth"
));

// ini_set('display_errors',1);

/**
 * ***************************************************************************
 */
/**
 * ************** DECONNEXION AUTOMATIQUE AU FORUM ***************************
 */
/**
 * ***************************************************************************
 */

/*
 *
 * if(isset($_SESSION['forum_connected']) && ($_SESSION['forum_connected'] == true) && isset($_SESSION['forum_user_id']))
 * {
 *
 *
 * $DBforumhostname = MYSQLHOST;
 * $DBforumusername = LOGIN;
 * $DBforumpassword = PASSWD;
 *
 * try {
 * $dbh = new PDO("mysql:host=$DBforumhostname;dbname=Forumv2", $DBforumusername, $DBforumpassword);
 *
 *
 * $stmt1 = $dbh->prepare("DELETE FROM flux_online WHERE user_id=".$_SESSION['forum_user_id']);
 * $stmt1->execute();
 *
 * $stmt2 = $dbh->prepare("UPDATE flux_users SET last_visit=".time()." WHERE id=".$_SESSION['forum_user_id']);
 * $stmt2->execute();
 *
 * pun_setcookie(1, pun_hash(uniqid(rand(), true)), time() + 31536000);
 * $_SESSION['forum_connected'] = false;
 *
 * $dbh = null;
 * }
 * catch(PDOException $e)
 * {
 * echo $e->getMessage();
 * }
 *
 *
 *
 * }
 * //else
 * // trigger_error('did not pass in auto logout !!! ');
 */

/**
 * ***************************************************************************
 */
/**
 * ************** FIN - DECONNEXION AUTOMATIQUE AU FORUM *********************
 */
/**
 * ***************************************************************************
 */

$sess->destroy();
$auth->logout();

echo redirect(CONFIG_HOST . "/include/login.inc.php");

?>