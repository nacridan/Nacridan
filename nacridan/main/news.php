<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");



$db = DB::getDB();



$dbn = new DBCollection("SELECT * FROM News WHERE type=2 ORDER BY id DESC", $db);

$BOTTOMTitle->add("<div class='registration_title'><b>Les anciennes annonces</b></div>");
$BOTTOMTitle->add("<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");



while (! $dbn->eof()) {
    $date = strtotime($dbn->get("date"));
    $str = "<div style='color: #EEEEEE;'>" . gmdate("d-m-Y", $date) . "<b><center>" . $dbn->get("title") . "</center></b><br/><br/>";
    $str .= bbcode($dbn->get("content")) . "<br/><br/>";
    
    $str .= "</div>";
    $str .= "<div style='color: #EEEEEE'>";
    
    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbn->get("id_Player"), $db);
    
    $str .= "<i>" . $dbp->get("name") . "</i>";
    $str .= "<br/><br/>";
    $str .= "</div>";
    $BOTTOMTitle->add($str);
    $BOTTOMTitle->add("<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
    $dbn->next();
}



$MAIN_PAGE->render();

?>
