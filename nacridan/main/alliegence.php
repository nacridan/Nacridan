<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

require_once (HOMEPATH . "/class/DBObject.inc.php");

$db = DB::getDB();
$nacridan = null;

$lang = "fr";
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = null;
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

if (isset($_GET["id"]))
    $_GET["id"] = quote_smart($_GET["id"]);

if (isset($_POST["NextEvt_x"]) || isset($_POST["NextEvt_y"])) {
    if (isset($_GET["id"])) {
        $dbp = new DBCollection("SELECT id FROM Team WHERE 1", $db);
        $_GET["id"] = min($dbp->count(), $_GET["id"] + 1);
    }
}

if (isset($_POST["PrevEvt_x"]) || isset($_POST["PrevEvt_y"])) {
    if (isset($_GET["id"]))
        $_GET["id"] = max(1, $_GET["id"] - 1);
}

if (isset($_POST["search"])) {
    if (is_numeric($_POST["search"])) {
        $_GET["id"] = $_POST["search"];
    } else {
        $dbp = new DBCollection("SELECT id FROM Team WHERE name='" . quote_smart($_POST["search"]) . "'", $db);
        if (! $dbp->eof()) {
            $_GET["id"] = $dbp->get("id");
        }
    }
}

if (isset($_GET["id"])) {
    $id = quote_smart($_GET["id"]);
    $team = new Team();
    $team->externDBObj("TeamBody");
    $team->load($id, $db);
    
    $teambody = $team->getObj("TeamBody");
    
    $str = "<table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    
    $str .= "<b><h1>" . $team->get('name') . " (" . $team->get('id') . ")</h1></b></td>\n";
    
    $str .= "</tr><tr>";
    $str .= "<td align='right'>\n";
    $extra = "";
    
    if (isset($_GET["body"])) {
        $extra = "&body=1";
    }
    
    $str .= "<form method='POST' action='?id=" . $_GET["id"] . $extra . "'>" . localize("Recherche (n°/nom)") .
         " <input name='search' type='text' size=30> <input style='vertical-align:middle' id='Previous' class='eventbtnext' type='image' name='PrevEvt' src='../pics/misc/left.gif' height='16px'> <input style='vertical-align:middle' id='Previous' class='eventbtprevious' type='image' name='NextEvt' src='../pics/misc/right.gif' height='16px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    $str .= "</tr>\n";
    $str .= "</table>\n";
    $str .= "<a href='alliegance.php?id=" . $id . "' class='tabmenu'>&nbsp;&nbsp;" . localize('I N F O R M A T I O N') . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;\n";
    $str .= "<a href='alliegance.php?id=" . $id . "&body=1' class='tabmenu'>" . localize('M E M B R E S') . "</a>\n";
    
    $str .= "<br/><br/>\n";
    
    /*
     * $str.="<table><tr><td valign='top'>\n";
     * $str.="<table class='maintable' width='270px'>\n";
     * $str.="<tr>\n";
     * $str.="<td colspan='2' class='mainbgtitle'><b>".localize('I N F O R M A T I O N')."</b>\n";
     * $str.="</td>\n";
     * $str.="</tr>\n";
     *
     * $str.="<tr><td class='mainbglabel' width='110px'><b>".localize('NOM :')."</b></td><td class='mainbgbody'>".$team->get('name')."</td></tr>\n";
     *
     * $str.="</table>\n";
     * $str.="<br/>\n";
     */
    if (! isset($_GET["body"])) {
        $str .= "<table class='maintable' width='640px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        
        $str .= "<tr><td class='mainbgbody'>" . bbcode($teambody->get("body")) . "</td></tr>\n";
        $str .= "</table>";
        
        // $str.="</td><td> <img src='".getImgSrc($curplayer)."'></td></tr>";
        // $str.="</table>";
        
        $MAIN_BODY->add($str);
    } else {
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr><td colspan='3' class='mainbgtitle'><b>" . localize('P E R S O N N A G E S') . "</b></td></tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel' width='30' align='center'>" . localize('Niveau') . "</td>\n";
        $str .= "<td class='mainbglabel' width='175'>" . localize('Nom') . "</td>\n";
        $str .= "<td class='mainbglabel' width='175' >" . localize('Grade') . "</td>\n";
        $str .= "</tr>\n";
        
        $idRetrieve = $_GET["id"];
        if (is_numeric($idRetrieve)) {
            // Player
            $dbplayer = new DBCollection("SELECT Player.id,Player.name,Player.id_Team,level,racename,TeamRankInfo.name AS rank FROM Player LEFT JOIN TeamRank ON Player.id=TeamRank.id_Player LEFT JOIN TeamRankInfo ON TeamRankInfo.id=TeamRank.id_TeamRankInfo WHERE Player.id_Team=" . $idRetrieve, $db, 0, 0);
            
            $data = array();
            $dist = array();
            $distVal = 0;
            while (! $dbplayer->eof()) {
                $level = $dbplayer->get("level");
                
                $name = "<a href='../conquest/profile.php?id=" . $dbplayer->get("id") . "' class='stylepc popupify'>" . $dbplayer->get("name") . "</a>";
                
                $alliegance = $dbplayer->get("rank");
                
                $data[] = array(
                    "level" => $level,
                    "name" => $name,
                    "race" => $dbplayer->get("racename"),
                    "id" => $dbplayer->get("id"),
                    "grade" => $alliegance
                );
                $dbplayer->next();
            }
            foreach ($data as $arr) {
                $str .= "<tr>\n";
                $str .= "<td class='mainbgbody' align='center'> " . $arr["level"] . " </td>\n";
                $str .= "<td class='mainbgbody'>" . $arr["name"] . " (" . $arr["race"] . ")</td>\n";
                $str .= "<td class='mainbgbody' align='left'> " . $arr["grade"] . " </td>\n";
            }
        }
        $str .= "</table>\n";
        $MAIN_BODY->add($str);
    }
    
    $MAIN_PAGE->render();
}
