<?php

class Land
{

    public $m_map;

    public $m_db;

    public $m_typeLand;

    function Land($typeLand, $db)
    {
        $this->initNPC($typeLand, $db);
    }

    function initNPC($typeLand, $db)
    {
        $this->m_typeLand = $typeLand;
        // echo "SELECT * FROM BasicRace WHERE PC='NO' AND ".$typeLand."!=-1 AND ".$typeLand."!=0\n";
        $dbnpc = new DBCollection("SELECT * FROM BasicRace WHERE PC='NO' AND " . $typeLand . "!=-1 AND " . $typeLand . "!=0", $db, 0, 0);
        while (! $dbnpc->eof()) {
            $freq = $dbnpc->get("frequency");
            if ($freq > 1)
                $freq *= ($freq - 1);
            $value = $freq * $dbnpc->get($typeLand);
            $this->m_map[$dbnpc->get("id")]["min"] = $dbnpc->get("levelMin");
            $this->m_map[$dbnpc->get("id")]["max"] = $dbnpc->get("levelMax");
            $this->m_map[$dbnpc->get("id")]["value"] = $value;
            $dbnpc->next();
        }
    }

    function getIDRndNPC(&$level)
    {
        $val = rand(1, PREC);
        if ($val >= (PREC * 0.99)) {
            $level = $level + rand(5, 10);
        } else {
            if ($val >= (PREC * 0.80)) {
                $level = $level + rand(3, 4);
            } else {
                if ($val >= (PREC * 0.5)) {
                    $level = $level + rand(1, 2);
                }
            }
        }
        return $this->getIDNPC($level);
    }

    function getIDNPC($level)
    {
        // echo "LEVEL: ".$level."\n";
        if (isset($this->m_map)) {
            /*
             * $total=0;
             * foreach($this->m_map as $key => $val)
             * {
             * if($level>=$val["min"] && $level<=$val["max"])
             * {
             * $total+=$val["value"];
             * }
             * }
             */
            // $curtotal=0;
            $vector = new VectorProba();
            foreach ($this->m_map as $key => $val) {
                if ($level >= $val["min"] && $level <= $val["max"]) {
                    $vector->pushBack($key, $val["value"]);
                }
            }
            $vector->setInterval(PREC);
            $index = $vector->searchUpperOrEqual(rand(1, PREC));
            if ($index >= 0) {
                $ret = $vector->getKey($index);
                return $ret;
            } else
                return - 1;
        }
        return - 1;
    }
}

?>