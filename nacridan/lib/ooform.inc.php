<?php

/*
 * ooform
 *
 * Object Oriented Forms (a light version of oohfrom.inc (phplib))
 *
 * by Jails
 *
 */
class oo_element
{

    public $m_name;

    public $m_value;

    public $m_exclude;

    public $m_extrahtml;

    public $m_error;

    public $m_errorclass;

    function self_get($val)
    {}

    function self_show($val)
    {
        print $this->self_get($val);
    }

    function self_validate($val)
    {
        return false;
    }

    function self_load_default($val)
    {
        $this->m_value = $val;
    }
    
    // Helper function for compatibility
    function setup_element($a)
    {
        foreach ($a as $k => $v) {
            $var = "m_" . $k;
            $this->$var = $v;
        }
    }
}
// end ELEMENT
class oo_hidden extends oo_element
{

    public $m_hidden = 1;

    function oo_hidden($a)
    {
        $this->setup_element($a);
    }

    function self_get($val)
    {
        $str = "";
        
        $v = $this->m_value;
        $n = $this->m_name;
        $str .= "<input type='hidden' name='$n' value='$v'";
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= "/>";
        
        return $str;
    }
}
// end HIDDEN
class oo_reset extends oo_element
{

    public $m_src;

    function oo_reset($a)
    {
        $this->setup_element($a);
    }

    function self_get($val)
    {
        $str = "<input name='$this->m_name' type=reset value='$val'";
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= "/>";
        
        return $str;
    }
}
// end RESET
class oo_submit extends oo_element
{

    public $m_src;

    function oo_submit($a)
    {
        $this->setup_element($a);
    }

    function self_get($val)
    {
        $str = "";
        
        $sv = empty($val) ? $this->m_value : $val;
        $str .= "<input name='$this->m_name' value='$sv'";
        if ($this->m_src) {
            $str .= " type='image' src='$this->src'";
        } else {
            $str .= " type='submit'";
        }
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= "/>";
        
        return $str;
    }

    function self_load_default($val)
    {
        // SUBMIT will not change its value
    }
}
// end SUBMIT
class ooform
{

    public $m_elements;

    public $m_hidden;

    public $m_jvs_name;

    function ooform()
    {
        $this->m_elements = array();
    }

    function clear()
    {
        $this->m_elements = array();
    }

    function get_start($method = "", $action = "", $target = "", $form_name = "")
    {
        global $_SERVER;
        
        $str = "";
        
        if (! $method) {
            $method = "post";
        }
        if (! $action) {
            $action = $_SERVER["PHP_SELF"];
        }
        if (! $target) {
            $target = "_self";
        }
        
        $str .= "<form name='$form_name' ";
        $str .= " method='$method'";
        $str .= " action='$action'";
        $str .= " target='$target'";
        
        if ($this->m_jvs_name) {
            $str .= " onsubmit=\"${form_name}InitValidation();return {$this->m_jvs_name}\"";
        }
        $str .= ">";
        
        return $str;
    }

    function get_js_validation($form_name)
    {
        $str = "";
        $str .= "<script type=\"text/javascript\" src=\"" . CONFIG_HOST . "/javascript/jsval.js\"></script>\n";
        $str .= "<script type=\"text/javascript\">\n//<![CDATA[\n";
        $str .= "function ${form_name}InitValidation() {\n";
        $str .= "var objForm = document.forms[\"${form_name}\"];\n";
        foreach ($this->m_elements as $name => $val) {
            
            $el = $val["ob"];
            if (isset($el->m_valid_regex)) {
                $str .= "objForm.${name}.regexp = /{$el->m_valid_regex}/;\n";
            }
            
            if (isset($el->m_valid_e)) {
                $str .= "objForm.${name}.err = \"{$el->m_valid_e}\";\n";
            }
            
            if (isset($el->m_maxvalue)) {
                $str .= "objForm.${name}.maxvalue = '{$el->m_maxvalue}';\n";
            }
            
            if (isset($el->m_minvalue)) {
                $str .= "objForm.${name}.minvalue = '{$el->m_minvalue}';\n";
            }
            
            if (isset($el->m_exclude)) {
                $str .= "objForm.${name}.exclude = '{$el->m_exclude}';\n";
            }
            
            if (isset($el->m_maxlength)) {
                $str .= "objForm.${name}.maxlength = '{$el->m_maxlength}';\n";
            }
            
            if (isset($el->m_minlength)) {
                $str .= "objForm.${name}.minlength = '{$el->m_minlength}';\n";
            }
            
            if (isset($el->m_errorclass)) {
                $str .= "objForm.${name}.errorclass = '{$el->m_errorclass}';\n";
            }
            
            if (isset($el->m_targetid)) {
                $str .= "objForm.${name}.targetid= '{$el->m_targetid}';\n";
            }
            if (isset($el->m_targetmsg)) {
                $str .= "objForm.${name}.targetmsg = \"{$el->m_targetmsg}\";\n";
            }
            if (isset($el->m_targeterr)) {
                $str .= "objForm.${name}.targeterr = '{$el->m_targeterr}';\n";
            }
            if (isset($el->m_targetcss)) {
                $str .= "objForm.${name}.targetcss = '{$el->m_targetcss}';\n";
            }
        }
        $str .= "}\n//]]>\n</script>\n";
        return $str;
    }

    function gen_js_validation($jvs_name = "", $form_name = "")
    {
        if ($form_name != "") {
            $this->m_jvs_name = $jvs_name;
            return $this->get_js_validation($form_name);
        }
    }

    function start($method = "", $action = "", $target = "", $form_name = "")
    {
        print $this->get_start($method, $action, $target, $form_name);
    }

    function get_finish($after = "", $before = "")
    {
        $str = "";
        
        if ($this->m_hidden) {
            foreach ($this->m_hidden as $k => $elname) {
                $str .= $this->get_element($elname);
            }
        }
        $str .= "</form>";
        
        return $str;
    }

    function finish($after = "", $before = "")
    {
        print $this->get_finish($after, $before);
    }

    function add_element($tab)
    {
        if (! is_array($tab)) {
            return false;
        }
        
        $t = ("oo_" . $tab["type"]);
        
        if ($tab["type"] == "radio" && isset($this->m_elements[$tab["name"]]) && $this->m_elements[$tab["name"]]["ob"] != null) {
            $el = $this->m_elements[$tab["name"]]["ob"];
            $el->addValue($tab["value"]);
            if (isset($tab["checked"])) {
                $el->m_checked = $tab["checked"];
            }
        } else {
            $el = new $t($tab);
            $el->m_type = $t;
            
            if ($tab["type"] == "radio") {
                $el->addValue($tab["value"]);
            }
        }
        
        $this->m_elements[$el->m_name]["ob"] = $el;
        
        if (isset($el->m_hidden) && $el->m_hidden) {
            $this->m_hidden[] = $el->m_name;
        }
    }

    function get_element($name, $value = "")
    {
        $str = "";
        $x = 0;
        
        if (! isset($this->m_elements[$name])) {
            return false;
        }
        
        $el = $this->m_elements[$name]["ob"];
        
        if ($value == "") {
            $value = $el->m_value;
        }
        
        $str .= $el->self_get($value);
        return $str;
    }

    function show_element($name, $value = "")
    {
        print $this->get_element($name, $value);
    }

    function load_defaults($deflist)
    {
        foreach ($this->m_elements as $key => $val) {
            if (isset($deflist[$key])) {
                $el = $this->m_elements[$key]["ob"];
                $el->self_load_default($deflist[$key]);
                $this->m_elements[$key]["ob"] = $el; // no refs -> must copy back
            }
        }
    }

    function validate()
    {
        $allErr = array();
        foreach ($this->m_elements as $key => $val) {
            $el = $this->m_elements[$key]["ob"];
            
            if (isset($el->m_check) && $el->m_check == 1) {
                preg_match("/(.+)check$/", $key, $nc);
                $el2 = $this->m_elements[$nc[1]]["ob"];
                if (! ($el2->m_value == $el->m_value)) {
                    $ret = $el2->m_check_e;
                    $el->m_error = true;
                }
            } else {
                if (($ret = $el->self_validate($el->m_value)) != false) {
                    $el->m_error = true;
                }
            }
            $this->m_elements[$key]["ob"] = $el; // no refs -> must copy back
            if ($ret != "")
                $allErr[$key] = $ret;
        }
        return $allErr;
    }
} /* end FORM */

include ("oo_text.inc.php");
include ("oo_select.inc.php");
include ("oo_radio.inc.php");
include ("oo_checkbox.inc.php");
include ("oo_textarea.inc.php");
include ("oo_file.inc.php");

?>
