<?php

class MiniMapCase
{

    public $flags;

    public $pj;

    public $pnj;

    public $items;

    public $gate;

    public $building;

    public $place;

    public $exploitation;

    public $type;

    public $labelGround;

    public $groundPaCost;

    public $rose;

    public $xx;

    public $yy;

    public function MiniMapCase($type, $labelGround, $groundPaCost, $xx, $yy)
    {
        $this->flags = array(
            "PJ" => 0,
            "PNJ" => 0,
            "ITEM" => 0,
            "GOLD" => 0,
            "BUILDING" => 0,
            "GATE" => 0,
            "EXPLOITATION" => 0
        );
        $this->type = $type;
        $this->labelGround = $labelGround;
        $this->groundPaCost = $groundPaCost;
        $this->pnj = array();
        $this->items = array();
        $this->xx = $xx;
        $this->yy = $yy;
    }

    public function add_pj($pj)
    {
        $this->pj = $pj;
        $this->flags["PJ"] = 1;
    }

    public function add_rose($pj)
    {
        $this->rose = $pj;
        $this->flags["ROSE"] = 1;
    }

    public function add_pnj($pnj)
    {
        $this->pnj[] = $pnj;
        $this->flags["PNJ"] = 1;
    }

    public function add_item($item)
    {
        $this->items[] = $item;
        $this->flags["ITEM"] = 1;
    }

    public function add_gold($item)
    {
        $this->items[] = $item;
        $this->flags["GOLD"] = 1;
    }

    public function add_portail($gate)
    {
        $this->gate = $gate;
        $this->flags["GATE"] = 1;
    }

    public function add_building($building)
    {
        $this->building = $building;
        $this->flags["BUILDING"] = 1;
    }

    public function add_exploitation($exploitation)
    {
        $this->exploitation = $exploitation;
        $this->flags["EXPLOITATION"] = 1;
    }

    public function to_case()
    {
        $flags = $this->flags;
        $str = "";
        
        if ($flags["EXPLOITATION"]) {
            $exName = $this->exploitation["name"];
            $exType = $this->exploitation["type"];
            $validEx = 0;
            switch ($exType) {
                case 'deposit':
                    if ($exName == 'Emeraude') {
                        $str .= "<div class='minimapemeraude' ></div>";
                        $validEx = 1;
                    } else {
                        $str .= "<div class='minimaprubis'></div>";
                        $validEx = 1;
                    }
                    break;
                
                case 'bush':
                    switch ($exName) {
                        case 'Garnach':
                            $str .= "<div class='minimapgarnach'></div>";
                            $validEx = 1;
                            break;
                        
                        case 'Folliane':
                            $str .= "<div class='minimapfolliane'></div>";
                            $validEx = 1;
                            break;
                        
                        case 'Razhot':
                            $str .= "<div class='minimaprazhot'></div>";
                            $validEx = 1;
                            break;
                    }
                    break;
                
                case 'body':
                    if ($exName == 'Cuir') {
                        $str .= "<div class='minimapleather'></div>";
                        $validEx = 1;
                    } else {
                        $str .= "<div class='minimapscale'></div>";
                        $validEx = 1;
                    }
                    
                    break;
                
                case 'crop':
                    {
                        $str .= "<div class='minimapflaxfield'></div>";
                        $validEx = 1;
                    }
            }
        }
        
        if ($flags["GATE"]) {
            $gate = $this->gate;
            if ($gate["nbNPC"] > 0)
                $str .= "<div class='minimapgate'>";
            else
                $str .= "<div class='minimapgateinactive'>";
        }
        
        if ($flags["BUILDING"]) {
            $bd = $this->building;
            
            if ($bd["progress"] != $bd["sp"])
                $picname = "chantier.png";
            elseif (file_exists("../pics/map/buildings/" . $bd['pic']))
                $picname = $bd['pic'];
            else
                $picname = "unknownbuilding.png";
            
            $str .= "<div  class='minimapbuilding' style='background-image: url(../pics/map/buildings/" . $picname . ");'>";
        }
        
        if ($flags["PJ"]) {
            $style = "";
            if ($this->pj['picture'] == "" && file_exists("../pics/map/players/" . $this->pj['id_BasicRace'] . ".png")) {
                $picname = $this->pj['id_BasicRace'];
                if ($this->pj["gender"] == "F" && $this->pj["authlevel"] >= 0)
                    $picname .= "f";
            } elseif (file_exists("../pics/map/players/customized/" . $this->pj['picture'] . ".png")) {
                $picname = "customized/" . $this->pj['picture'];
            } else
                $picname = "unknown";
            if ($this->pj["hidden"] == 2)
                $style = "opacity:0.5;";
            $str .= "<div  class='minimappnj' style='background-image: url(../pics/map/players/" . $picname . ".png); " . $style . "'></div>";
        }
        
        if ($flags["PNJ"]) {
            foreach ($this->pnj as $testpnj) {
                if ($testpnj['picture'] != "" && file_exists("../pics/map/players/customized/" . $testpnj['picture'] . ".png")) {
                    $picname = "customized/" . $testpnj['picture'];
                } else {
                    if (file_exists("../pics/map/players/" . $testpnj['id_BasicRace'] . ".png"))
                        $picname = $testpnj['id_BasicRace'];
                    else
                        $picname = "unknown";
                }
                
                $str .= "<div  class='minimappnj' style='background-image: url(../pics/map/players/" . $picname . ".png);'></div>";
            }
        }
        // $str .="<div class='clr'></div>";
        
        if (($flags["ITEM"]) || ($flags["GOLD"]))
            $str .= "<div class='minimapitem'></div>";
        
        if ($flags["BUILDING"])
            $str .= "</div>";
        
        if ($flags["GATE"])
            $str .= "</div>";
        
        return $str;
    }

    public function case_type()
    {
        $type = $this->type;
        
        if ($type == "plain")
            return localize("Plaine");
        elseif ($type == "mountain")
            return localize("Montagne");
        elseif ($type == "ocean")
            return localize("Océan");
        elseif ($type == "marsh")
            return localize("Marais");
        elseif ($type == "forest")
            return localize("Forêt");
        elseif ($type == "river")
            return localize("Rivière");
        elseif ($type == "desert")
            return localize("Désert");
        elseif ($type == "polar")
            return localize("Plaine Arctique");
        elseif ($type == "cobblestone")
            return localize("Espace pavé");
        elseif ($type == "flowers")
            return localize("Plaine fleurie");
        elseif ($type == "beatenearth")
            return localize("Terre battue");
        else
            return localize("Inconnu");
    }
    // ------------------ Fonction de construction du panneau du bas (détail de la case et actions)
    public function to_panel($minimap, $db, $timeToMove)
    {
        $xx = $this->xx;
        $yy = $this->yy;
        $curplayer = $minimap->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $teamp = $curplayer->get("id_Team");
        // $can_move = $minimap->can_move($curplayer);
        $flags = $this->flags;
        // $timeToMove = $this->groundPaCost;//$curplayer->getTimeToMove($db);
        // onMouseOver='javascript:hideOverlay();'
        $str = "<div class='minimappanel' id='panel_" . $xx . "_" . $yy . "' >";
        
        // ------------ LIEU et PERSOS
        $str .= "<div class='minimappanelleft' >";
        
        $str .= "<table class='maintable minimappanelleftwidth'>";
        $str .= "<tr><td  class='mainbgtitle' align='center' > <b>Lieu</b> et <b>Personnages</b>  </td>";
        $str .= "</table>";
        
        // --------- DEPLACEMENT, créer une fonction de test can_move (terrain, prisonnier, case occupée etc ...)
        
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        $BAF = new BasicActionFactory();
        $moveStr = "";
        
        $dbp = new DBCollection(
            "SELECT id, id_BasicRace FROM Player WHERE hidden!=3 AND hidden!=13 AND hidden!=23 AND (status='PC' or id_BasicRace=263 or id_BasicRace=" . ID_BASIC_RACE_FEU_FOL .
                 " or id_BasicRace=160 or level<3) AND inbuilding=0 AND disabled=0 AND map=" . $map . " AND x=" . $xx . " AND y=" . $yy, $db, 0, 0);
        $dbe = new DBCollection("SELECT id FROM Exploitation WHERE (type='crop' or type='bush' or type='body') AND map=" . $map . " AND x=" . $xx . " AND y=" . $yy, $db, 0, 0);
        
        if ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= $timeToMove && $BAF->freePlace($xx, $yy, $map, $db, 0, 1) && ($curplayer->get("inbuilding") == 0) &&
             $curplayer->get("id_BasicRace") != 263 && $curplayer->get("id_BasicRace") != 112) {
            if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                $moveStr .= "</td><td valign='middle' align='center'><form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>";
                $moveStr .= "<input type='hidden' name='XYNEW'  value='" . $xx . "," . $yy . "' />";
                $moveStr .= "<input id='submitbt' type='submit' name='submitbt' value='Se déplacer &nbsp (" . $timeToMove . "PA)' />";
                $dbe = new DBCollection(
                    "SELECT BasicEvent.id, BasicEvent.description FROM Move LEFT JOIN BasicEvent ON Move.id_BasicEvent=BasicEvent.id WHERE Move.id_Player=" . $curplayer->get("id"), 
                    $db, 0, 0);
                if (! $dbe->eof() && $curplayer->get("state") == "walking") {
                    $item = array();
                    $moveStr .= "<br/><select  name='MOVE_TYPE'>";
                    $moveStr .= "<option value='1' selected='selected'>" . localize("-- normalement --") . "</option>";
                    while (! $dbe->eof()) {
                        $item[] = array(
                            localize("--" . substr($dbe->get("description") . "--", 35)) => $dbe->get("id")
                        );
                        $dbe->next();
                    }
                    
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value)
                            $moveStr .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                    $moveStr .= "</select>";
                }
                $moveStr .= "<input name='action' type='hidden' value='" . MOVE2D . "' />";
                $moveStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />";
                $moveStr .= "</form>";
            }
        } elseif ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= $timeToMove && $BAF->freePlace($xx, $yy, $map, $db, 0, 1) && ($curplayer->get("inbuilding") != 0) &&
             $curplayer->get("id_BasicRace") != 263) {
            if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                $moveStr .= "</td><td><form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                $moveStr .= "<input name='XYNEW' type='hidden' value='" . $xx . "," . $yy . "' />";
                // $moveStr.="<input name='YNEW' type='hidden' value='".$yy."' />";
                $moveStr .= "<input id='submitbt' type='submit' name='submitbt' value='Sortir du bâtiment' />";
                $moveStr .= "<input name='action' type='hidden' value='" . LEAVE_BUILDING . "' />";
                $moveStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $moveStr .= "</form>";
            }
        } 

        elseif ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= 2 && $dbp->count() > 0 && $dbp->get("id_BasicRace") == 160) {
            if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                while (! $dbp->eof()) {
                    if ($curplayer->canSeePlayerById($dbp->get("id"), $db) && ! ($curplayer->get("inbuilding") != 0 && $dbp->get("id_BasicRace") >= 5)) {
                        $moveStr .= "</td><td><form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                        $moveStr .= "<input name='TARGET_ID' type='hidden' value='" . $dbp->get("id") . "' />";
                        $moveStr .= "<input id='submitbt' type='submit' name='submitbt' value='Boire' />";
                        $moveStr .= "<input name='action' type='hidden' value='" . HOSTEL_DRINK . "' />";
                        $moveStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $moveStr .= "</form>";
                    }
                    $dbp->next();
                }
            }
        } 

        elseif ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= ($timeToMove + 0.5) && $dbp->count() > 0) {
            if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                if ($curplayer->canSeePlayerById($dbp->get("id"), $db) && ! ($curplayer->get("inbuilding") != 0 && $dbp->get("id_BasicRace") >= 5)) {
                    $moveStr .= "</td><td><form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                    $moveStr .= "<input name='TARGET_ID' type='hidden' value='" . $dbp->get("id") . "' />";
                    $moveStr .= "<input id='submitbt' type='submit' name='submitbt' value='Bousculer' />";
                    $moveStr .= "<input name='action' type='hidden' value='" . HUSTLE . "' />";
                    $moveStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $moveStr .= "</form>";
                }
            }
        } 

        elseif ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= DESTROY_AP && ($dbe->count() > 0)) {
            $curplayer->get("recall");
            $curplayer->get("level_up");
            $curplayer->get("state");
            if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                $moveStr .= "</td><td><form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                $moveStr .= "<input name='TARGET_ID' type='hidden' value='" . $dbe->get("id") . "' />";
                $moveStr .= "<input id='submitbt' type='submit' name='submitbt' value='Détruire (".DESTROY_AP."PA)' />";
                $moveStr .= "<input name='action' type='hidden' value='" . DESTROY . "' />";
                $moveStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $moveStr .= "</form>";
            }
        }
        
        $str .= "<table class='maintable minimappanelleftwidth'>";
        $str .= "<tr class='mainbgbody' ><td>" . $this->case_type() . "</td><td>  " . $xx . "/" . $yy . " " . $moveStr . "</td></tr>";
        
        $str .= "</table>";
        
        // --------------------------- PJ
        
        $str .= "<table class='maintable minimappanelleftwidth' >";
        if ($flags["PJ"]) {
            
            $alliegance = "";
            $pj = $this->pj;
            if ($pj["id_Team"] != 0)
                $alliegance = " de l'Ordre " . $pj["alliegance"];
            
            $str .= "<tr class='mainbgbody'><td width=50px>Niv. " . $pj["level"] . "</td> <td> " . $pj["name"] . " (id " . $pj["id"] . ") " . $alliegance . "  " . $pj["state"] .
                 "</td></tr>\n";
        }
        
        // --------------------------- BUILDINGS
        
        if ($flags["BUILDING"]) {
            
            $bd = $this->building;
            
            $enterStr = "";
            
            if ($curplayer->get("inbuilding") == 0)
                $dispStr = "Entrer";
            else
                $dispStr = "Passer dans le bâtiment voisin";
            
            if ($bd["progress"] == $bd["sp"] && distHexa($xx, $yy, $xp, $yp) == 1 && $curplayer->get("ap") >= $this->groundPaCost && ($BAF->canEnter($curplayer, $bd["id"], $db)) &&
                 $curplayer->get("id_BasicRace") != 263) {
                if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                    $enterStr .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                    $enterStr .= "<input type='hidden' name='IDBUILDING'  value='" . $bd["id"] . "' />";
                    $enterStr .= "<input id='submitbt' type='submit' name='submitbt' value='" . $dispStr . "' />";
                    $enterStr .= "<input name='action' type='hidden' value='" . ENTER_BUILDING . "' />";
                    $enterStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $enterStr .= "</form>";
                }
            } elseif ((distHexa($xx, $yy, $xp, $yp) == 1) && $curplayer->get("ap") >= DRINK_SPRING_AP && ($bd["type"] == 15) && $curplayer->get("id_BasicRace") != 263 &&
                 $curplayer->get("state") != "turtle") {
                if (! $curplayer->get("recall") && ! $curplayer->get("level_up") && $curplayer->get("state") != "questionning" && $curplayer->get("state") != 'prison') {
                    $enterStr .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
                    $enterStr .= "<input type='hidden' name='IDBUILDING'  value='" . $bd["id"] . "' />";
                    $enterStr .= "<input id='submitbt' type='submit' name='submitbt' value='Boire (" . DRINK_SPRING_AP . " PA)' />";
                    $enterStr .= "<input name='action' type='hidden' value='" . DRINK_SPRING . "' />";
                    $enterStr .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $enterStr .= "</form>";
                }
            }
            
            $compInfo = "";
            if ($bd["value"] > 0 && $bd["type"] == 10)
                $compInfo = " (en vente)";
            elseif ($bd["type"] == 10 && $bd["owner"] != "") {
                $compInfo = $bd["owner"];
            }
            if ($bd["type"] == 15) {
                $compInfo = ', Utilis.(s) restante(s) : ' . $bd["value"];
            }
            
            if ($bd["repair"] == 1)
                $str .= "<tr class='mainbgbody'><td width=90px >Niv. " . $bd["level"] . "</td> <td >  <span class='buildingname'>" . $bd["name"] . "</span> (id " . $bd["id"] .
                     ") <br/> En construction : " . (floor($bd["progress"] * 100 / $bd["sp"])) . "%</td></tr>\n";
            elseif ($bd["repair"] == 2)
                $str .= "<tr class='mainbgbody'><td width=90px >Niv. " . $bd["level"] . "</td> <td >  <span class='buildingname'>" . $bd["name"] . "</span> (id " . $bd["id"] .
                     ") <br/> En démolition : " . (100 - (floor($bd["progress"] * 100 / $bd["sp"]))) . "%</td></tr>\n";
            elseif ($bd["repair"] == 3)
                $str .= "<tr class='mainbgbody'><td width=90px >Niv. " . $bd["level"] . "</td> <td >  <span class='buildingname'>" . $bd["name"] . "</span> (id " . $bd["id"] .
                     ") <br/> En réparation : " . $bd["currsp"] . "/" . $bd["sp"] . "</td></tr>\n";
            else
                $str .= "<tr class='mainbgbody'><td width=90px >Niv. " . $bd["level"] . "</td> <td >  <span class='buildingname'>" . $bd["name"] . "</span> (id " . $bd["id"] . ") " .
                     $bd["cityname"] . "<br/> PS : " . $bd["currsp"] . "/" . $bd["sp"] . $compInfo . "</td></tr>\n";
            
            if (! ($bd["state"] == "" && $enterStr == ""))
                $str .= "<tr class='mainbgbody'><td> " . $bd["state"] . "</td><td align='center' valign='bottom'>" . $enterStr . "</td></tr>";
        }
        
        // --------------------------- EXPLOITATION
        
        if ($flags["EXPLOITATION"]) {
            
            $exp = $this->exploitation;
            $expType = "";
            switch ($exp["type"]) {
                case "deposit":
                    $expType = "Gisement";
                    break;
                
                case "bush":
                    $expType = "Buisson";
                    break;
                
                case "body":
                    $expType = "Dépouille";
                    break;
                
                case "crop":
                    $expType = "Champ";
                    break;
            }
            $str .= "<tr><td class='mainbgbody'> " . $expType . " niveau " . $exp["level"] . " : <span class='ressourcename'>" . $exp["name"] . "</span></td>\n";
        }
        
        // --------------------------- PORTAIL
        
        if ($flags["GATE"]) {
            $gate = $this->gate;
            $str .= "<tr class='mainbgbody'> <td width=50px>Niv. " . $gate["level"] . "</td> <td> " . $gate["name"] . " (id : " . $gate["id"] . ")</td></tr>\n";
        }
        
        // ---------------------------- PNJ -------------------
        if ($flags["PNJ"]) {
            foreach ($this->pnj as $pnj) {
                $str .= "<tr class='mainbgbody'><td width=50px>Niv. " . $pnj["level"] . "</td> <td> " . $pnj["name"] . " " . $pnj["state"] . " " . $pnj["owner"] . " </td></tr>\n";
            }
        }
        $str .= "</table>";
        $str .= "</div>";
        // ------------ OBJETS------
        
        $str .= "<div class='minimappanelright' >";
        
        $str .= "<table class='maintable minimappanelrightwidth'>";
        $str .= "<tr><td class='mainbgtitle' align='center'> <b>Objets</b>  </td></tr>";
        $str .= "</table>";
        
        $str .= "<table class='maintable minimappanelrightwidth'>";
        if ($flags["ITEM"] || $flags["GOLD"]) {
            
            foreach ($this->items as $item) {
                
                $str .= "<tr class='mainbgbody'> " . $item["name"] . " </td></tr>\n";
            }
        }
        $str .= "</table>";
        
        $str .= "</div>";
        $str .= "</div>";
        
        return $str;
    }
}

?>
