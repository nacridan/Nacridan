<?php

class oo_file extends oo_element
{

    public $m_size;

    function oo_file($a)
    {
        $this->setup_element($a);
    }

    function self_get($val)
    {
        $str = "";
        $str .= "<input type='hidden' name='MAX_FILE_SIZE' value=$this->m_size>\n";
        $str .= "<input type='file' name='$this->m_name'";
        if ($this->m_errorclass && $this->m_error == true) {
            $str .= " class='$this->m_errorclass'";
        }
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= ">";
        return $str;
    }
} // end FILE
