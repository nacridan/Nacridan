<?php

// TODO, meilleure gestion des années bisextile, actuellement code valide de 2013 à 2016.
class NacridanCalendar
{

    private $currentRealDate;

    private $v1startRealTime;

    private $v1startArtassianYear;

    private $ArtMonth;

    private $ArtWeek;

    private $ArtDay;

    function NacridanCalendar()
    {
        $this->currentRealDate = new DateTime('now');
        $this->v1startRealTime = new DateTime('2006-09-01');
        $this->v1startArtassianYear = 887;
        $this->ArtMonth = array(
            1 => 'Fauchebrin',
            2 => 'Roussylve',
            3 => 'Sombrebruine',
            4 => 'Morterre',
            5 => 'Froidebise',
            6 => 'Grieffroi',
            7 => 'Clairsol',
            8 => 'Boisvert',
            9 => 'Vifazur',
            10 => 'Semailles',
            11 => 'Jachère',
            12 => 'Orchaume'
        );
        
        $this->ArtWeek = array(
            1 => 'première semaine',
            2 => 'deuxième semaine',
            3 => 'troisième semaine',
            4 => 'quatrième semaine'
        );
        $this->ArtDay = array(
            1 => 'Jour du Labeur',
            2 => 'Jour des Seigneurs',
            3 => 'Jour des Etoiles',
            4 => 'Jour des Ténèbres',
            5 => 'Jour des Héraults',
            6 => 'Jour d\'Arryn',
            7 => 'Jour d\'Hélion'
        );
    }

    function RealToArtassian($dateToConvert)
    {
        $this->currentRealDate = new DateTime($dateToConvert);
        $this->v1startRealTime = new DateTime('2006-09-01');
        $interval = $this->v1startRealTime->diff($this->currentRealDate);
        $currentArtassianYear = $this->v1startArtassianYear + intval($interval->format('%y'));
        $Nd = $interval->format('%a');
        
        $Nd - 2; // 2 jours en moins pour les deux années bissextile
        $NYears = floor($Nd / 365);
        $currentArtassianYear = $this->v1startArtassianYear + $NYears;
        $NdcurrY = $Nd - ($NYears * 365 + 2);
        $NArtMonth = floor($NdcurrY / (4 * 7 + 2)); // 4 semaine 7 jours et 2 jours sacrés par mois
        $NdcurrM = $NdcurrY - $NArtMonth * (4 * 7 + 2);
        $currentArtassianWeek = "";
        if ($NdcurrM == 0)
            $currentArtassianDay = 'Second jour sacré';
        elseif ($NdcurrM == 29)
            $currentArtassianDay = 'Premier jour sacré';
        else {
            $Nweeks = floor($NdcurrM / 7);
            $currentArtassianWeek .= $this->ArtWeek[($Nweeks + 1)];
            
            $Nday = $NdcurrM - $Nweeks * 7;
            $currentArtassianDay = $this->ArtDay[($Nday + 1)];
            $currentArtassianDay = $currentArtassianDay . " de la " . $currentArtassianWeek;
        }
        
        $currentArtassianMonth = $this->ArtMonth[($NArtMonth + 1)];
        
        return $currentArtassianDay . " du mois de " . $currentArtassianMonth . " de l'an " . $currentArtassianYear;
    }
}

?>