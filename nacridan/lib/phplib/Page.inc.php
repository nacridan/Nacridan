<?php

function page_open($feature)
{
    if (isset($feature["sess"])) {
        global $sess;
        $config["enabled"] = "true";
        $sess = new $feature["sess"]($config);
        $sess->start();
        
        if (isset($feature["auth"])) {
            global $auth;
            if (! $sess->has("auth")) {
                $auth = new $feature["auth"]();
                $auth->start();
                $sess->set("auth", $auth);
            } else {
                $auth = $sess->get("auth");
                $auth->start();
                $sess->set("auth", $auth);
            }
        }
    }
    
    if (isset($_POST["username"]) && isset($_POST["password"])) {
        if (strtotime($_SESSION["auth"]->auth["lastlog"]) < strtotime("2007-08-31 22:59:00")) {
            redirect(CONFIG_HOST . "/main/disclaimer.php");
        }
        
        $query = sprintf("UPDATE Member SET lastlog='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" . $_SESSION["auth"]->auth["uid"]);
        
        $db = DB::getDB();
        $dbc = new DBCollection($query, $db, 0, 0, false);
    }
}

function page_load($feature)
{
    if (isset($feature["sess"])) {
        global $sess;
        $config["enabled"] = "true";
        $sess = new $feature["sess"]($config);
        $sess->start();
        
        if (isset($feature["auth"])) {
            global $auth;
            if (! $sess->has("auth")) {
                $auth = new $feature["auth"]();
                $sess->set("auth", $auth);
            } else {
                $auth = $sess->get("auth");
                $sess->set("auth", $auth);
            }
        }
    }
}
?>