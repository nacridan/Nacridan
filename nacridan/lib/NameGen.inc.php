<?php

class NameGen
{

    public $m_race;

    public $m_syllable;

    public $m_keytab;

    public $m_syltab;

    function NameGen($race)
    {
        $this->m_race = $race;
        $this->m_race = strtolower($this->m_race);
        
        switch ($this->m_race) {
            case "nain":
                
                $this->m_syllable["nain"][0] = array(
                    "a" => 3,
                    "e" => 3,
                    "i" => 2,
                    "o" => 2,
                    "u" => 1,
                    "y" => 1
                );
                $this->m_syllable["nain"][1] = array(
                    "b" => 1,
                    "c" => 1,
                    "d" => 1,
                    "f" => 1,
                    "g" => 3,
                    "h" => 1,
                    "j" => 1,
                    "k" => 4,
                    "kr" => 1,
                    "m" => 2,
                    "n" => 2,
                    "p" => 2,
                    "r" => 4,
                    "t" => 2,
                    "thr" => 1,
                    "th" => 1,
                    "v" => 1,
                    "x" => 1,
                    "z" => 1
                );
                $this->m_syllable["nain"][2] = array(
                    "in" => 1,
                    "an" => 1,
                    "ak" => 1,
                    "or" => 1,
                    "on" => 1,
                    "ok" => 1,
                    "oïn" => 1,
                    "aïn" => 1,
                    "im" => 1,
                    "aïm" => 1,
                    "oïm" => 1
                );
                $this->m_syllable["nain"][3] = array(
                    " le grand" => 1,
                    " le fourbe" => 1,
                    " le tégneux" => 1,
                    " le sage" => 1,
                    " le preux" => 1,
                    " le premier" => 1,
                    " II" => 1,
                    " troisième du nom" => 1,
                    " le vaillant" => 1,
                    " le robuste" => 1,
                    " le rusé" => 1
                );
                
                $this->m_keytab["nain"] = array(
                    "0,1+0/1+0/1+2,,3,1+2+3"
                );
                
                break;
            default:
                printf("Unknown race %s <br/>", $this->m_race);
                break;
        }
        
        $this->m_syltab = array();
        $this->buildFreq($this->m_syllable[$this->m_race]);
    }

    function buildFreq()
    {
        $cpt = 0;
        
        if ($this->m_syllable[$this->m_race]) {
            foreach ($this->m_syllable[$this->m_race] as $key) {
                foreach ($key as $val => $nb) {
                    for ($i = 0; $i < $nb; $i ++) {
                        $this->m_syltab[$cpt][] = $val;
                    }
                }
                $cpt ++;
            }
        }
    }

    function writeSyl($tok)
    {
        $syl = "";
        if ($tok != null) {
            $tab = split("\+", $tok);
            foreach ($tab as $num) {
                $size = count($this->m_syltab[$num]);
                $syl .= $this->m_syltab[$num][rand(0, $size - 1)];
            }
        }
        return $syl;
    }

    function gen()
    {
        $size = count($this->m_keytab[$this->m_race]);
        $strkey = $this->m_keytab[$this->m_race][rand(0, $size - 1)];
        
        $token = split("/", $strkey);
        
        $name = "";
        
        foreach ($token as $tok) {
            $tab = split(",", $tok);
            $size = count($tab);
            
            if ($size > 1) {
                $name .= $this->writeSyl($tab[rand(0, $size - 1)]);
            } else {
                $name .= $this->writeSyl($tok);
            }
        }
        return $name;
    }
}

?>
