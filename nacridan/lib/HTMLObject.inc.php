<?php
require_once (HOMEPATH . "/lib/timer/Timer.php");

class HTMLCounter
{

    public static $nextID;
}

class HTMLSingle
{

    public $extra;

    public $type;

    public $id;

    public $enableCache;

    public $cacheLite;

    public function HTMLSingle($type, $id = "", $extra = "")
    {
        if ($id == "") {
            $this->id = $this->allocateID($type);
        } else {
            $this->id = $id;
        }
        
        $this->type = $type;
        $this->extra = $extra;
    }

    protected function allocateID($type)
    {
        HTMLCounter::$nextID ++;
        return $type . HTMLCounter::$nextID;
    }

    public function enableCache($cacheLite, $idCache)
    {
        $this->enableCache = true;
        $this->cacheLite = $cacheLite;
        $this->idCache = $idCache;
    }

    public function remove($idCache)
    {
        $cacheLite->remove($idCache);
    }

    public function toString()
    {
        $str = "<" . $this->type . " " . $this->extra . " />\n";
        return $str;
    }

    public function render()
    {
        if ($this->enableCache) {
            if ($data = $this->cacheLite->get($this->idCache)) {
                echo $data;
            } else {
                $data = $this->toString();
                $this->cacheLite->save($data, $this->idCache);
                echo $data;
            }
        } else
            echo $this->toString();
    }
}

class HTMLObject extends HTMLSingle
{

    public $doctype;

    public $content;

    public function HTMLObject($type, $id = "", $extra = "")
    {
        parent::HTMLSingle($type, $id, $extra);
        $doctype = "";
        $this->content = array();
    }

    public function setDoctype($doctype)
    {
        $this->doctype = $doctype;
    }

    public function add($html)
    {
        if (is_object($html)) {
            if ($html->id == "")
                $html->id = $this->allocateID("unknown");
            
            $this->content[$html->id] = $html;
        } else {
            $id = $this->allocateID("string");
            $this->content[$id] = $html;
        }
    }

    public function getObjectById($id)
    {
        $tabobject = $this;
        $ret;
        foreach (explode(":", $id) as $key => $val) {
            if (isset($tabobject->content[$val])) {
                $ret = $tabobject->content[$val];
                if (is_object($ret)) {
                    $tabobject = $ret;
                }
            } else {
                throw new Exception("Undefinied ID (" . $id . ")");
            }
        }
        return $ret;
    }

    public function addNewHTMLObject($type, $id = "", $extra = "")
    {
        if ($id == "") {
            $id = $this->allocateID($type);
        }
        
        $html = new HTMLObject($type, $id, $extra);
        $this->content[$id] = $html;
        // $this->pos++;
        return $html;
    }

    public function toString()
    {
        $str = $this->doctype . "\n";
        $str .= "<" . $this->type . " " . $this->extra . ">\n";
        
        foreach ($this->content as $key => $val) {
            if (is_object($val)) {
                $str .= $val->toString();
            } else {
                $str .= $val;
            }
        }
        $str .= "</" . $this->type . ">\n";
        return $str;
    }
}

class HTMLInput extends HTMLSingle
{

    public $name;

    public $value;

    public function HTMLInput($type, $name, $value, $id = "", $extra = "", $reload = 1)
    {
        parent::HTMLSingle($type, $id, $extra);
        $this->name = $name;
        $this->value = $value;
        if (isset($_POST[$name]) && $reload) {
            $this->value = $_POST[$name];
        }
    }

    public function toString()
    {
        $str = "<input type='" . $this->type . "' name='" . $this->name . "' value='" . $this->value . "' " . $this->extra . "/>\n";
        
        return $str;
    }
}

class HTMLSelector extends HTMLSingle
{

    public $name;

    public $values;

    public $selected;

    public $multiple;

    public function HTMLSelector($name, $id = "", $extra = "", $multiple = false, $reload = 1)
    {
        parent::HTMLSingle($type, $id, $extra);
        $this->multiple = $multiple;
        $this->name = $name;
        
        if (isset($_POST[$name]) && $reload) {
            if (is_array($_POST[$name]) && $this->multiple) {
                foreach ($_POST[$name] as $key => $value) {
                    $this->selected[] = $value;
                }
            } else {
                $this->selected = $_POST[$name];
            }
        }
    }

    public function addOption($label, $value, $selected = false, $extra = "")
    {
        $selattr = "";
        if ($selected)
            $selattr = "selected=\"selected\"";
        
        if (is_array($this->selected)) {
            foreach ($this->selected as $key => $postvalue) {
                if ($value == $postvalue) {
                    $selattr = "selected=\"selected\"";
                } else {
                    $selattr = "";
                }
            }
        } else {
            if ($this->selected == $value)
                $selattr = "selected=\"selected\"";
        }
        
        $this->values[] = "<option value='" . $value . "' " . $selattr . ">" . $label . "</option>\n";
    }

    public function addGroup($label, $extra = "")
    {
        $this->values[] = "<optgroup " . $extra . " label='" . $label . "'/>\n";
    }

    public function toString()
    {
        $str = "<select name='" . $this->name . "' " . $this->extra . " >\n";
        foreach ($this->values as $key => $option) {
            $str .= $option;
        }
        $str .= "</select>\n";
        return $str;
    }
}

?>