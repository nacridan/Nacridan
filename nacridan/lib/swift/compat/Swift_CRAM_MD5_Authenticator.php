<?php

/**
 * CRAM-MD5 authenticator compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/Authenticator/CRAMMD5.php');

class Swift_CRAM_MD5_Authenticator extends Swift_Authenticator_CRAMMD5
{
}

?>