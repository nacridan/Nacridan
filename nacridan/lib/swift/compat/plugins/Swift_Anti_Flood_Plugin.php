<?php

/**
 * Anti-flood plugin compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/../Swift/Plugin/AntiFlood.php');

class Swift_Anti_Flood_Plugin extends Swift_Plugin_AntiFlood
{
}

?>