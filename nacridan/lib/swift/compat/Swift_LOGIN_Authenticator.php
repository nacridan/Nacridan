<?php

/**
 * LOGIN authenticator compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/Authenticator/LOGIN.php');

class Swift_LOGIN_Authenticator extends Swift_Authenticator_LOGIN
{
}

?>