<?php
require_once '../config.php';
require_once SIMPLETEST_BASE . '/unit_tester.php';
require_once SIMPLETEST_BASE . '/reporter.php';
require_once '../../Swift/Stream.php';
require_once '../../Swift/Stream/Processor.php';
require_once 'SmtpStub.php';

class TestOfSmtpStub extends UnitTestCase
{

    private function readFullResponse($stream)
    {
        $ret = "";
        do {
            $line = fgets($stream);
            if ($line == "")
                return;
            $ret .= $line;
        } while (substr($line, 3, 1) != " ");
        return $ret;
    }

    public function testWaitingForEOL()
    {
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        $processor->addObserver(new FakeStream_SmtpStub($processor));
        
        $this->assertFalse($processor->isHanging());
        fwrite($stream, "Command has no EOL");
        $this->assertTrue($processor->isHanging());
        $this->assertEqual($processor->command, "Command has no EOL");
        fwrite($stream, " yet\r\n");
        $this->assertFalse($processor->isHanging());
        // The command should have been flushed
        $this->assertEqual($processor->command, "");
        
        fclose($stream);
    }

    public function testValidSendSequence()
    {
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        $processor->addObserver(new FakeStream_SmtpStub($processor));
        
        SmtpMsg::setExtensions(array(
            "PIPELINING",
            "8BITMIME"
        ));
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        // We can only say this once
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        // We can get right down to DATA fine now
        fwrite($stream, "MAIL FROM: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // We can send these many times in succession
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // We can only send a string ending with \r\n.\r\n now
        fwrite($stream, "DATA\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::dataGoAhead() . "\r\n");
        
        // Note! The correct behaviour is to keep waiting for the CRLF.CRLF
        fwrite($stream, "Test message line 1\r\n");
        $this->assertTrue($processor->isHanging());
        
        fwrite($stream, "Test message line 2\r\n.\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // We can send another message
        fwrite($stream, "MAIL FROM: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fwrite($stream, "DATA\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::dataGoAhead() . "\r\n");
        
        fwrite($stream, "Test message 2\r\n.\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fwrite($stream, "QUIT\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fclose($stream);
    }

    public function testInvalidSequence()
    {
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        $processor->addObserver(new FakeStream_SmtpStub($processor));
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        // We can only say this once
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        // This shouldn't work because we need MAIL FROM
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badCommand() . "\r\n");
        
        // Even though it screamed at us before, it should forgive us now
        fwrite($stream, "MAIL FROM: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // We shouldn't be able to do this yet!
        fwrite($stream, "DATA\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badCommand() . "\r\n");
        
        // And the RCPT TO should work ok
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // NOW, and only now should DATA work
        fwrite($stream, "DATA\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::dataGoAhead() . "\r\n");
        
        fwrite($stream, "Test message 2\r\n.\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        fwrite($stream, "QUIT\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // The connection would technically be closed so this should fail
        fwrite($stream, "MAIL FROM: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badCommand() . "\r\n");
        
        fclose($stream);
    }

    public function testInvalidSyntax()
    {
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        $processor->addObserver(new FakeStream_SmtpStub($processor));
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        // We can only say this once
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        // Too many >>>'s, should bail out!
        fwrite($stream, "MAIL FROM: <foo@bar>>>>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badCommand() . "\r\n");
        
        // But it'll forgive us
        fwrite($stream, "MAIL FROM: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // And the RCPT TO should work ok
        fwrite($stream, "RCPT TO: <foo@bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::OK() . "\r\n");
        
        // This envelope will be rejected, but we can still send cos the first is OK
        fwrite($stream, "RCPT TO: <foo@&&&^bar>\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badCommand() . "\r\n");
        
        // This should work fine
        fwrite($stream, "DATA\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::dataGoAhead() . "\r\n");
        
        // This shouldn't do anything
        fwrite($stream, "QUIT\r\n");
        $this->assertTrue($processor->isHanging());
        
        fclose($stream);
    }

    public function testAuthentication()
    {
        $user = "test";
        $pass = "pass123";
        
        // Successful auth....
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        SmtpMsg::setExtensions(array(
            "PIPELINING",
            "AUTH PLAIN"
        ));
        
        $smtp_obs = new FakeStream_SmtpStub($processor);
        $smtp_obs->setUser($user);
        $smtp_obs->setPass($pass);
        
        $processor->addObserver($smtp_obs);
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        fwrite($stream, "AUTH PLAIN " . base64_encode("$user\0$user\0$pass") . "\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::authSuccess() . "\r\n");
        
        fclose($stream);
        
        // Bad auth (incorrect details)
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        SmtpMsg::setExtensions(array(
            "PIPELINING",
            "AUTH PLAIN"
        ));
        
        $smtp_obs = new FakeStream_SmtpStub($processor);
        $smtp_obs->setUser($user);
        $smtp_obs->setPass($pass);
        
        $processor->addObserver($smtp_obs);
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        fwrite($stream, "AUTH PLAIN " . base64_encode("xxxxx\0$pass") . "\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::authFail() . "\r\n");
        
        fclose($stream);
        
        // Bad auth (called at wrong time)
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        SmtpMsg::setExtensions(array(
            "PIPELINING",
            "AUTH PLAIN"
        ));
        
        $smtp_obs = new FakeStream_SmtpStub($processor);
        $smtp_obs->setUser($user);
        $smtp_obs->setPass($pass);
        
        $processor->addObserver($smtp_obs);
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        fwrite($stream, "AUTH PLAIN " . base64_encode("$user\0$user\0$pass") . "\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badAuthCommand() . "\r\n");
        
        fclose($stream);
        
        // Bad auth (called too many times)
        $stream = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        SmtpMsg::setExtensions(array(
            "PIPELINING",
            "AUTH PLAIN"
        ));
        
        $smtp_obs = new FakeStream_SmtpStub($processor);
        $smtp_obs->setUser($user);
        $smtp_obs->setPass($pass);
        
        $processor->addObserver($smtp_obs);
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::greeting() . "\r\n");
        
        fwrite($stream, "EHLO localhost\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::EHLO() . "\r\n");
        
        fwrite($stream, "AUTH PLAIN " . base64_encode("$user\0$user\0$pass") . "\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::authSuccess() . "\r\n");
        
        fwrite($stream, "AUTH PLAIN " . base64_encode("$user\0$user\0$pass") . "\r\n");
        $this->assertFalse($processor->isHanging());
        
        $response = $this->readFullResponse($stream);
        $this->assertEqual($response, SmtpMsg::badAuthCommand() . "\r\n");
        
        fclose($stream);
    }
}

if (! defined('IN_ALLTESTS')) {
    $test = new TestOfSmtpStub();
    $test->run(new HtmlReporter());
}

?>