<?php

/**
 * Simply contains a set of responses for SMTP commands
 */
class SmtpMsg
{

    /**
     * The EHLO response to the list of commands on offer
     *
     * @var string extensions
     */
    public static $extList = "";

    /**
     * Build a new EHLO response string advertising the extensions set
     *
     * @param
     *            array extensions
     */
    static public function setExtensions($array)
    {
        if (! empty($array))
            $ret = "250-fakestream.spoofed Hello localhost.localdomain [127.0.0.1]";
        else
            return self::$extList = "250 fakestream.spoofed Hello localhost.localdomain [127.0.0.1]";
        
        $num = count($array);
        for ($i = 0; $i < $num; $i ++) {
            if ($i < $num - 1)
                $ret .= "\r\n250-" . $array[$i];
            else
                $ret .= "\r\n250 " . $array[$i];
        }
        self::$extList = $ret;
    }

    static public function greeting()
    {
        return "220 fakestream.spoofed ESMTP FakeStream 0.0.0 " . date('r', strtotime('20060817'));
    }

    static public function badCommand()
    {
        return "500 Bad command";
    }

    static public function badAuthCommand()
    {
        return "503 Auth not available";
    }

    static public function authFail()
    {
        return "535 Authentication failed";
    }

    static public function authSuccess()
    {
        return "235 Authentication succeeded";
    }

    static public function EHLO()
    {
        return self::$extList;
    }

    static public function OK()
    {
        return "250 OK";
    }

    static public function dataGoAhead()
    {
        return "354 Go Ahead";
    }
}

/**
 * The observer for the FakeStream class
 * Sends SMTP responses to SMTP commands
 */
class FakeStream_SmtpStub implements Swift_IStream
{

    /**
     * An instance of the stream which it resides inside
     *
     * @var object stream
     */
    private $stream;

    /**
     * The ending needed to finish a command
     *
     * @var string ending
     */
    private $ending = "\r\n";

    /**
     * If a valid command has yet been sent
     *
     * @var bool valid command
     */
    private $validCommandSent = false;

    /**
     * If RCPT envelopes have been added
     *
     * @var bool rcpt
     */
    private $rcptSent = false;

    /**
     * If a mail from envelope is present
     *
     * @var bool mail from
     */
    private $mailSent = false;

    /**
     * If the data command has just been set
     *
     * @var bool sending data
     */
    private $dataSent = false;

    /**
     * A fake username
     *
     * @var string username
     */
    private $user = null;

    /**
     * A fake password
     *
     * @var string password
     */
    private $pass = null;

    /**
     * If the user has already authenticated
     *
     * @var bool authenticated
     */
    private $authenticated = false;

    /**
     * Constructor
     *
     * @param
     *            object parent stream object
     */
    public function __construct($stream)
    {
        $this->stream = $stream;
        $this->stream->setResponse($this->getGreeting());
    }

    /**
     * Set a fake username
     *
     * @param
     *            string user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Set a fake password
     *
     * @param
     *            string pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * Try to get the SMTP greeting
     *
     * @return string response
     */
    public function getGreeting()
    {
        $this->validCommandSent = false;
        return SmtpMsg::greeting();
    }

    /**
     * Send a 500 error
     *
     * @return string response
     */
    public function getBadCommand()
    {
        return SmtpMsg::badCommand();
    }

    /**
     * Try to get the EHLO response
     *
     * @return string response
     */
    public function getEHLO()
    {
        if (! $this->validCommandSent) {
            $this->validCommandSent = true;
            return SmtpMsg::EHLO();
        } else
            return $this->getBadCommand();
    }

    /**
     * Try to send a 250 response
     *
     * @return string response
     */
    public function getOK()
    {
        $this->validCommandSent = true;
        return SmtpMsg::OK();
    }

    /**
     * Try to get a suitable response to the MAIL from command
     *
     * @return string response
     */
    public function getMAIL()
    {
        if ($this->validCommandSent && ! $this->rcptSent && ! $this->dataSent) {
            $dot_atom_re = '[-!#\$%&\'\*\+\/=\?\^_`{}\|~0-9A-Z]+(?:\.[-!#\$%&\'\*\+\/=\?\^_`{}\|~0-9A-Z]+)*';
            $implemented_domain_re = '[-0-9A-Z]+(?:\.[-0-9A-Z]+)*';
            $full_pattern = $dot_atom_re . '(?:@' . $implemented_domain_re . ')?';
            
            if (preg_match("/^\s*mail\ +from:\s*(?:(<" . $full_pattern . ">)|(" . $full_pattern . ")|(<\ *>))\s*\r\n$/i", $this->stream->command)) {
                $this->mailSent = true;
                return $this->getOK();
            } else
                return $this->getBadCommand();
        } else
            return $this->getBadCommand();
    }

    /**
     * Try to get a suitable response to the RCPT to command
     *
     * @return string response
     */
    public function getRCPT()
    {
        if ($this->validCommandSent && $this->mailSent && ! $this->dataSent) {
            $dot_atom_re = '[-!#\$%&\'\*\+\/=\?\^_`{}\|~0-9A-Z]+(?:\.[-!#\$%&\'\*\+\/=\?\^_`{}\|~0-9A-Z]+)*';
            $implemented_domain_re = '[-0-9A-Z]+(?:\.[-0-9A-Z]+)*';
            $full_pattern = $dot_atom_re . '(?:@' . $implemented_domain_re . ')?';
            
            if (preg_match("/^\s*rcpt\ +to:\s*(?:(<" . $full_pattern . ">)|(" . $full_pattern . ")|(<\ *>))\s*\r\n$/i", $this->stream->command)) {
                $this->rcptSent = true;
                return $this->getOK();
            } else
                return $this->getBadCommand();
        } else
            return $this->getBadCommand();
    }

    /**
     * Try to get a suitable response to the QUIT command
     *
     * @return string response
     */
    public function getQUIT()
    {
        $this->validCommandSent = false;
        $this->dataSent = false;
        $this->mailSent = false;
        $this->rcptSent = false;
        return SmtpMsg::OK();
    }

    /**
     * Try to get a suitable response to the RSET command
     *
     * @return string response
     */
    public function getRSET()
    {
        $this->dataSent = false;
        $this->mailSent = false;
        $this->rcptSent = false;
        return SmtpMsg::OK();
    }

    /**
     * Try to get a suitable response to the DATA command
     *
     * @return string response
     */
    public function getDataGoAhead()
    {
        if ($this->validCommandSent && $this->rcptSent && ! $this->dataSent) {
            if (preg_match("/^\s*data\s*\r\n/i", $this->stream->command)) {
                $this->validCommandSent = true;
                $this->dataSent = true;
                $this->mailSent = false;
                $this->rcptSent = false;
                $this->ending = "\r\n.\r\n";
                return SmtpMsg::dataGoAhead();
            } else
                return $this->getBadCommand();
        } else
            return $this->getBadCommand();
    }

    /**
     * Try to get a suitable response after a message was sent
     *
     * @return string response
     */
    public function getMessageSent()
    {
        $this->ending = "\r\n";
        $this->dataSent = false;
        return SmtpMsg::OK();
    }

    /**
     * Try to run AUTH plain authentication and send a response
     *
     * @return string response
     */
    public function getAUTH()
    {
        if ($this->validCommandSent && stripos(SmtpMsg::$extList, "AUTH PLAIN") && ! $this->mailSent && ! $this->rcptSent && ! $this->dataSent && ! $this->authenticated) {
            if (preg_match("/^\s*AUTH\ +PLAIN\ +([A-Z0-9=]+)\s*\r\n$/i", $this->stream->command, $matches)) {
                $parts = explode("\0", base64_decode($matches[1]));
                if (count($parts) == 3) {
                    if ($parts[0] == $this->user && $parts[2] == $this->pass) {
                        $this->authenticated = true;
                        return SmtpMsg::authSuccess();
                    } else
                        return SmtpMsg::authFail();
                } else
                    return SmtpMsg::authFail();
            } else
                return SmtpMsg::badAuthCommand();
        } else
            return SmtpMsg::badAuthCommand();
    }

    /**
     * Runs appropriate commands and get responses
     *
     * @return string response
     */
    public function command(&$string)
    {
        $this->stream->hanging = true;
        if (substr($string, - (strlen($this->ending))) != $this->ending)
            return;
        
        $keyword = @strtolower(preg_replace('/^\s*([A-Z]+)\s*.*\r\n$/i', '$1', $string));
        switch ($keyword) {
            case 'ehlo':
                $this->stream->setResponse($this->getEHLO());
                break;
            case 'mail':
                $this->stream->setResponse($this->getMAIL());
                break;
            case 'rcpt':
                $this->stream->setResponse($this->getRCPT());
                break;
            case 'data':
                $this->stream->setResponse($this->getDataGoAhead());
                break;
            case 'quit':
                $this->stream->setResponse($this->getQUIT());
                break;
            case 'auth':
                $this->stream->setResponse($this->getAUTH());
                break;
            case 'rset':
                $this->stream->setResponse($this->getRSET());
                break;
            default:
                if ($this->dataSent)
                    $this->stream->setResponse($this->getMessageSent());
                else
                    $this->stream->setResponse($this->getBadCommand());
                break;
        }
        
        $string = "";
        $this->stream->hanging = false;
    }
}

?>