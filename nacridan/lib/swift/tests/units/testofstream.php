<?php
require_once '../config.php';
require_once SIMPLETEST_BASE . '/unit_tester.php';
require_once SIMPLETEST_BASE . '/reporter.php';
require_once '../../Swift/Stream.php';
require_once '../../Swift/Stream/Processor.php';

class TestOfStream extends UnitTestCase
{

    private $stream;

    private $processor;

    public function setup()
    {
        $this->processor = Swift_Stream_Processor::getInstance();
    }

    public function testCreatingStream()
    {
        $this->assertFalse($this->processor->isOpen);
        $this->stream = fopen('swift://esmtp', 'w+');
        $this->assertTrue($this->processor->isOpen);
        $this->assertTrue(is_resource($this->stream));
    }

    public function testReading()
    {
        $this->processor->setResponse("250 TESTING");
        $response = fgets($this->stream);
        $this->assertEqual($response, "250 TESTING\r\n");
    }

    public function testExceedEofHang()
    {
        $this->assertFalse($this->processor->isHanging());
        $response = fgets($this->stream);
        $this->assertTrue($this->processor->isHanging());
    }

    public function testWriting()
    {
        $written = fwrite($this->stream, "TEST WRITING\r\n");
        $this->assertTrue($written);
        $this->assertEqual($this->processor->command, "TEST WRITING\r\n");
    }

    public function testClosingStream()
    {
        $this->assertTrue(is_resource($this->stream));
        $this->assertTrue($this->processor->isOpen);
        fclose($this->stream);
        $this->assertFalse($this->processor->isOpen);
        $this->assertFalse(is_resource($this->stream));
    }
}

if (! defined('IN_ALLTESTS')) {
    $test = new TestOfStream();
    $test->run(new HtmlReporter());
}

?>