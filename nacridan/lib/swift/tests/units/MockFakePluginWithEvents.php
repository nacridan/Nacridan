<?php

// Ignore the calls to parent::<method>()
// ... they are purely to notify the mock
class MockFakePluginWithEvents extends MockFakePlugin
{

    public $pluginName = 'Fake';

    public $swift;

    public function loadBaseObject(&$swift)
    {
        parent::loadBaseObject(&$swift);
        $this->swift = & $swift;
    }

    public function callSwiftAddCc($address)
    {
        $this->swift->addCc($address);
    }

    public function onLoad()
    {
        parent::onLoad();
    }

    public function onSend()
    {
        parent::onSend();
    }

    public function onBeforeSend()
    {
        parent::onBeforeSend();
    }

    public function onBeforeCommand()
    {
        parent::onBeforeCommand();
    }

    public function onCommand()
    {
        parent::onCommand();
    }

    public function onResponse()
    {
        parent::onResponse();
    }

    public function onUnload()
    {
        parent::onUnload();
    }

    public function onClose()
    {
        parent::onClose();
    }

    public function onError()
    {
        parent::onError();
    }

    public function onFail()
    {
        parent::onFail();
    }

    public function onLog()
    {
        parent::onLog();
    }

    public function onConnect()
    {
        parent::onConnect();
    }

    public function onAuthenticate()
    {
        parent::onAuthenticate();
    }
}

?>