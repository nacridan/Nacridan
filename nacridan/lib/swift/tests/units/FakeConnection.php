<?php

/**
 * FakeConnection - Part of the Swift Mailer Unit Test stuff
 * This connection DOES NOT do anything in terms of sending mail,
 * It simply provides the means to have a conversation in SMTP without actually
 * using an external server or binary.  It will respond correctly, and fail when it
 * it should fail etc.
 */

// Requires the FakeStream stuff
class FakeConnection implements Swift_IConnection
{

    /**
     * Just a boolean value for when we're connected
     *
     * @var bool connected
     */
    public $connected = false;

    /**
     * SMTP Connection socket
     *
     * @var resource
     */
    public $socket;

    /**
     * SMTP Read part of I/O for Swift
     *
     * @var resource (reference)
     */
    public $readHook;

    /**
     * SMTP Write part of I/O for Swift
     *
     * @var resource (reference)
     */
    public $writeHook;

    /**
     * The fake plugin can also have a fake username and password
     *
     * @var string username
     */
    private $user;

    /**
     * Password (optional)
     *
     * @var string password
     */
    private $pass;

    /**
     * Constructor
     *
     * @param
     *            array faked extensions
     * @param
     *            string fake username
     * @param
     *            string fake password
     */
    public function __construct($extensions = array("8BITMIME", "PIPELINING", "AUTH PLAIN"), $user = null, $pass = null)
    {
        SmtpMsg::setExtensions($extensions);
        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Establishes a connection with the MTA
     * The SwiftInstance Object calls this
     *
     * @return bool
     */
    public function start()
    {
        return $this->connect();
    }

    /**
     * Establishes a connection with the MTA
     *
     * @return bool @private
     */
    protected function connect()
    {
        $this->socket = fopen('swift://esmtp', 'w+');
        $processor = Swift_Stream_Processor::getInstance();
        
        $obs = new FakeStream_SmtpStub($processor);
        if ($this->user)
            $obs->setUser($this->user);
        if ($this->pass)
            $obs->setPass($this->pass);
        
        $processor->addObserver($obs);
        
        $this->readHook = & $this->socket;
        $this->writeHook = & $this->socket;
        
        if (! $this->socket)
            return $this->connected = false;
        else
            return $this->connected = true;
    }

    /**
     * Closes the connection with the MTA
     * Called by the SwiftInstance object
     *
     * @return void
     */
    public function stop()
    {
        $this->disconnect();
    }

    /**
     * Closes the connection with the MTA
     *
     * @return void
     */
    protected function disconnect()
    {
        if ($this->connected && $this->socket) {
            fclose($this->socket);
            $this->readHook = false;
            $this->writeHook = false;
            $this->socket = false;
            
            $this->connected = false;
        }
    }

    /**
     * Returns TRUE if the socket is connected
     *
     * @return bool connected
     */
    public function isConnected()
    {
        return $this->connected;
    }
}

?>