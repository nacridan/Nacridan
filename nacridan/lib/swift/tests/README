Last Revised 17th August 2006 by Chris Corbyn


This directory contains a series of fine-grained tests for Swift Mailer.
There are two types of test; Unit Tests and Smoke Tests.

Unit Tests are invloved with profiling individual components of the library and
checking that everything works correctly at a code-level.

Smoke Tests require a little amount of human checking.

Using the Unit Tests:
----------------------

In order to run these Units you need to have a copy of SimpleTest ( >= 1.0.1 alpha3)
on your system.  You can download this completely free from SourceForge (just Google it ;))

You *need* to edit the file called "config.php" before trying to run the test scripts.
For the Units, the only part of this file you will need to edit is the path to SimpleTest.

The Unit Tests are all inside the directory named "units".  Don't move them, they have paths
coded into them which will break if you do.

For the unit tests of Swift a fake connection has been created so that we don't need to rely
on an SMTP server behaving like it should, or being available at all!  We should first check
this is working.  Run "testoffakestream.php" to ensure the stream itself is functioning, then
run "testofsmtpstub.php" to check that the Fake SMTP protocol is doing as expected.

If the tests pass, you'll see a green bar and an indication of the number of tests it ran. If
the tests fail, things go a red color and there will be an explanation of what didn't pass,
quoting the test name and the line number.

Once you've established that the fake connection is working OK you can go on to test Swift.
Run "testofswift.php".  This makes use of the things you already tested.  Hopefully it will
go green and pass the test.

If the FakeStream and SmtpStub tests passed then there is no reason for Swift to fail its
Unit Test.  If it does, contact me via the website if you don't mind because I may need to look
into something.

The file called "alltests.php" will run all 3 unit tests in one go.

What do these units test?

Well... they don't go so far as to test what happens with issues in the network etc.  They
do test if Swift works correctly however.  To ensure things are set up correctly, the fake
connection is used in the tests.

The tests cover:

 * Adding and removing recipients, parts and attachments
 * Connecting via a given a connection interface
 * Sending single mails and batch mails
 * Plugin loading, access control and event handling
 * Command/response catching & failing
 * Authentication
 * Security possibilites with header injection
 * Character set detection & correct encoding
 * Several functions internal to Swift



Using the Smoke Tests:
----------------------

The smoke tests are all inside the directory named "smokes".  Just like with the Units, you
need SimpleTest to execute these.  Download it from SourceForge if you don't have it.

The Smoke Tests double as Unit tests on a real world smtp server but the unit tests under
these circumstances are far less reliable than the ones using the fake connection in the units
directory since problems may lie with the network.

You *need* to have either a working sendmail connection or a working SMTP server which *does not*
require authentication in order to run these tests.

Edit the file named "config.php" to provide what details you can.

NOTE: In config.php the email address to send to MUST be one you have access to since 8 emails
will be sent out to it for you to finish the tests.

Run either testofsmtp.php and/or testofsendmail.php depending upon your server setup.  The SMTP
test may take A LONG TIME depending upin the speed of the server and the network.  Be patient.

If everything passed the tests at a PHP-level you should see a green bar indicating that all
tests passed.  If anything goes red, something went wrong.  Feel free to let me know about this
although you may be better running the proper Unit Tests under the units directory first since
the failure may be network related.

Now you need to go an read your emails.  Fingers crossed, you'll have 8 new emails numberd 1 to 8
with "Swift <test name>" in the subject line.  Open them in turn and follow the instructions. You'll
be asked to compare what you see in the email with some small images online.  Not all of the tests
will look right if you don't have certain fonts installed (The tests require arabic fonts in
iso-8859-6, UTF-8 fonts, spanish fonts and greek fonts).
