<?php

/**
 * This is the Connection Interface for Swift Mailer, a PHP Mailer class.
 *
 * @package	Swift Mailer
 * @version	>= 0.0.4
 * @author	Chris Corbyn
 * @date	20th May 2006
 * @license http://www.gnu.org/licenses/lgpl.txt Lesser GNU Public License
 *
 * @copyright Copyright &copy; 2006 Chris Corbyn - All Rights Reserved.
 * @filesource
 * 
 *    "Chris Corbyn" <chris@w3style.co.uk>
 *
 */

/**
 * Swift Connection Handler Interface.
 * Describes the methods which connection handlers should implement
 */
interface Swift_IConnection
{

    /**
     * Required properties
     *
     * public readHook;
     * public writeHook;
     */
    
    /**
     * Establishes a connection with the MTA
     *
     * @return bool
     */
    public function start();

    /**
     * Closes the connection with the MTA
     *
     * @return void
     */
    public function stop();
}

?>
