<?php
require ('../../Swift.php');
require ('../../Swift/Connection/SMTP.php');

$mailer = new Swift(new Swift_Connection_SMTP('smtp.somedomain.com'));

$file = '../../files/durham.jpg';

// If anything goes wrong you can see what happened in the logs
if ($mailer->isConnected()) {
    // Add as many parts as you need here
    $mailer->addPart('Have a look at durhaml!!');
    $mailer->addPart('Have a look at durham <strong>NOW</strong>!!', 'text/html');
    
    // Add attachments as raw strings
    $mailer->addAttachment(file_get_contents($file), 'dur.jpg', 'image/jpeg');
    
    // Leaving the body out of send() makes the mailer send a multipart message
    $mailer->send('joe@bloggs.com', '"Your name" <you@yourdomain.com>', 'Some Subject');
    
    $mailer->close();
} else
    echo "The mailer failed to connect. Errors: <pre>" . print_r($mailer->errors, 1) . "</pre><br />
	Log: <pre>" . print_r($mailer->transactions, 1) . "</pre>";

?>
