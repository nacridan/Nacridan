<?php
require ('../../Swift.php');
require ('../../Swift/Connection/SMTP.php');
require ('../../Swift/Plugin/Errors.php'); // Include the file

$mailer = new Swift(new Swift_Connection_SMTP('smtp.somedomain.com'));

// Load in the plugin as a new object
$mailer->loadPlugin(new Swift_Plugin_Errors());

// If anything goes wrong you can see what happened in the logs
if ($mailer->isConnected()) // Optional
{
    // Sends a simple email
    $mailer->send('"Joe Bloggs" <joe@bloggs.com>', '"Your name" <you@yourdomain.com>', 'Some Subject', "Hello Joe it's only me!");
    // Closes cleanly... works without this but it's not as polite.
    $mailer->close();
} else
    echo "The mailer failed to connect. Errors: <pre>" . print_r($mailer->errors, 1) . "</pre><br />
	Log: <pre>" . print_r($mailer->transactions, 1) . "</pre>";

?>
