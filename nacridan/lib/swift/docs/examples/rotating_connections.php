<?php
require ('Swift.php');
require ('Swift/Connection/SMTP.php');
require ('Swift/Connection/Rotator.php');
require ('Swift/Plugin/ConnectionRotator.php');

// The mailer will now establish a connection with the server
$mailer = new Swift(
    // Add as many connections as you want. They can be any combination of types
    new Swift_Connection_Rotator(array(
        new Swift_Connection_SMTP('smtp.domain1.co.uk'),
        new Swift_Connection_SMTP('smtp.domain2.tld'),
        new Swift_Connection_SMTP('mail.domain3.com')
    )));

// We need this plugin loaded for the connections to rotate
$mailer->loadPlugin(new Swift_Plugin_ConnectionRotator());

$list = array(
    '"Chris" <chris@host1.tld>',
    '"Chris" <chris@host2.tld>'
);

// If anything goes wrong you can see what happened in the logs
if ($mailer->isConnected()) // Optional
{
    // Sends a simple email
    $mailer->send($list, '"Your name" <you@yourdomain.com>', 'Rotator connection', "Connection rotating!!!");
    // Closes cleanly... works without this but it's not as polite.
    $mailer->close();
}

?>