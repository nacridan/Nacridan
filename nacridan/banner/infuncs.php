<?php
// "in" functions leave one instance on the display list
// (though they can use more than one), and return that instance
function fadein($movie, $shape)
{
    $i = $movie->add($shape);
    $i->moveTo(600, 30);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function slideleftin($movie, $shape)
{
    $i = $movie->add($shape);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->moveTo(600 - ($j - 20) * ($j - 20), 30);
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function sliderightin($movie, $shape)
{
    $i = $movie->add($shape);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->moveTo(600 + ($j - 20) * ($j - 20), 30);
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function zoomin($movie, $shape)
{
    $i = $movie->add($shape);
    $i->moveTo(600, 30);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->scaleTo(sqrt(sqrt($j / 20)));
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function skewin($movie, $shape)
{
    $i = $movie->add($shape);
    $i->moveTo(600, 30);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->skewXTo((20 - $j) * (20 - $j) / 200);
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function rotatein($movie, $shape)
{
    $i = $movie->add($shape);
    $i->moveTo(600, 30);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->rotateTo((20 - $j) * (20 - $j) / 30);
        $i->multColor(1.0, 1.0, 1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function stretchdownin($movie, $shape)
{
    $i = $movie->add($shape);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->moveTo(600, 30 * $j / 20);
        $i->scaleTo(1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function stretchupin($movie, $shape)
{
    $i = $movie->add($shape);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i->moveTo(600, 30 + 30 * (20 - $j) / 20);
        $i->scaleTo(1.0, $j / 20);
        $movie->nextFrame();
    }
    
    return $i;
}

function doubleslidein($movie, $shape)
{
    $i1 = $movie->add($shape);
    $i2 = $movie->add($shape);
    
    for ($j = 0; $j <= 20; ++ $j) {
        $i1->moveTo(600 - ($j - 20) * ($j - 20) / 2, 30);
        $i2->moveTo(600 + ($j - 20) * ($j - 20) / 2, 30);
        $i1->multColor(1.0, 1.0, 1.0, $j * $j / 400);
        $i2->multColor(1.0, 1.0, 1.0, $j * $j / 400);
        $movie->nextFrame();
    }
    
    $movie->remove($i2);
    return $i1;
}

$infuncs = array(
    fadein,
    sliderightin,
    slideleftin,
    zoomin,
    doubleslidein,
    skewin,
    rotatein,
    stretchupin,
    stretchdownin
);
?>
