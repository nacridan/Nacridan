<?php
// if file is older than 30 minutes, go fetch a new copy
@@$s = stat("slashdot.xml");

if (! $s || $s[9] < time() - 1800) // idx 9 is last modification time
{
    $page = file("http://slashdot.org/slashdot.xml");
    $page = implode("", $page);
    
    fwrite(fopen("slashdot.xml", "w"), $page);
} else {
    $page = file("slashdot.xml");
    $page = implode("", $page);
}

// borrowed from http://www.wiredstart.com

preg_match_all("/<title\>(.+?)<\/title\>\s+<url\>(.+?)<\/url\>/i", $page, $news, PREG_PATTERN_ORDER);
$i = 0;

while (list (, $match) = each($news[1])) {
    $url[$i] = $news[2][$i];
    $match = str_replace('&amp;', '&', $match);
    $match = str_replace('&lt;', '<', $match);
    $match = str_replace('&gt;', '>', $match);
    $match = str_replace('<i>', '', $match);
    $match = str_replace('</i>', '', $match);
    
    $title[$i] = $match;
    ++ $i;
}

$count = $i;

include ('infuncs.php');
include ('outfuncs.php');

$m = new SWFMovie();
$m->setDimension(1200, 60);
$m->setBackground(0, 0x66, 0x66);
$m->setRate(24.0);

$f = new SWFFont('Techno.fdb');

// make a hit region for the button

$hit = new SWFShape();
$hit->setRightFill($hit->addFill(0, 0, 0));
$hit->movePenTo(- 600, - 30);
$hit->drawLine(1200, 0);
$hit->drawLine(0, 60);
$hit->drawLine(- 1200, 0);
$hit->drawLine(0, - 60);

for ($i = 0; $i < $count; ++ $i) {
    $t = new SWFText();
    $t->setFont($f);
    $t->setHeight(40);
    $t->setColor(0xff, 0xff, 0xff);
    $t->moveTo(- 40 * $f->getWidth($title[$i]) / 1024 / 2, 20);
    $t->addString($title[$i]);
    
    $b[$i] = new SWFButton();
    $b[$i]->addShape($hit, SWFBUTTON_HIT);
    $b[$i]->addShape($t, SWFBUTTON_OVER | SWFBUTTON_UP | SWFBUTTON_DOWN);
    $b[$i]->addAction(new SWFAction("getURL('$url[$i]','popup');"), SWFBUTTON_MOUSEUP);
}

for ($n = 0; $n < 4; ++ $n) {
    for ($i = 0; $i < $count; ++ $i) {
        $infunc = $infuncs[rand(0, count($infuncs) - 1)];
        $instance = $infunc($m, $b[$i]);
        
        for ($j = 0; $j < 60; ++ $j)
            $m->nextFrame();
        
        $outfunc = $outfuncs[rand(0, count($outfuncs) - 1)];
        $outfunc($m, $b[$i], $instance);
    }
}

header("Content-type: application/x-shockwave-flash");
$m->output();
?>
