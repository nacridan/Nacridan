﻿-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 11 Juillet 2012 à 22:19
-- Version du serveur: 5.1.30
-- Version de PHP: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--



INSERT INTO `BasicRace` (`id`, `id_BasicProfil`, `name`, `gender`, `PC`, `class`, `state`, `description`) VALUES
(5, 5, 'Garde', 'M', 'No', 'humanoïde', 'walking', "À mi-chemin entre le soldat de métier et le policier, les membres des cohortes urbaines assurent le respect des lois et, dans une certaine mesure, la sécurité des rues dans les plus grandes cités de l'île, moyennant parfois, car nul n'est parfait, une certaine contrepartie financière (« pour le bal de la garde, citoyen »). Si la majorité des bourgs, comme Vieille-Tour, n'ont qu'un faible effectif de miliciens souvent peu combatifs, essentiellement destinés à éjecter les clients remuants des tavernes et à effaroucher les monstres les plus aventureux, les riches cités d'Artasse, Tonak et Earok, elles, ont les moyens d'entretenir de fortes troupes, expérimentées et aguerries, en mesure d'assurer la protection des frontières de leurs royaumes respectifs et de calmer les ambitions de jeunes aventuriers par trop remuants, la bile échauffée par trop de mauvaise bière. Dans leur grande majorité, les gardes sont d'ailleurs eux-mêmes des aventuriers ou des vétérans aguerris des Guerres Démoniques – le guet de Tonak étant par ailleurs célèbre pour incorporer un grand nombre de nains dans ses rangs. Ces guerriers, las d'errer sur les routes, ont choisi une existence plus confortable, qui ouvre en plus droit à une retraite bien méritée. En plus de leurs compétences martiales, les gardes disposent d'un armement de premier choix (financé par la ville) forgé à Octobian ou à Djin, et bien mal inspiré serait l'aventurier de bas niveau qui tenterait de résister au bras armé de l'Ordre. Mais, en dépit de leur nombre et de leur préparation, les milices urbaines seraient bien incapables d'avoir les excellentes statistiques qui font l'orgueil de tous les commissaires divisionnaires de l'île sans les fibules de téléportation, fabriquées à grands frais par les technomages d'Izandar. Ces artefacts, dont la portée reste limitée, sont si coûteux et rares que seuls les meilleurs et les plus sûrs des gardes se les voient confier. Détail amusant : la majorité des gardes urbaines ayant adopté, sur le modèle des anciennes légions de fer d'Artasse, un uniforme comportant une longue tunique d'écailles d'acier, la malice populaire de l'île désigne sous le nom de « poiscaille » ou « hareng saur » les dignes représentants de la violence légitime.

« 22, v'là la poiscaille !! » Cri traditionnel d'un membre de l'Union obscure informant ses complices en train de dépouiller un ivrogne dans une ruelle sombre d'Earok qu'une paire de flics approche, in Dictionnaire de l'Argot des Provinces, par Morllyn.");
