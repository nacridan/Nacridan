-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:17
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(122, 'Temple', 14, 103, 1, 1, 0, '', 100, 100, 447, 230, 1),
(123, 'Échoppe', 5, 103, 1, 1, 0, '', 100, 100, 447, 232, 1),
(124, 'Auberge', 1, 103, 1, 1, 0, '', 100, 100, 445, 232, 1),
(125, 'Auberge', 1, 103, 1, 1, 0, '', 100, 100, 449, 228, 1),
(126, 'Palais Royal', 12, 103, 1, 1, 0, '', 100, 100, 447, 228, 1),
(127, 'École de Combat', 6, 103, 1, 1, 0, '', 100, 100, 444, 230, 1),
(128, 'École des Métiers', 8, 103, 1, 1, 0, '', 100, 100, 444, 231, 1),
(129, 'École de Magie', 7, 103, 1, 1, 0, '', 100, 100, 445, 230, 1),
(130, 'Comptoir Commercial', 4, 103, 1, 1, 0, '', 100, 100, 446, 228, 1),
(131, 'Banque', 2, 103, 1, 1, 0, '', 100, 100, 447, 227, 1),
(132, 'Caserne', 3, 103, 1, 1, 0, '', 100, 100, 447, 234, 1),
(133, 'Prison', 13, 103, 1, 1, 0, '', 100, 100, 448, 233, 1),
(134, 'Guilde des Artisans', 9, 103, 1, 1, 0, '', 100, 100, 449, 230, 1),
(135, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 447, 233, 1),
(136, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 444, 233, 1),
(137, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 445, 233, 1),
(138, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 444, 234, 1),
(139, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 445, 234, 1),
(140, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 448, 232, 1),
(141, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 449, 232, 1),
(142, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 450, 230, 1),
(143, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 451, 230, 1),
(144, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 450, 229, 1),
(145, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 451, 229, 1),
(146, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 451, 228, 1),
(147, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 449, 227, 1),
(148, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 450, 227, 1),
(149, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 450, 226, 1),
(150, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 451, 226, 1),
(151, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 448, 226, 1),
(152, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 447, 226, 1),
(153, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 446, 227, 1),
(154, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 445, 228, 1),
(155, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 443, 230, 1),
(156, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 443, 231, 1),
(157, 'Maison', 10, 103, 1, 1, 0, '', 100, 100, 443, 232, 1),
(158, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 442, 234, 1),
(159, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 442, 233, 1),
(160, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 442, 232, 1),
(161, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 442, 231, 1),
(162, 'Rempart', 24, 103, 1, 1, 0, '', 150, 150, 442, 230, 1),
(163, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 443, 229, 1),
(164, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 444, 228, 1),
(165, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 445, 227, 1),
(166, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 446, 226, 1),
(167, 'Rempart', 23, 103, 1, 1, 0, '', 150, 150, 447, 225, 1),
(168, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 448, 225, 1),
(169, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 449, 225, 1),
(170, 'Porte Ouverte', 26, 103, 1, 1, 0, '', 150, 150, 450, 225, 1),
(171, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 451, 225, 1),
(172, 'Rempart', 20, 103, 1, 1, 0, '', 150, 150, 452, 225, 1),
(173, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 452, 226, 1),
(174, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 452, 227, 1),
(175, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 452, 228, 1),
(176, 'Rempart', 16, 103, 1, 1, 0, '', 150, 150, 452, 229, 1),
(177, 'Rempart', 22, 103, 1, 1, 0, '', 150, 150, 452, 230, 1),
(178, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 451, 231, 1),
(179, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 450, 232, 1),
(180, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 449, 233, 1),
(181, 'Rempart', 17, 103, 1, 1, 0, '', 150, 150, 448, 234, 1),
(182, 'Rempart', 19, 103, 1, 1, 0, '', 150, 150, 447, 235, 1),
(183, 'Porte Ouverte', 26, 103, 1, 1, 0, '', 150, 150, 446, 235, 1),
(184, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 445, 235, 1),
(185, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 443, 235, 1),
(186, 'Rempart', 18, 103, 1, 1, 0, '', 150, 150, 444, 235, 1);
