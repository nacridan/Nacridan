-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:25
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(433, 'Temple', 14, 11, 1, 1, 0, '', 100, 100, 166, 21, 1),
(434, 'Échoppe', 5, 11, 1, 1, 0, '', 100, 100, 168, 21, 1),
(435, 'Auberge', 1, 11, 1, 1, 0, '', 100, 100, 166, 23, 1),
(436, 'Auberge', 1, 11, 1, 1, 0, '', 100, 100, 163, 24, 1),
(437, 'Guilde des Artisans', 9, 11, 1, 1, 0, '', 100, 100, 168, 19, 1),
(438, 'École de Magie', 7, 11, 1, 1, 0, '', 100, 100, 166, 19, 1),
(439, 'École de Combat', 6, 11, 1, 1, 0, '', 100, 100, 166, 18, 1),
(440, 'École des Métiers', 8, 11, 1, 1, 0, '', 100, 100, 165, 19, 1),
(441, 'Comptoir Commercial', 4, 11, 1, 1, 0, '', 100, 100, 164, 23, 1),
(442, 'Banque', 2, 11, 1, 1, 0, '', 100, 100, 163, 23, 1),
(443, 'Caserne', 3, 11, 1, 1, 0, '', 100, 100, 163, 21, 1),
(444, 'Prison', 13, 11, 1, 1, 0, '', 100, 100, 162, 21, 1),
(445, 'Palais Royal', 12, 11, 1, 1, 0, '', 100, 100, 164, 21, 1),
(446, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 166, 24, 1),
(447, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 162, 23, 1),
(448, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 162, 24, 1),
(449, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 162, 25, 1),
(450, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 166, 17, 1),
(451, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 165, 18, 1),
(452, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 164, 19, 1),
(453, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 169, 17, 1),
(454, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 170, 17, 1),
(455, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 169, 18, 1),
(456, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 168, 18, 1),
(457, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 170, 19, 1),
(458, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 170, 20, 1),
(459, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 170, 21, 1),
(460, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 169, 20, 1),
(461, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 169, 21, 1),
(462, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 167, 23, 1),
(463, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 168, 23, 1),
(464, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 167, 24, 1),
(465, 'Maison', 10, 11, 1, 1, 0, '', 100, 100, 166, 25, 1),
(466, 'Rempart', 25, 11, 1, 1, 0, '', 150, 150, 161, 26, 1),
(467, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 162, 26, 1),
(468, 'Rempart', 26, 11, 1, 1, 0, '', 150, 150, 163, 26, 1),
(469, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 164, 26, 1),
(470, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 165, 26, 1),
(471, 'Rempart', 19, 11, 1, 1, 0, '', 150, 150, 166, 26, 1),
(472, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 167, 25, 1),
(473, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 168, 24, 1),
(474, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 169, 23, 1),
(475, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 170, 22, 1),
(476, 'Rempart', 22, 11, 1, 1, 0, '', 150, 150, 171, 21, 1),
(477, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 171, 20, 1),
(478, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 171, 19, 1),
(479, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 171, 18, 1),
(480, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 171, 17, 1),
(481, 'Rempart', 20, 11, 1, 1, 0, '', 150, 150, 171, 16, 1),
(482, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 170, 16, 1),
(483, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 169, 16, 1),
(484, 'Rempart', 26, 11, 1, 1, 0, '', 150, 150, 168, 16, 1),
(485, 'Rempart', 18, 11, 1, 1, 0, '', 150, 150, 167, 16, 1),
(486, 'Rempart', 23, 11, 1, 1, 0, '', 150, 150, 166, 16, 1),
(487, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 165, 17, 1),
(488, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 164, 18, 1),
(489, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 163, 19, 1),
(490, 'Rempart', 17, 11, 1, 1, 0, '', 150, 150, 162, 20, 1),
(491, 'Rempart', 24, 11, 1, 1, 0, '', 150, 150, 161, 21, 1),
(492, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 161, 22, 1),
(493, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 161, 23, 1),
(494, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 161, 24, 1),
(495, 'Rempart', 16, 11, 1, 1, 0, '', 150, 150, 161, 25, 1);
