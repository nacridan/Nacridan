-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:23
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(496, 'Temple', 14, 23, 1, 1, 0, '', 100, 100, 508, 54, 1),
(497, 'Échoppe', 5, 23, 1, 1, 0, '', 100, 100, 509, 55, 1),
(498, 'Auberge', 1, 23, 1, 1, 0, '', 100, 100, 507, 56, 1),
(499, 'Auberge', 1, 23, 1, 1, 0, '', 100, 100, 509, 51, 1),
(500, 'Guilde des Artisans', 9, 23, 1, 1, 0, '', 100, 100, 506, 55, 1),
(501, 'École de Magie', 7, 23, 1, 1, 0, '', 100, 100, 510, 53, 1),
(502, 'École de Combat', 6, 23, 1, 1, 0, '', 100, 100, 505, 55, 1),
(503, 'École des Métiers', 8, 23, 1, 1, 0, '', 100, 100, 505, 56, 1),
(504, 'Comptoir Commercial', 4, 23, 1, 1, 0, '', 100, 100, 510, 51, 1),
(505, 'Banque', 2, 23, 1, 1, 0, '', 100, 100, 509, 52, 1),
(506, 'Caserne', 3, 23, 1, 1, 0, '', 100, 100, 507, 52, 1),
(507, 'Prison', 13, 23, 1, 1, 0, '', 100, 100, 506, 53, 1),
(508, 'Palais Royal', 12, 23, 1, 1, 0, '', 100, 100, 507, 53, 1),
(509, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 507, 57, 1),
(510, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 512, 51, 1),
(511, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 512, 52, 1),
(512, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 512, 53, 1),
(513, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 511, 52, 1),
(514, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 511, 53, 1),
(515, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 509, 57, 1),
(516, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 510, 56, 1),
(517, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 511, 55, 1),
(518, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 509, 56, 1),
(519, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 510, 55, 1),
(520, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 510, 50, 1),
(521, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 511, 50, 1),
(522, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 507, 51, 1),
(523, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 506, 52, 1),
(524, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 505, 53, 1),
(525, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 504, 55, 1),
(526, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 504, 56, 1),
(527, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 504, 57, 1),
(528, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 506, 58, 1),
(529, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 505, 58, 1),
(530, 'Maison', 10, 23, 1, 1, 0, '', 100, 100, 506, 57, 1),
(531, 'Rempart', 25, 23, 1, 1, 0, '', 150, 150, 503, 59, 1),
(532, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 504, 59, 1),
(533, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 505, 59, 1),
(534, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 506, 59, 1),
(535, 'Porte Ouverte', 26, 23, 1, 1, 0, '', 150, 150, 507, 59, 1),
(536, 'Rempart', 19, 23, 1, 1, 0, '', 150, 150, 508, 59, 1),
(537, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 509, 58, 1),
(538, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 510, 57, 1),
(539, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 511, 56, 1),
(540, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 512, 55, 1),
(541, 'Rempart', 22, 23, 1, 1, 0, '', 150, 150, 513, 54, 1),
(542, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 513, 53, 1),
(543, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 513, 52, 1),
(544, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 513, 51, 1),
(545, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 513, 50, 1),
(546, 'Rempart', 20, 23, 1, 1, 0, '', 150, 150, 513, 49, 1),
(547, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 512, 49, 1),
(548, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 511, 49, 1),
(549, 'Rempart', 18, 23, 1, 1, 0, '', 150, 150, 510, 49, 1),
(550, 'Porte Ouverte', 26, 23, 1, 1, 0, '', 150, 150, 509, 49, 1),
(551, 'Rempart', 23, 23, 1, 1, 0, '', 150, 150, 508, 49, 1),
(552, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 507, 50, 1),
(553, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 506, 51, 1),
(554, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 505, 52, 1),
(555, 'Rempart', 17, 23, 1, 1, 0, '', 150, 150, 504, 53, 1),
(556, 'Rempart', 24, 23, 1, 1, 0, '', 150, 150, 503, 54, 1),
(557, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 503, 55, 1),
(558, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 503, 56, 1),
(559, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 503, 57, 1),
(560, 'Rempart', 16, 23, 1, 1, 0, '', 150, 150, 503, 58, 1);
