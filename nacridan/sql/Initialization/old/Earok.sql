-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:08
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(241, 'Temple', 14, 135, 1, 1, 0, '', 100, 100, 252, 301, 1),
(242, 'Échoppe', 5, 135, 1, 6, 0, '', 300, 300, 252, 299, 1),
(243, 'Auberge', 1, 135, 1, 6, 0, '', 300, 300, 250, 301, 1),
(244, 'Auberge', 1, 135, 1, 1, 0, '', 100, 100, 254, 298, 1),
(245, 'Guilde des Artisans', 9, 135, 1, 1, 0, '', 100, 100, 250, 303, 1),
(246, 'École de Magie', 7, 135, 1, 5, 0, '', 420, 420, 252, 303, 1),
(247, 'École de Combat', 6, 135, 1, 1, 0, '', 100, 100, 253, 303, 1),
(248, 'École des Métiers', 8, 135, 1, 5, 0, '', 420, 420, 252, 304, 1),
(249, 'Comptoir Commercial', 4, 135, 1, 1, 0, '', 100, 100, 254, 299, 1),
(250, 'Banque', 2, 135, 1, 1, 0, '', 100, 100, 255, 298, 1),
(251, 'Caserne', 3, 135, 1, 1, 0, '', 100, 100, 255, 301, 1),
(252, 'Prison', 13, 135, 1, 1, 0, '', 100, 100, 256, 301, 1),
(253, 'Palais Royal', 12, 135, 1, 1, 0, '', 100, 100, 254, 301, 1),
(254, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 255, 300, 1),
(255, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 256, 300, 1),
(256, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 256, 299, 1),
(257, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 249, 304, 1),
(258, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 250, 304, 1),
(259, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 252, 298, 1),
(260, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 254, 303, 1),
(261, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 253, 304, 1),
(262, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 252, 305, 1),
(263, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 250, 305, 1),
(264, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 249, 305, 1),
(265, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 248, 305, 1),
(266, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 249, 302, 1),
(267, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 249, 301, 1),
(268, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 254, 297, 1),
(269, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 255, 297, 1),
(270, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 250, 299, 1),
(271, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 251, 298, 1),
(272, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 252, 297, 1),
(273, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 251, 299, 1),
(274, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 248, 301, 1),
(275, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 248, 302, 1),
(276, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 248, 303, 1),
(277, 'Maison', 10, 135, 1, 1, 0, '', 100, 100, 256, 297, 1),
(278, 'Porte Fermée', 21, 135, 1, 1, 0, '', 150, 150, 253, 296, 1),
(279, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 254, 296, 1),
(280, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 255, 296, 1),
(281, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 256, 296, 1),
(282, 'Rempart', 23, 135, 1, 1, 0, '', 150, 150, 252, 296, 1),
(283, 'Rempart', 20, 135, 1, 1, 0, '', 150, 150, 257, 296, 1),
(284, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 251, 297, 1),
(285, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 250, 298, 1),
(286, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 249, 299, 1),
(287, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 248, 300, 1),
(288, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 257, 297, 1),
(289, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 257, 298, 1),
(290, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 257, 299, 1),
(291, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 257, 300, 1),
(292, 'Rempart', 24, 135, 1, 1, 0, '', 150, 150, 247, 301, 1),
(293, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 247, 302, 1),
(294, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 247, 303, 1),
(295, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 247, 304, 1),
(296, 'Rempart', 16, 135, 1, 1, 0, '', 150, 150, 247, 305, 1),
(297, 'Rempart', 25, 135, 1, 1, 0, '', 150, 150, 247, 306, 1),
(298, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 248, 306, 1),
(299, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 249, 306, 1),
(300, 'Rempart', 18, 135, 1, 1, 0, '', 150, 150, 250, 306, 1),
(301, 'Porte Ouverte', 26, 135, 1, 1, 0, '', 150, 150, 251, 306, 1),
(302, 'Rempart', 19, 135, 1, 1, 0, '', 150, 150, 252, 306, 1),
(303, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 253, 305, 1),
(304, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 254, 304, 1),
(305, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 255, 303, 1),
(306, 'Rempart', 17, 135, 1, 1, 0, '', 150, 150, 256, 302, 1),
(307, 'Rempart', 22, 135, 1, 1, 0, '', 150, 150, 257, 301, 1);
