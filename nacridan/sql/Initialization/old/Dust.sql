-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:13
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(187, 'Temple', 14, 158, 1, 1, 0, '', 100, 100, 130, 377, 1),
(188, 'Échoppe', 5, 158, 1, 1, 0, '', 100, 100, 128, 380, 1),
(189, 'Auberge', 1, 158, 1, 1, 0, '', 100, 100, 130, 380, 1),
(190, 'École de Magie', 7, 158, 1, 1, 0, '', 100, 100, 132, 377, 1),
(191, 'École de Combat', 6, 158, 1, 1, 0, '', 100, 100, 133, 376, 1),
(192, 'École des Métiers', 8, 158, 1, 1, 0, '', 100, 100, 133, 377, 1),
(193, 'Caserne', 3, 158, 1, 1, 0, '', 100, 100, 133, 374, 1),
(194, 'Prison', 13, 158, 1, 1, 0, '', 100, 100, 134, 373, 1),
(195, 'Palais Royal', 12, 158, 1, 1, 0, '', 100, 100, 132, 375, 1),
(196, 'Banque', 2, 158, 1, 1, 0, '', 100, 100, 132, 374, 1),
(197, 'Comptoir Commercial', 4, 158, 1, 1, 0, '', 100, 100, 130, 375, 1),
(198, 'Auberge', 1, 158, 1, 1, 0, '', 100, 100, 128, 377, 1),
(199, 'Guilde des Artisans', 9, 158, 1, 1, 0, '', 100, 100, 130, 374, 1),
(200, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 128, 379, 1),
(201, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 127, 380, 1),
(202, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 130, 379, 1),
(203, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 131, 379, 1),
(204, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 127, 377, 1),
(205, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 131, 380, 1),
(206, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 132, 379, 1),
(207, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 134, 377, 1),
(208, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 134, 376, 1),
(209, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 134, 375, 1),
(210, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 133, 373, 1),
(211, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 129, 375, 1),
(212, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 127, 378, 1),
(213, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 130, 373, 1),
(214, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 129, 374, 1),
(215, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 128, 375, 1),
(216, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 126, 377, 1),
(217, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 126, 378, 1),
(218, 'Maison', 10, 158, 1, 1, 0, '', 100, 100, 126, 379, 1),
(219, 'Porte Ouverte', 26, 158, 1, 1, 0, '', 150, 150, 132, 372, 1),
(220, 'Rempart', 18, 158, 1, 1, 0, '', 150, 150, 133, 372, 1),
(221, 'Rempart', 18, 158, 1, 1, 0, '', 150, 150, 134, 372, 1),
(222, 'Rempart', 18, 158, 1, 1, 0, '', 150, 150, 131, 372, 1),
(223, 'Rempart', 20, 158, 1, 1, 0, '', 150, 150, 135, 372, 1),
(224, 'Rempart', 23, 158, 1, 1, 0, '', 150, 150, 130, 372, 1),
(225, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 129, 373, 1),
(226, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 128, 374, 1),
(227, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 127, 375, 1),
(228, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 126, 376, 1),
(229, 'Rempart', 24, 158, 1, 1, 0, '', 150, 150, 125, 377, 1),
(230, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 125, 378, 1),
(231, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 125, 379, 1),
(232, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 125, 380, 1),
(233, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 135, 373, 1),
(234, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 135, 374, 1),
(235, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 135, 375, 1),
(236, 'Rempart', 16, 158, 1, 1, 0, '', 150, 150, 135, 376, 1),
(237, 'Rempart', 22, 158, 1, 1, 0, '', 150, 150, 135, 377, 1),
(238, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 134, 378, 1),
(239, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 133, 379, 1),
(240, 'Rempart', 17, 158, 1, 1, 0, '', 150, 150, 132, 380, 1);
