-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 21:21
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `StaticModifier_BasicRace`
--

CREATE TABLE IF NOT EXISTS `StaticModifier_BasicRace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `dodge` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `regen` varchar(32) NOT NULL DEFAULT '////////',
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `magicResist` varchar(32) NOT NULL DEFAULT '////////',
  `time` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `StaticModifier_BasicRace`
--

INSERT INTO `StaticModifier_BasicRace` (`id`, `attack`, `dodge`, `damage`, `armor`, `regen`, `hp`, `magicSkill`, `magicResist`, `time`) VALUES
(1, '/4///////', '/3///////', '3////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(2, '/3///////', '/4///////', '3////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(3, '/3///////', '/3///////', '4////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(4, '/3///////', '/3///////', '3////////', '///////0/', '1////////', '///////30/', '/4///////', '////////', '///////720/');
