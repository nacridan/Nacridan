-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:29
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Modifier`
--

CREATE TABLE IF NOT EXISTS `Modifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `hp_bm` varchar(32) NOT NULL DEFAULT '////////',
  `strength` varchar(32) NOT NULL DEFAULT '////////',
  `strength_bm` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity_bm` varchar(32) NOT NULL DEFAULT '////////',
  `speed` varchar(32) NOT NULL DEFAULT '////////',
  `speed_bm` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill_bm` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `armor_bm` varchar(32) NOT NULL DEFAULT '////////',
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `attack_bm` varchar(32) NOT NULL DEFAULT '////////',
  `defense` varchar(32) NOT NULL DEFAULT '////////',
  `defense_bm` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `damage_bm` varchar(32) NOT NULL,
  `timeAttack` varchar(32) NOT NULL DEFAULT '////////',
  `timeAttack_bm` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=358 ;

--
-- Contenu de la table `Modifier`
--
