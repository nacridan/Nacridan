-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:26
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Member`
--

CREATE TABLE IF NOT EXISTS `Member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL DEFAULT '',
  `authlevel` tinyint(4) NOT NULL DEFAULT '0',
  `lang` varchar(4) NOT NULL DEFAULT '',
  `utc` varchar(32) NOT NULL DEFAULT 'Europe/Paris',
  `newplayer` tinyint(4) NOT NULL DEFAULT '0',
  `maxNbPlayers` tinyint(4) NOT NULL DEFAULT '1',
  `display` tinyint(4) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `firstlog` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastlog` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=6565 ;

--
-- Contenu de la table `Member`
--

