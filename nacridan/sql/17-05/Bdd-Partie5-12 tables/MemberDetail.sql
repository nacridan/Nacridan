-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:27
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `MemberDetail`
--

CREATE TABLE IF NOT EXISTS `MemberDetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Member` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `postcode` smallint(6) NOT NULL DEFAULT '0',
  `city` varchar(20) NOT NULL DEFAULT '',
  `country` varchar(20) NOT NULL DEFAULT '',
  `state` varchar(20) NOT NULL DEFAULT '',
  `phone` varchar(16) NOT NULL DEFAULT '',
  `firstname` varchar(30) NOT NULL DEFAULT '',
  `lastname` varchar(30) NOT NULL DEFAULT '',
  `sex` enum('M','F') NOT NULL DEFAULT 'M',
  `height` bigint(20) NOT NULL DEFAULT '0',
  `weight` bigint(20) NOT NULL DEFAULT '0',
  `aircolor` varchar(10) NOT NULL DEFAULT '',
  `eyecolor` varchar(10) NOT NULL DEFAULT '',
  `born` date NOT NULL DEFAULT '0000-00-00',
  `job` varchar(30) NOT NULL DEFAULT '',
  `smoker` varchar(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `login` (`id_Member`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=6565 ;

--
-- Contenu de la table `MemberDetail`
--

