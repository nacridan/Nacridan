-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:22
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Player`
--

CREATE TABLE IF NOT EXISTS `Player` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Member` bigint(20) NOT NULL DEFAULT '0',
  `id_World` int(11) NOT NULL DEFAULT '0',
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `id_Owner` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicRace` int(11) NOT NULL DEFAULT '0',
  `id_Modifier` bigint(20) DEFAULT NULL,
  `id_Upgrade` bigint(20) NOT NULL DEFAULT '0',
  `id_Detail` bigint(20) NOT NULL DEFAULT '0',
  `id_Mission` int(11) NOT NULL DEFAULT '0',
  `id_Caravan` bigint(20) NOT NULL DEFAULT '0',
  `id_FighterGroup` bigint(20) NOT NULL DEFAULT '0',
  `id_City` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(30) DEFAULT NULL,
  `gender` enum('M','F','N') NOT NULL DEFAULT 'M',
  `racename` varchar(30) NOT NULL DEFAULT '',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `map` int(11) NOT NULL DEFAULT '1',
  `level` smallint(6) NOT NULL DEFAULT '0',
  `merchant_level` int(11) NOT NULL DEFAULT '1',
  `xp` smallint(6) NOT NULL DEFAULT '0',
  `ap` float NOT NULL DEFAULT '0',
  `currhp` smallint(6) NOT NULL DEFAULT '0',
  `hp` smallint(6) NOT NULL DEFAULT '0',
  `money` mediumint(9) NOT NULL DEFAULT '0',
  `moneyBank` mediumint(9) NOT NULL DEFAULT '0',
  `state` enum('walking','riding','flying','creeping','turtle') NOT NULL DEFAULT 'walking',
  `status` enum('PC','NPC') NOT NULL DEFAULT 'PC',
  `resurrectid` int(11) NOT NULL DEFAULT '0',
  `resurrect` tinyint(4) NOT NULL DEFAULT '0',
  `levelUp` tinyint(4) NOT NULL DEFAULT '0',
  `overload` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `prison` int(11) NOT NULL DEFAULT '0',
  `recall` tinyint(4) NOT NULL DEFAULT '0',
  `incity` tinyint(4) NOT NULL DEFAULT '0',
  `inbuilding` bigint(20) NOT NULL DEFAULT '0',
  `room` int(11) NOT NULL DEFAULT '0',
  `fightStyle` int(7) NOT NULL,
  `id_Player$target` bigint(20) NOT NULL DEFAULT '0',
  `behaviour` int(11) NOT NULL DEFAULT '0',
  `nbkill` int(11) NOT NULL DEFAULT '0',
  `nbdeath` int(11) NOT NULL DEFAULT '0',
  `nbkillpc` int(11) NOT NULL DEFAULT '0',
  `curratb` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `playatb` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nextatb` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `authlevel` tinyint(4) NOT NULL DEFAULT '0',
  `ip` varchar(39) NOT NULL DEFAULT '',
  `ip_data` bigint(20) NOT NULL DEFAULT '0',
  `advert` int(11) NOT NULL DEFAULT '0',
  `Quest_Received` longtext NOT NULL,
  `atb` smallint(6) NOT NULL DEFAULT '720',
  `totalip` int(11) NOT NULL DEFAULT '0',
  `resurrectx` smallint(6) NOT NULL DEFAULT '0',
  `resurrecty` smallint(6) NOT NULL DEFAULT '0',
  `arenax` smallint(6) NOT NULL DEFAULT '0',
  `arenay` smallint(6) NOT NULL DEFAULT '0',
  `lastdeath` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastarena` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `inarena` tinyint(4) NOT NULL DEFAULT '0',
  `moneyHistory` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Modifier` (`id_Modifier`),
  KEY `id_Member` (`id_Member`),
  KEY `id_World` (`id_World`),
  KEY `id_Team` (`id_Team`),
  KEY `id_BasicRace` (`id_BasicRace`),
  KEY `level` (`level`),
  KEY `status` (`status`),
  KEY `DLA` (`atb`),
  KEY `nextDLA` (`nextatb`),
  KEY `xy` (`x`,`y`),
  KEY `incity` (`incity`),
  KEY `id_Mission` (`id_Mission`),
  KEY `ip` (`ip`),
  KEY `playatb` (`playatb`),
  KEY `currDLA` (`curratb`),
  KEY `nbkill` (`nbkill`),
  KEY `nbdeath` (`nbdeath`),
  KEY `nbkillpc` (`nbkillpc`),
  KEY `money` (`money`),
  KEY `resurrect` (`resurrect`),
  KEY `hidden` (`hidden`),
  KEY `id_FighterGroup` (`id_FighterGroup`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=276412 ;

--
-- Contenu de la table `Player`
--
