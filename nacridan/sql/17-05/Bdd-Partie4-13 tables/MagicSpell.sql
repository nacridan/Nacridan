-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 12 Février 2012 à 12:32
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `MagicSpell`
--

DROP TABLE IF EXISTS `MagicSpell`;
CREATE TABLE IF NOT EXISTS `MagicSpell` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicMagicSpell` int(11) NOT NULL DEFAULT '0',
  `skill` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_Player` (`id_Player`),
  KEY `id_BasicMagicSpell` (`id_BasicMagicSpell`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `MagicSpell`
--

INSERT INTO `MagicSpell` (`id`, `id_Player`, `id_BasicMagicSpell`, `skill`) VALUES
(1, 1, 2, 87),
(2, 5, 3, 87),
(3, 5, 4, 87),
(4, 5, 5, 87),
(5, 5, 6, 89),
(6, 5, 16, 87),
(7, 5, 17, 87),
(8, 5, 18, 87),
(9, 6, 2, 87),
(10, 6, 7, 87),
(11, 6, 8, 87),
(12, 6, 15, 87),
(13, 6, 10, 87),
(14, 6, 19, 87),
(15, 6, 9, 87),
(16, 7, 1, 87),
(17, 7, 12, 87),
(18, 7, 13, 87),
(19, 7, 14, 87),
(20, 7, 20, 87),
(21, 7, 11, 87),
(22, 8, 2, 87),
(23, 8, 21, 87),
(24, 8, 22, 87),
(25, 8, 23, 87),
(26, 8, 24, 87),
(27, 8, 25, 87);
