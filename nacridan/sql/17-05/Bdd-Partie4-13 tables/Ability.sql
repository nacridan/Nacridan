-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:42
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Ability`
--

CREATE TABLE IF NOT EXISTS `Ability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicAbility` bigint(20) NOT NULL DEFAULT '0',
  `skill` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_player` (`id_Player`),
  KEY `id_BasicAbility` (`id_BasicAbility`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `Ability`
--

