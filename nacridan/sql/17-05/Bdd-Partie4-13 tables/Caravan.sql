-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 17:26
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Caravan`
--

CREATE TABLE IF NOT EXISTS `Caravan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL DEFAULT '1',
  `id_Player` bigint(30) NOT NULL DEFAULT '0',
  `finalprice` bigint(20) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `id_startbuilding` int(11) NOT NULL,
  `id_endbuilding` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19560 ;

--
-- Contenu de la table `Caravan`
--

