-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 21:16
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `PJAlliance`
--

CREATE TABLE IF NOT EXISTS `PJAlliance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player$src` int(11) NOT NULL DEFAULT '0',
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Player` (`id_Player$src`,`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=685 ;
