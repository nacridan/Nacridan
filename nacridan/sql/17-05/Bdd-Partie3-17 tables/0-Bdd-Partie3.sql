-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 18 Juin 2013 à 15:01
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `FighterGroup`
--

CREATE TABLE IF NOT EXISTS `FighterGroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Player` (`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=363 ;

--
-- Contenu de la table `FighterGroup`
--

INSERT INTO `FighterGroup` (`id`, `id_Player`, `date`) VALUES
(362, 2006, '2013-06-17 07:26:11');

-- --------------------------------------------------------

--
-- Structure de la table `FighterGroupAskJoin`
--

CREATE TABLE IF NOT EXISTS `FighterGroupAskJoin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Guest` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `FighterGroupAskJoin`
--


-- --------------------------------------------------------

--
-- Structure de la table `FighterGroupMsg`
--

CREATE TABLE IF NOT EXISTS `FighterGroupMsg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_FighterGroup` bigint(20) NOT NULL,
  `id_Player` int(20) NOT NULL,
  `body` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `FighterGroupMsg`
--


-- --------------------------------------------------------

--
-- Structure de la table `Mail`
--

CREATE TABLE IF NOT EXISTS `Mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') NOT NULL DEFAULT '1',
  `title` tinytext NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_receiver` (`id_Player$receiver`),
  KEY `id_sender` (`id_Player$sender`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Contenu de la table `Mail`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailAlert`
--

CREATE TABLE IF NOT EXISTS `MailAlert` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_receiver` (`id_Player$receiver`),
  KEY `id_sender` (`id_Player$sender`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `MailAlert`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailAlias`
--

CREATE TABLE IF NOT EXISTS `MailAlias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `alias` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_Player` (`id_Player`,`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=370 ;

--
-- Contenu de la table `MailAlias`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailArchive`
--

CREATE TABLE IF NOT EXISTS `MailArchive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `MailArchive`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailBody`
--

CREATE TABLE IF NOT EXISTS `MailBody` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `recipientid` text NOT NULL,
  `recipient` text NOT NULL,
  `cpt` tinyint(4) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `body` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cpt` (`cpt`),
  KEY `id_Player` (`id_Player`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Contenu de la table `MailBody`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailContact`
--

CREATE TABLE IF NOT EXISTS `MailContact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$friend` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Player` (`id_Player`,`id_Player$friend`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6092 ;

--
-- Contenu de la table `MailContact`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailSend`
--

CREATE TABLE IF NOT EXISTS `MailSend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `id_Player` (`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `MailSend`
--


-- --------------------------------------------------------

--
-- Structure de la table `MailTrade`
--

CREATE TABLE IF NOT EXISTS `MailTrade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_receiver` (`id_Player$receiver`),
  KEY `id_sender` (`id_Player$sender`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `MailTrade`
--


-- --------------------------------------------------------

--
-- Structure de la table `NameRepertory`
--

CREATE TABLE IF NOT EXISTS `NameRepertory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `NameRepertory`
--


-- --------------------------------------------------------

--
-- Structure de la table `PJAlliance`
--

CREATE TABLE IF NOT EXISTS `PJAlliance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player$src` int(11) NOT NULL DEFAULT '0',
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Player` (`id_Player$src`,`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=685 ;

--
-- Contenu de la table `PJAlliance`
--


-- --------------------------------------------------------

--
-- Structure de la table `PJAlliancePJ`
--

CREATE TABLE IF NOT EXISTS `PJAlliancePJ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player$src` int(11) NOT NULL DEFAULT '0',
  `id_Player$dest` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Player` (`id_Player$src`,`id_Player$dest`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3955 ;

--
-- Contenu de la table `PJAlliancePJ`
--


-- --------------------------------------------------------

--
-- Structure de la table `Team`
--

CREATE TABLE IF NOT EXISTS `Team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_TeamBody` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `id_Player` (`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=357 ;

--
-- Contenu de la table `Team`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamAlliance`
--

CREATE TABLE IF NOT EXISTS `TeamAlliance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team$src` int(11) NOT NULL DEFAULT '0',
  `id_Team$dest` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `body` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Team$src_2` (`id_Team$src`,`id_Team$dest`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=487 ;

--
-- Contenu de la table `TeamAlliance`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamAlliancePJ`
--

CREATE TABLE IF NOT EXISTS `TeamAlliancePJ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team$src` int(11) NOT NULL DEFAULT '0',
  `id_Player` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `body` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Team$src_2` (`id_Team$src`,`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=553 ;

--
-- Contenu de la table `TeamAlliancePJ`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamAskJoin`
--

CREATE TABLE IF NOT EXISTS `TeamAskJoin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_Player` (`id_Player`,`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1329 ;

--
-- Contenu de la table `TeamAskJoin`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamBody`
--

CREATE TABLE IF NOT EXISTS `TeamBody` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Team` (`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=357 ;

--
-- Contenu de la table `TeamBody`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamRank`
--

CREATE TABLE IF NOT EXISTS `TeamRank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_TeamRankInfo` int(11) NOT NULL DEFAULT '0',
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_Player` (`id_Player`,`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1680 ;

--
-- Contenu de la table `TeamRank`
--


-- --------------------------------------------------------

--
-- Structure de la table `TeamRankInfo`
--

CREATE TABLE IF NOT EXISTS `TeamRankInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `auth1` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth2` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth3` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth4` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`),
  KEY `id_Team` (`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7121 ;

--
-- Contenu de la table `TeamRankInfo`
--

