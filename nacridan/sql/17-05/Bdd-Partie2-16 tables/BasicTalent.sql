﻿-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 25 Octobre 2011 à 21:52
-- Version du serveur: 5.1.30
-- Version de PHP: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `basictalent`
--

DROP TABLE IF EXISTS `BasicTalent`;
CREATE TABLE IF NOT EXISTS `BasicTalent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `ident` varchar(20) NOT NULL,
  `PA` smallint(7) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `basictalent`
--

INSERT INTO `BasicTalent` (`id`, `name`, `ident`, `PA`, `type`) VALUES
(1, 'Miner', 'TALENT_MINE', 8, 1),
(2, 'Dépecer', 'TALENT_DISMEMBER', 4, 1),
(3, 'Teiller', 'TALENT_SCUTCH', 8, 1),
(4, 'Récolter', 'TALENT_HARVEST', 5, 1),
(5, 'Dialecte Kradjeckien', 'TALENT_SPEAK', 7, 1),
(6, 'Couper du bois', 'TALENT_CUT', 5, 1),
(7, 'Artisanat du fer', 'TALENT_IRONCRAFT', 8, 3),
(8, 'Artisanat du bois', 'TALENT_WOODCRAFT', 8, 3),
(9, 'Artisanat du cuir', 'TALENT_LEATHERCRAFT', 8, 3),
(10, 'Artisanat des écailles', 'TALENT_SCALECRAFT', 8, 3),
(11, 'Artisanat du lin', 'TALENT_LINENCRAFT', 8, 3),
(12, 'Artisanat des plantes', 'TALENT_PLANTCRAFT', 8, 3),
(13, 'Artisanat des gemmes', 'TALENT_GEMCRAFT', 8, 3),
(14, 'Ferronner', 'TALENT_IRONREFINE', 5, 2),
(15, 'Tanner', 'TALENT_LEATHERREFINE', 5, 2),
(16, 'Ecailler', 'TALENT_SCALEREFINE', 5, 2),
(17, 'Effiler', 'TALENT_LINENREFINE', 5, 2),
(18, 'Tailler', 'TALENT_GEMREFINE', 5, 2),
(19, 'Filtrer', 'TALENT_PLANTREFINE', 5, 2),
(20, 'Charpenterie', 'TALENT_WOODREFINE', 5, 2),
(21, 'Appel du Kradjeck', 'TALENT_KRADJECKCALL', 2, 4),
(22, 'Détection des ressources', 'TALENT_DETECTION', 2, 4),
(23, 'Connaissance des monstres', 'TALENT_MONSTER', 2, 4);
