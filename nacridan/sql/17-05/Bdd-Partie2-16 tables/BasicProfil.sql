-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 18 Juillet 2013 à 16:28
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `NacridanV2B`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicProfil`
--

CREATE TABLE IF NOT EXISTS `BasicProfil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `hp` tinyint(4) NOT NULL DEFAULT '0',
  `strength` tinyint(4) NOT NULL DEFAULT '0',
  `dexterity` tinyint(4) NOT NULL DEFAULT '0',
  `speed` tinyint(4) NOT NULL DEFAULT '0',
  `magicSkill` tinyint(4) NOT NULL DEFAULT '0',
  `armor` tinyint(4) NOT NULL DEFAULT '0',
  `attack_bm` tinyint(4) NOT NULL DEFAULT '0',
  `defense_bm` tinyint(4) NOT NULL DEFAULT '0',
  `damage_bm` tinyint(4) NOT NULL DEFAULT '0',
  `magicSkill_bm` tinyint(4) NOT NULL DEFAULT '0',
  `fightStyle` int(7) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=264 ;

--
-- Contenu de la table `BasicProfil`
--

INSERT INTO `BasicProfil` (`id`, `name`, `hp`, `strength`, `dexterity`, `speed`, `magicSkill`, `armor`, `attack_bm`, `defense_bm`, `damage_bm`, `magicSkill_bm`, `fightStyle`) VALUES
(100, 'Rat géant', 5, 5, 4, 1, 0, 2, 2, 0, 2, 0, 3),
(101, 'Crapaud géant', 7, 5, 3, 0, 0, 2, 0, 0, 4, 0, 3),
(102, 'Chauve-souris géante', 2, 2, 4, 7, 0, 1, 1, 4, 0, 0, 1),
(103, 'Kobold', 3, 0, 5, 2, 5, 1, 1, 0, 0, 4, 2),
(104, 'Loup', 2, 3, 5, 5, 0, 1, 3, 0, 2, 0, 2),
(105, 'Araignée éclipsante', 3, 2, 4, 6, 0, 1, 1, 4, 0, 0, 1),
(106, 'Fourmi guerrière géante', 3, 6, 5, 1, 0, 1, 4, 0, 1, 0, 3),
(107, 'Fourmi Reine', 6, 5, 4, 0, 0, 3, 0, 0, 3, 0, 3),
(108, 'Homme Lézard', 3, 5, 6, 1, 0, 3, 2, 0, 1, 0, 3),
(109, 'Mange-Coeur', 3, 6, 4, 2, 0, 2, 0, 0, 4, 0, 3),
(110, 'Araignée Sabre', 2, 1, 5, 7, 0, 1, 1, 4, 0, 0, 1),
(111, 'Araignée piègeuse géante', 4, 3, 7, 1, 0, 1, 4, 1, 0, 0, 2),
(112, 'Ame en peine', 4, 6, 4, 1, 0, 2, 1, 0, 3, 0, 1),
(113, 'Fantôme', 2, 0, 3, 4, 6, 1, 0, 1, 0, 4, 1),
(114, 'Esprit terrifiant', 3, 6, 4, 0, 2, 1, 3, 2, 0, 0, 3),
(115, 'DoppleGanger', 2, 4, 2, 2, 5, 1, 0, 0, 2, 3, 2),
(116, 'Assassin Runique', 3, 1, 8, 3, 1, 2, 2, 0, 2, 0, 2),
(117, 'Ange noir', 3, 0, 0, 5, 7, 0, 0, 2, 0, 4, 4),
(118, 'Gobelin', 3, 3, 5, 4, 0, 1, 3, 0, 2, 0, 2),
(119, 'Shaman Gobelin', 2, 0, 4, 2, 7, 0, 0, 0, 0, 5, 4),
(120, 'Gobelin Archer', 2, 0, 9, 4, 0, 0, 3, 2, 0, 0, 2),
(121, 'Goule', 4, 6, 4, 1, 0, 2, 0, 0, 4, 0, 3),
(122, 'Ombre', 2, 0, 0, 5, 8, 0, 0, 1, 0, 5, 4),
(123, 'Blême', 3, 0, 0, 6, 6, 0, 0, 0, 4, 2, 4),
(124, 'Golem de chair', 7, 5, 3, 0, 0, 3, 0, 0, 3, 0, 3),
(125, 'Momie', 3, 0, 4, 2, 6, 1, 0, 0, 0, 5, 4),
(126, 'Squelette', 4, 5, 5, 1, 0, 2, 2, 0, 2, 0, 2),
(127, 'Gobelours', 7, 5, 3, 0, 0, 3, 1, 0, 2, 0, 3),
(128, 'Orc', 4, 4, 6, 1, 0, 2, 3, 0, 1, 0, 2),
(129, 'Horreur des sous bois', 5, 0, 5, 5, 0, 0, 4, 0, 0, 0, 1),
(130, 'Fée des bois', 1, 0, 2, 4, 8, 0, 0, 2, 0, 4, 4),
(131, 'Mantes prêtresse', 3, 0, 9, 3, 0, 1, 1, 0, 4, 0, 2),
(138, 'Feu Fol', 3, 0, 4, 2, 6, 0, 0, 2, 0, 4, 4),
(253, 'Kradjeck Ferreux', 5, 2, 2, 5, 1, 5, 0, 0, 0, 0, 1),
(263, 'Tortue Géante', 8, 4, 2, 1, 0, 5, 0, 0, 0, 0, 1),
(132, 'Hobgobelin', 5, 5, 5, 0, 0, 4, 1, 0, 2, 0, 3),
(133, 'Troll', 5, 2, 8, 0, 0, 2, 2, 0, 2, 0, 2),
(134, 'Necromancien', 2, 0, 5, 0, 8, 0, 2, 0, 0, 4, 4),
(135, 'Zombi', 3, 3, 5, 1, 2, 1, 2, 0, 1, 0, 2),
(136, 'Combattant squelette', 7, 2, 2, 2, 2, 4, 2, 0, 2, 0, 2),
(144, 'Serpent Aqualien', 4, 6, 5, 0, 0, 1, 2, 0, 3, 0, 3),
(145, 'Abobination des marais', 5, 4, 4, 2, 0, 2, 2, 0, 2, 0, 2),
(146, 'Basilic Majeur', 3, 0, 5, 2, 5, 1, 2, 0, 0, 4, 4),
(147, 'Basilic mineur', 3, 2, 4, 6, 0, 0, 4, 0, 2, 0, 1),
(148, 'Manticore', 5, 5, 5, 0, 0, 1, 4, 0, 2, 0, 2),
(149, 'Dragon des dunes', 3, 0, 4, 4, 4, 3, 2, 1, 0, 0, 4),
(150, 'Scorpion géant', 5, 3, 4, 3, 0, 3, 4, 0, 1, 0, 2),
(152, 'Basilic Roi', 4, 0, 4, 1, 6, 1, 5, 0, 0, 0, 4),
(153, 'Vouivre', 3, 2, 4, 4, 2, 0, 2, 3, 0, 1, 2),
(154, 'Etranglesaule', 7, 6, 3, 0, 0, 4, 4, 0, 0, 0, 3),
(156, 'Gargouille', 4, 2, 5, 4, 0, 4, 1, 0, 4, 0, 2),
(155, 'Sauteur Fauchard', 4, 7, 4, 0, 0, 1, 2, 0, 4, 0, 3),
(157, 'Golem d''Argile', 7, 5, 3, 0, 0, 3, 4, 0, 1, 0, 3),
(158, 'Dopplegangeur majeur', 2, 2, 2, 2, 7, 0, 0, 4, 0, 0, 1),
(159, 'Araignée titanesque', 7, 4, 4, 0, 0, 2, 4, 0, 1, 0, 2),
(160, 'Golem de bière', 4, 2, 6, 3, 0, 0, 2, 0, 2, 0, 2),
(161, 'Dragon de ven', 5, 0, 2, 4, 4, 0, 0, 3, 0, 3, 1),
(170, 'Araignée Colossale', 6, 4, 4, 1, 0, 2, 2, 0, 3, 0, 2),
(171, 'Gnoll', 3, 3, 4, 3, 2, 1, 3, 2, 0, 0, 2);
