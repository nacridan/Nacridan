-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:45
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicBuilding`
--

CREATE TABLE IF NOT EXISTS `BasicBuilding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Maison',
  `pic` varchar(32) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  `solidity` enum('Fragile','Medium','Solid') NOT NULL DEFAULT 'Fragile',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=163 ;

--
-- Contenu de la table `BasicBuilding`
--

INSERT INTO `BasicBuilding` (`id`, `name`, `pic`, `price`, `solidity`) VALUES
(1, 'Auberge', 'auberge.png', 200, 'Fragile'),
(2, 'Banque', 'banque.png', 1500, 'Solid'),
(3, 'Caserne', 'caserne.png', 10000, 'Fragile'),
(4, 'Comptoir Commercial', 'comptoir-commercial.png', 200, 'Medium'),
(5, 'Échoppe', 'echoppe.png', 200, 'Fragile'),
(6, 'École de Combat', 'ecole-combat.png', 400, 'Medium'),
(7, 'École de Magie', 'ecole-magie.png', 400, 'Medium'),
(8, 'École des Métiers', 'ecole-metiers.png', 400, 'Medium'),
(9, 'Guilde des Artisans', 'guilde-artisans.png', 500, 'Medium'),
(10, 'Maison', 'maison.png', 200, 'Fragile'),
(11, 'Palais du Gouverneur', 'palais-gouverneur.png', 10000, 'Fragile'),
(12, 'Palais Royal', 'palais-royal.png', 10000, 'Fragile'),
(13, 'Prison', 'prison.png', 10000, 'Fragile'),
(14, 'Temple', 'temple.png', 500, 'Solid'),
(15, 'Bassin Divin', 'bassin-divin.png', 0, 'Fragile'),
(16, 'Rempart', 'mur-pierre-NS.png', 50, 'Solid'),
(17, 'Rempart', 'mur-pierre-NE.png', 50, 'Solid'),
(18, 'Rempart', 'mur-pierre-EO.png', 50, 'Solid'),
(19, 'Rempart', 'mur-pierre-coin-SE.png', 50, 'Solid'),
(20, 'Rempart', 'mur-pierre-coin-NE.png', 50, 'Solid'),
(21, 'Porte Fermée', 'mur-pierre-porte-N.png', 50, 'Solid'),
(22, 'Rempart', 'mur-pierre-coin-E.png', 50, 'Solid'),
(23, 'Rempart', 'mur-pierre-coin-NO.png', 50, 'Solid'),
(24, 'Rempart', 'mur-pierre-coin-O.png', 50, 'Solid'),
(25, 'Rempart', 'mur-pierre-coin-SO.png', 50, 'Solid'),
(26, 'Porte Ouverte', 'mur-pierre-porte-N-ouverte.png', 50, 'Solid'),
(27, 'Mur de Terre', 'mur-de-terre.png', 50, 'Medium');
