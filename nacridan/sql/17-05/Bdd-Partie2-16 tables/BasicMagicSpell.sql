-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Ven 03 Août 2012 à 16:35
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicMagicSpell`
--

CREATE TABLE IF NOT EXISTS `BasicMagicSpell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `ident` varchar(30) NOT NULL DEFAULT '',
  `school` tinyint(4) NOT NULL DEFAULT '1',
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `PA` int(11) NOT NULL DEFAULT '0',
  `usable` int(7) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `BasicMagicSpell`
--

INSERT INTO `BasicMagicSpell` (`id`, `name`, `ident`, `school`, `level`, `PA`, `usable`) VALUES
(1, 'Toucher brulant', 'SPELL_BURNING', 10, 0, 8, 0),
(2, 'Armure d''Athlan', 'SPELL_ARMOR', 10, 0, 5, 0),
(3, 'Larmes de vie', 'SPELL_TEARS', 10, 0, 7, 0),
(4, 'Soufle d''Athlan', 'SPELL_CURE', 5, 1, 5, 0),
(5, 'Charme de vitalité', 'SPELL_REGEN', 5, 1, 8, 0),
(7, 'Bénédiction', 'SPELL_BLESS', 6, 1, 5, 0),
(8, 'Réveil de la terre', 'SPELL_WALL', 6, 1, 8, 10),
(9, 'Barrière enflammée', 'SPELL_BARRIER', 6, 1, 5, 0),
(12, 'Poing du démon', 'SPELL_DEMONPUNCH', 7, 1, 6, 0),
(6, 'Bassin Divin', 'SPELL_SPRING', 5, 1, 9, 10),
(11, 'Boule de feu', 'SPELL_FIREBALL', 7, 1, 8, 0),
(13, 'Brasier', 'SPELL_FIRE', 7, 2, 10, 10),
(14, 'Sang de lave', 'SPELL_BLOOD', 7, 2, 8, 0),
(10, 'Bouclier magique', 'SPELL_SHIELD', 6, 2, 5, 0),
(15, 'Souffle de négation', 'SPELL_NEGATION', 6, 2, 8, 0),
(16, 'Pluie sacrée', 'SPELL_RAIN', 5, 2, 9, 10),
(17, 'Recharge du bassin', 'SPELL_RELOAD', 5, 2, 5, 10),
(18, 'Soleil de guérison', 'SPELL_SUN', 5, 2, 10, 10),
(19, 'Bulle de vie', 'SPELL_BUBBLE', 6, 2, 8, 0),
(20, 'Piliers infernaux', 'SPELL_PILLARS', 7, 2, 9, 10),
(21, 'Malédiction d''Arcxos', 'SPELL_CURSE', 8, 1, 8, 0),
(22, 'Ailes de colère', 'SPELL_ANGER', 8, 1, 8, 0),
(23, 'Téléportation', 'SPELL_TELEPORT', 8, 2, 8, 0),
(24, 'Toucher de lumière', 'SPELL_LIGHTTOUCH', 8, 2, 5, 0),
(25, 'Projection de l''âme', 'SPELL_SOUL', 8, 1, 5, 0),
(26, 'Rappel', 'SPELL_RECALL', 8, 2, 12, 0),
(27, 'Appel du feu', 'SPELL_FIRECALL', 7, 1, 10, 0);
