-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:53
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicTemplate`
--

CREATE TABLE IF NOT EXISTS `BasicTemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Modifier_BasicTemplate` bigint(20) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `frequency` tinyint(4) NOT NULL DEFAULT '0',
  `numset` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `name2` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `BasicTemplate`
--

INSERT INTO `BasicTemplate` (`id`, `id_Modifier_BasicTemplate`, `rank`, `frequency`, `numset`, `name`, `name2`) VALUES
(1, 1, 50, 8, 0, 'du Kraken', 'Colossal(e)'),
(2, 2, 70, 3, 1, 'du Griffon', 'Flamboyant(e)'),
(3, 3, 80, 3, 2, 'de la Manticore', 'Foudroyant(e)'),
(4, 4, 60, 8, 0, 'du Phénix', 'Ethéré(e)'),
(5, 5, 90, 3, 3, 'du Dragon', 'Eternel(le)');
