-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 21 Mai 2013 à 14:52
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `BuildingAction`
--

CREATE TABLE IF NOT EXISTS `BuildingAction` (
  `id` bigint(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `ident` varchar(30) NOT NULL,
  `price` bigint(15) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT '0',
  `ap` int(7) NOT NULL,
  `id_BasicBuilding` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `BuildingAction`
--

INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`) VALUES
(401, 'Téléportation', 'TELEPORT', 0, 5, 12, 14),
(402, 'Soin', 'TEMPLE_HEAL', 4, 1, 2, 14),
(403, 'Bénédiction', 'TEMPLE_BLESSING', 9, 3, 2, 14),
(404, 'Fixation de l''âme', 'TEMPLE_RESURRECT', 15, 5, 0, 14),
(405, 'Dormir une nuit', 'BEDROOM_SLEEPING', 4, 1, 12, 1),
(406, 'Boire un verre', 'HOSTEL_DRINK', 0, 1, 1, 1),
(408, 'Payer une tournée', 'HOSTEL_ROUND', 5, 5, 5, 1),
(409, 'Poster une annonce', 'HOSTEL_NEWS', 2, 3, 3, 1),
(413, 'Déposer de l''argent', 'BANK_DEPOSIT', 9, 1, 0, 4),
(414, 'Retirer de l''argent', 'BANK_WITHDRAW', 0, 0, 0, 4),
(415, 'Transfert d''argent', 'BANK_TRANSFERT', 9, 1, 0, 4),
(416, 'Achat echoppe basique', 'BASIC_SHOP_BUY', 90, 10, 0, 5),
(418, 'Achat échoppe principale', 'MAIN_SHOP_BUY', 90, 10, 0, 5),
(419, 'Commander équipment', 'GUILD_ORDER_EQUIP', 70, 30, 0, 9),
(421, 'Commander enchantement', 'GUILD_ORDER_ENCHANT', 70, 30, 0, 9),
(425, 'Réparation', 'SHOP_REPAIR', 70, 30, 0, 5),
(427, 'Envoi colis', 'SEND_PACKAGE', 2, 3, 0, 4),
(450, 'Sortilège élémentaire', 'SCHOOL_LEARN_SPELL_0', 50, 30, 12, 7),
(451, 'Sortillège aspirant', 'SCHOOL_LEARN_SPELL_1', 400, 100, 12, 7),
(452, 'Sortilège adepte', 'SCHOOL_LEARN_SPELL_2', 1000, 500, 12, 7),
(453, 'Compétence élémentaire', 'SCHOOL_LEARN_ABILITY_0', 50, 30, 12, 6),
(454, 'Compétence aspirant', 'SCHOOL_LEARN_ABILITY_1', 400, 100, 12, 6),
(455, 'Compétence adepte', 'SCHOOL_LEARN_ABILITY_2', 1000, 500, 12, 6),
(457, 'Savoir-faire raffinage', 'SCHOOL_LEARN_TALENT_1', 30, 20, 12, 8),
(458, 'Savoir-faire artisanat', 'SCHOOL_LEARN_TALENT_2', 60, 40, 12, 8),
(459, 'Savoir faire nature', 'SCHOOL_LEARN_TALENT_3', 10, 5, 10, 8),
(456, 'Talent extraction', 'SCHOOL_LEARN_TALENT_0', 15, 10, 12, 8),
(441, 'Message commercial', 'HOSTEL_TRADE', 5, 5, 2, 1);
