UPDATE `BuildingAction` SET `id_BasicBuilding` = '7' WHERE `BuildingAction`.`id` = 410 AND `BuildingAction`.`id_BasicBuilding` = 12;
UPDATE `BuildingAction` SET `id_BasicBuilding` = '6' WHERE `BuildingAction`.`id` = 411 AND `BuildingAction`.`id_BasicBuilding` = 12;
UPDATE `BuildingAction` SET `id_BasicBuilding` = '8' WHERE `BuildingAction`.`id` = 412 AND `BuildingAction`.`id_BasicBuilding` = 12;

INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(430, 'Construction d''un bâtiment', 'PALACE_CONSTRUCT_BUILDING', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(431, 'Réparation d''un bâtiment', 'PALACE_REPAIR_BUILDING', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(432, 'Destruction d''un bâtiment', 'PALACE_DESTROY_BUILDING', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(438, 'Renommage d''un village', 'PALACE_RENAME_CITY', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(439, 'Fortification d''un village', 'PALACE_FORTIFY_CITY', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(443, 'Amélioration des fortifications', 'PALACE_UP_FORTIFY_CITY', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(461, 'Terrassement d''un village', 'PALACE_TERRASSEMENT', 0, 0, 0, 11, 'No', 'Yes');
INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES(462, 'Terraformation d''un village', 'PALACE_TERRAFORMATION', 0, 0, 0, 11, 'No', 'Yes');