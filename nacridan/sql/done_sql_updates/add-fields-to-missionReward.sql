ALTER TABLE `MissionReward` ADD `id_Player` bigint(20) NOT NULL DEFAULT '0' AFTER `idMission`;
ALTER TABLE `MissionReward` ADD `MissionDuration` int NOT NULL DEFAULT '0' AFTER `ExtranameEquipStop`;
ALTER TABLE `MissionReward` ADD `MissionMark` int NOT NULL DEFAULT '0' AFTER `ExtranameEquipStop`;
ALTER TABLE `MissionReward` ADD `NumberOfParticipants` int NOT NULL DEFAULT '0' AFTER `ExtranameEquipStop`;
ALTER TABLE `MissionReward` ADD `PlayerLevelAtThatTime` int NOT NULL DEFAULT '0' AFTER `ExtranameEquipStop`;