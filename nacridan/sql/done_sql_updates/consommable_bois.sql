INSERT INTO `EquipmentType` ( `name`, `wearable`, `small`, `mask`, `zone`) VALUES
('repair_consumable',    'No',    'No',    0,    0);


INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(405, 999,    47,    2,    'Huile des bosquets',    8,    60);

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(406, 999,    47,    1,    'Fusil à aiguiser',    8,    60);

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(407, 999,    47,    1,    'Décoction protectrice',    8,    60);

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(408, 999,    47,    3,    'Poudre d''ivoire nocturne',    8,    60);

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(409, 999,    47,    5,    'Elixir de la nouvelle aube',    8,    60);

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(410, 999,    47,    4,    'Gel d''Eclipse',    8,    60);


INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(53, 17, 'MAINTAIN_EQUIPMENT', '{player} a entretenu son équipement.');
