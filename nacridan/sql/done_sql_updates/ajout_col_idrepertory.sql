ALTER TABLE `Mail` ADD `id_repertory` INT NULL DEFAULT NULL ;
ALTER TABLE `MailAlert` ADD `id_repertory` INT NULL DEFAULT NULL ;
ALTER TABLE `MailArchive` ADD `id_repertory` INT NULL DEFAULT NULL ;
ALTER TABLE `MailSend` ADD `id_repertory` INT NULL DEFAULT NULL ;
ALTER TABLE `MailTrade` ADD `id_repertory` INT NULL DEFAULT NULL ;

UPDATE `NameRepertory` set name_Repertory=replace(name_Repertory, '\\''', '''') WHERE name_Repertory like '%\\''%';

UPDATE Mail, NameRepertory 
SET Mail.id_repertory=NameRepertory.id 
WHERE Mail.name_Repertory = NameRepertory.name_Repertory and Mail.id_Player$receiver = NameRepertory.id_Player;

UPDATE MailAlert, NameRepertory 
SET MailAlert.id_repertory=NameRepertory.id 
WHERE MailAlert.name_Repertory = NameRepertory.name_Repertory and MailAlert.id_Player$receiver = NameRepertory.id_Player;

UPDATE MailArchive, NameRepertory 
SET MailArchive.id_repertory=NameRepertory.id 
WHERE MailArchive.name_Repertory = NameRepertory.name_Repertory and MailArchive.id_Player = NameRepertory.id_Player;

UPDATE MailSend, NameRepertory 
SET MailSend.id_repertory=NameRepertory.id 
WHERE MailSend.name_Repertory = NameRepertory.name_Repertory and MailSend.id_Player = NameRepertory.id_Player;

UPDATE MailTrade, NameRepertory 
SET MailTrade.id_repertory=NameRepertory.id 
WHERE MailTrade.name_Repertory = NameRepertory.name_Repertory and MailTrade.id_Player$receiver = NameRepertory.id_Player;

