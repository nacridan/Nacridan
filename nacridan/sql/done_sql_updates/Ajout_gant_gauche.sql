INSERT INTO `Modifier_BasicEquipment` (`id`, `hp`, `hp_bm`, `strength`, `strength_bm`, `dexterity`, `dexterity_bm`, `speed`, `speed_bm`, `magicSkill`, `magicSkill_bm`, `attack`, `attack_bm`, `defense`, `defense_bm`, `damage`, `damage_bm`, `armor`, `armor_bm`, `timeAttack`, `timeAttack_bm`) VALUES
(59, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1.2/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(60, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1.2/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(61, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1.2/', '////////', '////////', '////////'),
(63, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1.2/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////');


INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(59, 59, 16, 4, 'Gant gauche de cuir', 32, 60),
(60, 60, 16, 3, 'Gant gauche d''écailles', 32, 60),
(61, 61, 16, 1, 'Gant gauche de maille', 32, 60),
(62, 63, 16, 5, 'Gant gauche magique', 32, 60);

UPDATE `BasicEvent` SET `description` = '{player} n''a pas réussi à se téléporter.' WHERE `BasicEvent`.`id` =232;
UPDATE `BasicEvent` SET `description` = '{player} s''est téléporté{gendersrc}.' WHERE `BasicEvent`.`id` =230;
UPDATE `BasicEvent` SET `description` = '{player} a accepté d''être téléporté{gendersrc} auprès de {target}.' WHERE `BasicEvent`.`id` =30;
UPDATE `BasicEvent` SET `description` = '{player} a refusé d''être téléporté{gendersrc} auprès de {target}.' WHERE `BasicEvent`.`id` =31;
UPDATE `BasicEvent` SET `description` = '{player} s''est téléporté{gendersrc}.' WHERE `BasicEvent`.`id` =601;

UPDATE `BasicRace` SET `name` = 'Araignée colossale' WHERE `BasicRace`.`id` =170;