CREATE TABLE `TeamMsg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Team` bigint(20) NOT NULL,
  `id_Player` int(20) NOT NULL,
  `body` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `Player`
 ADD `time_TeamMsg_read` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `time_FighterGroupMsg_read`;