UPDATE Building SET sp=sp*2,currsp=currsp*2,progress=progress*2;
INSERT INTO `BuildingAction` (`id` ,`name` ,`ident` ,`price` ,`profit` ,`ap` ,`id_BasicBuilding` ,`taxable`)
VALUES (
'465', 'Mort du prêtre', 'PRIEST_DEATH', '0', '0', '0', '14', 'No'
);
INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(458, 30, 'PRIEST_DEATH_EVENT', '{player} a tué le prêtre du village.');

ALTER TABLE `BuildingAction` ADD `treasureAccount` ENUM( 'Yes', 'No' ) NOT NULL;
UPDATE `BuildingAction` SET `treasureAccount` = 'Yes';
UPDATE `BuildingAction` SET `treasureAccount` = 'No' WHERE `BuildingAction`.`id` =465;

INSERT INTO `BuildingAction` (`id` ,`name` ,`ident` ,`price` ,`profit` ,`ap` ,`id_BasicBuilding` ,`taxable`,`treasureAccount`)
VALUES (
'466', 'Mort du garde du palais', 'GUARD_DEATH', '0', '0', '0', '14', 'No','No'
);
INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(459, 13, 'GUARD_DEATH_EVENT', '{player} a tué le garde du palais.');