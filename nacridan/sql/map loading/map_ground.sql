CREATE TABLE `MapGround` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `color` varchar(20) NOT NULL,
  `ground` varchar(20) NOT NULL,
  `modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `x` (`x`,`y`,`map`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `MapZone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `color` varchar(20) NOT NULL,
  `zone` int(11) NOT NULL,
  `modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `x` (`x`,`y`,`map`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES ('454', '13', 'TERRASSEMENT_EVENT', '{player} a terrassé son village.');
INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES ('455', '13', 'TERRAFORMATION_EVENT', '{player} a terraformé son village.');


CREATE TABLE `Ground` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ground` VARCHAR( 20 ) NOT NULL ,
`label` VARCHAR( 40 ) NOT NULL ,
`color` VARCHAR( 20 ) NOT NULL,
`cost_PA` DOUBLE NOT NULL 
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `Ground` ADD UNIQUE (
`code`
);
ALTER TABLE `Ground` ADD UNIQUE (
`label`
);
ALTER TABLE `Ground` ADD UNIQUE (
`color`
);

insert into Ground(color,ground,label,cost_PA) Values ('0 0 255 0','ocean','Océan',100);
insert into Ground(color,ground,label,cost_PA) Values ('129 96 58 0','beatenearth','Terre battue',0.5);
insert into Ground(color,ground,label,cost_PA) Values ('144 169 88 0','forest','Forêt',1.5);
insert into Ground(color,ground,label,cost_PA) Values ('148 126 107 0','mountain','Montagne',2);
insert into Ground(color,ground,label,cost_PA) Values ('151 151 150 0','cobblestone','Espace pavé',0.5);
insert into Ground(color,ground,label,cost_PA) Values ('179 213 137 0','plain','Plaine',1);
insert into Ground(color,ground,label,cost_PA) Values ('180 142 88 0','marsh','Marais',2);
insert into Ground(color,ground,label,cost_PA) Values ('233 217 173 0','desert','Désert',1.5);
insert into Ground(color,ground,label,cost_PA) Values ('244 192 69 0','flowers','Plaine fleurie',1);
insert into Ground(color,ground,label,cost_PA) Values ('255 255 255 0','polar','Plaine Arctique',1.5);
insert into Ground(color,ground,label,cost_PA) Values ('64 128 255 0','river','Rivière',100);

ALTER TABLE `MapGround` ADD `id_Ground` INT NOT NULL AFTER `map`;

ALTER TABLE `MapGround`
  DROP `color`,
  DROP `ground`;
  
