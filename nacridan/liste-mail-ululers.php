<?php

// ---liste des gens ayant acheté un an de bonus
function givelist_GGbonus()
{
    $liste = array();
    $liste[] = "aslasvan@gmail.com"; // Vyka
    $liste[] = "munozfabien@gmail.com";
    $liste[] = "toffeskien@gmail.com"; // Cratofix FPL
    $liste[] = "nicolasrpg@laposte.net"; // Hain
    $liste[] = "phassemat@gmail.com"; // Phil
    $liste[] = "kurien@hotmail.fr"; // Kurien
    $liste[] = "shastabounty@hotmail.fr"; // Kyrra
    $liste[] = "sjaillet@gmail.com"; // God ^^
    $liste[] = "paul.butin@laposte.net"; // Nidaime
    $liste[] = "unfly71@hotmail.com"; // MightyDuck
    $liste[] = "cyril.correia@yahoo.fr"; // Khazin
    $liste[] = "gdelfolie@gmail.com"; // Zaza
    $liste[] = "claude.longelin@gmail.com"; // Dilvich
    $liste[] = "alexandre.guittard@gmail.com"; // Aksho
    $liste[] = "stephane.melis2@hotmail.fr"; // Llyn
    $liste[] = "celine.pollet73@gmail.com"; // Belette
    $liste[] = "emdjii@gmail.com"; // MG
    $liste[] = "jerome@bianquis.fr";
    $liste[] = "dodie_didou@hotmail.com";
    $liste[] = "pierrette.boue@hotmail.fr";
    $liste[] = "ambrune@hotmail.fr";
    $liste[] = "ydecat@hotmail.com";
    $liste[] = "moanho@gmail.com";
    $liste[] = "ixnay_phil@hotmail.com";
    $liste[] = "dinfante@hotmail.fr";
    $liste[] = "lordthergal@gmail.com";
    $liste[] = "celine.pollet73@gmail.com";
    $liste[] = "ama.devaux@gmail.com";
    $liste[] = "aeli.lhum@gmail.com";
    return $liste;
}

// ---------- liste des gens ayant acheté un perso supplémentaire
function givelist_chara()
{
    $liste = array();
    $liste[] = "toffeskien@gmail.com"; // Cratofix FPL
    $liste[] = "nicolasrpg@laposte.net"; // Hain
    $liste[] = "phassemat@gmail.com"; // Phil
    $liste[] = "kurien@hotmail.fr"; // Kurien
    $liste[] = "shastabounty@hotmail.fr"; // Kyrra
    $liste[] = "sjaillet@gmail.com"; // God ^^
    $liste[] = "paul.butin@laposte.net"; // Nidaime
    $liste[] = "unfly71@hotmail.com"; // MightyDuck
    $liste[] = "cyril.correia@yahoo.fr"; // Khazin
    $liste[] = "gdelfolie@gmail.com"; // Zaza
    $liste[] = "claude.longelin@gmail.com"; // Dilvich
    $liste[] = "alexandre.guittard@gmail.com"; // Aksho
    $liste[] = "stephane.melis2@hotmail.fr"; // Llyn
    $liste[] = "celine.pollet73@gmail.com"; // Belette
    $liste[] = "emdjii@gmail.com"; // MG
    $liste[] = "jerome@bianquis.fr";
    $liste[] = "dodie_didou@hotmail.com";
    $liste[] = "pierrette.boue@hotmail.fr";
    $liste[] = "ambrune@hotmail.fr";
    $liste[] = "ydecat@hotmail.com";
    $liste[] = "moanho@gmail.com";
    $liste[] = "ixnay_phil@hotmail.com";
    $liste[] = "dinfante@hotmail.fr";
    $liste[] = "lordthergal@gmail.com";
    $liste[] = "celine.pollet73@gmail.com";
    $liste[] = "ama.devaux@gmail.com";
    $liste[] = "aeli.lhum@gmail.com";
    return $liste;
}

// ---------liste des gens ayant acheté l'armure naturelle
function givelist_armor()
{
    $liste = array();
    $liste[] = "shastabounty@hotmail.fr"; // Kyrra
    $liste[] = "sjaillet@gmail.com"; // God ^^
    $liste[] = "paul.butin@laposte.net"; // Nidaime
    $liste[] = "unfly71@hotmail.com"; // MightyDuck
    $liste[] = "cyril.correia@yahoo.fr"; // Khazin
    $liste[] = "gdelfolie@gmail.com"; // Zaza
    $liste[] = "claude.longelin@gmail.com"; // Dilvich
    $liste[] = "alexandre.guittard@gmail.com"; // Aksho
    $liste[] = "stephane.melis2@hotmail.fr"; // Llyn
    $liste[] = "celine.pollet73@gmail.com"; // Belette
    $liste[] = "emdjii@gmail.com"; // MG
    $liste[] = "jerome@bianquis.fr";
    $liste[] = "dodie_didou@hotmail.com";
    $liste[] = "pierrette.boue@hotmail.fr";
    $liste[] = "ambrune@hotmail.fr";
    $liste[] = "ydecat@hotmail.com";
    $liste[] = "moanho@gmail.com";
    $liste[] = "ixnay_phil@hotmail.com";
    $liste[] = "dinfante@hotmail.fr";
    $liste[] = "lordthergal@gmail.com";
    $liste[] = "ama.devaux@gmail.com";
    $liste[] = "aeli.lhum@gmail.com";
    return $liste;
}

// -------- liste des gens ayant acheté une maison
function givelist_house()
{
    $liste = array();
    
    $liste[] = "paul.butin@laposte.net"; // Nidaime
    $liste[] = "unfly71@hotmail.com"; // MightyDuck
    $liste[] = "cyril.correia@yahoo.fr"; // Khazin
    $liste[] = "gdelfolie@gmail.com"; // Zaza
    $liste[] = "claude.longelin@gmail.com"; // Dilvich
    $liste[] = "alexandre.guittard@gmail.com"; // Aksho
    $liste[] = "stephane.melis2@hotmail.fr"; // Llyn
    $liste[] = "celine.pollet73@gmail.com"; // Belette
    $liste[] = "emdjii@gmail.com"; // MG
    $liste[] = "jerome@bianquis.fr";
    $liste[] = "dodie_didou@hotmail.com";
    $liste[] = "pierrette.boue@hotmail.fr";
    $liste[] = "ambrune@hotmail.fr";
    $liste[] = "ydecat@hotmail.com";
    $liste[] = "moanho@gmail.com";
    $liste[] = "ixnay_phil@hotmail.com";
    $liste[] = "dinfante@hotmail.fr";
    $liste[] = "lordthergal@gmail.com";
    $liste[] = "celine.pollet73@gmail.com";
    $liste[] = "ama.devaux@gmail.com";
    $liste[] = "aeli.lhum@gmail.com";
    
    return $liste;
}

?>
