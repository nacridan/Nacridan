<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/include/constants.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");

require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");
require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

$db = DB::getDB();

// display AI status
exec("cd " . HOMEPATH . "/AI;./status.sh", $res);
$status = "unknown";

foreach ($res as $lign) {
    if ($lign >= 2)
        $status = "<img src='" . CONFIG_HOST . "/pics/register/green_ball.png'  style='position: relative; left: 0px; top: 7px; width:20px;' >";
    else
        $status = "<img src='" . CONFIG_HOST . "/pics/register/red_ball.png'  style='position: relative; left: 0px; top: 7px; width:20px;' >";
}

$BOTTOMTitle->add("<div class='login-left'><b>IA :</b> " . $status . " <br><br>GMT : " . gmdate('H:i:s') . "</div>");

$FORM = $BOTTOMTitle->addNewHTMLObject('div', '', 'class="loginfield"');

$str = '<form name="loginform" method="post" action="' . CONFIG_HOST . '/index.php" style="margin:0">';
$str .= '<div class="login-form-div">';
$str .= '<b> Identifiant </b><br>';
$str .= '<input type="text" name="username"><br><br>';
$str .= '<b> Mot de Passe </b><br>';
$str .= '<input type="password" name="password"><br>';
$str .= '<input type="checkbox" name="dla" checked="checked" /> <b>activer son tour de jeu</b>';
$str .= "</div>";
$str .= '<div class="login-button-container"><input type="submit" name="submit" class="login-button" value="" /></div>';
$str .= '</form>';

$FORM->add($str);

$strAds = '<a href="http://www.jeux-alternatifs.com/Nacridan-jeu763_hit-parade_1_1.html" target="_blank"><img src="http://www.jeux-alternatifs.com/im/bandeau/hitP_88x31_v1.gif" border="0" /></a>';
$strAds .= '<br><br><a href="#partner-area" >Voir tous les sites de référencement</a>';

$BOTTOMTitle->add("<div class='login-right'>" . $strAds . "</div>");

/**
 * ************************** END OF LOGIN FORM ******************
 */

// $BOTTOMTitle->add ( $strForm );

$strLinks = '<span class="login-one-link"><a href="' . CONFIG_HOST . '/main/register.php?confirmmail=true" class="attackevent">Mail d\'inscription non reçu ?</a> </span> ';
$strLinks .= '<br><span class="login-one-link"><a href="' . CONFIG_HOST . '/main/register.php?lostpasswd=true" class="attackevent">Mot de passe perdu ?</a>  </span>';
$strLinks .= '<br><span class="login-one-link"><a href="' . CONFIG_HOST . '/main/contact.php" class="attackevent popupify">Nous Contacter</a></span>';

$BOTTOMTitle->add('<div class="login-link">' . $strLinks . '</div>');

$dbn = new DBCollection("SELECT * FROM News WHERE type=2 ORDER BY id DESC", $db);


if ($dbn->count() > 0) {
    
    $date = strtotime($dbn->get("date"));
    
    $str = "<br><br><div style='color: #EEEEEE; font-weight: bold;'>" . gmdate("d-m-Y", $date) . "<center>*** ANNONCES ***</center><br/></div>";
    $str .= "<br/>";
    $str .= "<div style='color: #EEEEEE'>";
    
    $str .= "<b><center>" . $dbn->get("title") . "</center></b><br/><br/>";
    $str .= nl2br(bbcode($dbn->get("content"))) . "<br/><br/>";
    
    $str .= "</div>";
    $str .= "<div style='color: #EEEEEE'>";
    
    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbn->get("id_Player"), $db);
    
    $str .= "<i>" . $dbp->get("name") . "</i>";
    $str .= "<br/><br/>";
    $str .= "<a href='../main/news.php' class='statsevent'>>>>Les anciennes annonces</a>.";
    $str .= "</div>";
    $BOTTOMTitle->add("<div class='index-bottom-panels-news'> " . $str . " </div>");
}

// Fin annonces

$db = DB::getDB();
$dblast = new DBCollection(
    'SELECT Player.* FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE Player.authlevel=0 AND Player.id_Member!=0 AND Player.status="PC" AND Player.name IS NOT NULL ORDER BY creation DESC LIMIT 0,9', 
    $db);

$i = $j = 0;
$lastRegistred = array();
while (! $dblast->eof()) {
    /*
     * if($i>=9)
     * {
     * $j++;
     * $i=$j;
     * }
     */
    $lastRegistred[$i] = $dblast->get('name');
    $lastRegistredRace[$i] = $dblast->get('racename');
    $lastRegistredId[$i] = $dblast->get('id');
    // $i+=3;
    $i ++;
    $dblast->next();
}

// print_r($lastRegistred);
$dbActiveMember = new DBCollection('SELECT count(id) AS total FROM Member WHERE  authlevel=0 AND disabled!=99 AND ADDDATE(lastlog, INTERVAL 7 DAY)> NOW()', $db);
$dbActiveCharacter = new DBCollection(
    'SELECT count(Player.id) AS total FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE Player.id_member!=0  AND Player.authlevel=0 AND Player.disabled!=99 AND Player.status="PC" AND Player.name IS NOT NULL  AND ADDDATE(Player.playatb, INTERVAL 14 DAY)> NOW()', 
    $db);
$dbbanned = new DBCollection('SELECT count(id) AS banned FROM Banned WHERE 1', $db);

$BOTTOMTitle->add("<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
$BOTTOMTitle->add(
    '<div class="last-registered-container"><table style="width:100%"><tr><td colspan="11" height="22" align="center" bgcolor="#663333" valign="middle"><strong><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">Les Derniers Inscrits </br> (Nombre de Joueurs Actifs* : ' .
         $dbActiveMember->get('total') . ') <br> (Nombre de Personnages Actifs** : ' . $dbActiveCharacter->get('total') . ')</font></strong></td></tr>');
$BOTTOMTitle->add('<tr>');
for ($i = 0; $i < 3; $i ++) {
    $BOTTOMTitle->add(
        '<td align="center" bgcolor="#330000" height="19" valign="middle" width="37"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">#</font></td><td align="center" bgcolor="#663333" valign="middle" width="84"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Nom</strong></font></td><td align="center" bgcolor="#663333" valign="middle" width="56"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Race</strong></font></td>');
}
$BOTTOMTitle->add('</tr>');
for ($i = 0; $i < 3; $i ++) {
    $BOTTOMTitle->add('<tr>');
    for ($j = 0; $j < 3; $j ++) {
        $name = '';
        $race = '';
        if (isset($lastRegistred[$i + $j * 3]))
            $name = $lastRegistred[$i + $j * 3];
        if (isset($lastRegistredRace[$i + $j * 3]))
            $race = $lastRegistredRace[$i + $j * 3];
        if (isset($lastRegistredId[$i + $j * 3]))
            $id = $lastRegistredId[$i + $j * 3];
        
        $BOTTOMTitle->add(
            '<td align="center" bgcolor="#333333" height="19" valign="middle"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">' . ($i + $j * 3 + 1) .
                 '</font></td><td align="center" bgcolor="#666666" valign="middle"><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="-1"><a href=\'' . CONFIG_HOST .
                 '/main/profile.php?id=' . $id . '\' class=\'stylemainpc popupify\' target=\'_blank\'>' . $name . '</a></font></td><td align="center" bgcolor="#666666" valign="middle">' . $race . '</td>');
    }
    $BOTTOMTitle->add('</tr>');
}

$BOTTOMTitle->add(
    '<tr><td>&nbsp;</td></tr><tr><td colspan="11" height="22" align="center" bgcolor="#330000" valign="middle"><strong><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">Nombre de joueurs bannis pour cause de multi-comptes (Total : ' .
         $dbbanned->get('banned') . ')</font></strong></td></tr>');

$BOTTOMTitle->add(
    '</table><span style="color: #999999;">* : ayant joué dans les 14 derniers jours. <br>** : un joueur peut contrôler deux personnages. </span><br/> </br><a href="' . CONFIG_HOST .
         '/main/stats.php" class="statsevent">&gt;&gt;&gt; Plus de statistiques</a></div><br/>');

$BOTTOMTitle->add("<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
$BOTTOMTitle->add("<div id='partner-area' class='partner-area'> <b>Sites partenaires</b><br><br>");
$BOTTOMTitle->add(
    "Voici la liste des sites dans lesquels vous pouvez poster un commentaire ou une note. Si vous aimez le jeu, prenez quelques minutes pour le faire, cela aide fortement le jeu.");

$BOTTOMTitle->add(
    "<br><a href='http://www.jeu-gratuit.net/jeux-de-role-mmorpg/jeu-nacridan.html' alt='jeux gratuits' target='_blank'><img src='" . CONFIG_HOST .
         "/pics/new-homepage/partner-jeu-gratuit.png' ></a>");
$BOTTOMTitle->add(
    '<span class="login-one-link"><a href="http://www.tourdejeu.net/annu/fichejeu.php?id=5390" target="_blank"><img src="http://www.tourdejeu.net/images/bouton.gif" ></a></span>
');

$BOTTOMTitle->add(
    '<span class="login-one-link"><a href="http://www.2001jeux.fr/nacridan.html" target="_blank"><img src="http://www.2001jeux.fr/images-v3/logo-2001jeux.jpg"></a></span>
');

$BOTTOMTitle->add("</div>");

$BOTTOMTitle->add("</div>");

$MAIN_PAGE->render();

?>
