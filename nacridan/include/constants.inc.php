<?php

/*************************
 * Les constantes du jeu *
 *************************/

// TRIBUNE DE GROUPE DE CHASSE
// Le nombre de secondes ou un message dans la tribune reste accessible, a partir du dernier message non lu.
DEFINE("TRIBUNE_TIME_READABLE", 3 * 24 * 3600); // 3 jours
// Le nombre de secondes ou un message dans la tribune reste accessible au maximum
DEFINE("TRIBUNE_MAX_TIME_READABLE", 7 * 24 * 3600); // 7 jours

// TRIBUNE D'ORDRE
// Le nombre de secondes ou un message dans la tribune reste accessible, a partir du dernier message non lu.
DEFINE("TRIBUNE_ORDER_TIME_READABLE", 6 * 24 * 3600); // 6 jours
// Le nombre de secondes ou un message dans la tribune reste accessible au maximum
DEFINE("TRIBUNE_ORDER_MAX_TIME_READABLE", 14 * 24 * 3600); // 14 jours


// ATB
define("ATB_MAX_TIMEOUT", 3600); // On peut dépasser sa DLA d'1h avant de forcer le passage en session restreinte