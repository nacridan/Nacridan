#!/usr/bin/php
<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
$db = DB::getDB();

// Choix de la carte
$map = 2;
$im = imagecreatefrompng("mapghost0" . $map . ".png");

// Choix du type d'agglomération
$type = 'Village';

if ($type == 'Ville') {
    $taille = 6;
    $ground = imagecolorallocate($im, 151, 151, 150);
} elseif ($type == 'Bourg') {
    $taille = 4;
    $ground = imagecolorallocate($im, 151, 151, 150);
} elseif ($type == 'Village') {
    $taille = 4;
    $ground = imagecolorallocate($im, 129, 96, 58);
} else
    echo 'You screwed up...';

$dbc = new DBCollection("SELECT * from City WHERE map=" . $map . " AND type='" . $type . "'", $db);

$mapinfo = new MapInfo($db);

while (! $dbc->eof()) {
    echo $dbc->get("name") . "<br/>";
    
    $X = $dbc->get("x");
    $Y = $dbc->get("y");
    
    for ($i = 0; $i < 11; $i ++) {
        for ($j = 0; $j < 11; $j ++) {
            $Xtest = $X - 5 + $i;
            $Ytest = $Y - 5 + $j;
            
            $validzone = $mapinfo->getValidMap($Xtest, $Ytest, 0, $map);
            
            if ($validzone[0][0] && (distHexa($X, $Y, $Xtest, $Ytest) < $taille))
                $possiblePosition[] = array(
                    "x" => $Xtest,
                    "y" => $Ytest
                );
        }
    }
    
    foreach ($possiblePosition as $pos) {
        imagesetpixel($im, $pos["x"], $pos["y"], $ground);
    }
    
    $dbc->next();
}

imagepng($im, "mapghost0" . $map . ".png");

?>
