#!/usr/bin/php
<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
$db = DB::getDB();

for ($map = 1; $map < 3; $map ++) {
    $im = imagecreate(640, 400);
    
    $background = imagecolorallocate($im, 0, 0,0);
    $city = imagecolorallocate($im, 50, 50, 50);
    imagecolortransparent($im, $background);
    imagefill($im, 0, 0, $background);
    
    $dbc = new DBCollection("SELECT * from City WHERE map=" . $map . " AND (name='Village' OR name='Pilier')", $db);
    
    while (! $dbc->eof()) {
        imageline($im, $dbc->get("x") - 1, $dbc->get("y"), $dbc->get("x") + 1, $dbc->get("y"), $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") - 1, $dbc->get("x"), $dbc->get("y") + 1, $city);
        $dbc->next();
    }
    
    $dbc = new DBCollection("SELECT * from City WHERE map=" . $map . " AND name='Bourg'", $db);
    
    while (! $dbc->eof()) {
        imageline($im, $dbc->get("x") - 2, $dbc->get("y"), $dbc->get("x"), $dbc->get("y") - 2, $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") - 2, $dbc->get("x") + 2, $dbc->get("y"), $city);
        imageline($im, $dbc->get("x") + 2, $dbc->get("y"), $dbc->get("x"), $dbc->get("y") + 2, $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") + 2, $dbc->get("x") - 2, $dbc->get("y"), $city);
        
        imageline($im, $dbc->get("x") - 1, $dbc->get("y"), $dbc->get("x"), $dbc->get("y") - 1, $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") - 1, $dbc->get("x") + 1, $dbc->get("y"), $city);
        imageline($im, $dbc->get("x") + 1, $dbc->get("y"), $dbc->get("x"), $dbc->get("y") + 1, $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") + 1, $dbc->get("x") - 1, $dbc->get("y"), $city);
        
        $dbc->next();
    }
    
    imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . "/mapghostcity0" . $map . ".png");
}
?>
