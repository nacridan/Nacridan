<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
$db = DB::getDB();

$dbB = new DBCollection("SELECT id,id_BasicBuilding FROM Building", $db, 0, 0);
while (! $dbB->eof()) {
    
    switch ($dbB->get("id_BasicBuilding")) {
        case 6: // ECOLE COMBAT
            $building = new Building();
            $building->load($dbB->get("id"), $db);
            $test1["9"] = 0;
            $building->set("school", serialize($test1));
            $building->updateDB($db);
            echo "bâtiment " . $building->get("id") . " : " . $building->get("name") . " -> GUERRIER </br>";
            break;
        
        case 7: // ECOLE MAGIE
            $building = new Building();
            $building->load($dbB->get("id"), $db);
            $test2["10"] = 0;
            $building->set("school", serialize($test2));
            $building->updateDB($db);
            echo "bâtiment " . $building->get("id") . " : " . $building->get("name") . " -> MAGIE </br>";
            break;
    }
    
    $dbB->next();
}

?>
