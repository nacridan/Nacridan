#!/usr/bin/php
<?php
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
$db = DB::getDB();

for ($map = 1; $map < 3; $map ++) {
    
    echo '..' . MAPS_STATIC_FOLDER . '/mapghostcity0' . $map . '.png';
    echo HOMEPATH . MAPS_STATIC_FOLDER . '/mapghostcity0' . $map . '.png';
    
    $im = @imagecreatefrompng(HOMEPATH . MAPS_STATIC_FOLDER . '/mapghostcity0' . $map . '.png');
    $gray = imagecolorallocate($im, 0, 0, 0);
    
    $dbc = new DBCollection("SELECT x,y FROM City WHERE captured > 3 and map=" . $map, $db);
    while (! $dbc->eof()) {
        imageellipse($im, $dbc->get("x"), $dbc->get("y"), 15, 15, $gray); // imageellipse fonction PHP permettant de dessiner une forme géométrique éllipse.
                                                                          // Voir doc pour plus d'info : http://www.php.net/manual/fr/function.imageellipse.php
        $dbc->next();
    }
    
    imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . '/mapghostcitycaptured0' . $map . '.png'); // création de la nouvelle image
    imagedestroy($im); // on l'ibère l'espace mémoire allouer à la création
}

?>
