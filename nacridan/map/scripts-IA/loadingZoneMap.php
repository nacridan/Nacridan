<?php
set_time_limit(8000);
require_once ("../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();

$dbi = new DBCollection("truncate table MapZone;", $db);

for ($map = 1; $map < 3; $map ++) {
    $imgarea = imagecreatefrompng(HOMEPATH . MAPS_STATIC_FOLDER . "/maparea0" . $map . ".png");
    for ($i = 0; $i < 640; $i ++) {
        for ($j = 0; $j < 400; $j ++) {
            $colors = getColorFromMap($i, $j, $imgarea);
            $rgbStr = $colors['red'] . " " . $colors['green'] . " " . $colors['blue'];
            $zone = getZoneFromColor($i, $j, intval($colors['red']));
            if ($colors['alpha'] == "") {
                $colors['alpha'] = "0";
            }
            $rgbStrAlpha = $rgbStr . " " . $colors['alpha'];
            $dbi = new DBCollection("INSERT INTO MapZone(x, y, map, color,zone) VALUES (" . $i . ", " . $j . ", " . $map . ", '" . $rgbStrAlpha . "', '" . $zone . "');", $db, 0, 0, false);
        }
    }
}

function getColorFromMap($i, $j, $img)
{
    $rgb = imagecolorat($img, $i, $j);
    $colors = imagecolorsforindex($img, $rgb);
    return $colors;
}

function getZoneFromColor($i, $j, $red)
{
    $zone = floor($red / 10);
    return $zone;
}

?>