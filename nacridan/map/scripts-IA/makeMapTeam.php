#!/usr/bin/php
<?php
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
$db = DB::getDB();

$files1 = scandir(HOMEPATH . MAPS_DYNAMIC_FOLDER);

foreach ($files1 as $filename1) {
    if (strpos($filename1, "mapghostteam0") === 0) {
        echo "Deleting file " . $filename1 . "<br/>";
        unlink(HOMEPATH . MAPS_DYNAMIC_FOLDER . "/" . $filename1);
    } else {
        echo "Not deleting file " . $filename1 . "<br/>";
    }
}

$dbt = new DBCollection("SELECT * from Team WHERE 1", $db);

while (! $dbt->eof()) {
    
    for ($map = 1; $map < 3; $map ++) {
        $im = imagecreate(640, 400);
        
        $background = imagecolorallocate($im, 0, 0, 0);
        $city = imagecolorallocate($im, 255, 0, 0);
        imagecolortransparent($im, $background);
        imagefill($im, 0, 0, $background);
        
        $dbc = new DBCollection("SELECT * from Player WHERE map=" . $map . " AND id_Team=" . $dbt->get("id"), $db);
        
        while (! $dbc->eof()) {
            imageline($im, $dbc->get("x") - 1, $dbc->get("y"), $dbc->get("x") + 1, $dbc->get("y"), $city);
            imageline($im, $dbc->get("x"), $dbc->get("y") - 1, $dbc->get("x"), $dbc->get("y") + 1, $city);
            $dbc->next();
        }
        imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . "/mapghostteam0" . $map . "-" . $dbt->get("id") . "-" . uniqid(mt_rand(1, 150000), false) . ".png");
        imagedestroy($im);
    }
    $dbt->next();
}

?>
