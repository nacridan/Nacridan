<?php
require_once ("../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();

set_time_limit(8000);
$dbi = new DBCollection("truncate table MapGround;", $db);

for ($map = 1; $map < 3; $map ++) {
    $imgarea = imagecreatefrompng(HOMEPATH . MAPS_STATIC_FOLDER . "/mapghost0" . $map . ".png");
    for ($i = 0; $i < 640; $i ++) {
        for ($j = 0; $j < 400; $j ++) {
            $colors = getColorFromMap($i, $j, $imgarea);
            $rgbStr = $colors['red'] . " " . $colors['green'] . " " . $colors['blue'];
            if ($colors['alpha'] == "") {
                $colors['alpha'] = "0";
            }
            $rgbStrAlpha = $rgbStr . " " . $colors['alpha'];
            $dbi = new DBCollection("INSERT INTO MapGround(x, y, map, id_Ground) select " . $i . ", " . $j . ", " . $map . ", Ground.id from Ground where color = '" . $rgbStrAlpha . "';", $db, 0, 0, false);
        }
    }
}

function getColorFromMap($i, $j, $img)
{
    $rgb = imagecolorat($img, $i, $j);
    $colors = imagecolorsforindex($img, $rgb);
    return $colors;
}
?>