<?php

/*
 * SELECT name, Player.id_Member, email
 * FROM Player
 * LEFT JOIN Member ON Player.id_Member = Member.id
 * LEFT JOIN MemberDetail ON MemberDetail.id_Member = Member.id
 * WHERE Player.name = 'Adah'
 */

// Les ---- indique que le testeur est bien dans le bon groupe dans le forum
function givelist()
{
    $liste = array();
    
    $liste[] = "paul.butin@gmail.com"; // Nidaime -----
    $liste[] = "ydecat@hotmail.com"; // Nanik-----
    $liste[] = "archloch_gt3@hotmail.com"; // Zaza ----
    $liste[] = "arnjea@gmail.com"; // Démone Lame -----
    $liste[] = "seb.caillot@orange.fr"; // Ancien HL de MH
    $liste[] = "gdseb.delfino@laposte.net"; // Adah -----
    $liste[] = "benoit.lanselle@hotmail.fr"; // Albus ----
    $liste[] = "leca.sonia@neuf.fr"; // Nathanaël -----
    $liste[] = "raistlin@nerim.net"; // Raistlin ^^ ------
    $liste[] = "pshittroll@live.fr"; // Wanderbegen
    $liste[] = "marco.resto@wanadoo.fr"; // Amlika -----
    $liste[] = "guillaume.inglese@gmail.com"; // Yogui dev ZoryaZilla (vérifié) -----
    $liste[] = "cnam_mail@yahoo.fr"; // MightyDuck ------
    $liste[] = "laurenttexier33@gmail.com"; // Samyouaf ------
    $liste[] = "koudboul@laposte.net"; // joueur HL de la V1 à une époque, HL MH ------
    $liste[] = "carylt@gmail.com"; // Lune Rouge V1, joueur MH et Thaanis ------
    $liste[] = "julien1david@hotmail.com"; // Balin -------
    $liste[] = "yoan.vuarant@laposte.net"; // Rincevent + HL MH ----
    $liste[] = "macruz007@hotmail.com"; // Blanche Naine + HL MH -------
    $liste[] = "jean-marc.pellon@laposte.net"; // Mine Autohre V1 -----
    $liste[] = "sjaillet@gmail.com"; // God ^^ ------
    $liste[] = "llodra@free.fr"; // Unaildaulx lvl 108 V1 -------
    $liste[] = "soco931@hotmail.fr"; // Micko -------
    $liste[] = "emdjii@gmail.com"; // -----
    $liste[] = "grolengrumpy@aliceadsl.fr"; // grumpy len
    $liste[] = "loulouboylove@hotmail.fr"; // Jorel tapeau
    $liste[] = "ixnay_phil@hotmail.com"; // Alicia
    $liste[] = "etienne-76@hotmail.fr"; // --- aregon HL MH
    $liste[] = "groscrom@hotmail.fr"; // ---- Grumpy Len
    $liste[] = "metalbc40@voila.fr"; // ----- Shkrou
    $liste[] = "bernardfabre1@hotmail.com"; // ----- Nerba + HL MH
    $liste[] = "beuzonboris@hotmail.fr"; // ----- Bobomaster
    $liste[] = "loris.64@gmail.com"; // loris MH
    $liste[] = "ambrune@hotmail.fr"; // Ambrune V1
    $liste[] = "rlemauff@free.fr"; // Tahl - V1
    $liste[] = "p.dannoux@gmail.com"; // Pich
    $liste[] = "swal4u@gmail.com"; // MS
    $liste[] = "emmanuel-claudine@wanadoo.fr"; // manu (hermes)
    $liste[] = "alainprovost@videotron.ca"; // Suma Dartson
    $liste[] = "moanho@gmail.com"; // MoaNho
    $liste[] = "lymnade@yahoo.fr"; // Antares (Lune Rouge V1)
    $liste[] = "rodben69@hotmail.fr"; // RODROC
    $liste[] = "toffeskien@gmail.com"; // Cratofix FPL.
    $liste[] = "chr.burdin@gmail.com"; // kraa jeune de la V1.
    $liste[] = "sylvainbarbass@hotmail.com"; // Kiliad jeune de la V1.
    $liste[] = "targuel@hotmail.fr"; // Ululer
    $liste[] = "nicolasrpg@laposte.net"; // Anuvico
    $liste[] = "gbe@tropchouette.com"; // Alabrok, joueur MH
    $liste[] = "troll.oizo@free.fr"; // Jon Snow, V1 + MH
    $liste[] = "adrien.montagut@gmail.com"; // django V1, donateur ulule en retard
    $liste[] = "joffrey.carlet@gmail.com"; // Stagiaire PHP
    $liste[] = "stephane.sanchez.b@gmail.com"; // stagiaire webdesign
    $liste[] = "nette.romain@gmail.com"; // Ange Blood - Romain V1
                                         
    // ------------ Dev
    
    $liste[] = "lordthergal@gmail.com";
    $liste[] = "stephane.melis2@hotmail.fr"; // -----
    
    $liste[] = "claude.longelin@gmail.com"; // -----
    $liste[] = "celine.pollet73@gmail.com"; // -----
    $liste[] = "ama.devaux@gmail.com"; // /-----
    $liste[] = "ainowallen@gmail.com";
    $liste[] = "aeli.lhum@gmail.com";
    
    return $liste;
}

// echo "Actuellement ".count(givelist())." bêta-testeurs...";

// ------------------------- ENVOIE MAIL --------------------------
/*
 * DEFINE ("SMTPDOMAIN","nacridan.com");
 * DEFINE ("SMTPHOST","localhost");
 * DEFINE ("SMTPLOGIN","");
 * DEFINE ("SMTPPASS","");
 *
 * DEFINE ("HOMEPATH","/var/www/nacridan/nacridan-v2-test/nacridan");
 *
 *
 * function sendEmail($email,&$res){
 * if(SMTPDOMAIN!="nacridan.com")
 * return;
 *
 * require_once(HOMEPATH."/lib/swift/Swift.php");
 * require_once(HOMEPATH."/lib/swift/Swift/Connection/SMTP.php");
 *
 * $subject="[Nacridan 2] Informations sur la bêta fermée";
 * $message = "Bonjour,\n\n";
 * $message .= "La bêta fermée sera lancée vendredi 10 août à 11h sur le site suivant :\n\n";
 * $message .= "www.nacridan.com/nacridan-v2-test/nacridan\n\n";
 * $message .= "Si vous n'êtes pas encore inscrit au forum, il est conseillé de le faire assez rapidement. Vous serez inclu dans le groupe Bêta-Testeurs avec l'accès à la catégorie du forum dédiée à la bêta. Tous les bugs et commentaires concernant la V2 seront à poster là-bas. Si vous êtes déjà inscrit, vérifiez bien que vous faites partie du groupe Bêta-Testeurs, vous devriez avoir accès aux 8 sections dédiées à la V2 : annonces, correction des textes, etc. \n\n";
 * $message .= "www.nacridan.com/forum\n\n";
 * $message .= "Une partie des règles du jeu sont déjà en ligne, nous continuons, à travailler dessus :\n\n";
 * $message .= "http://www.nacridan.com/nacridan-v2-test/nacridan/i18n/rules/fr/rules.php\n\n";
 * $message .= "Lors de votre inscription au jeu vendredi, pensez à utiliser l'adresse email que vous avez fournie. Vous aurez la possibilité de créer trois personnages avec le même compte. Le premier sera créé lors de votre première connexion. Les deux autres pourront être créés à partir du menu Conquête, à l'intérieur du jeu. Pour passer d'un personnage à l'autre, il vous suffira d'utiliser le menu déroulant en haut à droite, une fois connecté.\n\n";
 * $message .= "C'est un projet amateur aux moyens financiers limités, donc nous utiliserons le serveur de la V1 pour la bêta fermée de la V2. Il n'est malheureusement pas possible d'être connecté en même temps sur la V1 et la V2.'\n\n";
 *
 *
 *
 *
 * $message .= "-------------------------------------\n";
 * $message .= "Rappel : Si vous recevez cet email, vous vous êtes inscrit avec succès à la bêta fermée de Nacridan 2. Si ce n'est pas le cas, ou que vous êtes finalement indisponible, signalez le dans le forum www.nacridan.com/forum pour faire retirer votre adresse de cette liste.\n";
 * $message .= "-------------------------------------\n";
 *
 * // --------------- html
 *
 * $messagehtml = "Bonjour,<br/><br/>";
 * $messagehtml .= "La bêta fermée sera lancée vendredi 10 août à 11h sur le site suivant :<br/><br/>";
 * $messagehtml .= "www.nacridan.com/nacridan-v2-test/nacridan<br/><br/>";
 * $messagehtml .= "Si vous n'êtes pas encore inscrit au forum, il est conseillé de le faire assez rapidement. Vous serez inclu dans le groupe Bêta-Testeurs avec l'accès à la catégorie du forum dédiée à la bêta. Tous les bugs et commentaires concernant la V2 seront à poster là-bas. Si vous êtes déjà inscrit, vérifiez bien que vous faites partie du groupe Bêta-Testeurs, vous devriez avoir accès aux 8 sections dédiées à la V2 : annonces, correction des textes, etc.<br/><br/>";
 * $messagehtml .= "www.nacridan.com/forum<br/><br/>";
 * $messagehtml .= "Une partie des règles du jeu sont déjà en ligne, nous continuons, à travailler dessus :<br/><br/>";
 * $messagehtml .= "http://www.nacridan.com/nacridan-v2-test/nacridan/i18n/rules/fr/rules.php<br/><br/>";
 * $messagehtml .= "Lors de votre inscription au jeu vendredi, pensez à utiliser l'adresse email que vous avez fournie. Vous aurez la possibilité de créer trois personnages avec le même compte. Le premier sera créé lors de votre première connexion. Les deux autres pourront être créés à partir du menu Conquête, à l'intérieur du jeu. Pour passer d'un personnage à l'autre, il vous suffira d'utiliser le menu déroulant en haut à droite, une fois connecté.<br/><br/>";
 * $messagehtml .= "C'est un projet amateur aux moyens financiers limités, donc nous utiliserons le serveur de la V1 pour la bêta fermée de la V2. Il n'est malheureusement pas possible d'être connecté en même temps sur la V1 et la V2.<br/><br/>";
 *
 *
 * $messagehtml .= "-------------------------------------<br/>";
 * $messagehtml .= "Rappel : Si vous recevez cet email, vous vous êtes inscrit avec succès à la bêta fermée de Nacridan 2. Si ce n'est pas le cas, ou que vous êtes finalement indisponible, signalez le dans le forum www.nacridan.com/forum pour faire retirer votre adresse de cette liste.<br/>";
 * $messagehtml .= "-------------------------------------<br/>";
 *
 * $to = $email;
 * $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
 * if ($mailer->isConnected()) //Optional
 * {
 *
 * if(SMTPLOGIN!="" && SMTPPASS!="")
 * {
 * require_once(HOMEPATH."/lib/swift/Swift/Authenticator/PLAIN.php");
 * $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
 * $mailer->authenticate(SMTPLOGIN, SMTPPASS);
 * }
 *
 * //Add as many parts as you need here
 * $mailer->addPart($message);
 * $mailer->addPart($messagehtml, 'text/html');
 * $mailer->setCharset("UTF-8");
 * $mailer->send(
 * $to,
 * "Nacridan <noreply@".SMTPDOMAIN.".>",
 * $subject
 * );
 *
 * $mailer->close();
 * $res = "Mail should be sent successfully";
 * }
 * else
 * $res = "Error not connected to SMTP";
 * }
 */
// ---- phase d'envoie
/*
 * $res = "test";
 *
 *
 * $list = givelist();
 * foreach($list as $email)
 * {
 * sendEmail($email,$res);
 * echo "<br/>".$res."<br/>";
 * }
 */

?>
