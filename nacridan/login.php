<?php
require_once ("./conf/config.ini.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/include/game.login.inc.php");

$FORM = $MAIN_BODY->addNewHTMLObject('div', '', 'class="loginfield"');

$FORM->add('<form name="loginform" method="post" action="' . CONFIG_HOST . '/index.php" style="margin:0">');

$FORM->add(localize('Identifiant :') . '<br/>');
$FORM->add(new HTMLInput('text', 'username', '', '', 'size="14" maxlength="32"'));
$FORM->add('<br/><br/>');
$FORM->add(localize('Mot de Passe :') . '<br/>');
$FORM->add(new HTMLInput('password', 'password', '', '', 'size="14" maxlength="32"', 0));
$FORM->add('<br/><br/>');
$str = '<input type="checkbox" name="dla" checked="checked" style="margin-left: 0px;"/>' . localize('(activer sa DLA)') . '<br/>';
$str .= '<input type="submit" name="submit" value="ok" /><br/>';

$str .= '<br/>';
$str .= 'GMT : ' . gmdate('H:i:s') . '<br/>';
$str .= '<br/>';

if (! isset($_GET['lostpasswd']) && ! isset($_GET['confirmmail'])) {
    $str .= '<a href="' . CONFIG_HOST . '/main/register.php?lostpasswd=true" class="attackevent">' . localize('Mot de passe perdu ?') . '</a>';
    $str .= '<br/>';
    $str .= '<br/>';
    $str .= '<a href="' . CONFIG_HOST . '/main/register.php?confirmmail=true" class="attackevent">' . localize('Mail d"inscription <br/>non reçu ?') . '</a>';
    $str .= '<br/>';
    
    $str .= '<br/>';
    $str .= '<a href="' . CONFIG_HOST . '/main/contact.php" class="attackevent popupify">Nous Contacter</a>';
}

$str .= '</form></div>';
$FORM->add($str);

$MAIN_CENTER = $MAIN_BODY->addNewHTMLObject("div", "", "class='centerarea'");
$db = DB::getDB();
$dbn = new DBCollection("SELECT * FROM News WHERE type=2 ORDER BY id DESC", $db);
if ($dbn->count() > 0) {
    $date = strtotime($dbn->get("date"));
    $str = "<div style='color: #EEEEEE; font-weight: bold;'>" . gmdate("d-m-Y", $date) . "</div>";
    $str .= "<div style='color: #EEEEEE'>";
    $str .= "<b><center>" . $dbn->get("title") . "</center></b>";
    $str .= nl2br($dbn->get("content"));
    $str .= "</div>";
    $str .= "<div style='color: #EEEEEE'>";
    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbn->get("id_Player"), $db);
    $str .= "<i>" . $dbp->get("name") . "</i>";
    $str .= "<br/>";
    $str .= "<a href='main/news.php' class='statsevent'>>>>Les anciennes annonces</a>.";
    $str .= "</div>";
}
$MAIN_CENTER->add($str);

$MAIN_BOTTOM = $MAIN_BODY->addNewHTMLObject("div", "", "class='bottomarea' align='center'");

$db = DB::getDB();
$dblast = new DBCollection('SELECT * FROM Player WHERE authlevel=0 AND id_Member!=0 AND status="PC" AND name IS NOT NULL ORDER BY creation DESC LIMIT 0,9', $db);

$i = $j = 0;
$lastRegistred = array();
while (! $dblast->eof()) {
    $lastRegistred[$i] = $dblast->get('name');
    $lastRegistredRace[$i] = $dblast->get('racename');
    $i ++;
    $dblast->next();
}

$MAIN_BOTTOM->add('<table style="width:93%"><tr><td colspan="11" height="22" align="center" bgcolor="#663333" valign="middle"><strong><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">Les Derniers Inscrits</font></strong></td></tr>');
$MAIN_BOTTOM->add('<tr>');
for ($i = 0; $i < 3; $i ++) {
    $MAIN_BOTTOM->add('<td align="center" bgcolor="#330000" height="19" valign="middle" width="37"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">#</font></td><td align="center" bgcolor="#663333" valign="middle" width="84"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Nom</strong></font></td><td align="center" bgcolor="#663333" valign="middle" width="56"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Race</strong></font></td><td rowspan="4" valign="top" width="30">&nbsp;</td>');
}
$MAIN_BOTTOM->add('</tr>');
for ($i = 0; $i < 3; $i ++) {
    $MAIN_BOTTOM->add('<tr>');
    for ($j = 0; $j < 3; $j ++) {
        $name = '';
        $race = '';
        if (isset($lastRegistred[$i + $j * 3]))
            $name = $lastRegistred[$i + $j * 3];
        if (isset($lastRegistredRace[$i + $j * 3]))
            $race = $lastRegistredRace[$i + $j * 3];
        $MAIN_BOTTOM->add('<td align="center" bgcolor="#333333" height="19" valign="middle"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">' . ($i + $j * 3 + 1) . '</font></td><td align="center" bgcolor="#666666" valign="middle"><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="-1">' . $name . '</font></td><td align="center" bgcolor="#666666" valign="middle">' . $race . '</td>');
    }
    $MAIN_BOTTOM->add('</tr>');
}
$MAIN_BOTTOM->add('</table>');

$str = "</div>";
$MAIN_BODY->add($str);

$google = "<script src='http://www.google-analytics.com/urchin.js' type='text/javascript'>
</script>
<script type='text/javascript'>
_uacct = 'UA-1166023-1';
urchinTracker();
</script>";

$MAIN_BODY->add($google);

$MAIN_PAGE->render();

/*
 * mysql_query("update Player set ap='8' where id='6920'");
 * echo mysql_error();
 */
?>
